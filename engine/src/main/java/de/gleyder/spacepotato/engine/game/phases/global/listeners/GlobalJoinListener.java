package de.gleyder.spacepotato.engine.game.phases.global.listeners;

import de.gleyder.spacepotato.engine.game.config.EngineMessages;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayer;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayerRegistry;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class GlobalJoinListener implements Listener {

    private final GamePlayerRegistry<GamePlayer<GameTeam>> playerRegistry;
    private final int maxPlayers;

    @EventHandler
    public void onJoin(PlayerLoginEvent event) {
        if (this.playerRegistry.getAll().size() > this.maxPlayers)
            event.disallow(PlayerLoginEvent.Result.KICK_FULL, EngineMessages.PHASE_GLOBAL_SERVER$FULL.getMessage(false));
    }

}
