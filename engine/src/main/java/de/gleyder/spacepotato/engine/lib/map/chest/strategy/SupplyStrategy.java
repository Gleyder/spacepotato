package de.gleyder.spacepotato.engine.lib.map.chest.strategy;

import org.bukkit.inventory.Inventory;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface SupplyStrategy {

    void supply(Inventory inventory);

}
