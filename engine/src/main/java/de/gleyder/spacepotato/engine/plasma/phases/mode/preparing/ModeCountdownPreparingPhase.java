package de.gleyder.spacepotato.engine.plasma.phases.mode.preparing;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.engine.game.phases.services.numerator.GameCountdown;
import de.gleyder.spacepotato.engine.game.phases.services.numerator.GameCountdownService;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaMessages;
import de.gleyder.spacepotato.engine.plasma.phases.PlasmaPhases;
import lombok.NonNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ModeCountdownPreparingPhase extends GamePhase {

    public ModeCountdownPreparingPhase(@NonNull SpacePotatoRegistry registry,
                                       @NonNull PlasmaEngineSettings engineSettings) {
        super(registry, PlasmaPhases.MODE_PREPARING_COUNTDOWN.getId());
        this.addServices(
                new GameCountdownService(
                        new GameCountdown(
                                registry.getPlugin(),
                                engineSettings.getModeCountdownPreparingDuration(),
                                engineSettings.getPlayerRegistry().getBroadcastChannel(),
                                PlasmaMessages.PHASE_MODE_PREPARING_COUNTDOWN.getMessage(true),
                                end -> {
                                    engineSettings.getPhaseManager().stop(PlasmaPhases.MODE_PREPARING_COUNTDOWN.getId());
                                    engineSettings.getPhaseManager().start(engineSettings.getIngamePhase());
                                }
                        )
                )
        );
    }

}
