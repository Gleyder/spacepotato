package de.gleyder.spacepotato.engine.plasma.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@Setter
@Getter
@NoArgsConstructor
public class PlasmaEngineSave {

    private boolean preparation;

}
