package de.gleyder.spacepotato.engine.lib.participant;

import com.google.common.collect.ImmutableList;
import de.gleyder.spacepotato.core.broadcast.SuppliedBroadcastChannel;
import de.gleyder.spacepotato.engine.lib.participant.interfaces.GameParticipantRegistry;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class GamePlayerRegistry<P extends GamePlayer<? extends GameTeam>> implements GameParticipantRegistry<P> {

    private final ParticipantStateMap<P> participantStateMap;
    private final Logger logger;

    @Getter
    private final SuppliedBroadcastChannel broadcastChannel;

    public GamePlayerRegistry() {
        this.participantStateMap = new ParticipantStateMap<>();
        this.logger = LoggerFactory.getLogger(this.getClass());
        this.broadcastChannel = new SuppliedBroadcastChannel(() -> GamePlayerRegistry.this.getAll().stream().map(p -> p.getPlayer()).collect(Collectors.toList()));
    }

    public abstract P create(Player player);

    void changeState(@NotNull GamePlayer participant, String state) {
        //noinspection unchecked
        this.participantStateMap.setState((P) participant, state);
    }

    public P register(Player player) {
        P p = this.create(player);
        this.participantStateMap.register(p);
        return p;
    }

    @Override
    public void register(@NotNull P participant) {
        this.participantStateMap.register(participant);
        participant.onRegister();
        this.logger.info("Registered " + participant);
    }

    public void unregister(@NotNull UUID uuid) {
        P participant = this.participantStateMap.getParticipant(uuid);
        participant.onUnregister();
        this.participantStateMap.unregister(participant);
        this.logger.info("Unregistered " + participant);
    }

    public void unregister(@NotNull Player player) {
        P participant = this.participantStateMap.getParticipant(player.getUniqueId());
        if (participant == null)
            return;

        this.unregister(participant.getUniqueId());
    }

    @Override
    public void unregister(@NotNull P participant) {
        this.unregister(participant.getUniqueId());
    }

    public P get(@NotNull Player player) {
        return this.participantStateMap.getParticipant(player.getUniqueId());
    }

    public P get(@NotNull UUID uniqueId) {
        return this.participantStateMap.getParticipant(uniqueId);
    }

    public boolean contains(@NotNull Player player) {
        return this.get(player) != null;
    }

    public boolean contains(@NotNull UUID uniqueId) {
        return this.get(uniqueId) != null;
    }

    public SuppliedBroadcastChannel getBroadcast(@NotNull String state) {
        return new SuppliedBroadcastChannel(() -> getAll(state).stream().map(p -> p.getPlayer()).collect(Collectors.toList()));
    }

    @Override
    public boolean contains(@NotNull P participant) {
        return this.get(participant.getUniqueId()) != null;
    }

    public ImmutableList<P> getPlaying() {
        return this.participantStateMap.getAll(PlayingState.PLAYING.getId());
    }

    public ImmutableList<P> getSpectating() {
        return this.participantStateMap.getAll(PlayingState.SPECTATING.getId());
    }

    @Override
    public ImmutableList<P> getAll() {
        return this.participantStateMap.getAll();
    }

    public ImmutableList<P> getAll(@NotNull String state) {
        return this.participantStateMap.getAll(state);
    }

}
