package de.gleyder.spacepotato.engine.game.phases.global;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.engine.game.phases.EnginePhases;
import de.gleyder.spacepotato.engine.game.phases.global.listeners.GlobalJoinListener;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayer;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayerRegistry;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GlobalPhase extends GamePhase {

    public GlobalPhase(@NotNull SpacePotatoRegistry registry,
                       @NotNull GamePlayerRegistry<GamePlayer<GameTeam>> playerRegistry,
                       int maxPlayers) {
        super(registry, EnginePhases.GLOBAL.getId());
        this.addListeners(new GlobalJoinListener(playerRegistry, maxPlayers));
    }

}
