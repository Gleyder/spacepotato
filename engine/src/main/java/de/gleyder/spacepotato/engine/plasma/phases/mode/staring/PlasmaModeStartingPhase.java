package de.gleyder.spacepotato.engine.plasma.phases.mode.staring;

import de.gleyder.spacepotato.engine.game.phases.services.numerator.GameCountdownService;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import de.gleyder.spacepotato.engine.plasma.countdowns.lobby.ModeStartingCountdown;
import de.gleyder.spacepotato.engine.plasma.listeners.mode.starting.PlasmaModeStartingCountdownListener;
import de.gleyder.spacepotato.engine.plasma.phases.PlasmaPhases;
import lombok.NonNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PlasmaModeStartingPhase extends GamePhase {

    public PlasmaModeStartingPhase(@NonNull PlasmaEngineSettings settings) {
        super(settings.getSpacePotatoRegistry(), PlasmaPhases.MODE_STARTING.getId());
        ModeStartingCountdown startingCountdown = new ModeStartingCountdown(this.registry.getPlugin(), settings);

        this.addServices(new GameCountdownService(startingCountdown));
        this.addListeners(new PlasmaModeStartingCountdownListener(settings, startingCountdown));
    }

}
