package de.gleyder.spacepotato.engine;

import de.gleyder.spacepotato.core.configuration.messages.MessageLoader;
import de.gleyder.spacepotato.engine.game.config.EngineMessages;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The engine's main class
 */
public final class SpacePotatoEnginePlugin extends JavaPlugin {

    @Getter
    private static SpacePotatoEnginePlugin instance;

    @Getter
    private MessageLoader messageLoader;

    @Override
    public void onLoad() {
        instance = this;
        this.messageLoader = new MessageLoader(this, EngineMessages.class);
        this.messageLoader.load();
    }

    @Override
    public void onDisable() {
        instance = null;
    }

}
