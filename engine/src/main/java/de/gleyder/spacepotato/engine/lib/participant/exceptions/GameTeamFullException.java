package de.gleyder.spacepotato.engine.lib.participant.exceptions;

import de.gleyder.spacepotato.engine.lib.participant.GameTeam;

public class GameTeamFullException extends RuntimeException {

    public GameTeamFullException(GameTeam gameTeam) {
        super("GameTeam " + gameTeam.getName() + "#" + gameTeam.getKey() + " is full");
    }

}
