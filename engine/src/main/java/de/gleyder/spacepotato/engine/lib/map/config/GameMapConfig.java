package de.gleyder.spacepotato.engine.lib.map.config;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface GameMapConfig {

    String worldName();
    String gameMapName();

}
