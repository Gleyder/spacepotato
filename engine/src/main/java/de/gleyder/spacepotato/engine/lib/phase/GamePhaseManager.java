package de.gleyder.spacepotato.engine.lib.phase;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Manager for loading and unloading of game phases
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GamePhaseManager {

    @Getter
    private final ConcurrentHashMap<String, GamePhase> activatePhases;

    private final Logger logger;

    public GamePhaseManager() {
        this.activatePhases = new ConcurrentHashMap<>();
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public void start(@NotNull GamePhase gamePhase) {
        gamePhase.load();
        this.activatePhases.put(gamePhase.getId(), gamePhase);
        this.logger.info("Started GamePhase " + gamePhase.getId());
    }

    public void stop(@NotNull GamePhase phase) {
        this.stop(phase.getId());
    }

    public void stop(@NotNull String id) {
        GamePhase gamePhase = this.activatePhases.get(id);
        if (gamePhase == null)
            throw new NullPointerException("No Phase with id " + id + " is currently running");

        gamePhase.unload();
        this.activatePhases.remove(id);
        this.logger.info("Stopped GamePhase " + gamePhase.getId());
    }

    public void stopAll() {
        this.stopAll(false);
    }

    public void stopAll(boolean withGlobal) {
        this.activatePhases.keySet().forEach(id -> {
            if (withGlobal && this.isGlobal(id))
                return;
            this.stop(id);
        });
    }

    private boolean isGlobal(@NotNull String id) {
        return id.startsWith("global");
    }

    public boolean isPhaseRunning(@NotNull String id) {
        return this.activatePhases.containsKey(id);
    }

}
