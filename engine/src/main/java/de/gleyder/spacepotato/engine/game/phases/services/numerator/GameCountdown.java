package de.gleyder.spacepotato.engine.game.phases.services.numerator;

import de.gleyder.spacepotato.core.broadcast.BroadcastChannel;
import de.gleyder.spacepotato.core.chrono.Numerator;
import de.gleyder.spacepotato.core.utils.string.SuppliedString;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.time.temporal.ChronoUnit;
import java.util.function.Consumer;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GameCountdown extends Numerator.Countdown {

    private final BroadcastChannel channel;
    private final String message;
    private Consumer<GameCountdown> consumer;

    public GameCountdown(@NotNull JavaPlugin javaPlugin,
                         int duration,
                         @NotNull BroadcastChannel channel,
                         @NotNull String message,
                         @NotNull Consumer<GameCountdown> consumer) {
        super(duration, ChronoUnit.SECONDS, javaPlugin);
        this.channel = channel;
        this.message = message;
        this.consumer = consumer;
    }

    @Override
    public void onUpdate() {
        this.channel.sendMessage(new SuppliedString(this.message).addValue(this.getCurrentValue()).build());
    }

    @Override
    public void onFinish() {
        this.consumer.accept(this);
    }

}
