package de.gleyder.spacepotato.engine.plasma;

import de.gleyder.spacepotato.core.configuration.SpacePotatoSave;
import de.gleyder.spacepotato.core.configuration.messages.MessageLoader;
import de.gleyder.spacepotato.engine.SpacePotatoEnginePlugin;
import de.gleyder.spacepotato.engine.lib.verifier.SetupVerifier;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSave;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaMessages;
import de.gleyder.spacepotato.engine.plasma.phases.lobby.PlasmaLobbyPhase;
import de.gleyder.spacepotato.engine.plasma.phases.preparation.PlasmaPreparationPhase;
import de.gleyder.spacepotato.engine.plasma.phases.teleport.TeleportPhase;
import lombok.Getter;
import lombok.NonNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PlasmaEngine {

    private final PlasmaEngineSettings settings;

    @Getter
    private final SetupVerifier setupVerifier;


    private SpacePotatoSave<PlasmaEngineSave> save;

    private boolean activated;

    public PlasmaEngine(@NonNull PlasmaEngineSettings settings) {
        this.settings = settings;
        this.setupVerifier = new SetupVerifier();
        this.save = new SpacePotatoSave<>(settings.getSpacePotatoRegistry().getPlugin(), "plasmaConfig", PlasmaEngineSave.class);
    }

    public void activate() {
        MessageLoader messageLoader = new MessageLoader(SpacePotatoEnginePlugin.getInstance(), PlasmaMessages.class);
        messageLoader.load();

        PlasmaEngineSave save = this.save.load();
        if (save == null) {
            save = new PlasmaEngineSave();
            save.setPreparation(true);
        }

        if (save.isPreparation()) {
            this.settings.getPhaseManager().start(new PlasmaPreparationPhase(this.settings.getSpacePotatoRegistry()));
            this.setupVerifier.check(false);
        } else {
            this.setupVerifier.check(true);

            this.settings.getPhaseManager().start(new PlasmaLobbyPhase(this.settings));
        }

        this.activated = true;
    }

    public void deactivate() {
        this.settings.getPhaseManager().stopAll(true);
        this.activated = false;
    }

    public void prepareMode() {
        this.checkActivation();

        TeleportPhase teleportPhase = this.settings.getTeleportPhase();
        this.settings.getPhaseManager().start(teleportPhase);
        teleportPhase.teleport();
        this.settings.getPhaseManager().stopAll();
        this.settings.getPhaseManager().start(this.settings.getPreparingPhase());
    }

    public void startMode() {
        this.checkActivation();

        this.settings.getIngamePhase().addListeners();
        this.settings.getPhaseManager().start(this.settings.getIngamePhase());
    }

    private void checkActivation() {
        if (!this.activated)
            throw new IllegalStateException("PlasmaEngine not activated!");
    }
}
