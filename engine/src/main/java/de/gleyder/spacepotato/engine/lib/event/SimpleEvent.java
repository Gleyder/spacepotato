package de.gleyder.spacepotato.engine.lib.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public abstract class SimpleEvent extends Event {

    private static final HandlerList HANDLER_LIST;

    static {
        HANDLER_LIST = new HandlerList();
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public final HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    @Override
    public boolean callEvent() {
        return super.callEvent();
    }

    @Override
    public @NotNull String getEventName() {
        return super.getEventName();
    }


}
