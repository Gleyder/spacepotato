package de.gleyder.spacepotato.engine.plasma.config;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayer;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayerRegistry;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import de.gleyder.spacepotato.engine.lib.participant.GameTeamRegistry;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import de.gleyder.spacepotato.engine.lib.phase.GamePhaseManager;
import de.gleyder.spacepotato.engine.plasma.phases.lobby.PlasmaLobbyPhase;
import de.gleyder.spacepotato.engine.plasma.phases.mode.end.PlasmaEndPhase;
import de.gleyder.spacepotato.engine.plasma.phases.teleport.TeleportPhase;
import lombok.Getter;
import org.bukkit.Location;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */

@Getter
public class PlasmaEngineSettings {

    private final SpacePotatoRegistry spacePotatoRegistry;

    public PlasmaEngineSettings(SpacePotatoRegistry spacePotatoRegistry) {
        this.spacePotatoRegistry = spacePotatoRegistry;
        this.lobbyPhase = new PlasmaLobbyPhase(this);
        this.endPhase = new PlasmaEndPhase(this);
    }

    private TeleportPhase teleportPhase;

    private GamePhase preparingPhase;
    private GamePhase ingamePhase;

    private Location spawn;

    private int maxPlayers;
    private int minPlayers;
    private int minTime;
    private int modeStartingDuration;
    private int modeCountdownPreparingDuration;
    private int modeStoppingCountdown;

    private boolean manuellStart = false;

    private GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> playerRegistry;
    private GameTeamRegistry<? extends GameTeam> teamRegistry;

    private PlasmaLobbyPhase lobbyPhase;
    private PlasmaEndPhase endPhase;

    private GamePhaseManager phaseManager = new GamePhaseManager();

    public PlasmaEngineSettings setTeleportPhase(TeleportPhase teleportPhase) {
        this.teleportPhase = teleportPhase;
        return this;
    }

    public PlasmaEngineSettings setPreparingPhase(GamePhase preparingPhase) {
        this.preparingPhase = preparingPhase;
        return this;
    }

    public PlasmaEngineSettings setIngamePhase(GamePhase ingamePhase) {
        this.ingamePhase = ingamePhase;
        return this;
    }

    public PlasmaEngineSettings setSpawn(Location spawn) {
        this.spawn = spawn;
        return this;
    }

    public PlasmaEngineSettings setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
        return this;
    }

    public PlasmaEngineSettings setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
        return this;
    }

    public PlasmaEngineSettings setMinTime(int minTime) {
        this.minTime = minTime;
        return this;
    }

    public PlasmaEngineSettings setModeStartingDuration(int modeStartingDuration) {
        this.modeStartingDuration = modeStartingDuration;
        return this;
    }

    public PlasmaEngineSettings setModeCountdownPreparingDuration(int modeCountdownPreparingDuration) {
        this.modeCountdownPreparingDuration = modeCountdownPreparingDuration;
        return this;
    }

    public PlasmaEngineSettings setModeStoppingCountdown(int modeStoppingCountdown) {
        this.modeStoppingCountdown = modeStoppingCountdown;
        return this;
    }

    public PlasmaEngineSettings setPlayerRegistry(GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> playerRegistry) {
        this.playerRegistry = playerRegistry;
        return this;
    }

    public PlasmaEngineSettings setTeamRegistry(GameTeamRegistry<? extends GameTeam> teamRegistry) {
        this.teamRegistry = teamRegistry;
        return this;
    }

    public PlasmaEngineSettings setLobbyPhase(PlasmaLobbyPhase lobbyPhase) {
        this.lobbyPhase = lobbyPhase;
        return this;
    }

    public PlasmaEngineSettings setEndPhase(PlasmaEndPhase endPhase) {
        this.endPhase = endPhase;
        return this;
    }

    public PlasmaEngineSettings setPhaseManager(GamePhaseManager phaseManager) {
        this.phaseManager = phaseManager;
        return this;
    }

    public PlasmaEngineSettings setManuellStart(boolean manuellStart) {
        this.manuellStart = manuellStart;
        return this;
    }


}
