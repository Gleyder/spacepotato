package de.gleyder.spacepotato.engine.lib.utils;

import org.bukkit.Difficulty;
import org.bukkit.GameRule;
import org.bukkit.World;

public class WorldUtils {

    public static void setupWorld(World world) {
        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
        world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
        world.setGameRule(GameRule.DO_MOB_SPAWNING, false);
        world.setGameRule(GameRule.SPECTATORS_GENERATE_CHUNKS, false);
        world.setGameRule(GameRule.DO_ENTITY_DROPS, false);
        world.setGameRule(GameRule.SHOW_DEATH_MESSAGES, false);

        world.setAutoSave(false);
        world.setThundering(false);
        world.setStorm(false);
        world.setTime(6000);
        world.setDifficulty(Difficulty.NORMAL);
        world.setPVP(true);
    }

}
