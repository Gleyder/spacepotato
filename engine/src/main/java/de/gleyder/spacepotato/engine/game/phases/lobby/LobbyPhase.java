package de.gleyder.spacepotato.engine.game.phases.lobby;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.engine.game.listeners.ProhibitionListener;
import de.gleyder.spacepotato.engine.game.phases.EnginePhases;
import de.gleyder.spacepotato.engine.game.phases.lobby.listeners.LobbyPhasePlayerListener;
import de.gleyder.spacepotato.engine.game.phases.lobby.listeners.LobbyPhaseSpawnListener;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayer;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayerRegistry;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import org.bukkit.Location;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class LobbyPhase extends GamePhase {

    public LobbyPhase(@NotNull SpacePotatoRegistry registry,
                      @NotNull GamePlayerRegistry<? extends GamePlayer< ? extends GameTeam>> playerRegistry,
                      @NotNull Location spawn) {
        super(registry, EnginePhases.LOBBY.getId());

        this.addListeners(
                new LobbyPhasePlayerListener(playerRegistry, spawn),
                new LobbyPhaseSpawnListener(spawn),
                new ProhibitionListener()
        );
    }

}
