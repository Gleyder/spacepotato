package de.gleyder.spacepotato.engine.plasma.phases.mode.end;

import de.gleyder.spacepotato.engine.game.listeners.ProhibitionListener;
import de.gleyder.spacepotato.engine.game.phases.services.numerator.GameCountdown;
import de.gleyder.spacepotato.engine.game.phases.services.numerator.GameCountdownService;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaMessages;
import de.gleyder.spacepotato.engine.plasma.listeners.mode.end.PlasmaEndPhaseListener;
import de.gleyder.spacepotato.engine.plasma.phases.PlasmaPhases;
import lombok.NonNull;
import org.bukkit.Bukkit;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PlasmaEndPhase extends GamePhase {

    public PlasmaEndPhase(@NonNull PlasmaEngineSettings settings) {
        super(settings.getSpacePotatoRegistry(), PlasmaPhases.MODE_END_PHASE.getId());
        this.addListeners(new ProhibitionListener());
        this.addListeners(new PlasmaEndPhaseListener());

        this.addServices(
                new GameCountdownService(
                        new GameCountdown(
                                this.registry.getPlugin(),
                                settings.getModeStoppingCountdown(),
                                settings.getPlayerRegistry().getBroadcastChannel(),
                                PlasmaMessages.PHASE_MODE_END_COUNTDOWN.getMessage(false),
                                end -> {
                                    Bukkit.getServer().shutdown();
                                }
                        )
                )
        );
    }

}
