package de.gleyder.spacepotato.engine.lib.verifier;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface SetupTest {

    void test() throws SetupTestException;
    Type getType();

    enum Type {
        OPTIONAL, REQUIRED;
    }

}
