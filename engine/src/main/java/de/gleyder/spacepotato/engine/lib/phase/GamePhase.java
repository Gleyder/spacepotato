package de.gleyder.spacepotato.engine.lib.phase;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GamePhase {

    private final List<GameService> services;
    private final List<Listener> listeners;

    @SuppressWarnings("WeakerAccess")
    protected final SpacePotatoRegistry registry;

    @Getter
    protected final String id;

    /**
     * Creates a GameComponent
     *
     * @param registry      a space potato registry
     */
    public GamePhase(@NonNull SpacePotatoRegistry registry, @NonNull String id) {
        this.services = new ArrayList<>();
        this.listeners = new ArrayList<>();
        this.registry = registry;
        this.id = id;
    }

    /**
     * Adds an array of listeners
     *
     * @param listeners     an array of listeners
     */
    @SuppressWarnings("WeakerAccess")
    public void addListeners(Listener... listeners) {
        this.listeners.addAll(Arrays.asList(listeners));
    }

    /**
     * Adds an array of services
     *
     * @param services      an array of services
     */
    public void addServices(GameService... services) {
        this.services.addAll(Arrays.asList(services));
    }

    /**
     * Loads the component
     */
    void load() {
        this.services.forEach(GameService::load);
        this.listeners.forEach(this.registry::registerListener);
    }

    /**
     * Unloads the component
     */
    void unload() {
        this.services.forEach(GameService::unload);
        this.listeners.forEach(HandlerList::unregisterAll);
    }

}
