package de.gleyder.spacepotato.engine.plasma.phases.lobby;

import de.gleyder.spacepotato.engine.lib.listeners.SeaLevelListener;
import de.gleyder.spacepotato.engine.lib.listeners.TeleportJoinListener;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import de.gleyder.spacepotato.engine.plasma.listeners.lobby.PlasmaLobbyPhaseModeStartingListener;
import de.gleyder.spacepotato.engine.plasma.listeners.lobby.PlasmaLobbyPhasePlayerListener;
import de.gleyder.spacepotato.engine.plasma.listeners.lobby.PlasmaLobbyPhaseProhibitionListener;
import de.gleyder.spacepotato.engine.plasma.phases.PlasmaPhases;
import lombok.NonNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PlasmaLobbyPhase extends GamePhase {

    public PlasmaLobbyPhase(@NonNull PlasmaEngineSettings settings) {
        super(settings.getSpacePotatoRegistry(), PlasmaPhases.LOBBY.getId());
        this.addListeners(
                new PlasmaLobbyPhasePlayerListener(settings.getPlayerRegistry(), settings.getMaxPlayers()),
                new PlasmaLobbyPhaseProhibitionListener(),
                new TeleportJoinListener(settings.getSpawn()),
                new SeaLevelListener(0, settings.getSpawn())
        );


        if (!settings.isManuellStart()) {
            this.addListeners(new PlasmaLobbyPhaseModeStartingListener(settings));
        }
    }

}
