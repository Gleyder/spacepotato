package de.gleyder.spacepotato.engine.lib.participant.interfaces;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface Registrable {

    void onRegister();
    void onUnregister();

}
