package de.gleyder.spacepotato.engine.game;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.gui.impl.GUIInventoryFactory;
import de.gleyder.spacepotato.engine.game.phases.templates.PhasesTemplate;
import de.gleyder.spacepotato.engine.lib.map.GameMap;
import de.gleyder.spacepotato.engine.lib.map.GameMapLoader;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayer;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayerRegistry;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import de.gleyder.spacepotato.engine.lib.participant.GameTeamRegistry;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.permissions.ServerOperator;
import org.jetbrains.annotations.NotNull;

import java.util.function.Predicate;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@Getter
public class EngineSettingsProvider {

    private final SpacePotatoRegistry spacePotatoRegistry;
    private final GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> playerRegistry;
    private final GameTeamRegistry<? extends GameTeam> teamRegistry;
    private final GameMapLoader<? extends GameMap> mapLoader;
    private final PhasesTemplate template;

    private GUIInventoryFactory inventoryFactory;
    private Predicate<Player> loginCheck;

    public EngineSettingsProvider(@NotNull SpacePotatoRegistry spacePotatoRegistry,
                                  @NotNull GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> playerRegistry,
                                  @NotNull GameTeamRegistry<? extends GameTeam> teamRegistry,
                                  @NotNull GameMapLoader<? extends GameMap> mapLoader,
                                  @NotNull PhasesTemplate template) {
        this.spacePotatoRegistry = spacePotatoRegistry;
        this.playerRegistry = playerRegistry;
        this.teamRegistry = teamRegistry;
        this.mapLoader = mapLoader;
        this.template = template;

        this.inventoryFactory = new GUIInventoryFactory(spacePotatoRegistry);
        this.loginCheck = ServerOperator::isOp;
    }

    public EngineSettingsProvider setLoginCheck(Predicate<Player> loginCheck) {
        this.loginCheck = loginCheck;
        return this;
    }

    public EngineSettingsProvider setInventoryFactory(GUIInventoryFactory inventoryFactory) {
        this.inventoryFactory = inventoryFactory;
        return this;
    }

}
