package de.gleyder.spacepotato.engine.plasma.listeners.mode.end;

import de.gleyder.spacepotato.engine.plasma.config.PlasmaMessages;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PlasmaEndPhaseListener implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        event.disallow(PlayerLoginEvent.Result.KICK_OTHER, PlasmaMessages.PHASE_MODE_END_LOGIN.getMessage(false));
    }

}
