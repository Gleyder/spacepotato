package de.gleyder.spacepotato.engine.lib.phase;

import de.gleyder.spacepotato.engine.lib.utils.Loadable;

/**
 * A game service can be everything, which is need for a game mode.
 * It can be attached to a stage or a scene.
 * Since that it is basically a loadable
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface GameService extends Loadable {

}
