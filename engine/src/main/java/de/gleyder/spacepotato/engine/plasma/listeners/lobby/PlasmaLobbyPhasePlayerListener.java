package de.gleyder.spacepotato.engine.plasma.listeners.lobby;

import de.gleyder.spacepotato.engine.lib.participant.GamePlayer;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayerRegistry;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import de.gleyder.spacepotato.engine.lib.participant.PlayingState;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaMessages;
import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Manages the GamePlayers.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class PlasmaLobbyPhasePlayerListener implements Listener {

    private final GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> playerRegistry;
    private final int maxPlayers;

    /*
     * Registers a GamePlayer and set its state to PLAYING
     * The player is kicked if the player is full
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerLogin(PlayerLoginEvent event) {
        if (this.playerRegistry.getPlaying().size() == this.maxPlayers) {
            event.disallow(PlayerLoginEvent.Result.KICK_FULL, PlasmaMessages.PHASE_LOBBY_SERVER$FULL.getMessage(false));
            return;
        }

        var player = this.playerRegistry.register(event.getPlayer());
        player.changeState(PlayingState.PLAYING.getId());
    }

    /*
     * Unregisters a GamePlayer
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerQuit(PlayerQuitEvent event) {
        this.playerRegistry.unregister(event.getPlayer());
    }

}
