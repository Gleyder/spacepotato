package de.gleyder.spacepotato.engine.plasma.listeners.lobby;

import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import de.gleyder.spacepotato.engine.plasma.phases.mode.staring.PlasmaModeStartingPhase;
import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Activates and deactivates the ModeStaring Countdown.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class PlasmaLobbyPhaseModeStartingListener implements Listener {

    private final PlasmaEngineSettings settings;

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (this.settings.getPlayerRegistry().getPlaying().size() == this.settings.getMinPlayers()) {
            this.settings.getPhaseManager().start(new PlasmaModeStartingPhase(this.settings));
        }
    }

}
