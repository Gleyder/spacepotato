package de.gleyder.spacepotato.engine.game.config;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface EngineConfig {

    int maxPlayers();
    int minPlayers();
    int stoppingCountdown();

}
