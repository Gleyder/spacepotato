package de.gleyder.spacepotato.engine.game.phases.lobby.listeners;

import de.gleyder.spacepotato.core.cleanup.PlayerCleanUp;
import de.gleyder.spacepotato.engine.game.config.EngineMessages;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayer;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayerRegistry;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import de.gleyder.spacepotato.engine.lib.participant.PlayingState;
import lombok.AllArgsConstructor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class LobbyPhasePlayerListener implements Listener {

    private final GamePlayerRegistry playerRegistry;
    private final Location spawn;

    @EventHandler(priority = EventPriority.LOW)
    public void onRegister(PlayerLoginEvent event) {
        GamePlayer<GameTeam> gamePlayer = this.playerRegistry.create(event.getPlayer());
        this.playerRegistry.register(gamePlayer);
        gamePlayer.changeState(PlayingState.PLAYING.getId());
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage(EngineMessages.PHASE_LOBBY_JOIN.getMessage(false, event.getPlayer().getName()));
        PlayerCleanUp.resetPlayer(event.getPlayer());
        PlayerCleanUp.clearInventory(event.getPlayer());
        event.getPlayer().teleport(this.spawn);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onUnregister(PlayerQuitEvent event) {
        this.playerRegistry.unregister(event.getPlayer());
        event.setQuitMessage(EngineMessages.PHASE_LOBBY_QUIT.getMessage(false, event.getPlayer().getName()));
    }

}
