package de.gleyder.spacepotato.engine.lib.verifier.tests;

import de.gleyder.spacepotato.engine.lib.verifier.SetupTest;
import de.gleyder.spacepotato.engine.lib.verifier.SetupTestException;
import lombok.AllArgsConstructor;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class NotNullValueTest implements SetupTest {

    private final String id;
    private final Object object;

    @Override
    public void test() throws SetupTestException {
        if (this.object == null)
            throw new SetupTestException(this.id + " is null");
    }

    @Override
    public Type getType() {
        return Type.REQUIRED;
    }

}
