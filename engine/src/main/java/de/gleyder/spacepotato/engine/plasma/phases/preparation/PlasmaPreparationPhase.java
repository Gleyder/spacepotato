package de.gleyder.spacepotato.engine.plasma.phases.preparation;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import de.gleyder.spacepotato.engine.plasma.listeners.preparation.PlasmaPreparationJoinListener;
import de.gleyder.spacepotato.engine.plasma.phases.PlasmaPhases;
import lombok.NonNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PlasmaPreparationPhase extends GamePhase {

    public PlasmaPreparationPhase(@NonNull SpacePotatoRegistry registry) {
        super(registry, PlasmaPhases.PREPARATION.getId());
        this.addListeners(new PlasmaPreparationJoinListener());
    }

}
