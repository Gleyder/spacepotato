package de.gleyder.spacepotato.engine.lib.utils;

import lombok.Getter;

public enum GamePhaseKey {

    LOBBY("lobby"),
    WAITING("waiting"),
    STARTING("starting"),
    LOADING("loading"),
    INGAME("ingame"),
    END("end"),
    ;

    @Getter
    private final String key;

    GamePhaseKey(String key) {
        this.key = key;
    }


}
