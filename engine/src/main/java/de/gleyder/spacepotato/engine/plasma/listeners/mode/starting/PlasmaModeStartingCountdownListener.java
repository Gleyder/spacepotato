package de.gleyder.spacepotato.engine.plasma.listeners.mode.starting;

import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import de.gleyder.spacepotato.engine.plasma.countdowns.lobby.ModeStartingCountdown;
import de.gleyder.spacepotato.engine.plasma.phases.PlasmaPhases;
import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class PlasmaModeStartingCountdownListener implements Listener {

    private final PlasmaEngineSettings settings;
    private final ModeStartingCountdown startingCountdown;

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
         if (this.startingCountdown.getCurrentValue() > this.settings.getMinTime() && this.settings.getPlayerRegistry().getPlaying().size() < this.settings.getMinPlayers()) {
             this.settings.getPhaseManager().stop(PlasmaPhases.MODE_STARTING.getId());
         }
    }

}
