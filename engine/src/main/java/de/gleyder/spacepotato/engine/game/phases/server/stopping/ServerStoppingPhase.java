package de.gleyder.spacepotato.engine.game.phases.server.stopping;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.engine.game.phases.EnginePhases;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ServerStoppingPhase extends GamePhase {

    public ServerStoppingPhase(@NotNull SpacePotatoRegistry registry) {
        super(registry, EnginePhases.SERVER_STOPPING.getId());
    }

}
