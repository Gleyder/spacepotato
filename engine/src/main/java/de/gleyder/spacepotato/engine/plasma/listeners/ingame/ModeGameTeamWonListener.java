package de.gleyder.spacepotato.engine.plasma.listeners.ingame;

import de.gleyder.spacepotato.engine.lib.event.GameTeamWonEvent;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class ModeGameTeamWonListener implements Listener {

    private final PlasmaEngineSettings settings;

    @EventHandler
    public void onGameTeamWon(GameTeamWonEvent<? extends GameTeam> event) {
        this.settings.getPhaseManager().stopAll();
        this.settings.getPhaseManager().start(this.settings.getEndPhase());
    }

}
