package de.gleyder.spacepotato.engine.lib.listeners;

import lombok.AllArgsConstructor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class SeaLevelListener implements Listener {

    private final int high;
    private final Location resetLocation;

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Location location = event.getTo();

        if (location.getBlockY() <= this.high)
            event.getPlayer().teleport(this.resetLocation);
    }

}
