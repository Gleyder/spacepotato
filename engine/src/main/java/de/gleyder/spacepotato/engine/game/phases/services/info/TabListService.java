package de.gleyder.spacepotato.engine.game.phases.services.info;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.engine.lib.phase.GameService;
import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class TabListService implements GameService {

    private final SpacePotatoRegistry spacePotatoRegistry;
    private final Listener listener;

    public TabListService(String header, String footer, SpacePotatoRegistry spacePotatoRegistry) {
        this.spacePotatoRegistry = spacePotatoRegistry;
        this.listener = new ServiceListener(header, footer);
    }

    @Override
    public void load() {
        this.spacePotatoRegistry.registerListener(this.listener);
    }

    @Override
    public void unload() {
        this.spacePotatoRegistry.unregisterListener(this.listener);
    }

    @AllArgsConstructor
    public static class ServiceListener implements Listener {

        private final String header, footer;

        @EventHandler
        public void onJoin(PlayerJoinEvent event) {
            event.getPlayer().setPlayerListHeaderFooter(this.header, this.footer);
        }

        @EventHandler
        public void onQuit(PlayerQuitEvent event) {
            event.getPlayer().setPlayerListHeaderFooter((String) null, null);
        }

    }

}
