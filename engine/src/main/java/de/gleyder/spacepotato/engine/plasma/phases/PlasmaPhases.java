package de.gleyder.spacepotato.engine.plasma.phases;

import lombok.Getter;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum PlasmaPhases {

    LOBBY("plasma-lobby"),
    PREPARATION("plasma-preparation"),
    MODE_STARTING("plasma-mode-starting"),
    MODE_TELEPORT("plasma-mode-teleport"),
    MODE_PREPARING_COUNTDOWN("plasma-mode-preparing-countdown"),
    MODE_INSTANT_START_PHASE("plasma-mode-instant-start"),
    MODE_END_PHASE("plasma-mode-end"),
    ;

    @Getter
    private final String id;

    PlasmaPhases(String id) {
        this.id = id;
    }


}
