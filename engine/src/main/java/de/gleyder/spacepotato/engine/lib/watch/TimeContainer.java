package de.gleyder.spacepotato.engine.lib.watch;

/**
 * Stores information about seconds to show it in various ways, like hours.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class TimeContainer {

    private final int minutes, hours, seconds;

    /**
     * Creates a time container
     *
     * @param pSeconds      time in seconds
     */
    @SuppressWarnings("WeakerAccess")
    public TimeContainer(float pSeconds) {
        float hourMultiply = 3600F/1F, secMinMultiply = 60F;

        float fHours = pSeconds / hourMultiply;
        this.hours = (int) fHours;

        float fMinutes = (fHours - this.hours) * secMinMultiply;
        this.minutes = (int) fMinutes;

        this.seconds =  (int) ((fMinutes - (int) fMinutes) * secMinMultiply);
    }


    public int getMinutes() {
        return this.minutes;
    }

    public int getHours() {
        return this.hours;
    }

    public int getSeconds() {
        return this.seconds;
    }

}
