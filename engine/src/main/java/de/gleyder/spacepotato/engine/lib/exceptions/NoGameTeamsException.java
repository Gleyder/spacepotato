package de.gleyder.spacepotato.engine.lib.exceptions;

/**
 * @author Gleyder
 */
public class NoGameTeamsException extends RuntimeException {

    public NoGameTeamsException() {
        super("There are no GameTeams");
    }

}
