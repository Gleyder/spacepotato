package de.gleyder.spacepotato.engine.plasma.phases.teleport;

import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import de.gleyder.spacepotato.engine.plasma.phases.PlasmaPhases;
import lombok.NonNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class TeleportPhase extends GamePhase {

    protected final PlasmaEngineSettings engineSettings;

    public TeleportPhase(@NonNull PlasmaEngineSettings settings) {
        super(settings.getSpacePotatoRegistry(), PlasmaPhases.MODE_TELEPORT.getId());
        this.engineSettings = settings;
    }

    public abstract void teleport();

}
