package de.gleyder.spacepotato.engine.plasma.countdowns.lobby;

import de.gleyder.spacepotato.core.chrono.Numerator;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayer;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayerRegistry;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import de.gleyder.spacepotato.engine.lib.phase.GamePhaseManager;
import de.gleyder.spacepotato.engine.lib.timedexecutor.TimedExecutor;
import de.gleyder.spacepotato.engine.lib.timedexecutor.TimedRunnable;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaMessages;
import de.gleyder.spacepotato.engine.plasma.phases.PlasmaPhases;
import lombok.NonNull;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.time.temporal.ChronoUnit;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ModeStartingCountdown extends Numerator.Countdown {

    private final TimedExecutor timedExecutor;
    private final GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> playerRegistry;
    private final int minPlayers;
    private final GamePhaseManager manager;

    public ModeStartingCountdown(@NotNull JavaPlugin javaPlugin, @NonNull PlasmaEngineSettings settings) {
        super(settings.getModeStartingDuration(), ChronoUnit.MINUTES, javaPlugin);
        this.minPlayers = settings.getMinPlayers();
        this.manager = settings.getPhaseManager();
        this.timedExecutor = new TimedExecutor(settings.getModeStartingDuration());
        this.playerRegistry = settings.getPlayerRegistry();
        this.timedExecutor.addTimedRunnable(
                TimedRunnable.createGameAbsolute(time -> settings.getPlayerRegistry().getBroadcastChannel().sendMessage(PlasmaMessages.PHASE_MODE_STARTING_COUNTDOWN.getMessage(true, time)))
        );
    }

    @Override
    public void onFinish() {
        if (this.playerRegistry.getPlaying().size() < this.minPlayers) {
            this.playerRegistry.getBroadcastChannel().sendMessage(PlasmaMessages.PHASE_MODE_STARTING_NOT$ENOUGH$PLAYERS.getMessage(true));
            this.manager.stop(PlasmaPhases.MODE_STARTING.getId());
        }
    }


    @Override
    public void onUpdate() {
        this.timedExecutor.execute(this.getCurrentValue());
    }

}
