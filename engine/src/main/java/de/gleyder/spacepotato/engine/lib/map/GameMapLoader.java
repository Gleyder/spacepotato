package de.gleyder.spacepotato.engine.lib.map;

import de.gleyder.spacepotato.core.configuration.ConfigHelper;
import de.gleyder.spacepotato.core.configuration.SpacePotatoConfigBinder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class GameMapLoader<T extends GameMap> {

    @Getter
    private final JavaPlugin plugin;
    private final ConfigHelper configHelper;
    private final Logger logger;

    @Getter
    private final Set<T> mapList;

    @Getter
    private final String folderPath;

    @Setter
    @Getter
    private T currentMap;

    public GameMapLoader(@NotNull JavaPlugin plugin, String filename) {
        this.plugin = plugin;
        this.mapList = new HashSet<>();
        this.configHelper = new ConfigHelper(this.plugin);
        this.folderPath = filename == null ? "maps" : filename;
        this.logger = LoggerFactory.getLogger(this.getClass());
     }

    public abstract T create(SpacePotatoConfigBinder spacePotatoConfigBinder);

    public T getMapByName(@NonNull String name) {
        for (T t : this.mapList) {
            if (t.getMapConfig().gameMapName().equals(name))
                return t;
        }
        return null;
    }

    public void load() {
        String[] mapNames = this.configHelper.getFileInDataFolder(this.folderPath).list();
        if (mapNames == null || mapNames.length == 0) {
            this.logger.warn("Couldn't find any game maps");
            return;
        }

        for (String mapName : mapNames) {
            SpacePotatoConfigBinder spacePotatoConfigBinder = new SpacePotatoConfigBinder(this.plugin, this.folderPath + "/" + mapName, "config");

            try {
                T gameMap = this.create(spacePotatoConfigBinder);
                this.mapList.add(gameMap);
                this.logger.info("Added GameMap: {}", gameMap.getMapConfig().worldName());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    public void unload() {
        this.mapList.clear();
        this.logger.info("GameMaps unloaded");
    }

    public void reload() {
        this.unload();
        this.load();
    }

}
