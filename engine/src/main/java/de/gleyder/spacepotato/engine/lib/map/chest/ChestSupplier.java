package de.gleyder.spacepotato.engine.lib.map.chest;

import de.gleyder.spacepotato.core.configuration.SpacePotatoSave;
import de.gleyder.spacepotato.engine.lib.map.chest.strategy.InventorySupplyStrategy;
import de.gleyder.spacepotato.engine.lib.map.chest.strategy.SupplyStrategy;
import lombok.Getter;
import org.bukkit.World;
import org.bukkit.block.Chest;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ChestSupplier {

    @Getter
    private final SpacePotatoSave<GameChestSave> save;

    private final Set<Chest> chestSet;
    private final World world;
    private final SupplyStrategy supplyStrategy;
    private final Logger logger;

    public ChestSupplier(JavaPlugin plugin, String filename, World world, int maxItems) {
        this.save = new SpacePotatoSave<>(plugin, filename, GameChestSave.class);
        this.chestSet = new HashSet<>();
        this.world = world;
        this.supplyStrategy = new InventorySupplyStrategy(this.save.load().getItemChanceEntryList(), maxItems);
        this.logger = plugin.getSLF4JLogger();
    }

    public void load() {
        this.chestSet.addAll(this.save.load().getChestLocations()
                .stream()
                .map(chestLocation -> {
                    try {
                        chestLocation.toChest(this.world);
                        return chestLocation.toChest(world);
                    } catch (ClassCastException e) {
                        this.logger.warn(e.getMessage());
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
        this.logger.info("Loaded Chest Locations");
    }

    public void supply() {
        this.chestSet.forEach(chest -> this.supplyStrategy.supply(chest.getInventory()));
    }

    public void unload() {
        this.chestSet.clear();
    }

    public void reload() {
        this.unload();
        this.load();
    }

}
