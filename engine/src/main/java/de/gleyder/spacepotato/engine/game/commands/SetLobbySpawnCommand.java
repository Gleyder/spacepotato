package de.gleyder.spacepotato.engine.game.commands;

import de.gleyder.spacepotato.core.command.annotations.ExecutorMethod;
import de.gleyder.spacepotato.core.command.conditions.OperatorCondition;
import de.gleyder.spacepotato.core.command.logic.CommandAdapter;
import de.gleyder.spacepotato.core.configuration.SpacePotatoSave;
import de.gleyder.spacepotato.engine.game.config.EngineMessages;
import de.gleyder.spacepotato.engine.game.config.EngineSave;
import org.bukkit.entity.Player;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SetLobbySpawnCommand extends CommandAdapter {

    private final SpacePotatoSave<EngineSave> save;

    public SetLobbySpawnCommand(SpacePotatoSave<EngineSave> save) {
        super("setLobbySpawn");
        this.save = save;
    }

    @Override
    public void define() {
        this.getBuilder()
                .addCondition(new OperatorCondition())
                .buildAndAdd();
    }

    @ExecutorMethod
    public void $native(Player player) {
        EngineSave engineSave = this.save.load();
        engineSave.setSpawn(player.getLocation());
        this.save.store(engineSave);
        EngineMessages.COMMAND_LOBBY$SPAWN$SET.sendMessage(player, true, player.getLocation());
    }

}
