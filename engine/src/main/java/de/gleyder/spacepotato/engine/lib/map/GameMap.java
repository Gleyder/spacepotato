package de.gleyder.spacepotato.engine.lib.map;

import de.gleyder.spacepotato.core.configuration.SpacePotatoConfigBinder;
import de.gleyder.spacepotato.engine.lib.map.config.GameMapConfig;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.World;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GameMap {

    @Getter
    private final SpacePotatoConfigBinder spacePotatoConfigBinder;

    @Getter
    private final GameMapConfig mapConfig;

    @Getter
    private final World world;

    public GameMap(SpacePotatoConfigBinder spacePotatoConfigBinder) {
        this.spacePotatoConfigBinder = spacePotatoConfigBinder;
        this.mapConfig = spacePotatoConfigBinder.getConfigurationProvider().bind("", GameMapConfig.class);
        this.world = Bukkit.getWorld(this.mapConfig.worldName());

        if (this.world == null)
            throw new NullPointerException("Couldn't load a world with the name: " + this.mapConfig.worldName());
    }


}
