package de.gleyder.spacepotato.engine.game.commands;

import de.gleyder.spacepotato.core.command.annotations.ExecutorMethod;
import de.gleyder.spacepotato.core.command.logic.CommandAdapter;
import de.gleyder.spacepotato.engine.game.config.EngineMessages;
import de.gleyder.spacepotato.engine.game.phases.EnginePhases;
import de.gleyder.spacepotato.engine.lib.phase.GamePhaseManager;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class StartCommand extends CommandAdapter {

    private final GamePhaseManager manager;

    public StartCommand(@NotNull GamePhaseManager manager) {
        super("start");
        this.manager = manager;
    }

    @Override
    public void define() {
        this.getBuilder()
                .buildAndAdd();
    }

    @ExecutorMethod
    public void $native(Player player) {
        if (!this.manager.isPhaseRunning(EnginePhases.LOBBY_MANUEL_START.getId())) {
            EngineMessages.PHASE_LOBBY_MANUEL$START_NOT$IN$MANUEL.sendMessage(player, true);
            return;
        }

        this.manager.stopAll();
    }

}