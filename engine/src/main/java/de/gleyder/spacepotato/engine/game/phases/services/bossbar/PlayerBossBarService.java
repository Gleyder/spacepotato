package de.gleyder.spacepotato.engine.game.phases.services.bossbar;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.utils.string.KeyReplacer;
import de.gleyder.spacepotato.engine.game.config.EngineMessages;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayer;
import de.gleyder.spacepotato.engine.lib.participant.GamePlayerRegistry;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import de.gleyder.spacepotato.engine.lib.phase.GameService;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PlayerBossBarService implements GameService {

    private final int minPlayers;
    private final GamePlayerRegistry<? extends GamePlayer< ? extends GameTeam>> playerRegistry;
    private final BossBar bossBar;
    private final float incrementor;
    private final SpacePotatoRegistry spacePotatoRegistry;
    private final Listener listener;

    public PlayerBossBarService(@NotNull SpacePotatoRegistry spacePotatoRegistry,
                                int minPlayers,
                                @NotNull GamePlayerRegistry<? extends GamePlayer< ? extends GameTeam>> playerRegistry) {
        this.spacePotatoRegistry = spacePotatoRegistry;
        this.minPlayers = minPlayers;
        this.playerRegistry = playerRegistry;
        this.bossBar = Bukkit.createBossBar(this.getBossbarTitle(), BarColor.BLUE, BarStyle.SOLID);
        this.incrementor = 1F / (float) this.minPlayers;
        this.listener = new PlayerBossBarServiceListener(this);
    }

    @Override
    public void load() {
        this.bossBar.setVisible(true);
        this.playerRegistry.getPlaying().forEach(p -> this.bossBar.addPlayer(p.getPlayer()));
        this.spacePotatoRegistry.registerListener(this.listener);
    }

    @Override
    public void unload() {
        this.bossBar.setVisible(false);
        this.bossBar.removeAll();
        this.spacePotatoRegistry.unregisterListener(this.listener);
    }

    void addPlayer(Player player) {
        this.bossBar.addPlayer(player);
        this.updateBossBar();
    }

    void removePlayer(Player player) {
        this.bossBar.removePlayer(player);
        this.updateBossBar();
    }

    private void updateBossBar() {
        float progress = this.incrementor * this.playerRegistry.getPlaying().size();
        if (progress > 1)
            progress = 1;

        this.bossBar.setProgress(progress);
        this.bossBar.setTitle(this.getBossbarTitle());
    }

    private String getBossbarTitle() {
        KeyReplacer replacer = new KeyReplacer(EngineMessages.PHASE_WAITING$FOR$PLAYERS_BOSSBAR$NAME.getMessage(false));
        replacer.replace("min", this.minPlayers);

        int currentPlayerSize = this.playerRegistry.getPlaying().size();
        if (currentPlayerSize >= this.minPlayers)
            replacer.addValue(ChatColor.GREEN + "" + currentPlayerSize + ChatColor.RESET);
        else
            replacer.addValue(ChatColor.RED + "" + currentPlayerSize + ChatColor.RESET);
        return replacer.build();
    }

}
