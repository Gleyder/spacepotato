package de.gleyder.spacepotato.engine.lib.watch;

import de.gleyder.spacepotato.core.chrono.Looper;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.Duration;

/**
 * Counts the elapsed seconds of a game
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GameWatch extends Looper {

    private long seconds;

    /**
     * Creates a game watch
     *
     * @param plugin        a plugin
     */
    public GameWatch(JavaPlugin plugin) {
        super(Duration.ofSeconds(1), false, plugin);
        this.seconds = 0;
    }

    /**
     * Updates the counter
     */
    @Override
    public void onUpdate() {
        this.seconds++;
    }

    /**
     * Resets the counter
     */
    public void reset() {
        this.seconds = 0;
    }

    /**
     * Gets the elapsed time in a time container
     *
     * @return      a time container
     */
    public TimeContainer getTimeContainer() {
        return new TimeContainer(this.seconds);
    }

}
