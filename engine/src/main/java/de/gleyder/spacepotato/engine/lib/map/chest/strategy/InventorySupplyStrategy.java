package de.gleyder.spacepotato.engine.lib.map.chest.strategy;

import lombok.Getter;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class InventorySupplyStrategy implements SupplyStrategy {

    private final Random random;

    @Getter
    private final List<ItemChanceEntry> entryList;

    private final int maxItems;

    public InventorySupplyStrategy(List<ItemChanceEntry> itemChanceEntries, int maxItems) {
        this.random = new Random();
        this.entryList = itemChanceEntries;
        this.maxItems = maxItems;
    }

    @Override
    public void supply(Inventory inventory) {
        inventory.clear();

        int itemCounter = 0;
        List<ItemStack> generatedItems = new ArrayList<>();

        //Generates the items
        for (ItemChanceEntry chanceEntry : this.entryList) {
            if (chanceEntry.getChance() >= this.random.nextDouble())
                continue;
            if (this.maxItems == itemCounter)
                break;

            generatedItems.add(chanceEntry.getItemStack().clone());
            itemCounter++;
        }

        //Calculates how many dummies are missing to fill up an inventory
        int missingDummies = inventory.getSize() - generatedItems.size();
        if (missingDummies > 0) {
            for (int i = 0; i < missingDummies; i++)
                generatedItems.add(null);
        }

        //Randomizes the order of the items
        Collections.shuffle(generatedItems, this.random);

        //Adds the items to the inventory
        for (int index = 0; index < generatedItems.size(); index++) {
            ItemStack itemStack = generatedItems.get(index);
            if (itemStack == null)
                continue;

            inventory.setItem(index, itemStack);
        }
    }

}
