package de.gleyder.spacepotato.engine.game.phases.services.numerator;

import de.gleyder.spacepotato.core.chrono.Numerator;
import de.gleyder.spacepotato.engine.lib.phase.GameService;
import lombok.AllArgsConstructor;
/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class GameCountdownService implements GameService {

    private final Numerator.Countdown gameCountdown;

    @Override
    public void load() {
        this.gameCountdown.start();
    }

    @Override
    public void unload() {
        this.gameCountdown.abort();
    }

}
