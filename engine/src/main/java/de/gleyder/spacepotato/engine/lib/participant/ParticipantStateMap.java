package de.gleyder.spacepotato.engine.lib.participant;

import com.google.common.collect.ImmutableList;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Stores participants according to their state
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @param <P> The participant type
 */
class ParticipantStateMap<P extends GamePlayer<? extends GameTeam>> {

    private final Map<String, HashMap<UUID, P>> playerStateMap;

    /**
     * Creates a ParticipantStateMap
     */
    ParticipantStateMap() {
        this.playerStateMap = new ConcurrentHashMap<>();
    }

    /**
     * Registers a participant
     *
     * @param participant       a participant
     */
    void register(@NotNull P participant) {
        this.add(participant);
    }

    /**
     * Unregisters a participant
     *
     * @param participant       a participant
     */
    void unregister(@NotNull P participant) {
        this.remove(participant);
    }

    /**
     * Registers a participant
     *
     * @param uuid      a uuid
     */
    void unregister(@NotNull UUID uuid) {
        P participant = this.getParticipant(uuid);
        if (participant == null)
            return;

        this.remove(participant);
    }

    void setState(@NotNull P participant, String state) {
        this.remove(participant);
        participant.setState(state);
        this.add(participant);
    }

    private boolean contains(@NotNull String state, @NotNull UUID uuid) {
        return this.getMap(state).containsKey(uuid);
    }

    private boolean contains(@NotNull UUID uuid) {
        return this.getParticipant(uuid) != null;
    }

    P getParticipant(@NotNull UUID uuid) {
        for (HashMap<UUID, P> value : this.playerStateMap.values()) {
            P participant = value.get(uuid);
            if (participant != null)
                return participant;
        }
        return null;
    }

    private P getParticipant(@NotNull String state, @NotNull UUID uuid) {
        return this.playerStateMap.get(state).get(uuid);
    }

    private void add(@NotNull P participant) {
        this.remove(participant);

        this.getMap(participant.getState())
                .put(participant.getUniqueId(), participant);
    }

    private void remove(@NotNull P participant) {
        for (HashMap<UUID, P> value : this.playerStateMap.values()) {
            value.remove(participant.getUniqueId());

            if (value.isEmpty())
                this.playerStateMap.remove(participant.getState());
        }
    }

    private HashMap<UUID, P> getMap(@NotNull String state) {
        return this.playerStateMap.computeIfAbsent(state, ignored -> new HashMap<>());
    }

    public ImmutableList<P> getAll(@NotNull String string) {
        ImmutableList.Builder<P> var = new ImmutableList.Builder<>();
        this.playerStateMap.values().forEach(map -> var.addAll(map.values()));
        return var.build();
    }

    public Collection<P> getAllAsList(@NotNull String string) {
        return this.playerStateMap.get(string).values();
    }

    @SuppressWarnings("WeakerAccess")
    public ImmutableList<P> getAll() {
        ImmutableList.Builder<P> builder = new ImmutableList.Builder<>();

        for (HashMap<UUID, P> value : this.playerStateMap.values()) {
            builder.addAll(value.values());
        }

        return builder.build();
    }

    ArrayList<P> getAllAsList() {
        ArrayList<P> arrayList = new ArrayList<>();

        for (HashMap<UUID, P> value : this.playerStateMap.values()) {
            arrayList.addAll(value.values());
        }

        return arrayList;
    }


}
