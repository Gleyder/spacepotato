package de.gleyder.spacepotato.engine.lib.utils;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface Loadable {

    void load();
    void unload();

}
