package de.gleyder.spacepotato.engine.game;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.configuration.SpacePotatoConfigBinder;
import de.gleyder.spacepotato.core.configuration.SpacePotatoSave;
import de.gleyder.spacepotato.core.configuration.handler.LoadoutHandler;
import de.gleyder.spacepotato.engine.SpacePotatoEnginePlugin;
import de.gleyder.spacepotato.engine.game.config.EngineConfig;
import de.gleyder.spacepotato.engine.game.config.EngineSave;
import de.gleyder.spacepotato.engine.game.phases.preparing.PreparingPhase;
import de.gleyder.spacepotato.engine.lib.phase.GamePhaseManager;
import de.gleyder.spacepotato.engine.lib.verifier.SetupVerifier;
import lombok.Getter;

/**
 * Main class for the engine.
 * This class starts and ends the game
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SpacePotatoEngine {

    @Getter
    private final SetupVerifier setupVerifier;

    @Getter
    private final SpacePotatoSave<EngineSave> spaceSave;

    @Getter
    private final SpacePotatoConfigBinder spacePotatoConfigBinder;

    @Getter
    private final GamePhaseManager gameManager;

    @Getter
    private final EngineSettingsProvider settingsProvider;

    @Getter
    private final SpacePotatoRegistry spacePotatoRegistry;

    @Getter
    private final EngineConfig engineConfig;

    @Getter
    private final EngineSave engineSave;

    /**
     * Creates a SpacePotatoEngine
     */
    public SpacePotatoEngine(EngineSettingsProvider provider) {
        LoadoutHandler engineLoadout = new LoadoutHandler(SpacePotatoEnginePlugin.getInstance());

        this.setupVerifier = new SetupVerifier();
        this.gameManager = new GamePhaseManager();

        this.settingsProvider = provider;
        this.spacePotatoRegistry = provider.getSpacePotatoRegistry();

        this.spacePotatoConfigBinder = new SpacePotatoConfigBinder(SpacePotatoEnginePlugin.getInstance(), null,"engineConfig");
        this.spaceSave = new SpacePotatoSave<>(SpacePotatoEnginePlugin.getInstance(), "engineSave", EngineSave.class);

        engineLoadout.addFile(this.spacePotatoConfigBinder, EngineConfig.class);

        engineLoadout.load();

        this.engineConfig = this.spacePotatoConfigBinder.getConfigurationProvider().bind("", EngineConfig.class);
        this.engineSave = this.spaceSave.load();
    }

    /**
     * Register commands
     */
    private void registerCommands() {
        this.settingsProvider.getTemplate().registerCommands();
    }

    /**
     * Verifies that all necessary values are accessible
     */
    private void setupChecks() {
        this.settingsProvider.getTemplate().setupChecks();
    }

    /**
     * Activates the engine
     */
    public void activate() {
        this.registerCommands();
        this.setupChecks();

        if (this.engineSave.isSetup()) {
            this.gameManager.start(new PreparingPhase(this.spacePotatoRegistry, this.settingsProvider.getLoginCheck()));
            this.setupVerifier.check(false);
        } else {
            this.setupVerifier.check(true);

            this.settingsProvider.getTemplate().start();
        }
    }

    /**
     * Deactivates the engine
     */
    public void deactivate() {
        this.gameManager.stopAll();
    }

}
