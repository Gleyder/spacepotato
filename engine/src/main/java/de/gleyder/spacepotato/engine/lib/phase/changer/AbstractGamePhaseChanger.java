package de.gleyder.spacepotato.engine.lib.phase.changer;

import de.gleyder.spacepotato.engine.lib.phase.GamePhaseManager;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class AbstractGamePhaseChanger implements GamePhaseChanger {

    protected final GamePhaseManager manager;

    public AbstractGamePhaseChanger(@NotNull GamePhaseManager manager) {
        this.manager = manager;
    }

}
