package de.gleyder.spacepotato.engine.game.config;

import de.gleyder.spacepotato.core.configuration.messages.MessageMap;
import de.gleyder.spacepotato.core.utils.string.KeyReplacer;
import org.bukkit.entity.Player;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum EngineMessages {

    PHASE_PREPARING_LOGIN$FAILED,
    PHASE_PREPARING_WELCOME$MESSAGE,

    PHASE_LOBBY_JOIN,
    PHASE_LOBBY_QUIT,

    PHASE_LOBBY_MANUEL$START_NOT$IN$MANUEL,

    PHASE_MODE_STARTING_COUNTDOWN,
    PHASE_MODE_PREPARING_COUNTDOWN,

    PHASE_WAITING$FOR$PLAYERS_BOSSBAR$NAME,

    PHASE_GLOBAL_SERVER$FULL,

    COMMAND_LOBBY$SPAWN$SET,
    COMMAND_COMBAT$AREA$LOC$SET,

    COMMAND_INTERPRETER$NULL,

    MAP_SELECTOR_LABEL,
    MAP_SELECTOR_LORE,
    ;

    static MessageMap MESSAGE_MAP;
    static String PREFIX;

    String path;

    public void sendMessage(Player player, boolean prefix, Object... objects) {
        player.sendMessage(this.getMessage(null, prefix, objects));
    }

    public KeyReplacer getKeyableMessage(boolean prefix, Object... objects) {
        return new KeyReplacer(this.getMessage(null, prefix, objects));
    }

    public String getMessage(String lang, boolean prefix, Object... objects) {
        return (prefix ? PREFIX + " " : "") + MESSAGE_MAP.getSuppliedMessage(lang, this.path, objects);
    }

    public String getMessage(boolean prefix, Object... objects) {
        return this.getMessage(null, prefix, objects);
    }

    @Override
    public String toString() {
        return this.getMessage(null, false);
    }

}
