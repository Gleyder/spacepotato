package de.gleyder.spacepotato.engine.plasma.command;

import de.gleyder.spacepotato.core.builder.command.MutableArgumentBuilder;
import de.gleyder.spacepotato.core.command.annotations.ExecutorMethod;
import de.gleyder.spacepotato.core.command.interpreters.IntegerInterpreter;
import de.gleyder.spacepotato.core.command.logic.CommandAdapter;
import de.gleyder.spacepotato.engine.plasma.PlasmaEngine;
import lombok.NonNull;
import org.bukkit.entity.Player;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PlasmaModeStartCommand extends CommandAdapter {

    private final PlasmaEngine engine;

    public PlasmaModeStartCommand(@NonNull PlasmaEngine engine) {
        super("start");
        this.engine = engine;
    }

    @Override
    public void define() {
        this.getBuilder()
                .setMethodName("$native")
                .addArgument(new MutableArgumentBuilder()
                        .setInterpreter(new IntegerInterpreter())
                        .setTabCompletion("duration")
                        .build())
                .buildAndAdd();
    }

    @ExecutorMethod
    public void $native(Player player, int duration) {
        this.engine.prepareMode();
    }

}
