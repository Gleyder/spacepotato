package de.gleyder.spacepotato.engine.game.phases.lobby.listeners;

import de.gleyder.spacepotato.core.cleanup.PlayerCleanUp;
import lombok.AllArgsConstructor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class LobbyPhaseSpawnListener implements Listener {

    private final Location spawn;

    @EventHandler(priority = EventPriority.LOWEST)
    public void onRespawn(PlayerRespawnEvent event) {
        event.setRespawnLocation(this.spawn);
        PlayerCleanUp.resetPlayer(event.getPlayer());
        PlayerCleanUp.clearInventory(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onMove(PlayerMoveEvent event) {
        Location location = event.getTo();

        if (location.getY() < 0)
            event.getPlayer().teleport(this.spawn);
    }

}
