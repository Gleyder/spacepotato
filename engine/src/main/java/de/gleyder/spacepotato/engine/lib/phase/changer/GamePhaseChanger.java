package de.gleyder.spacepotato.engine.lib.phase.changer;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface GamePhaseChanger {

    void change();
    void reverse();

}
