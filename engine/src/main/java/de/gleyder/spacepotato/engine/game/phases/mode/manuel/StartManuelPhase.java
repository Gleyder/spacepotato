package de.gleyder.spacepotato.engine.game.phases.mode.manuel;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class StartManuelPhase extends GamePhase {

    public StartManuelPhase(@NotNull SpacePotatoRegistry registry, @NotNull String id) {
        super(registry, id);
    }

}
