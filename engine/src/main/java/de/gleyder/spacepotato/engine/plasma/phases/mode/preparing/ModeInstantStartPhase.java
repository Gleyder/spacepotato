package de.gleyder.spacepotato.engine.plasma.phases.mode.preparing;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import de.gleyder.spacepotato.engine.plasma.PlasmaEngine;
import de.gleyder.spacepotato.engine.plasma.config.PlasmaEngineSettings;
import de.gleyder.spacepotato.engine.plasma.phases.PlasmaPhases;
import lombok.NonNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ModeInstantStartPhase extends GamePhase {

    public ModeInstantStartPhase(@NonNull SpacePotatoRegistry registry,
                                 @NonNull PlasmaEngine engine,
                                 @NonNull PlasmaEngineSettings settings) {
        super(registry, PlasmaPhases.MODE_INSTANT_START_PHASE.getId());
        settings.getPhaseManager().stop(this.getId());
        engine.startMode();
    }

}
