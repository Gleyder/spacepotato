package de.gleyder.spacepotato.engine.plasma.config;

import de.gleyder.spacepotato.core.utils.string.KeyReplacer;
import org.bukkit.entity.Player;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum PlasmaMessages {

    PHASE_PREPARATION_NOT$OP,
    PHASE_PREPARATION_WELCOME$MESSAGE,

    PHASE_MODE_PREPARING_COUNTDOWN,

    PHASE_MODE_END_COUNTDOWN,
    PHASE_MODE_END_LOGIN,

    PHASE_MODE_STARTING_COUNTDOWN,
    PHASE_MODE_STARTING_NOT$ENOUGH$PLAYERS,

    PHASE_LOBBY_SERVER$FULL
    ;

    static de.gleyder.spacepotato.core.configuration.messages.MessageMap MESSAGE_MAP;
    static String PREFIX;

    String path;

    public void sendMessage(Player player, boolean prefix, Object... objects) {
        player.sendMessage(this.getMessage(null, prefix, objects));
    }

    public KeyReplacer getKeyableMessage(boolean prefix, Object... objects) {
        return new KeyReplacer(this.getMessage(null, prefix, objects));
    }

    public String getMessage(String lang, boolean prefix, Object... objects) {
        return (prefix ? PREFIX + " " : "") + MESSAGE_MAP.getSuppliedMessage(lang, this.path, objects);
    }

    public String getMessage(boolean prefix, Object... objects) {
        return this.getMessage(null, prefix, objects);
    }

    @Override
    public String toString() {
        return this.getMessage(null, false);
    }
}
