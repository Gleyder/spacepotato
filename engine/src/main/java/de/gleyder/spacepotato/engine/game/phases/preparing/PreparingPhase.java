package de.gleyder.spacepotato.engine.game.phases.preparing;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.engine.game.phases.EnginePhases;
import de.gleyder.spacepotato.engine.game.phases.preparing.listeners.PreparingStageLoginListener;
import de.gleyder.spacepotato.engine.lib.phase.GamePhase;
import lombok.NonNull;
import org.bukkit.entity.Player;

import java.util.function.Predicate;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PreparingPhase extends GamePhase {

    public PreparingPhase(@NonNull SpacePotatoRegistry registry, Predicate<Player> loginCheck) {
        super(registry, EnginePhases.PREPARING.getId());
        this.addListeners(new PreparingStageLoginListener(loginCheck));
    }

}
