package de.gleyder.spacepotato.engine.game.phases.templates;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface PhasesTemplate {

    void start();
    void registerCommands();
    void setupChecks();

}
