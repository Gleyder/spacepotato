package de.gleyder.spacepotato.engine.lib.participant.spectator;

import de.gleyder.spacepotato.engine.lib.event.GamePlayerChangeStateEvent;
import de.gleyder.spacepotato.engine.lib.participant.PlayingState;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class SpectatorListener implements Listener {

    private final JavaPlugin plugin;

    @EventHandler
    public void toSpectator(GamePlayerChangeStateEvent event) {
        if (!event.getNewState().equals(PlayingState.SPECTATING.getId()))
            return;
        Player player = event.getGamePlayer().getPlayer();

        Bukkit.getOnlinePlayers().forEach(onlinePlayer -> onlinePlayer.hidePlayer(this.plugin, player));
    }

    @EventHandler
    public void fromSpectator(GamePlayerChangeStateEvent event) {
        if ( !(event.getOldState().equals(PlayingState.SPECTATING.getId()) && !event.getNewState().equals(PlayingState.SPECTATING.getId())) )
            return;
        Player player = event.getGamePlayer().getPlayer();

        Bukkit.getOnlinePlayers().forEach(onlinePlayer -> onlinePlayer.showPlayer(this.plugin, player));
    }

}
