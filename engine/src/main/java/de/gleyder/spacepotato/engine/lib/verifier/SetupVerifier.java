package de.gleyder.spacepotato.engine.lib.verifier;

import de.gleyder.spacepotato.core.utils.string.SuppliedString;
import org.apache.commons.lang.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SetupVerifier {

    private final List<SetupTest> setupTestList;
    private final Logger logger;

    public SetupVerifier() {
        this.setupTestList = new ArrayList<>();
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public void addTest(SetupTest setupTest) {
        this.setupTestList.add(setupTest);
    }

    public void check(boolean throwsException) {
        this.logger.info("Running all tests");
        this.runOptionalTests();
        this.runRequiredChecks(throwsException);
        this.logger.info("Ran all tests");
    }

    public void runOptionalTests() {
        this.logger.info("Running optional tests...");
        AtomicInteger failedTests = new AtomicInteger();
        this.getSetupTestList(SetupTest.Type.OPTIONAL)
                .forEach(setupTest -> {
                    try {
                        setupTest.test();
                    } catch (SetupTestException e) {
                        this.logger.warn(e.getMessage());
                        failedTests.getAndIncrement();
                    }
                });
        this.logger.info("Failed optional tests: " + failedTests.intValue());
    }

    public void runRequiredChecks(boolean throwsException) {
        this.logger.info("Running required tests...");
        AtomicInteger failedTests = new AtomicInteger();
        this.getSetupTestList(SetupTest.Type.REQUIRED)
                .forEach(setupTest -> {
                    if (throwsException) {
                        setupTest.test();
                        this.printSuccessTest(setupTest);
                    } else {
                        try {
                            setupTest.test();
                            this.printSuccessTest(setupTest);
                        } catch (SetupTestException e) {
                            e.printStackTrace();
                            failedTests.getAndIncrement();
                        }
                    }
                });
        this.logger.info("Failed required tests: " + failedTests.intValue());
    }

    private void printSuccessTest(SetupTest test) {
        this.logger.info(
                new SuppliedString("Test {} was positive")
                        .addValue(ClassUtils.getShortClassName(test.getClass()))
                        .build()
        );
    }

    private List<SetupTest> getSetupTestList(SetupTest.Type type) {
        return this.setupTestList.stream()
                .filter(setupTest -> setupTest.getType() == type)
                .collect(Collectors.toList());
    }

}
