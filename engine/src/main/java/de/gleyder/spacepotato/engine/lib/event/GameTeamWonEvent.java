package de.gleyder.spacepotato.engine.lib.event;

import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.Cancellable;

public class GameTeamWonEvent<T extends GameTeam> extends SimpleEvent implements Cancellable {

    @Getter
    private final T gameTeam;

    @Getter
    @Setter
    private String message;

    private boolean cancelled;

    public GameTeamWonEvent(T gameTeam, String message) {
        this.gameTeam = gameTeam;
        this.message = message;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }

}
