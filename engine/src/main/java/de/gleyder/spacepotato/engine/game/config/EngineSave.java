package de.gleyder.spacepotato.engine.game.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bukkit.Location;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EngineSave {

    private boolean setup;
    private Location spawn;
    private Location combatAreaLoc1, combatAreaLoc2;

}
