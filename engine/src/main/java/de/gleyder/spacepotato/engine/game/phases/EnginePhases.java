package de.gleyder.spacepotato.engine.game.phases;

import lombok.Getter;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum EnginePhases {

    PREPARING("preparing"),
    LOBBY("lobby"),
    LOBBY_MANUEL_START("lobby-manuel-start"),
    MODE_PREPARING("mode-preparing"),
    MODE_STARTING("mode-starting"),
    SERVER_STOPPING("server-stopping"),
    MIN_PLAYERS_WAITING("min-players-waiting"),
    MODE_AUTOMATIC_STARTING("mode-automatic-starting"),
    GLOBAL("global"),

    ;

    @Getter
    String id;

    EnginePhases(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.id;
    }
}
