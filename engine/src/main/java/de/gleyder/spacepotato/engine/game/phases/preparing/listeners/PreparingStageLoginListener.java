package de.gleyder.spacepotato.engine.game.phases.preparing.listeners;

import de.gleyder.spacepotato.engine.game.config.EngineMessages;
import lombok.AllArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.function.Predicate;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class PreparingStageLoginListener implements Listener {

    private final Predicate<Player> loginCheck;

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        System.out.println("ENGINE");

        Player player = event.getPlayer();
        if (this.loginCheck.test(player))
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, EngineMessages.PHASE_PREPARING_LOGIN$FAILED.getMessage(false));
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.getPlayer().sendMessage(EngineMessages.PHASE_PREPARING_WELCOME$MESSAGE.getMessage(false));
    }

}
