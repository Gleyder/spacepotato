package de.gleyder.spacepotato.engine.lib.timedexecutor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Gleyder
 */
public class TimedExecutor {

    private final List<TimedRunnable> runnableList;
    private final int maxValue;

    public TimedExecutor(int maxValue) {
        this.runnableList = new ArrayList<>();
        this.maxValue = maxValue;
    }

    public void addTimedRunnable(TimedRunnable timedRunnable) {
        this.runnableList.add(timedRunnable);
    }

    public void execute(int value) {
        this.runnableList.forEach(timedRunnable -> {
            for (int i : timedRunnable.getAbsolute()) {
                if (i == value) {
                    timedRunnable.getConsumer().accept(value);
                    break;
                }
            }

            for (float v : timedRunnable.getRelative()) {
                if (Math.round(v * this.maxValue) == value) {
                    timedRunnable.getConsumer().accept(value);
                    break;
                }
            }
        });
    }

}
