package de.gleyder.spacepotato.engine.lib.map.chest;

import de.gleyder.spacepotato.engine.lib.map.chest.strategy.ItemChanceEntry;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("WeakerAccess")
@NoArgsConstructor
@Setter
@Getter
public class GameChestSave {

    private String worldName;
    private Set<ChestLocation> chestLocations;
    private List<ItemChanceEntry> itemChanceEntryList;

}
