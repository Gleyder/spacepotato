package de.gleyder.spacepotato.engine.lib.participant;

import com.google.common.collect.ImmutableList;
import de.gleyder.spacepotato.core.broadcast.BroadcastChannel;
import de.gleyder.spacepotato.core.broadcast.ManualBroadcastChannel;
import de.gleyder.spacepotato.engine.lib.event.GameTeamWonEvent;
import de.gleyder.spacepotato.engine.lib.exceptions.NoGameTeamsException;
import de.gleyder.spacepotato.engine.lib.participant.interfaces.GameParticipantRegistry;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class GameTeamRegistry<T extends GameTeam> implements GameParticipantRegistry<T> {

    private final HashMap<String, T> registeredTeams;
    private final GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> playerRegistry;

    public GameTeamRegistry(@NotNull GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> playerRegistry) {
        this.registeredTeams = new HashMap<>();
        this.playerRegistry = playerRegistry;
    }

    @Override
    public boolean contains(@NotNull T participant) {
        return registeredTeams.containsKey(participant.getKey());
    }

    @Override
    public void register(@NotNull T gameTeam) {
        this.registeredTeams.put(gameTeam.getKey(), gameTeam);
        gameTeam.onRegister();
    }

    public T getTeam(@NonNull String key) {
        return this.registeredTeams.get(key);
    }

    @SuppressWarnings("WeakerAccess")
    public void unregister(@NotNull String key) {
        T gameTeam = this.registeredTeams.get(key);
        if (gameTeam == null)
            return;

        this.registeredTeams.remove(key);
        if (this.registeredTeams.size() == 1) {
            T lastTeam = this.getLastTeam();
            GameTeamWonEvent event = new GameTeamWonEvent<>(lastTeam, "§9Das Team §e" + lastTeam.getName() + " §9hat gewonnen");
            Bukkit.getPluginManager().callEvent(event);
            if (!event.isCancelled()) {
                Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(event.getMessage()));
            }
        }
        gameTeam.onUnregister();
    }

    @Override
    public void unregister(@NotNull T gameTeam) {
        this.unregister(gameTeam.getKey());
    }

    public BroadcastChannel getBroadcaster() {
        List<Player> collection = new ArrayList<>();
        this.registeredTeams.values().forEach(t -> collection.addAll(t.getPlayers()));
        return new ManualBroadcastChannel(collection);
    }

    public void fillTeams() {
        if (this.getAll().isEmpty()) {
            throw new NoGameTeamsException();
        }

        List<GamePlayer> noTeams = new ArrayList<>();

        this.playerRegistry.getAll().forEach(gamePlayer -> {
            if (gamePlayer.getTeam() == null) {
                noTeams.add(gamePlayer);
            }
        });

        noTeams.forEach(gamePlayer -> {
            GameTeam weakestTeam = null;

            for (GameTeam gameTeam : this.getAll()) {
                if (weakestTeam == null) {
                    weakestTeam = gameTeam;
                    continue;
                }
                if (gameTeam.getPlayers().size() < weakestTeam.getPlayers().size())
                    weakestTeam = gameTeam;
            }

            //noinspection ConstantConditions
            weakestTeam.addPlayer(gamePlayer.getPlayer());
        });

        this.getAll().forEach(gameTeam -> {
            if (gameTeam.getPlayers().isEmpty())
                this.unregister(gameTeam.getKey());
        });
    }

    public ImmutableList<T> getAll() {
        return new ImmutableList.Builder<T>().addAll(this.registeredTeams.values()).build();
    }

    @SuppressWarnings("WeakerAccess")
    public T getLastTeam() {
        if (this.registeredTeams.size() == 1) {
            for (T value : this.registeredTeams.values()) {
                return value;
            }
            return null;
        } else {
            return null;
        }
    }

}
