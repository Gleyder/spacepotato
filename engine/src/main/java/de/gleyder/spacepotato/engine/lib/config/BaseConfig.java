package de.gleyder.spacepotato.engine.lib.config;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface BaseConfig {

    String prefix();

}
