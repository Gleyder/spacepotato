package de.gleyder.spacepotato.engine.lib.event;

import de.gleyder.spacepotato.engine.lib.participant.GamePlayer;
import de.gleyder.spacepotato.engine.lib.participant.GameTeam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.Cancellable;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class GamePlayerChangeStateEvent extends SimpleEvent implements Cancellable {

    @Getter
    private final String oldState;

    @Getter
    private final GamePlayer<? extends GameTeam> gamePlayer;

    @Setter
    @Getter
    private String newState;

    @Setter
    @Getter
    private boolean cancelled;


}
