package de.gleyder.spacepotato.engine.game.phases.services.map;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.builder.item.ItemBuilder;
import de.gleyder.spacepotato.core.clickable.hotbar.ClickableHotbar;
import de.gleyder.spacepotato.core.clickable.hotbar.ClickableHotbarRegistry;
import de.gleyder.spacepotato.core.clickable.hotbar.interfaces.SimpleClickAction;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryItem;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import de.gleyder.spacepotato.core.clickable.inventory.interfaces.LeftClickInventoryClickAction;
import de.gleyder.spacepotato.core.gui.impl.GUIInventoryFactory;
import de.gleyder.spacepotato.core.gui.impl.GUIInventoryRegistry;
import de.gleyder.spacepotato.core.gui.navigable.NavigableInventory;
import de.gleyder.spacepotato.engine.game.config.EngineMessages;
import de.gleyder.spacepotato.engine.lib.map.GameMap;
import de.gleyder.spacepotato.engine.lib.map.GameMapLoader;
import de.gleyder.spacepotato.engine.lib.phase.GameService;
import lombok.AllArgsConstructor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GameMapAdminSelectorService implements GameService {

    private final Listener listener;
    private final SpacePotatoRegistry spacePotatoRegistry;
    private final MapNavigableInventory navigableInventory;
    private final GameMapLoader<? extends GameMap> gameMapLoader;
    private final ClickableHotbarRegistry<Player> hotbarRegistry;

    public GameMapAdminSelectorService(@NotNull SpacePotatoRegistry spacePotatoRegistry, @NotNull GUIInventoryFactory inventoryFactory, GameMapLoader<? extends GameMap> gameMapLoader) {
        this.spacePotatoRegistry = spacePotatoRegistry;
        this.gameMapLoader = gameMapLoader;
        this.hotbarRegistry = spacePotatoRegistry.createHotbarRegistry();
        this.navigableInventory = inventoryFactory.createNavigableInventory(
                (registry, guiInventoryRegistry, inventoryRegistry) -> new MapNavigableInventory(guiInventoryRegistry, registry.getPlugin(), inventoryRegistry)
        );

        this.listener = new ServiceListener(this.hotbarRegistry, this.navigableInventory);
    }

    @Override
    public void load() {
        //GameMaps are loaded
        this.gameMapLoader.load();

        //Inventory is cleared
        this.navigableInventory.clear();

        //
        this.navigableInventory.addIterable(new ArrayList<>(this.gameMapLoader.getMapList()));
        this.navigableInventory.build();

        this.spacePotatoRegistry.registerListener(this.listener);
    }

    @Override
    public void unload() {
        this.spacePotatoRegistry.unregisterListener(this.listener);
        this.hotbarRegistry.unregisterAll();
        this.gameMapLoader.unload();
    }

    public static class MapNavigableInventory extends NavigableInventory<GameMap> {

        MapNavigableInventory(@NotNull GUIInventoryRegistry guiInventoryRegistry, @NotNull JavaPlugin plugin, @NotNull ClickableInventoryRegistry clickableInventoryRegistry) {
            super(guiInventoryRegistry, plugin, clickableInventoryRegistry);
        }

        @Override
        public ClickableInventoryItem toItem(GameMap gameMap) {
            ItemStack itemStack = new ItemBuilder(Material.PAPER)
                    .setDisplayName(gameMap.getMapConfig().gameMapName())
                    .setLore("WorldName: " + gameMap.getMapConfig().worldName())
                    .build();

            return new ClickableInventoryItem(new LeftClickInventoryClickAction() {
                @Override
                public void onClick(Player player, Inventory inventory, ItemStack item) {
                    player.teleport(gameMap.getWorld().getSpawnLocation());
                }
            }, itemStack);
        }

    }

    @AllArgsConstructor
    public static class ServiceListener implements Listener {

        private final ClickableHotbarRegistry<Player> clickableHotbarRegistry;
        private final MapNavigableInventory navigableInventory;

        @EventHandler
        public void onJoin(PlayerJoinEvent event) {
            Player player = event.getPlayer();
            ClickableHotbar<Player> clickableHotbar = ClickableHotbar.createPlayerHotbar(player);
            ItemStack mapSelector = new ItemBuilder(Material.COMPASS)
                    .setDisplayName(EngineMessages.MAP_SELECTOR_LABEL.getMessage(false))
                    .setLore(EngineMessages.MAP_SELECTOR_LORE.getMessage(false))
                    .build();
            this.clickableHotbarRegistry.register(clickableHotbar);

            clickableHotbar.setAction(mapSelector, new SimpleClickAction() {
                @Override
                public void onRightClick(Player player) {
                    ServiceListener.this.navigableInventory.open(player);
                }
            });
            player.getInventory().addItem(mapSelector);
        }

        @EventHandler
        public void onQuit(PlayerQuitEvent event) {
            this.clickableHotbarRegistry.unregister(event.getPlayer());
        }
    }

}
