package de.gleyder.spacepotato.engine.plasma.listeners.preparation;

import de.gleyder.spacepotato.engine.plasma.config.PlasmaMessages;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PlasmaPreparationJoinListener implements Listener {

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerLogin(PlayerLoginEvent event) {
        if (event.getPlayer().isOp())
            return;

        event.disallow(PlayerLoginEvent.Result.KICK_OTHER, PlasmaMessages.PHASE_PREPARATION_NOT$OP.getMessage(false));
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.getPlayer().sendMessage(PlasmaMessages.PHASE_PREPARATION_WELCOME$MESSAGE.getMessage(true));
    }

}
