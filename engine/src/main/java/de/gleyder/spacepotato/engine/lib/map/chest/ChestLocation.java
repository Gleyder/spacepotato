package de.gleyder.spacepotato.engine.lib.map.chest;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("WeakerAccess")
@Setter
@Getter
@NoArgsConstructor
public class ChestLocation {

    private int x, y, z;

    public Chest toChest(World world) {
        Block block = world.getBlockAt(this.x, this.y, this.z);
        if (!(block instanceof Chest))
            throw new ClassCastException("The block at x: " + this.x + ", y: " + this.y + ", z: " + this.z + " is not a chest");
        return (Chest) block;
    }

}
