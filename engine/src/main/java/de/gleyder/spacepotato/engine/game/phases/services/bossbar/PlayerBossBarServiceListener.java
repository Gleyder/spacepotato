package de.gleyder.spacepotato.engine.game.phases.services.bossbar;

import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class PlayerBossBarServiceListener implements Listener {

    private final PlayerBossBarService bossBarService;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        this.bossBarService.addPlayer(event.getPlayer());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        this.bossBarService.removePlayer(event.getPlayer());
    }

}
