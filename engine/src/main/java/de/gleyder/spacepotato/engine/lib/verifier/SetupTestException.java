package de.gleyder.spacepotato.engine.lib.verifier;

import de.gleyder.spacepotato.core.utils.exceptions.BaseRuntimeException;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SetupTestException extends BaseRuntimeException {

    public SetupTestException(String message, Object... objects) {
        super(message, objects);
    }

}
