package de.gleyder.spacepotato.engine.lib.map.chest.strategy;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.inventory.ItemStack;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@Setter
@Getter
@AllArgsConstructor
public class ItemChanceEntry {

    private ItemStack itemStack;
    private double chance;

}
