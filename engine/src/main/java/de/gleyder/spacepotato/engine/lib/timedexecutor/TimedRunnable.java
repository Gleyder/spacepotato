package de.gleyder.spacepotato.engine.lib.timedexecutor;

import lombok.Getter;

import java.util.function.Consumer;

/**
 * @author Gleyder
 */
public class TimedRunnable {

    @Getter
    private final Consumer<Integer> consumer;

    @Getter
    private final float[] relative;

    @Getter
    private final int[] absolute;

    public TimedRunnable(Consumer<Integer> consumer, float[] relative, int[] absolute) {
        this.consumer = consumer;
        this.relative = relative;
        this.absolute = absolute;
    }

    public static TimedRunnable createAbsolute(Consumer<Integer> consumer, int... absolute) {
        return new TimedRunnable(consumer, new float[0], absolute);
    }

    public static TimedRunnable createRelative(Consumer<Integer> consumer, float... relative) {
        return new TimedRunnable(consumer, relative, new int[0]);
    }

    public static TimedRunnable createGameAbsolute(Consumer<Integer> consumer) {
        return TimedRunnable.createAbsolute(consumer, 60, 30, 15, 10, 5, 4, 3, 2, 1);
    }

}
