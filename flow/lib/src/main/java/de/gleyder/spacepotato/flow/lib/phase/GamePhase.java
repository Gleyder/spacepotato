package de.gleyder.spacepotato.flow.lib.phase;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.flow.lib.phase.services.ListenerRegistryService;
import lombok.*;
import org.bukkit.event.Listener;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * A game phase contains listeners and services
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@ToString
public class GamePhase {

    @Setter(AccessLevel.PACKAGE)
    @Getter
    private boolean active;

    @Getter
    private final String id, tag;

    private final ListenerRegistryService listenerRegistry;
    private final List<GameService> registeredServices;

    @Setter(AccessLevel.PACKAGE)
    private SpacePotatoRegistry spacePotatoRegistry;

    /**
     * Creates a game phase
     *
     * @param id        the id
     * @param tag       the tag
     */
    public GamePhase(@NonNull String id, @Nullable String tag) {
        this.registeredServices = new ArrayList<>();
        this.listenerRegistry = new ListenerRegistryService(this.spacePotatoRegistry);
        this.registeredServices.add(this.listenerRegistry);
        this.id = id;
        this.tag = tag == null ? "default" : tag;
    }

    /**
     * Registers a service
     *
     * @param service       a game service
     */
    public void registerService(@NonNull GameService service) {
        this.registeredServices.add(service);
    }

    /**
     * Registers a listener
     *
     * @param listener      a listener
     */
    public void registerListener(@NonNull Listener listener) {
        this.listenerRegistry.getListenerList().add(listener);
    }

    /**
     * Loads the service
     */
    void load() {
        this.registeredServices.forEach(GameService::load);
    }

    /**
     * Unloads the service
     */
    void unload() {
        this.registeredServices.forEach(GameService::unload);
    }

}
