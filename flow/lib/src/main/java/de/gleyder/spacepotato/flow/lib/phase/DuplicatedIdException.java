package de.gleyder.spacepotato.flow.lib.phase;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class DuplicatedIdException extends RuntimeException {

    public DuplicatedIdException(String message) {
        super(message);
    }
}
