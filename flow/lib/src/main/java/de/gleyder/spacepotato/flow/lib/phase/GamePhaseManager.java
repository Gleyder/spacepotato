package de.gleyder.spacepotato.flow.lib.phase;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GamePhaseManager {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final List<GamePhase> registeredPhases;
    private final SpacePotatoRegistry spacePotatoRegistry;

    public GamePhaseManager(@NonNull SpacePotatoRegistry spacePotatoRegistry) {
        this.registeredPhases = new ArrayList<>();
        this.spacePotatoRegistry = spacePotatoRegistry;
    }

    public void register(@NonNull GamePhase phase) {
        this.checkId(phase.getId());

        this.registeredPhases.add(phase);
        phase.setSpacePotatoRegistry(this.spacePotatoRegistry);
        this.logger.info("Registered phase: {}", phase.getId());
    }

    public void startById(@NonNull String id) {
        this.start(this.getById(id));
    }

    public void stopById(@NonNull String id) {
        this.stop(this.getById(id));
    }

    public void startByTag(@NonNull String tag) {
        this.getByTag(tag).stream()
                .filter(phase -> !phase.isActive())
                .forEach(this::start);
    }

    public void stopByTag(@NonNull String tag) {
        this.getByTag(tag).stream()
                .filter(GamePhase::isActive)
                .forEach(this::stop);
    }

    @Nullable
    private GamePhase getById(@NonNull String id) {
        for (GamePhase registeredPhase : this.registeredPhases) {
            if (registeredPhase.getId().equals(id))
                return registeredPhase;
        }
        return null;
    }

    @NotNull
    private List<GamePhase> getByTag(@NonNull String tag) {
        List<GamePhase> phaseList = new ArrayList<>();
        for (GamePhase registeredPhase : this.registeredPhases) {
            if (!registeredPhase.getTag().equals(tag))
                continue;

            phaseList.add(registeredPhase);
        }
        return phaseList;
    }

    private void start(@NonNull GamePhase phase) {
        this.checkState(phase, false);

        phase.load();
        phase.setActive(true);
        this.logger.info("Started phase " + phase);
    }

    private void stop(@NonNull GamePhase phase) {
        this.checkState(phase, true);

        phase.unload();
        phase.setActive(false);
        this.logger.info("Stopped phase: " + phase);
    }

    private void checkId(@NonNull String id) {
        for (GamePhase registeredPhase : this.registeredPhases) {
            if (registeredPhase.getId().equals(id))
                throw new DuplicatedIdException("The id " + id + " already exists");
        }
    }

    private void checkState(@NonNull GamePhase phase, boolean shouldActivate) {
        if (shouldActivate != phase.isActive())
            throw new IllegalStateException("GamePhase " + phase.getId() + " is " + phase.isActive() + " but should be " + shouldActivate);
    }

}
