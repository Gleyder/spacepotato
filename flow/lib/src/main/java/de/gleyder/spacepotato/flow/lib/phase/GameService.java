package de.gleyder.spacepotato.flow.lib.phase;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface GameService {

    void load();
    void unload();

}
