package de.gleyder.spacepotato.flow.lib.phase.services;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.flow.lib.phase.GameService;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ListenerRegistryService implements GameService {

    @Getter
    private final List<Listener> listenerList;

    private final SpacePotatoRegistry potatoRegistry;

    public ListenerRegistryService(@NonNull SpacePotatoRegistry potatoRegistry) {
        this.listenerList = new ArrayList<>();
        this.potatoRegistry = potatoRegistry;
    }

    @Override
    public void load() {
        this.listenerList.forEach(this.potatoRegistry::registerListener);
    }

    @Override
    public void unload() {
        this.listenerList.forEach(this.potatoRegistry::unregisterListener);
    }
}

