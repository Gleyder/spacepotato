package de.gleyder.spacepotato.flow.battleworld.lib.participant.game.exceptions;

import de.gleyder.spacepotato.flow.battleworld.lib.participant.game.GameTeam;

public class GameTeamFullException extends RuntimeException {

    public GameTeamFullException(GameTeam gameTeam) {
        super("GameTeam " + gameTeam.getName() + "#" + gameTeam.getKey() + " is full");
    }

}
