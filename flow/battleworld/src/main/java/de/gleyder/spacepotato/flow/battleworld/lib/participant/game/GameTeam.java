package de.gleyder.spacepotato.flow.battleworld.lib.participant.game;

import com.google.common.collect.ImmutableList;
import de.gleyder.spacepotato.core.broadcast.BroadcastChannel;
import de.gleyder.spacepotato.core.broadcast.SuppliedBroadcastChannel;
import de.gleyder.spacepotato.core.builder.item.ItemBuilder;
import de.gleyder.spacepotato.core.interfaces.Optional;
import de.gleyder.spacepotato.core.utils.SpacePotatoColor;
import de.gleyder.spacepotato.flow.battleworld.lib.participant.game.exceptions.GameTeamFullException;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Team;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class GameTeam {

    @SuppressWarnings("WeakerAccess")
    protected final Set<Player> players;

    @SuppressWarnings("WeakerAccess")
    protected final ItemStack statusItem;

    @Getter
    protected final int maxSize;

    @Getter
    protected final BroadcastChannel broadcastGroup;

    @Getter
    protected final String key;

    @Getter
    protected final SpacePotatoColor spacePotatoColor;

    @Getter
    protected final String name;

    @Setter
    @Getter
    protected boolean friendlyFire;

    @Getter
    protected final Team scoreboardTeam;

    private final GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> registry;

    public GameTeam(@NotNull String key,
                    @NotNull String name,
                    int maxSize,
                    boolean friendlyFire,
                    @NotNull SpacePotatoColor spacePotatoColor,
                    @NotNull GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> registry) {
        this.key = key;
        this.friendlyFire = false;
        this.spacePotatoColor = spacePotatoColor;
        this.friendlyFire = friendlyFire;
        this.maxSize = maxSize;
        this.name = name;
        this.players = new HashSet<>();
        this.broadcastGroup = new SuppliedBroadcastChannel(() -> this.players);
        this.statusItem = spacePotatoColor.getBannerItem();
        this.scoreboardTeam = Bukkit.getScoreboardManager().getNewScoreboard().registerNewTeam(this.key);
        this.scoreboardTeam.setColor(this.spacePotatoColor.getChatColor());
        this.scoreboardTeam.setDisplayName(this.name);
        this.registry = registry;
    }

    @Optional
    protected void onRegister() {}

    @Optional
    protected void onUnregister() {}

    public void setOption(Team.Option option, Team.OptionStatus status) {
        this.scoreboardTeam.setOption(option, status);
    }

    public void setCanSeeFriendlyInvisible(boolean enabled) {
        this.scoreboardTeam.setCanSeeFriendlyInvisibles(enabled);
    }

    public boolean canSeeFriendlyInvisible() {
        return this.scoreboardTeam.canSeeFriendlyInvisibles();
    }

    public void addPlayer(@NotNull Player player) {
        if (this.getMaxSize() == this.players.size())
            throw new GameTeamFullException(this);

        this.registry.get(player.getUniqueId()).setTeam(this);
        this.players.add(player);
        this.scoreboardTeam.addEntry(player.getName());
        this.updateStatusItem();
    }

    public void removePlayer(@NotNull Player player) {
        this.players.remove(player);
        this.scoreboardTeam.removeEntry(player.getName());
        this.registry.get(player.getUniqueId()).setTeam(null);
        this.updateStatusItem();
    }

    public ImmutableList<Player> getPlayers() {
        return new ImmutableList.Builder<Player>().addAll(this.players).build();
    }

    public ItemStack getWoolItem() {
        return this.statusItem;
    }

    public List<String> getPlayerNames() {
        return this.players.stream().map(Player::getDisplayName).collect(Collectors.toList());
    }

    public boolean equals(GameTeam gameTeam) {
        return this.getKey().equalsIgnoreCase(gameTeam.getKey());
    }

    private void updateStatusItem() {
        new ItemBuilder(this.statusItem).setLore(this.getPlayerNames());
    }

    public boolean isEmpty() {
        return this.players.isEmpty();
    }

}
