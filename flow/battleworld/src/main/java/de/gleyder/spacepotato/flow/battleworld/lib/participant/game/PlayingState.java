package de.gleyder.spacepotato.flow.battleworld.lib.participant.game;

import lombok.Getter;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum PlayingState {

    SPECTATING("spectating"),
    RESPAWNING("respawning"),
    PLAYING("playing"),
    UNDEFINED("undefined"),
    ;

    @Getter
    String id;

    PlayingState(String id) {
        this.id = id;
    }
}
