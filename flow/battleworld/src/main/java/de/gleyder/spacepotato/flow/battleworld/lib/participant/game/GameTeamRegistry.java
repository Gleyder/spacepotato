package de.gleyder.spacepotato.flow.battleworld.lib.participant.game;

import com.google.common.collect.ImmutableList;
import de.gleyder.spacepotato.core.broadcast.BroadcastChannel;
import de.gleyder.spacepotato.core.broadcast.ManualBroadcastChannel;
import de.gleyder.spacepotato.flow.battleworld.lib.participant.game.event.GameTeamWonEvent;
import de.gleyder.spacepotato.flow.battleworld.lib.participant.game.interfaces.GameParticipantRegistry;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class GameTeamRegistry<T extends GameTeam> implements GameParticipantRegistry<T> {

    private final HashMap<String, T> registeredTeams;
    private final GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> playerRegistry;

    public GameTeamRegistry(@NotNull GamePlayerRegistry<? extends GamePlayer<? extends GameTeam>> playerRegistry) {
        this.registeredTeams = new HashMap<>();
        this.playerRegistry = playerRegistry;
    }

    @Override
    public boolean contains(@NotNull T participant) {
        return registeredTeams.containsKey(participant.getKey());
    }

    @Override
    public void register(@NotNull T gameTeam) {
        this.registeredTeams.put(gameTeam.getKey(), gameTeam);
        gameTeam.onRegister();
    }

    public T getTeam(@NonNull String key) {
        return this.registeredTeams.get(key);
    }

    @SuppressWarnings("WeakerAccess")
    public void unregister(@NotNull String key) {
        T gameTeam = this.registeredTeams.get(key);
        if (gameTeam == null)
            return;

        this.registeredTeams.remove(key);
        if (this.registeredTeams.size() == 1) {
            T lastTeam = this.registeredTeams.values().stream().findAny().get();
            GameTeamWonEvent event = new GameTeamWonEvent<>(lastTeam, "§9Das Team §e" + lastTeam.getName() + " §9hat gewonnen");
            Bukkit.getPluginManager().callEvent(event);
            if (!event.isCancelled()) {
                Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(event.getMessage()));
            }
        }
        gameTeam.onUnregister();
    }

    @Override
    public void unregister(@NotNull T gameTeam) {
        this.unregister(gameTeam.getKey());
    }

    public BroadcastChannel getBroadcaster() {
        List<Player> collection = new ArrayList<>();
        this.registeredTeams.values().forEach(t -> collection.addAll(t.getPlayers()));
        return new ManualBroadcastChannel(collection);
    }

    public ImmutableList<T> getAll() {
        return new ImmutableList.Builder<T>().addAll(this.registeredTeams.values()).build();
    }

}
