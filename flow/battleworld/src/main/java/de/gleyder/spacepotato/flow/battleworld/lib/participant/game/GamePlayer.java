package de.gleyder.spacepotato.flow.battleworld.lib.participant.game;

import de.gleyder.spacepotato.core.interfaces.Optional;
import de.gleyder.spacepotato.core.utils.string.SuppliedString;
import de.gleyder.spacepotato.flow.battleworld.lib.participant.game.event.GamePlayerChangeStateEvent;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * Represents a game player
 *
 * @param <T>       the team type
 */
public abstract class GamePlayer<T extends GameTeam> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Getter
    protected final Player player;

    @Getter
    protected String state;

    @Getter
    protected T team;

    @Getter
    protected GamePlayerRegistry<? extends GamePlayer<T>> playerRegistry;

    /**
     * Creates a game player
     *
     * @param player            a bukkit player
     * @param playerRegistry    a player registry
     */
    public GamePlayer(@NonNull Player player, GamePlayerRegistry<? extends GamePlayer<T>> playerRegistry) {
        this.player = player;
        this.state = PlayingState.UNDEFINED.getId();
        this.playerRegistry = playerRegistry;
    }

    @Optional
    @SuppressWarnings("WeakerAccess")
    protected void onRegister() {}

    @Optional
    @SuppressWarnings("WeakerAccess")
    protected void onUnregister() {}

    /**
     * Sends a message
     *
     * @param message   a message
     */
    public final void sendMessage(@NonNull String message) {
        this.player.sendMessage(message);
    }

    /**
     * Plays a sound
     *
     * @param sound         a sound
     * @param category      a sound category
     */
    public final void playSound(@NonNull Sound sound, @NonNull SoundCategory category) {
        this.player.playSound(this.player.getLocation(), sound, category, 1, 1);
    }

    /**
     * Plays a sound
     *
     * @param sound     a sound
     */
    public final void playSound(@NonNull Sound sound) {
        this.player.playSound(this.player.getLocation(), sound, 1, 1);
    }

    /**
     * Returns the unique id
     *
     * @return      the unique id
     */
    public final UUID getUniqueId() {
        return this.player.getUniqueId();
    }

    public final boolean equals(@NonNull Player player) {
        return this.player.getUniqueId().equals(player.getUniqueId());
    }

    public boolean equals(@NonNull GamePlayer gamePlayer) {
        return this.player.getUniqueId().equals(gamePlayer.getUniqueId());
    }

    public final boolean equals(@NonNull UUID uuid) {
        return this.player.getUniqueId().equals(uuid);
    }

    final void setTeam(GameTeam team) {
        this.team = (T) team;
    }

    void setState(String state) {
        this.state = state == null ? PlayingState.UNDEFINED.getId() : state;
    }

    public void changeState(String newState) {
        String oldState = this.state;
        GamePlayerChangeStateEvent event = new GamePlayerChangeStateEvent(oldState, this, newState, false);

        Bukkit.getPluginManager().callEvent(event);

        if (event.isCancelled())
            return;

        newState = event.getNewState();

        this.playerRegistry.changeState(this, newState);
        this.logger.info("Player {}'s state changed from {} to {}", player.getName(), oldState, newState);
    }

    @Override
    public String toString() {
        return new SuppliedString("GamePlayer{uuid={}, name={}, state={}}")
                .addValues(this.player.getUniqueId(), this.player.getName(), this.state)
                .build();
    }

}
