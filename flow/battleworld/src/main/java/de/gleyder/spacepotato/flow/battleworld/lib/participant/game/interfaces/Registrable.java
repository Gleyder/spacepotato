package de.gleyder.spacepotato.flow.battleworld.lib.participant.game.interfaces;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface Registrable {

    void onRegister();
    void onUnregister();

}
