package de.gleyder.spacepotato.flow.battleworld.lib.participant.game.interfaces;

import com.google.common.collect.ImmutableList;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface GameParticipantRegistry<P> {

    void register(P participant);
    void unregister(P participant);

    boolean contains(P participant);
    ImmutableList<P> getAll();

}
