package de.gleyder.spacepotato.test;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SpacePotatoTestPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        SpacePotatoRegistry registry = new SpacePotatoRegistry(this);
    }

}
