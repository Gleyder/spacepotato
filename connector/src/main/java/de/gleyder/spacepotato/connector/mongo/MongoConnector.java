package de.gleyder.spacepotato.connector.mongo;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import de.gleyder.spacepotato.connector.mongo.entities.MongoModel;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang.reflect.FieldUtils;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Stores MongoNode and syncs with the database
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class MongoConnector<M extends MongoModel, V extends MongoModelView, C extends MongoModelController<M>> {

    protected final MongoCollection<M> collection;
    protected final LoadingCache<ObjectId, MongoNode> cache;
    protected final JavaPlugin plugin;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private int expectedSize;

    /**
     *  Creates a MongoConnector
     *
     * @param collection        a collection
     */
    public MongoConnector(@NonNull MongoCollection<M> collection, @NonNull JavaPlugin plugin) {
        this.collection = collection;
        this.cache = Caffeine.newBuilder()
                .expireAfterWrite(2, TimeUnit.MINUTES)
                .build(new MongoCacheLoader());
        this.plugin = plugin;
    }

    public void removeModel(ObjectId objectId) {
        this.cache.invalidate(objectId);
        Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> {
            this.collection.deleteOne(Filters.eq(objectId));
            this.logger.info("Removed " + objectId);
            this.expectedSize--;
        });
    }

    public void addModel(M model) {
        this.cache.put(model.getId(), this.createNode(model));
        Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> {
            this.collection.insertOne(model);
            this.logger.info("Added " + model.getId());
            this.expectedSize++;
        });
    }

    public M getModel(ObjectId id) {
        MongoNode node = this.getNode(id);
        if (node == null)
            return null;
        return node.getModel();
    }

    public void load() {
        this.expectedSize = 0;
        this.collection.find().iterator().forEachRemaining(m -> {
            this.cache.put(m.getId(), this.createNode(m));
            this.expectedSize++;
        });
    }

    public Collection<MongoNode> getAll() {
        if (this.expectedSize != this.cache.asMap().size())
            this.load();

        return this.cache.asMap().values();
    }

    public void clear() {
        this.cache.invalidateAll();
    }

    public List<M> getModels() {
        return this.cache.asMap().values().stream().map(MongoNode::getModel).collect(Collectors.toList());
    }

    /**
     * Creates a view
     *
     * @param model     a model
     * @return          a view
     */
    public abstract V createView(M model);

    /**
     * Creates a controller
     *
     * @param model     a model
     * @return          a controller
     */
    public abstract C createController(M model);

    private V createViewInternal(M model) {
        V view = this.createView(model);
        if (view == null)
            throw new IllegalArgumentException("Method \"createView\" returns null");

        try {
            FieldUtils.writeField(view, "model", model, true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return view;
    }

    private C createControllerInternal(M model) {
        C controller = this.createController(model);
        if (controller == null)
            throw new IllegalArgumentException("Method \"createController\" returns null");

        try {
            FieldUtils.writeField(controller, "collection", this.collection, true);
            FieldUtils.writeField(controller, "cache", this.cache, true);
            FieldUtils.writeField(controller, "objectId", model.getId(), true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return controller;
    }

    /**
     * Returns a node
     *
     * @param id        an object id
     * @return          a mongo node
     */
    public MongoNode getNode(ObjectId id) {
        return this.cache.get(id);
    }

    /**
     * Creates a node
     *
     * @param model     a model
     * @return          a nodeö
     */
    private MongoNode createNode(M model) {
        return new MongoNode(model, this.createViewInternal(model), this.createControllerInternal(model));
    }

    /**
     * Stores the model, the view and the controller
     */
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public class MongoNode {

        @Getter(AccessLevel.PACKAGE)
        private final M model;

        @Getter
        private final V view;

        @Getter
        private final C controller;
    }

    /**
     * Cache for mongo documents
     */
    private class MongoCacheLoader implements CacheLoader<ObjectId, MongoNode> {

        @Nullable
        @Override
        public MongoNode load(@org.checkerframework.checker.nullness.qual.NonNull ObjectId key) {
            return MongoConnector.this.createNode(MongoConnector.this.collection.find(Filters.eq(key)).first());
        }

        @Override
        public @org.checkerframework.checker.nullness.qual.NonNull Map<ObjectId, MongoNode> loadAll(@org.checkerframework.checker.nullness.qual.NonNull Iterable<? extends ObjectId> keys) {
            Map<ObjectId, MongoNode> map = new HashMap<>();
            MongoConnector.this.collection.find().iterator().forEachRemaining(m -> map.put(m.getId(), MongoConnector.this.createNode(m)));
            return map;
        }

    }

}
