package de.gleyder.spacepotato.connector.mongo;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import de.gleyder.spacepotato.connector.mongo.entities.MongoModel;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

/**
 * Changes the properties of a model
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class MongoModelController<M extends MongoModel> {

    protected MongoCollection<M> collection;
    protected LoadingCache<ObjectId, MongoConnector.MongoNode> cache;
    protected ObjectId objectId;

    protected Bson getFilter() {
        return Filters.eq(this.objectId);
    }

    protected M getModel() {
        //noinspection unchecked
        return (M) this.cache.get(this.objectId).getModel();
    }

}
