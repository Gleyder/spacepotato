package de.gleyder.spacepotato.connector.mongo;

import de.gleyder.spacepotato.connector.mongo.entities.MongoModel;
import org.bson.types.ObjectId;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class MongoModelView<M extends MongoModel> {

    protected M model;

    public ObjectId getId() {
        return this.model.getId();
    }

}
