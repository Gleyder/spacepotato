package de.gleyder.spacepotato.connector.mongo.entities;

import org.bson.types.ObjectId;

/**
 * Represents a mongo model
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface MongoModel {

    ObjectId getId();

}
