package de.gleyder.spacepotato.core.command.exceptions;

/**
 * Thrown if an errors occurs in an option.
 * Used to define when a test failed.
 * The message is sent to the sender.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class OptionException extends Exception {

    /**
     * Creates a OptionException
     */
    public OptionException(String message) {
        super(message);
    }

}
