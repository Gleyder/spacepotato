package de.gleyder.spacepotato.core.builder.item.exceptions;

import lombok.NonNull;

/**
 * Thrown if an unknown PotionType is used in the builder
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class UnexpectedPotionBuilderInitException extends RuntimeException {

    public UnexpectedPotionBuilderInitException(@NonNull String message) {
        super(message);
    }
}
