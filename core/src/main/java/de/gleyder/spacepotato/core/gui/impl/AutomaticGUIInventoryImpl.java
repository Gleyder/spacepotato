package de.gleyder.spacepotato.core.gui.impl;

import de.gleyder.spacepotato.core.chrono.Looper;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import de.gleyder.spacepotato.core.gui.AutomaticGUIInventory;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.Duration;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class AutomaticGUIInventoryImpl extends GUIInventoryImpl implements AutomaticGUIInventory {

    @SuppressWarnings("FieldCanBeLocal")
    private Looper looper;

    private final Duration duration;
    private final JavaPlugin plugin;

    AutomaticGUIInventoryImpl(GUIInventoryRegistry guiInventoryRegistry, JavaPlugin plugin, ClickableInventoryRegistry registry, String title, int rows, Duration updateRate) {
        super(guiInventoryRegistry, registry, title, rows);
        this.duration = updateRate;
        this.plugin = plugin;
        this.run();
    }

    @Override
    public void run() {
        if (this.looper != null)
            this.stop();

        this.looper = new Looper(this.duration, false, this.plugin) {
            @Override
            public void onUpdate() {
                AutomaticGUIInventoryImpl.this.constructStatic();
            }
        };
        this.looper.start();
    }

    @Override
    public void stop() {
        this.looper.stop();
    }

}
