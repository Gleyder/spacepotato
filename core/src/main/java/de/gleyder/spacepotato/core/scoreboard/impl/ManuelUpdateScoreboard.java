package de.gleyder.spacepotato.core.scoreboard.impl;

import de.gleyder.spacepotato.core.scoreboard.ScoreboardCreation;
import lombok.NonNull;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.RenderType;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.scoreboard.impl.AbstractEasyScoreboard
 */
public class ManuelUpdateScoreboard extends AbstractEasyScoreboard {

    public ManuelUpdateScoreboard(@NonNull ScoreboardCreation creation, @NonNull String displayName, String name, String criteria, RenderType renderType, DisplaySlot displaySlot) {
        super(creation, displayName, name, criteria, renderType, displaySlot);
    }

}
