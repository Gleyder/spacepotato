package de.gleyder.spacepotato.core.configuration.typeadapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ItemStackDeserializer implements JsonDeserializer<ItemStack> {

    @Override
    public ItemStack deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            return ItemStackUtil.itemStackArrayFromBase64(context.deserialize(json, String.class))[0];
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
