package de.gleyder.spacepotato.core.gui;

/**
 * Represents an automatic gui inventory
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface AutomaticGUIInventory extends GUIInventory {

    /**
     * Runs the auto updater
     */
    void run();

    /**
     * Stops the auto updater
     */
    void stop();

}
