package de.gleyder.spacepotato.core;

import de.gleyder.spacepotato.core.clickable.block.ClickableBlockRegistry;
import de.gleyder.spacepotato.core.clickable.hotbar.ClickableHotbarRegistry;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import de.gleyder.spacepotato.core.clickable.item.ClickableItemRegistry;
import de.gleyder.spacepotato.core.clickable.mob.ClickableMobRegistry;
import de.gleyder.spacepotato.core.clickable.triggerplate.TriggerPlateRegistry;
import de.gleyder.spacepotato.core.command.logic.CommandAdapter;
import de.gleyder.spacepotato.core.command.logic.CommandRegistry;
import de.gleyder.spacepotato.core.empty.EmptyManager;
import de.gleyder.spacepotato.core.utils.string.ListSuppliedString;
import de.gleyder.spacepotato.core.utils.string.SuppliedString;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * A registry to register space potato related entities
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SpacePotatoRegistry {

    @Getter
    private final JavaPlugin plugin;

    private final CommandRegistry commandRegistry;
    private final Logger logger;

    /**
     * Creates a PaperRegistry
     *
     * @param plugin        a plugin
     */
    public SpacePotatoRegistry(JavaPlugin plugin) {
        this.plugin = plugin;
        this.commandRegistry = new CommandRegistry(this.plugin);
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    /**
     * Creates an empty manager
     *
     * @return      an empty manager
     */
    public EmptyManager createEmptyManager() {
        return new EmptyManager(this);
    }

    /**
     * Creates a new clickable item registry
     *
     * @return      an item registry
     */
    public <T> ClickableItemRegistry createClickableItemRegistry() {
        return new ClickableItemRegistry(this);
    }

    /**
     * Creates a new hotbar
     *
     * @return      a hotbar
     */
    public <T> ClickableHotbarRegistry<T> createHotbarRegistry() {
        return new ClickableHotbarRegistry<T>(this);
    }

    /**
     * Creates a new inventory registry
     *
     * @return      a clickable inventory registry
     */
    public ClickableInventoryRegistry createInventoryRegistry() {
        return new ClickableInventoryRegistry(this);
    }

    /**
     * Creates a new block registry
     *
     * @return      a block registry
     */
    public ClickableBlockRegistry createClickableBlockRegistry() {
        return new ClickableBlockRegistry(this);
    }

    /**
     * Creates a mob registry
     *
     * @return      a mob registry
     */
    public ClickableMobRegistry createClickableMobRegistry() {
        return new ClickableMobRegistry(this);
    }

    /**
     * Creates a trigger plate registry
     *
     * @return      a trigger plate registry
     */
    public TriggerPlateRegistry createTriggerPlateRegistry() {
        return new TriggerPlateRegistry(this);
    }

    /**
     * Registered commands
     *
     * @param commandAdapter        an adapter
     */
    public void registerCommand(CommandAdapter commandAdapter) {
        this.commandRegistry.register(commandAdapter);
    }

    /**
     * Registers an array of listeners
     *
     * @param listeners     an array of listener
     */
    public void registerListener(@NonNull Listener... listeners) {
        PluginManager manager = Bukkit.getPluginManager();
        for (Listener listener : listeners) {
            manager.registerEvents(listener, this.plugin);
            this.logger.info(this.getListenerInfo(listener));
        }
    }

    /**
     * Unregisters an array of listeners
     *
     * @param listeners     an array of listener
     */
    public void unregisterListener(@NonNull Listener... listeners) {
        for (Listener listener : listeners) {
            HandlerList.unregisterAll(listener);
            this.logger.info(this.getListenerInfo(listener));
        }
    }

    /**
     * Unregisters all listeners
     */
    public void unregisterAllListeners() {
        HandlerList.unregisterAll();
    }

    /**
     * Returns an info string of a listener class
     *
     * @param listener      a listener
     * @return              a string
     */
    private String getListenerInfo(@NonNull Listener listener) {
        ListSuppliedString<Method> listSuppliedString = new ListSuppliedString<>(
                Arrays.stream(listener.getClass().getDeclaredMethods()).filter(method -> method.isAnnotationPresent(EventHandler.class)).collect(Collectors.toList()),
                method -> {
                    SuppliedString suppliedString = new SuppliedString("Event: {}\nMethod Name: {}\nPriority: {}\n");
                    suppliedString.addValue(method.getParameters()[0].getType().getSimpleName());
                    suppliedString.addValue(method.getName());
                    suppliedString.addValue(method.getAnnotation(EventHandler.class).priority());
                    return suppliedString.build();
                }
        );

        listSuppliedString.setHeader("Registered listener " + listener.getClass().getSimpleName());
        return listSuppliedString.build();
    }

}
