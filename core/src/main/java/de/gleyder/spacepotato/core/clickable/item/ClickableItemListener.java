package de.gleyder.spacepotato.core.clickable.item;

import lombok.AllArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
class ClickableItemListener implements Listener {

    private final ClickableItemRegistry itemRegistry;

    @EventHandler
    public void onPlayerInteractAtBlock(PlayerInteractEvent event) {
        if (event.getItem() == null)
            return;

        for (ClickableItem clickableItem : this.itemRegistry.getAll()) {
            if (!clickableItem.test(event.getItem()))
                continue;

            boolean cancel = clickableItem.getAction().onClick(event.getPlayer(), event.getItem(), event.getHand(), event.getAction(), event.getClickedBlock(), null);
            event.setCancelled(cancel);
            break;
        }
    }

    @EventHandler
    public void onPlayerInteractAtEntity(PlayerInteractEntityEvent event) {
        if (!(event.getHand() == EquipmentSlot.HAND || event.getHand() == EquipmentSlot.OFF_HAND))
            return;
        ItemStack itemStack = null;
        Player player = event.getPlayer();
        switch (event.getHand()) {
            case HAND:
                itemStack = player.getInventory().getItemInMainHand();
                break;
            case OFF_HAND:
                itemStack = player.getInventory().getItemInOffHand();
                break;
        }

        for (ClickableItem clickableItem : this.itemRegistry.getAll()) {
            if (!clickableItem.test(itemStack))
                continue;

            boolean cancel = clickableItem.getAction().onClick(player, itemStack, event.getHand(), null, null, event.getRightClicked());
            event.setCancelled(cancel);
            break;
        }
    }

}
