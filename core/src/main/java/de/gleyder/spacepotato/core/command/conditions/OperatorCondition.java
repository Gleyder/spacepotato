package de.gleyder.spacepotato.core.command.conditions;

import de.gleyder.spacepotato.core.command.interfaces.Condition;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import org.bukkit.entity.Player;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class OperatorCondition implements Condition<Player> {

    @Override
    public boolean test(Player sender) {
        return sender.isOp();
    }

    @Override
    public String getMessage() {
        return CoreMessages.COMMAND_BASE_NO$PERM.getMessage(true);
    }

}
