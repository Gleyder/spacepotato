package de.gleyder.spacepotato.core.analyzer;

import lombok.Getter;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Enum of search query types.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum QueryType {

    /**
     * Indicates that at least one query result must be true
     */
    ANY(results -> {
        for (QueryResult result : results) {
            if (result.isSuccesses())
                return true;
        }
        return false;
    }),

    /**
     * Indicates that all query results must be true
     */
    ALL(results -> {
        int score = 0;
        for (QueryResult result : results) {
            if (result.isSuccesses())
                score++;
        }
        return score == results.size();
    }),

    /**
     * Indicates that all query results must be false
     */
    NONE(results-> {
        boolean match = false;
        for (QueryResult result : results) {
            if (result.isSuccesses()) {
                match = true;
                break;
            }
        }
        return !match;
    })
    ;

    @Getter
    private final Predicate<List<QueryResult<Object>>> predicate;

    /**
     * Creates a Type
     *
     * @param predicate       a predicate
     */
    QueryType(Predicate<List<QueryResult<Object>>> predicate) {
        this.predicate = predicate;
    }

    /**
     * test a list if query results
     *
     * @param list      a list of query results
     * @return          if the test was successful
     */
    public <T> boolean test(List<QueryResult<T>> list) {
        List<QueryResult<Object>> objectList = list.stream()
                .map(result -> new QueryResult<>((Object) result.getContent(), result.isSuccesses())).collect(Collectors.toList());

        return this.predicate.test(objectList);
    }

}
