package de.gleyder.spacepotato.core.command.impl;

import de.gleyder.spacepotato.core.command.entities.MutableArgument;
import de.gleyder.spacepotato.core.command.enums.ArgumentType;
import de.gleyder.spacepotato.core.command.enums.InterpreterMode;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.interfaces.Option;
import de.gleyder.spacepotato.core.utils.string.StringUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.function.Function;

/**
 * Implementation of MutableArgument.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@ToString
public class MutableArgumentImpl extends ArgumentImpl implements MutableArgument {

    @Setter
    @Getter
    protected Interpreter<Object> interpreter;

    @Setter
    @Getter
    protected Function<Object, Object> converter;

    @Setter
    @Getter
    protected Option<Object> option;

    @Setter
    @Getter
    protected InterpreterMode mode;

    @Setter
    @Getter
    protected String key;

    @Getter
    @Setter
    protected boolean endless;

    @Setter
    @Getter
    protected String description, completion;

    @Setter
    protected boolean multiArgumentsAllowed;

    /**
     * Creates a MutableArgumentImpl
     *
     * @param description               a description
     * @param interpreter               an interpreter
     * @param option                    an option
     * @param mode                      a mode
     * @param key                       a key
     * @param endless                   if it is endless
     * @param multiArgumentsAllowed     if multi arguments are allowed
     */
    public MutableArgumentImpl(String description, String completion, Interpreter<Object> interpreter, Option<Object> option, Function<Object, Object> converter, InterpreterMode mode, String key, boolean endless, boolean multiArgumentsAllowed) {
        super(ArgumentType.MUTABLE);
        this.description = StringUtil.emptyIfNull(description);
        this.interpreter = interpreter == null ? Interpreter.getDefault() : interpreter;
        this.option = option == null ? Option.getDefault() : option;
        this.mode = mode == null ? InterpreterMode.getDefault() : mode;
        this.completion = StringUtil.emptyIfNull(completion);
        this.key = StringUtil.emptyIfNull(key);
        this.converter = converter == null ? o -> o : converter;
        if (endless) {
            this.endless = true;
            this.multiArgumentsAllowed = true;
        } else {
            this.endless = false;
            this.multiArgumentsAllowed = multiArgumentsAllowed;
        }
    }

    @Override
    public boolean areMultiArgumentsAllowed() {
        return this.multiArgumentsAllowed;
    }

}
