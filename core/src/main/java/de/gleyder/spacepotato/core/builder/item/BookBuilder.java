package de.gleyder.spacepotato.core.builder.item;

import de.gleyder.spacepotato.core.builder.item.exceptions.MaterialNotSupportedException;
import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.NonNull;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

/**
 * Builder for books.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public final class BookBuilder implements IBuilder<ItemStack> {
    
    private final ItemStack book;
    private final BookMeta meta;

    /**
     * Creates a BookBuilder
     */
    public BookBuilder() {
        this.book = new ItemStack(Material.WRITTEN_BOOK);
        this.meta = (BookMeta) this.book.getItemMeta();
    }

    /**
     * Creates a BookBuilder
     *
     * @param itemStack     the item stack
     */
    public BookBuilder(@NonNull ItemStack itemStack) {
        this(itemStack, false);
    }

    /**
     * Creates a BookBuilder
     *
     * @param itemStack     the item
     * @param copy          if it should be copied
     */
    public BookBuilder(@NonNull ItemStack itemStack, boolean copy) {
        if (itemStack.getType() != Material.WRITTEN_BOOK) {
            throw new MaterialNotSupportedException(itemStack.getType() + " is not a book");
        }

        if (copy) {
            this.book = itemStack.clone();
        } else {
            this.book = itemStack;
        }

        this.meta = (BookMeta) this.book.getItemMeta();
    }

    /**
     * Sets the title
     *
     * @param title     the title
     * @return          this
     */
    public BookBuilder setTitle(@NonNull String title) {
        this.meta.setTitle(title);
        return this;
    }

    /**
     * Sets the author
     *
     * @param author    the author
     * @return          this
     */
    public BookBuilder setAuthor(@NonNull String author) {
        this.meta.setAuthor(author);
        return this;
    }

    /**
     * Adds a page
     *
     * @param content   the content
     * @return          this
     */
    public BookBuilder addPage(@NonNull String content) {
        this.meta.addPage(content);
        return this;
    }

    /**
     * Sets a page
     *
     * @param page      the page
     * @param content   the content
     * @return          this
     */
    public BookBuilder setPage(int page, @NonNull String content) {
        this.meta.setPage(page, content);
        return this;
    }

    /**
     * Sets the generation
     *
     * @param generation    the generation
     * @return              this
     */
    public BookBuilder setGeneration(@NonNull BookMeta.Generation generation) {
        this.meta.setGeneration(generation);
        return this;
    }

    /**
     * Converts it to an item builder
     *
     * @return      this
     */
    public ItemBuilder toItemBuilder() {
        return new ItemBuilder(this.build());
    }

    /**
     * Builds the item
     *
     * @return      the item
     */
    @Override
    public ItemStack build() {
        this.book.setItemMeta(this.meta);
        return this.book;
    }
    
}
