package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Translates an argument into an interpreter.
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class EnumInterpreter implements Interpreter<Enum<?>> {

    private final Class<? extends Enum> anEnum;
    private final List<String> tabCompletions;

    /**
     * Creates an EnumInterpreter
     *
     * @param anEnum        an enum
     * @param values        the list of enum vales
     */
    public EnumInterpreter(Class<? extends Enum> anEnum, Object[] values) {
        this.anEnum = anEnum;
        this.tabCompletions = Arrays.stream(values)
                .map(Object::toString)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
    }

    /**
     * Translates an argument into a enum
     *
     * @param argument                          an argument
     * @return                                  an enum
     * @throws InterpreterException     if the argument cannot be translated
     */
    @SuppressWarnings("unchecked")
    @Override
    public Enum<?> translate(String argument) throws InterpreterException {
        try {
            return Enum.valueOf(this.anEnum, argument);
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_ENUM.getKeyableMessage(true)
                    .replace("argument", argument)
                    .build()
            );
        }
    }

    @Override
    public List<String> getTabCompletion() {
        return this.tabCompletions;
    }

    @Override
    public Class<Enum<?>> getClassType() {
        return (Class<Enum<?>>) this.anEnum;
    }

}
