package de.gleyder.spacepotato.core.clickable.block;

import de.gleyder.spacepotato.core.clickable.block.interfaces.BlockClickAction;
import lombok.Getter;
import org.bukkit.block.Block;

/**
 * Represents a clickable block.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ClickableBlock {

    @Getter
    private final Block block;

    @Getter
    private BlockClickAction action;

    /**
     * Creates a ClickableBlock
     *
     * @param block         a block
     */
    public ClickableBlock(Block block) {
        this.block = block;
    }

    /**
     * Sets an action
     *
     * @param action        an action
     */
    public void setAction(BlockClickAction action) {
        this.action = action;
    }

}
