package de.gleyder.spacepotato.core.scoreboard.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents a scoreboard line
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
@Setter
@Getter
public class Line {

    private String entry, key;

}
