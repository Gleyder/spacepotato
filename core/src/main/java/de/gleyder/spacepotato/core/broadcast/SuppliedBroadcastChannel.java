package de.gleyder.spacepotato.core.broadcast;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * Players are accessed via a supplier
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SuppliedBroadcastChannel extends AbstractBroadcastChannel {

    private final Supplier<Collection<Player>> collectionSupplier;

    /**
     * Creates a SuppliedBroadcastChannel
     *
     * @param collectionSupplier        a supplier that supplies a collection of players
     */
    public SuppliedBroadcastChannel(@NotNull Supplier<Collection<Player>> collectionSupplier) {
        this.collectionSupplier = collectionSupplier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Player> getPlayers() {
        return this.collectionSupplier.get();
    }


}
