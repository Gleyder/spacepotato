package de.gleyder.spacepotato.core.configuration;

import lombok.Getter;
import org.apache.commons.io.FileUtils;

import java.io.File;

/**
 * Represents a temporary file
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class TemporaryFile {

    @Getter
    private final File file;

    @SuppressWarnings("WeakerAccess")
    public TemporaryFile(File file) {
        this.file = file;
    }

    /**
     * Deletes the file
     */
    public void delete() {
        FileUtils.deleteQuietly(this.file);
    }

}
