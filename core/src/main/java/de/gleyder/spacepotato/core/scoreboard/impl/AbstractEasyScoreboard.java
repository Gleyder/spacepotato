package de.gleyder.spacepotato.core.scoreboard.impl;

import de.gleyder.spacepotato.core.scoreboard.EasyScoreboard;
import de.gleyder.spacepotato.core.scoreboard.InsertionRule;
import de.gleyder.spacepotato.core.scoreboard.ScoreboardCreation;
import de.gleyder.spacepotato.core.scoreboard.exceptions.LineInsertionException;
import de.gleyder.spacepotato.core.utils.FinderUtil;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;

/**
 * Abstract implementation for the easy scoreboard
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.scoreboard.EasyScoreboard
 */
public class AbstractEasyScoreboard implements EasyScoreboard {

    @Getter
    private final Scoreboard scoreboard;

    @Getter
    private final String displayName, name, criteria;

    @Getter
    private final RenderType renderType;

    @Getter
    private final DisplaySlot displaySlot;

    private final LinkedList<Line> lineList;

    private Objective objective;

    /**
     * Creates an AbstractEasyScoreboard
     *
     * @param creation      a creation type
     * @param displayName   a display name
     * @param name          a name
     * @param criteria      a criteria
     * @param renderType    a render type
     * @param displaySlot   a display slot
     */
    AbstractEasyScoreboard(@NonNull ScoreboardCreation creation, @NonNull String displayName, String name, String criteria, RenderType renderType, DisplaySlot displaySlot) {
        this.scoreboard = creation.getBoard();
        this.displayName = displayName;
        this.name = name == null ? displayName : name;
        this.criteria = criteria == null ? "dummy" : criteria;
        this.renderType = renderType == null ? RenderType.INTEGER : renderType;
        this.displaySlot = displaySlot == null ? DisplaySlot.SIDEBAR : displaySlot;
        this.lineList = new LinkedList<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addLine(String entry) {
        this.addLine(entry, entry, InsertionRule.LAST);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addLine(@Nullable String entry, @Nullable String key, @Nullable InsertionRule rule) {
        entry = entry == null ? "null" : entry;
        key = key == null ? entry : key;
        rule = rule == null ? InsertionRule.FIRST : rule;

        this.checkDuplicatedEntry(entry);
        this.checkDuplicatedKey(key);

        Line line = new Line(entry, key);

        switch (rule) {
            case FIRST:
                this.lineList.addFirst(line);
                break;
            case LAST:
                this.lineList.addLast(line);
                break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void create(boolean force) {
        if (force) {
            this.remove();
            this.clearSlot();
        } else {
            if (this.objective != null)
                return;
        }

        this.objective = this.scoreboard.registerNewObjective(this.name, this.criteria, this.displayName, this.renderType);
        this.objective.setDisplaySlot(this.displaySlot);

        int counter = 0;
        for (Line line : this.lineList) {
            Score score = this.objective.getScore(line.getEntry());
            score.setScore(counter);
            counter++;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
        this.create(true);
    }

    @Override
    public void addPlayerIterable(Iterable<Player> playerIterable) {
        playerIterable.forEach(player -> player.setScoreboard(this.scoreboard));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPlayers(Player... players) {
        for (Player player : players) {
            player.setScoreboard(this.scoreboard);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDisplayName(@NonNull String displayName) {
        if (this.objective == null)
            return;

       this.objective.setDisplayName(displayName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDisplaySlot(@NonNull DisplaySlot slot) {
        if (this.objective == null)
            return;

        this.objective.setDisplaySlot(slot);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setRenderType(@NonNull RenderType type) {
        if (this.objective == null)
            return;

        this.objective.setRenderType(type);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove() {
        if (this.objective == null)
            return;

        this.objective.unregister();
        this.objective = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Line getLine(String key) {
        return FinderUtil.find(this.lineList, line -> line.getKey().equals(key));
    }

    /**
     * Clears the board slot
     */
    private void clearSlot() {
        Objective objective = this.scoreboard.getObjective(this.displaySlot);
        if (objective != null)
            objective.unregister();
    }

    /**
     * Checks for a duplicated entry
     *
     * @param entry     an entry
     */
    private void checkDuplicatedEntry(@NonNull String entry) {
        for (Line line : this.lineList) {
            if (line.getEntry().equals(entry))
                throw new LineInsertionException("The entry {} already exists", entry);
        }
    }

    /**
     * Checks for a duplicated key
     *
     * @param key       a key
     */
    private void checkDuplicatedKey(@NonNull String key) {
        if (this.getLine(key) != null)
            throw new LineInsertionException("The key {} already exists");
    }

}
