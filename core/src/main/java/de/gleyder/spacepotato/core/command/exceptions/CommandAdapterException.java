package de.gleyder.spacepotato.core.command.exceptions;

import de.gleyder.spacepotato.core.utils.exceptions.BaseRuntimeException;

/**
 * Thrown if an errors occurs in the command adapter.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see BaseRuntimeException
 */
public class CommandAdapterException extends BaseRuntimeException {

    /**
     * Creates a CommandAdapterException
     *
     * @param message       a message
     * @param objects       an array of objects
     */
    public CommandAdapterException(String message, Object... objects) {
        super(message, objects);
    }

}
