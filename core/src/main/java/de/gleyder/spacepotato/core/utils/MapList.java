package de.gleyder.spacepotato.core.utils;

import java.util.HashMap;
import java.util.Set;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class MapList<I, K, V> {

    private final HashMap<I, HashMap<K, V>> map;

    public MapList() {
        this.map = new HashMap<>();
    }

    public void create(I identifier) {
        this.map.put(identifier, new HashMap<>());
    }

    public void add(I identifier, K key, V value) {
        var hashMap = this.getHashMap(identifier);
        hashMap.put(key, value);
    }

    public void remove(I identifier, K key) {
        var hashMap = this.getHashMap(identifier);
        hashMap.remove(key);

        if (hashMap.isEmpty())
            this.map.remove(identifier);
    }

    public HashMap<K, V> getMap(I identifier) {
        return this.getHashMap(identifier);
    }

    public Set<I> getIdentifier() {
        return this.map.keySet();
    }

    public void clear(I identifier) {
        this.map.remove(identifier);
    }

    private HashMap<K, V> getHashMap(I identifier) {
        return this.map.computeIfAbsent(identifier, ignored -> new HashMap<>());
    }

}
