package de.gleyder.spacepotato.core.utils.string;

import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.Getter;
import lombok.NonNull;

import java.util.LinkedList;
import java.util.regex.Matcher;

/**
 * Replaces markers with values.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.interfaces.IBuilder
 */
@SuppressWarnings({"UnusedReturnValue"})
public class SuppliedString implements IBuilder<String> {

    @Getter
    protected String rootString;

    @Getter
    protected char prefix, suffix;
    protected LinkedList<String> values;

    /**
     * Creates a SuppliedString
     *
     * @param rootString    the root string
     */
    public SuppliedString(@NonNull String rootString) {
        this.values = new LinkedList<>();
        this.rootString = rootString;
        this.prefix = '{';
        this.suffix = '}';
    }

    /**
     * Adds a value
     *
     * @param value     a string
     * @return          itself
     */
    public SuppliedString addValue(Object value) {
        if (value == null)
            value = "null";

        this.values.add(value.toString());
        return this;
    }

    public SuppliedString addValues(Object... objects) {
        return this.addMultipleValues(objects);
    }

    public SuppliedString addMultipleValues(Object... values) {
        for (Object value : values) {
            this.addValue(value);
        }
        return this;
    }

    /**
     * Sets a value
     *
     * @param index     the index
     * @param value     the value
     * @return          itself
     */
    public SuppliedString setValue(int index, @NonNull Object value) {
        this.values.set(index, value.toString());
        return this;
    }

    public SuppliedString setPrefix(char prefix) {
        this.prefix = prefix;
        return this;
    }

    public SuppliedString setSuffix(char suffix) {
        this.suffix = suffix;
        return this;
    }

    /**
     * Builds the string
     *
     * @return  a string
     */
    @Override
    public String build() {
        /*
         * Loops through the root string.
         * When it finds a match it replaces it with the value from the index
         */
        int index = 0;
        for (int i = 0; i < this.rootString.toCharArray().length-1; i++) {
            char now = this.rootString.toCharArray()[i];
            char next = this.rootString.toCharArray()[i+1];

            if (now == this.prefix && next == this.suffix) {
                if (index < this.values.size()) {
                    this.rootString = this.rootString.replaceFirst(this.getPattern(), Matcher.quoteReplacement(this.values.get(index)));
                    index++;
                }
            }
        }
        return this.rootString;
    }

    /**
     * Returns the pattern
     *
     * @return      a string
     */
    private String getPattern() {
        return "\\" + this.prefix + "\\" + this.suffix;
    }

    @Override
    public String toString() {
        return this.build();
    }

}
