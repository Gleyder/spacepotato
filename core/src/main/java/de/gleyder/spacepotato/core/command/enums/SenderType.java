package de.gleyder.spacepotato.core.command.enums;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Enum for sender types.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum SenderType {

    /**
     * Indicates that the sender is player
     */
    PLAYER,

    /**
     * Indicates that the sender is a command block
     */
    COMMAND_BLOCK,

    /**
     * Indicates that the sender is a console
     */
    CONSOLE,

    /**
     * Indicates that the sender is not defined by a type
     */
    UNDEFINED;

    /**
     * Tries to get the type of a sender object
     *
     * @param sender        a sender
     * @return              a type
     */
    public static SenderType of(CommandSender sender) {
        if (sender instanceof Player)
            return PLAYER;
        else if (sender instanceof BlockCommandSender)
            return COMMAND_BLOCK;
        else if (sender instanceof ConsoleCommandSender)
            return CONSOLE;
        else
            return UNDEFINED;
    }

    /**
     * Returns the default type
     *
     * @return      a type
     */
    public static SenderType getDefault() {
        return PLAYER;
    }

}
