package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;

/**
 * Translates an argument into a short.
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class ShortInterpreter implements Interpreter<Short> {

    /**
     * Translates an argument into a short
     *
     * @param argument                          an argument
     * @return                                  a short
     * @throws InterpreterException     if the argument cannot be translated
     */
    @Override
    public Short translate(String argument) throws InterpreterException {
        try {
            return Short.valueOf(argument);
        } catch (NumberFormatException e) {
            throw new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_SHORT.getKeyableMessage(true)
                    .replace("arg", argument)
                    .replace("min", Short.MIN_VALUE)
                    .replace("max", Short.MAX_VALUE)
                    .build()
            );
        }
    }

    @Override
    public String getTabDescription() {
        return TabCompletionsHelper.getAsSingleTypeString("number");
    }


    @Override
    public Class<Short> getClassType() {
        return Short.class;
    }

}
