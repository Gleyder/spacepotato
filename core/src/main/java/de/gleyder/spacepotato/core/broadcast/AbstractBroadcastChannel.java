package de.gleyder.spacepotato.core.broadcast;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * Abstract implementation of BroadcastChannel
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 * @see de.gleyder.spacepotato.core.broadcast.BroadcastChannel
 */
public abstract class AbstractBroadcastChannel implements BroadcastChannel {

    /**
     * Returns a collection of players. Used to broadcast
     *
     * @return      a collection of players
     */
    public abstract Collection<Player> getPlayers();

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessage(@NotNull String message) {
        this.getPlayers().forEach(player -> player.sendMessage(message));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void playSound(@NotNull Sound sound) {
        this.playSound(sound, SoundCategory.MASTER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void playSound(@NotNull Location location, @NotNull Sound sound, @NotNull SoundCategory category, float volume, float pitch) {
        if (volume < 0)
            throw new NumberFormatException("Volume cannot be negative but is " + volume);
        if (pitch < 0)
            throw new NumberFormatException("Pitch cannot be negative but is " + pitch);

        this.getPlayers().forEach(player -> player.playSound(location, sound, category, volume, pitch));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void playSound(@NotNull Sound sound, @NotNull SoundCategory category, float volume, float pitch) {
        getPlayers().forEach(player -> player.playSound(player.getLocation(), sound, category, volume, pitch));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void playSound(@NotNull Sound sound, @NotNull SoundCategory category) {
        this.playSound(sound, category, 1, 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(@NotNull Player player) {
        return this.getPlayers().contains(player);
    }

}
