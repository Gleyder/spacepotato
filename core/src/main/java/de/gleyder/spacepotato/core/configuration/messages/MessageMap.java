package de.gleyder.spacepotato.core.configuration.messages;

import de.gleyder.spacepotato.core.utils.string.KeyReplacer;
import de.gleyder.spacepotato.core.utils.string.SuppliedString;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class MessageMap {

    private final HashMap<String, MessageFile> fileMap;
    private final String defaultLang;

    @SuppressWarnings("WeakerAccess")
    public MessageMap(@NotNull HashMap<String, MessageFile> fileMap, @NotNull String defaultLang) {
        this.fileMap = fileMap;
        this.defaultLang = defaultLang;

        if (this.fileMap.get(this.defaultLang) == null)
            throw new NullPointerException("Default lang \"" + this.defaultLang + "\" is not present");
    }

    private MessageFile getFile(String lang) {
        MessageFile messageFile = this.fileMap.get(lang);
        if (lang == null || messageFile == null)
            return this.fileMap.get(this.defaultLang);
        return messageFile;
    }

    public String getSuppliedMessage(String lang, @NotNull String path, Object... objects) {
        return new SuppliedString(this.getMessage(lang, path)).addMultipleValues(objects).build();
    }

    public KeyReplacer getKeyReplacedMessage(String lang, @NotNull String path) {
        return new KeyReplacer(this.getMessage(lang, path));
    }

    public String getMessage(@NotNull String path) {
        return this.getMessage(null, path);
    }

    public String getMessage(String lang, @NotNull String path) {
        return this.getFile(lang).getMessage(path);
    }

}
