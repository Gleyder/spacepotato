package de.gleyder.spacepotato.core.builder.command;

import de.gleyder.spacepotato.core.command.entities.MutableArgument;
import de.gleyder.spacepotato.core.command.enums.InterpreterMode;
import de.gleyder.spacepotato.core.command.impl.MutableArgumentImpl;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.interfaces.Option;
import de.gleyder.spacepotato.core.command.interpreters.Interpreters;

import java.util.function.Function;

/**
 * Builder for MutableArguments.
 * The same instance can be reused after calling build().
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.builder.command.AbstractArgumentBuilder
 */
public class MutableArgumentBuilder extends AbstractArgumentBuilder<MutableArgument, MutableArgumentBuilder> {

    public final static MutableArgument STRING;

    static {
        STRING = new MutableArgumentBuilder().build();
    }

    private Interpreter<Object> interpreter;
    private Function<Object, Object> converter;
    private Option<Object> option;
    private InterpreterMode mode;
    private boolean multiArgumentsAllowed, endless;
    private String key, completion;

    @SuppressWarnings("unchecked")
    public <I, O> MutableArgumentBuilder setConverter(Function<I, O> converter) {
        this.converter = (Function<Object, Object>) converter;
        return this;
    }

    /**
     * Sets an interpreter
     *
     * @param interpreters      an interpreter
     * @return                  this builder
     */
    public <T> MutableArgumentBuilder setInterpreter(Interpreters interpreters) {
        this.interpreter = interpreters.getInterpreter();
        return this;
    }
    /**
     * Sets an interpreter
     *
     * @param interpreter      an interpreter
     * @return                  this builder
     */
    @SuppressWarnings("unchecked")
    public <T> MutableArgumentBuilder setInterpreter(Interpreter<T> interpreter) {
        this.interpreter = (Interpreter<Object>) interpreter;
        return this;
    }

    /**
     * Sets an interpreter
     *
     * @param option        an option
     * @return              this builder
     */
    @SuppressWarnings("unchecked")
    public <T> MutableArgumentBuilder setOption(Option<T> option) {
        this.option = (Option<Object>) option;
        return this;
    }

    /**
     * Sets the key
     *
     * @param key       a key
     * @return          this builder
     */
    public MutableArgumentBuilder setKey(String key) {
        this.key = key;
        return this;
    }

    /**
     * Sets the argument to endless
     *
     * @return      this builder
     */
    public MutableArgumentBuilder setEndless() {
        this.endless = true;
        return this;
    }

    /**
     * Sets the argument to multi arguments are allowed
     *
     * @return      this build
     */
    public MutableArgumentBuilder setMultiArgumentsAllowed() {
        this.multiArgumentsAllowed = true;
        return this;
    }

    /**
     * Sets the interpreter mode
     *
     * @param mode      the mode
     * @return          this builder
     */
    public MutableArgumentBuilder setInterpreterMode(InterpreterMode mode) {
        this.mode = mode;
        return this;
    }

    /**
     * Sets the tab completion
     *
     * @param completion        a completion
     * @return                  this builder
     */
    public MutableArgumentBuilder setTabCompletion(String completion) {
        this.completion = completion;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
        super.reset();
        this.interpreter = null;
        this.option = null;
        this.mode = null;
        this.multiArgumentsAllowed = false;
        this.endless = false;
        this.key = null;
        this.completion = null;
        this.converter = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MutableArgument build() {
        MutableArgumentImpl mutableArgument = new MutableArgumentImpl(
                this.description,
                this.completion,
                this.interpreter,
                this.option,
                this.converter,
                this.mode,
                this.key,
                this.endless,
                this.multiArgumentsAllowed
        );
        this.reset();
        return mutableArgument;
    }

}
