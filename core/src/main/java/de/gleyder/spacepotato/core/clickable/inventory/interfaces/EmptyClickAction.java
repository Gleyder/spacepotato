package de.gleyder.spacepotato.core.clickable.inventory.interfaces;

import lombok.AllArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 */
@AllArgsConstructor
public class EmptyClickAction implements InventoryClickAction {

    private final boolean cancel;

    @Override
    public boolean onClick(@NotNull Player player, @NotNull Inventory inventory, @NotNull ItemStack item, @NotNull ClickType clickType) {
        return this.cancel;
    }

}
