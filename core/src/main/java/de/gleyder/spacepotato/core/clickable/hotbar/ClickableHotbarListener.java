package de.gleyder.spacepotato.core.clickable.hotbar;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

/**
 * Listener for ClickableHotbars.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ClickableHotbarListener<K> implements Listener {

    private final ClickableHotbarRegistry<K> registry;

    ClickableHotbarListener(ClickableHotbarRegistry<K> registry) {
        this.registry = registry;
    }

    /**
     * Calls the ClickableHotbarActions.
     *
     * @param event     the event
     */
    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getHand() != EquipmentSlot.HAND)
            return;
        Player player = event.getPlayer();

        this.registry.getMap().forEach((k, hotbar) -> {
            if (!hotbar.getPredicate().test(player))
                return;

            hotbar.getActions().forEach((obj, action) -> {
                if (obj instanceof ItemStack) {
                    ItemStack itemStack = (ItemStack) obj;
                    if (!itemStack.equals(event.getItem()))
                        return;
                } else {
                    Material material = (Material) obj;
                    if (event.getMaterial() != material)
                        return;
                }


                boolean cancelEvent = action.onClick(player, event.getClickedBlock(), event.getAction());
                event.setCancelled(cancelEvent);
            });
        });
    }

}
