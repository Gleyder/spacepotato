package de.gleyder.spacepotato.core.builder.command;

import de.gleyder.spacepotato.core.analyzer.QueryType;
import de.gleyder.spacepotato.core.command.conditions.PermissionCondition;
import de.gleyder.spacepotato.core.command.entities.Argument;
import de.gleyder.spacepotato.core.command.entities.StaticArgument;
import de.gleyder.spacepotato.core.command.enums.ArgumentType;
import de.gleyder.spacepotato.core.command.enums.SenderType;
import de.gleyder.spacepotato.core.command.impl.ArgumentImpl;
import de.gleyder.spacepotato.core.command.impl.ConditionContainerImpl;
import de.gleyder.spacepotato.core.command.interfaces.Condition;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.interpreters.Interpreters;
import lombok.NonNull;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Base class for command builders.
 * The same instance can be reused after calling build().
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class AbstractCommandPartBuilder<I, B> {

    private final static String DEFAULT_METHOD_NAME;

    static {
        DEFAULT_METHOD_NAME = "$native";
    }

    final List<ArgumentImpl> argumentList;
    ConditionContainerImpl conditionContainer;
    String methodName;
    SenderType senderType;
    Function<CommandSender, Object> senderConverter;

    /**
     * Creates an AbstractCommandPartBuilder
     */
    AbstractCommandPartBuilder() {
        this.argumentList = new ArrayList<>();
        this.conditionContainer = new ConditionContainerImpl(null);
    }

    /**
     * Adds an argument
     *
     * @param builder       a builder
     * @return              this builder
     */
    public B addArgument(@NonNull AbstractArgumentBuilder<? extends Argument, ? extends AbstractArgumentBuilder> builder) {
        this.addArgument(builder.build());
        return this.getThis();
    }

    public B setSenderConverter(Function<CommandSender, Object> function) {
        this.senderConverter = function;
        return this.getThis();
    }

    public B setConditionType(QueryType queryType) {
        this.conditionContainer.setQueryType(queryType);
        return this.getThis();
    }

    public B addCondition(Condition condition) {
        this.conditionContainer.getConditionList().add(condition);
        return this.getThis();
    }

    public B setPermission(@NotNull String permission) {
        this.addCondition(new PermissionCondition(permission));
        return this.getThis();
    }

    /**
     * Adds an argument
     *
     * @param label       a label
     * @return            this builder
     */
    public B addArgument(@NonNull String label) {
        this.addArgument(new StaticArgumentBuilder().setLabel(label).build());
        return this.getThis();
    }

    public <T> B addArgument(@NonNull Interpreter<T> interpreter) {
        this.addArgument(new MutableArgumentBuilder().setInterpreter(interpreter));
        return this.getThis();
    }

    public B addArgument(@NonNull Interpreters interpreters) {
        this.addArgument(new MutableArgumentBuilder().setInterpreter(interpreters));
        return this.getThis();
    }

    public <T> B addArguments(Iterable<Interpreter> iterable) {
        iterable.forEach(this::addArgument);
        return this.getThis();
    }

    /**
     * Adds an argument
     *
     * @param argument       a argument
     * @return              this builder
     */
    @SuppressWarnings({"UnusedReturnValue"})
    public B addArgument(@NonNull Argument argument) {
        this.argumentList.add((ArgumentImpl) argument);
        return this.getThis();
    }

    /**
     * Sets the executor method name
     *
     * @param name      a name
     * @return          this builder
     */
    public B setMethodName(@NonNull String name) {
        this.methodName = name;
        return this.getThis();
    }

    /**
     * Sets the sender type
     *
     * @param senderType        a sender type
     * @return                  this builder
     */
    public B setSenderType(@NonNull SenderType senderType) {
        this.senderType = senderType;
        return this.getThis();
    }

    /**
     * Resets the attributes to it's default values
     */
    void reset() {
        this.methodName = null;
        this.argumentList.clear();
        this.conditionContainer = new ConditionContainerImpl(null);
        this.senderType = null;
        this.senderConverter = null;
    }

    void tryAutoSetMethodName() {
        if (this.methodName != null)
            return;

        if (this instanceof SubCommandCategoryBuilder)
            return;

        if ( (this.argumentList.isEmpty() || this.onlyMutable())) {
            this.methodName = DEFAULT_METHOD_NAME;
            return;
        }

        Argument argument = this.argumentList.get(0);
        if (argument.getType() != ArgumentType.STATIC)
            return;

        this.methodName = ((StaticArgument) argument).getLabel();
    }

    private boolean onlyMutable() {
        for (ArgumentImpl argument : this.argumentList) {
            if (argument.getType() == ArgumentType.STATIC)
                return false;
        }
        return true;
    }

    /**
     * Builds the argument
     *
     * @return      an argument
     */
    public abstract I build();

    /**
     * Returns itself
     *
     * @return      this builder
     */
    @SuppressWarnings("unchecked")
    private B getThis() {
        return (B) this;
    }

}
