package de.gleyder.spacepotato.core.command.entities;

import de.gleyder.spacepotato.core.command.enums.InterpreterMode;
import de.gleyder.spacepotato.core.command.interfaces.Describable;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.interfaces.Option;

import java.util.function.Function;

/**
 * Represents a mutable argument.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.command.entities.Argument
 * @see de.gleyder.spacepotato.core.command.interfaces.Describable
 */
public interface MutableArgument extends Argument, Describable {

    Interpreter<Object> getInterpreter();
    Function<Object, Object> getConverter();
    Option<Object> getOption();
    InterpreterMode getMode();
    boolean areMultiArgumentsAllowed();
    boolean isEndless();
    String getKey();
    String getCompletion();

}
