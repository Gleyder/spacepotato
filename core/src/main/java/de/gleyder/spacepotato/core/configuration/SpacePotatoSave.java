package de.gleyder.spacepotato.core.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.gleyder.spacepotato.core.configuration.typeadapter.ItemStackDeserializer;
import de.gleyder.spacepotato.core.configuration.typeadapter.ItemStackSerializer;
import de.gleyder.spacepotato.core.configuration.typeadapter.LocationDeserializer;
import de.gleyder.spacepotato.core.configuration.typeadapter.LocationSerializer;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Saves an object in file, must be serializable by Gson.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SpacePotatoSave<T> {

    private final static Gson GSON;

    static {
        GSON = new GsonBuilder()
                .setLenient()
                .setPrettyPrinting()
                .registerTypeAdapter(ItemStack.class, new ItemStackSerializer())
                .registerTypeAdapter(ItemStack.class, new ItemStackDeserializer())
                .registerTypeAdapter(Location.class, new LocationSerializer())
                .registerTypeAdapter(Location.class, new LocationDeserializer())
                .create();
    }

    @Getter
    private final String filename;

    private final Class<T> tClass;

    private T cached;

    /**
     * Creates a SpacePotatoSave
     *
     * @param plugin        a plugin
     * @param filename      a filename without extension
     * @param tClass        the object's class
     */
    public SpacePotatoSave(@NotNull JavaPlugin plugin, @NotNull String filename, @NotNull Class<T> tClass) {
        ConfigHelper configHelper = new ConfigHelper(plugin);
        this.filename = configHelper.getDataFolderPath(ConfigHelper.withExtension(filename, ConfigHelper.JSON_EXTENSION));
        this.tClass = tClass;
    }

    /**
     * Loads the object
     *
     * @return      an object
     */
    public T load() {
        return this.load(false);
    }

    /**
     * Loads the object
     *
     * @param forceLoad     if false the object may not loaded from the file
     *                      but instead from cache
     * @return              an object
     */
    public T load(boolean forceLoad) {
        if (!forceLoad && this.cached != null)
            return this.cached;

        try {
            FileReader fileReader = new FileReader(this.filename);

            T object = GSON.fromJson(fileReader, this.tClass);
            fileReader.close();
            this.cached = object;
            return object;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Stores the object
     *
     * @param object        an object
     */
    public void store(T object) {
        String json = GSON.toJson(object);
        try {
            FileWriter fileWriter = new FileWriter(this.filename);
            fileWriter.write(json);
            this.cached = object;
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
