package de.gleyder.spacepotato.core.player;

import de.gleyder.spacepotato.core.utils.MaterialUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class PlayerModifier {

    @Getter
    private final Player player;

    public static void formatPlayer(@NotNull Player player) {
        new PlayerModifier(player).formatPlayer();
    }

    public void formatPlayer() {
        this.player.setHealthScale(20D);
        this.heal();
        this.clearInventory();
        this.clearPassengers();
        this.clearPotionEffects();
    }

    public void clearPassengers() {
        this.player.eject();
    }

    public void clearPotionEffects() {
        this.player.getActivePotionEffects().forEach(potionEffect -> this.player.removePotionEffect(potionEffect.getType()));
    }

    /**
     * Sets the player's health but checks if it exceed the health scale
     */
    public void safeSetHealth(double health) {
        if (health > this.player.getHealthScale())
            this.heal();
        else
            this.player.setHealth(health);
    }

    public void safeHeal(double amount) {
        this.safeSetHealth(this.player.getHealth() + amount);
    }

    /**
     * Heals the player completely
     */
    public void heal() {
        this.player.setHealth(this.player.getHealthScale());
    }

    /**
     * Clears the player's inventory including the off hand
     */
    public void clearInventory() {
        PlayerInventory inventory = this.player.getInventory();
        ItemStack[] content = inventory.getContents();
        for (int i = 0; i < content.length; i++) {
            ItemStack itemStack = content[i];
            if (!MaterialUtils.isAir(itemStack))
                inventory.setItem(i, new ItemStack(Material.AIR));
        }
        inventory.setItemInOffHand(new ItemStack(Material.AIR));
        inventory.setArmorContents(null);
    }

}