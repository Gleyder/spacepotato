package de.gleyder.spacepotato.core.command.logic;

import de.gleyder.spacepotato.core.command.exceptions.CommandRegistryException;
import de.gleyder.spacepotato.core.command.impl.ExecutorImpl;
import de.gleyder.spacepotato.core.command.impl.SubCommandCategoryImpl;
import de.gleyder.spacepotato.core.command.impl.SubCommandImpl;
import de.gleyder.spacepotato.core.utils.string.ListSuppliedString;
import de.gleyder.spacepotato.core.utils.string.SuppliedString;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;


/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class CommandRegistry {

    private final JavaPlugin javaPlugin;

    public CommandRegistry(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
    }

    public void register(@NonNull CommandAdapter adapter) {
        Class<?> clazz = adapter.getClass();
        if (adapter.getLabel() == null || adapter.getLabel().isEmpty())
            throw new CommandRegistryException("No command label was defined for the command class {}", clazz.getSimpleName());

        adapter.define();

        if (adapter.getSubCommandList().size() == 0)
            throw new CommandRegistryException("Command {} has no arguments. You cannot register a command without arguments", clazz.getSimpleName());

        for (SubCommandImpl subCommand : adapter.getSubCommandsImpl()) {
            subCommand.checkValidArguments();
            subCommand.getExecutor().setCommandAdapter(adapter);


            for (SubCommandCategoryImpl category : subCommand.getCategoryTreeSet()) {
                ExecutorImpl executor = category.getExecutor();

                if (!executor.getName().isEmpty()) {
                    executor.setCommandAdapter(adapter);
                    executor.searchForMethod(clazz);
                }
            }

            subCommand.getExecutor().searchForMethod(clazz);

            subCommand.applyCategories();
        }
        adapter.generate();

        CommandProcessor commandProcessor = new CommandProcessor(adapter);
        Bukkit.getServer().getCommandMap().register(adapter.getLabel(), "fallback", commandProcessor);

        ListSuppliedString<SubCommandImpl> listSuppliedString = new ListSuppliedString<>(adapter.getSubCommandsImpl(), subCommand -> {
            if (subCommand.getArguments().size() == 0)
                return "No SubCommands";

            SuppliedString suppliedString = new SuppliedString("SubCommand: {}");
            suppliedString.addValue(subCommand.visual());
            return suppliedString.build();
        });
        listSuppliedString.setHeader("Registered Command " + clazz.getSimpleName() + "(/" + adapter.getLabel() + ")");
        this.javaPlugin.getLogger().info(listSuppliedString.build());
    }

}
