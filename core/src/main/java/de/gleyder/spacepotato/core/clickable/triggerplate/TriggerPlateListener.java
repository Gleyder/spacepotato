package de.gleyder.spacepotato.core.clickable.triggerplate;

import lombok.AllArgsConstructor;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Listener for trigger plates.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class TriggerPlateListener implements Listener {

    private final TriggerPlateRegistry registry;

    @EventHandler
    public void onTrigger(PlayerInteractEvent event) {
        if (event.getAction() != Action.PHYSICAL)
            return;

        Block block = event.getClickedBlock();
        this.registry.getAll().forEach(triggerPlate -> {
            if (!triggerPlate.getBlock().equals(block))
                return;

            boolean cancel = triggerPlate.getAction().onTrigger(event.getPlayer(), TriggerPlateType.fromBlock(block), block);
            event.setCancelled(cancel);
        });
    }

}
