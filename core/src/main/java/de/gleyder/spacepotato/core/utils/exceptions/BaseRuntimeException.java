package de.gleyder.spacepotato.core.utils.exceptions;

import de.gleyder.spacepotato.core.utils.string.SuppliedString;

/**
 * RuntimeException with the supplier strings function used
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class BaseRuntimeException extends RuntimeException {

    /**
     * Creates a BaseRuntimeException
     *
     * @param message       a message
     * @param objects       an array of objects
     */
    public BaseRuntimeException(String message, Object... objects) {
        super(new SuppliedString(message).addMultipleValues(objects).build());
    }

}
