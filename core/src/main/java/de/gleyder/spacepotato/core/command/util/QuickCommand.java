package de.gleyder.spacepotato.core.command.util;

import de.gleyder.spacepotato.core.command.annotations.ExecutorMethod;
import de.gleyder.spacepotato.core.command.logic.CommandAdapter;
import org.bukkit.entity.Player;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class QuickCommand extends CommandAdapter {

    public QuickCommand(String label) {
        super(label);
    }

    public abstract void onCommand(Player player);

    @Override
    public final void define() {
        this.getBuilder()
                .setMethodName("single")
                .buildAndAdd();
    }

    @ExecutorMethod
    public final void single(Player player) {
        this.onCommand(player);
    }

}
