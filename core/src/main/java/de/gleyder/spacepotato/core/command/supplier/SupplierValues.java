package de.gleyder.spacepotato.core.command.supplier;

/**
 * @author Gleyder
 */
public interface SupplierValues {

    <V> void add(String key, V value);
    <V> void add(V value);

}
