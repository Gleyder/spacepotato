package de.gleyder.spacepotato.core.cleanup;

import org.bukkit.Chunk;
import org.bukkit.Difficulty;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.entity.EntityType;

/**
 * Class for the world clean up.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public final class WorldCleanUp {
    
    /**
     * Remove all entities from a world.
     *
     * @param world the world
     * @param types the entity types to be removed from the world
     */
    public static void removeEntities(World world, EntityType... types) {
        world.getEntities().forEach(entity->{
            for (EntityType type : types) {
                if (entity.getType().equals(type)) {
                    Chunk chunk = entity.getLocation().getChunk();
                    boolean loaded = false;
                    if (!chunk.isLoaded())
                        loaded = entity.getLocation().getChunk().load();
                    entity.remove();
                    if (loaded)
                        chunk.unload();
                    
                    break;
                }
            }
        });
    }
    
    /**
     * Setup a world for a server (set gamerules, time, ...)
     *
     * @param world the world
     */
    public static void setupWorld(World world) {
        world.setAutoSave(false);
        world.setTime(1000);
        world.setStorm(false);
        world.setThundering(false);
        world.setDifficulty(Difficulty.EASY);
        world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
        world.setGameRule(GameRule.SPECTATORS_GENERATE_CHUNKS, false);
        world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
        world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
        world.setGameRule(GameRule.DO_MOB_SPAWNING, false);
        world.setGameRule(GameRule.DO_FIRE_TICK, false);
    }
    
}
