package de.gleyder.spacepotato.core.clickable.hotbar;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.registry.AbstractUsesListenerRegistry;

/**
 * Registry for ClickableHotbars.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ClickableHotbarRegistry<K> extends AbstractUsesListenerRegistry<K, ClickableHotbar<K>> {

    public ClickableHotbarRegistry(SpacePotatoRegistry spacePotatoRegistry) {
        super(spacePotatoRegistry);
        this.init(new ClickableHotbarListener<>(this));
    }

    @Override
    public void register(ClickableHotbar<K> hotbar) {
        this.getMap().put(hotbar.getOwner(), hotbar);
    }

}
