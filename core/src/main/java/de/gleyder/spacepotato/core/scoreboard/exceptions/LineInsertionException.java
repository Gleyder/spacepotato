package de.gleyder.spacepotato.core.scoreboard.exceptions;

import de.gleyder.spacepotato.core.utils.exceptions.BaseRuntimeException;

/**
 * @author Gleyder
 */
public class LineInsertionException extends BaseRuntimeException {

    public LineInsertionException(String message, Object... objects) {
        super(message, objects);
    }

}
