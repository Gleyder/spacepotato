package de.gleyder.spacepotato.core.chat;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.broadcast.BroadcastChannel;
import de.gleyder.spacepotato.core.broadcast.ManualBroadcastChannel;
import de.gleyder.spacepotato.core.utils.UsesListener;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ChatManager implements UsesListener {

    private final Map<String, ManualBroadcastChannel> channelHashMap;
    private final SpacePotatoRegistry spacePotatoRegistry;
    private final ChatFormatter chatFormatter;
    private final Logger logger;
    private final Listener listener;

    public ChatManager(SpacePotatoRegistry spacePotatoRegistry, ChatFormatter formatter) {
        this.spacePotatoRegistry = spacePotatoRegistry;
        this.channelHashMap = new ConcurrentHashMap<>();
        this.chatFormatter = formatter == null ? new DefaultFormatter() : formatter;
        this.logger = LoggerFactory.getLogger(this.getClass());
        this.listener = new ChatListener(this);
        this.spacePotatoRegistry.registerListener(this.listener);
    }

    public void addPlayer(@NotNull String id, @NotNull Player player) {
        this.checkChannel(id);
        ManualBroadcastChannel broadcastChannel = this.getChannel(player);
        if (broadcastChannel != null)
            broadcastChannel.removePlayer(player);

        this.getChannel(id).addPlayer(player);
    }

    public void removePlayer(@NonNull Player player) {
        ManualBroadcastChannel channel = this.getChannel(player);
        channel.removePlayer(player);
    }

    public void sendMessage(@NotNull Player player, @NotNull String message) {
        BroadcastChannel broadcastChannel = this.getChannel(player);
        if (broadcastChannel == null) {
            this.logger.warn(player + " is not in any broadcast channel");
            return;
        }

        broadcastChannel.sendMessage(this.chatFormatter.format(player, message));
    }

    public void addChannel(@NotNull String id) {
        this.channelHashMap.put(id, new ManualBroadcastChannel());
    }

    public ManualBroadcastChannel getChannel(@NotNull Player player) {
        for (Map.Entry<String, ManualBroadcastChannel> entry : this.channelHashMap.entrySet()) {
            if (entry.getValue().contains(player))
                return entry.getValue();
        }
        return null;
    }

    public ManualBroadcastChannel getChannel(@NotNull String id) {
        return this.channelHashMap.get(id);
    }

    private void checkChannel(@NotNull String id) {
        if (this.channelHashMap.get(id) == null)
            throw new NullPointerException("Channel \"" + id + "\" does not exists");
    }

    @Override
    public void unregisterListeners() {
        this.spacePotatoRegistry.unregisterListener(this.listener);
    }

    static class DefaultFormatter implements ChatFormatter {

        @Override
        public String format(Player player, String message) {
            return "<" + player.getName() + "> " + message;
        }

    }

}
