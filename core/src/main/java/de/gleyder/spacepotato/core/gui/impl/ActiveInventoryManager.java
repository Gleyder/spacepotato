package de.gleyder.spacepotato.core.gui.impl;

import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventory;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

/**
 * Manages the active clickable inventories for the gui inventory
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ActiveInventoryManager {

    private final HashMap<Player, ClickableInventory> activeInventories;
    private final ClickableInventoryRegistry clickableInventoryRegistry;

    ActiveInventoryManager(@NotNull ClickableInventoryRegistry clickableInventoryRegistry) {
        this.activeInventories = new HashMap<>();
        this.clickableInventoryRegistry = clickableInventoryRegistry;
    }

    public void register(Player player, ClickableInventory clickableInventory) {
        if (this.get(player) != null) {
            this.unregister(player);
        }

        this.activeInventories.put(player, clickableInventory);
        this.clickableInventoryRegistry.register(clickableInventory);
    }

    public void unregister(Player player) {
        ClickableInventory clickableInventory = this.get(player);
        if (clickableInventory == null) {
            return;
        }

        this.clickableInventoryRegistry.unregister(clickableInventory.getInventory());
        this.activeInventories.remove(player);
    }

    public ClickableInventory get(Player player) {
        return this.activeInventories.get(player);
    }

}
