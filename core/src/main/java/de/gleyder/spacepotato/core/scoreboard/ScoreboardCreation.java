package de.gleyder.spacepotato.core.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Scoreboard;

/**
 * Indicates how a new scoreboard is created
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum ScoreboardCreation {

    /**
     * A new board is created
     */
    NEW,

    /**
     * The main board is used
     */
    MAIN;

    /**
     * Gets the scoreboard
     *
     * @return      a scoreboard
     */
    public Scoreboard getBoard() {
        switch (this) {
            case MAIN:
                return Bukkit.getScoreboardManager().getMainScoreboard();
            case NEW:
                return Bukkit.getScoreboardManager().getNewScoreboard();
            default:
                throw new UnsupportedOperationException("Only NEW and MAIN");
        }
    }
}
