package de.gleyder.spacepotato.core.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ArrayPrimitiveConverter {

    public static List<Character> charPrimitiveToClass(char[] chars) {
        List<Character> characterList = new ArrayList<>();
        for (char aChar : chars) {
            characterList.add(aChar);
        }
        return characterList;
    }

}
