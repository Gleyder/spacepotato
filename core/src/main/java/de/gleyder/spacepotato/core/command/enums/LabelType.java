package de.gleyder.spacepotato.core.command.enums;

/**
 * Enum for label types
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum LabelType {

    /**
     * Indicates that the arguments are appended at in front of the existing
     */
    PREFIX,

    /**
     * Indicates that the arguments are appended at the end of the existing
     */
    SUFFIX;


    /**
     * Returns the default type
     *
     * @return      a type
     */
    public static LabelType getDefault() {
        return PREFIX;
    }

}
