package de.gleyder.spacepotato.core.configuration.messages;

import de.gleyder.spacepotato.core.utils.string.KeyReplacer;
import org.bukkit.entity.Player;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum CoreMessages {

    INVENTORY_CLOSE$BUTTON$LABEL,
    INVENTORY_NEXT$BUTTON$LABEL,
    INVENTORY_PREVIOUS$BUTTON$LABEL,

    INVENTORY_NAVIGABLE_PAGE$NAME,

    COMMAND_INTERPRETER_EXCEPTION_FLOAT,
    COMMAND_INTERPRETER_EXCEPTION_DOUBLE,
    COMMAND_INTERPRETER_EXCEPTION_BOOLEAN,
    COMMAND_INTERPRETER_EXCEPTION_BYTE,
    COMMAND_INTERPRETER_EXCEPTION_ENUM,
    COMMAND_INTERPRETER_EXCEPTION_INTEGER,
    COMMAND_INTERPRETER_EXCEPTION_LONG,
    COMMAND_INTERPRETER_EXCEPTION_SHORT,
    COMMAND_INTERPRETER_EXCEPTION_MATERIAL,
    COMMAND_INTERPRETER_EXCEPTION_PLAYER,
    COMMAND_INTERPRETER_EXCEPTION_ENCHANTMENT,
    COMMAND_INTERPRETER_EXCEPTION_WORLD,
    COMMAND_INTERPRETER_RETURNS$NULL,

    COMMAND_INTERPRETER_DESCRIPTION_STRING,

    COMMAND_INTERPRETER_MULTI$ARGUMENT_SYNTAX,

    COMMAND_BASE_NOT$EXISTING,
    COMMAND_BASE_NO$PERM,
    COMMAND_BASE_SENDER$NOT$SUPPORTED,
    COMMAND_BASE_NO$DESCRIPTION,

    COMMAND_ARGUMENT$SUPPLIER_MULTI$ARGS$NOT$ALLOWED,

    BLOCK$SELECTOR_BLOCK$SELECTED,
    BLOCK$SELECTOR_ALREADY$ADDED,
    ;

    static de.gleyder.spacepotato.core.configuration.messages.MessageMap MESSAGE_MAP;
    static String PREFIX;

    String path;

    public void sendMessage(Player player, boolean prefix, Object... objects) {
        player.sendMessage(this.getMessage(null, prefix, objects));
    }

    public KeyReplacer getKeyableMessage(boolean prefix, Object... objects) {
        return new KeyReplacer(this.getMessage(null, prefix, objects));
    }

    public String getMessage(String lang, boolean prefix, Object... objects) {
        return (prefix ? PREFIX + " " : "") + MESSAGE_MAP.getSuppliedMessage(lang, this.path, objects);
   }

    public String getMessage(boolean prefix, Object... objects) {
        return this.getMessage(null, prefix, objects);
    }

    @Override
    public String toString() {
        return this.getMessage(null, false);
    }
}
