package de.gleyder.spacepotato.core.command.exceptions;

/**
 * Thrown if an errors occurs in an interpreter.
 * Used to define when a translation failed.
 * The message is sent to the sender.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class InterpreterException extends Exception {

    /**
     * Creates a ExecutorException
     */
    public InterpreterException(String message) {
        super(message);
    }

}
