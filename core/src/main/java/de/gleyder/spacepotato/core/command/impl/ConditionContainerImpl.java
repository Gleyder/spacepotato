package de.gleyder.spacepotato.core.command.impl;

import de.gleyder.spacepotato.core.analyzer.QueryResult;
import de.gleyder.spacepotato.core.analyzer.QueryType;
import de.gleyder.spacepotato.core.command.entities.ConditionContainer;
import de.gleyder.spacepotato.core.command.interfaces.Condition;
import lombok.Getter;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ConditionContainerImpl implements ConditionContainer {

    @Getter
    private final Set<Condition> conditionList;

    @Getter
    private QueryType queryType;

    public ConditionContainerImpl(QueryType queryType) {
        this.conditionList = new HashSet<>();
        this.queryType = queryType == null ? QueryType.ALL : queryType;
    }

    public ConditionContainer.Result test(CommandSender sender) {
        List<QueryResult<Condition>> queryResults = new ArrayList<>();
        for (Condition condition : this.conditionList) {
            //noinspection unchecked
            QueryResult<Condition> queryResult = new QueryResult<>(condition, condition.test(sender));
            queryResults.add(queryResult);
        }

        return new ConditionContainer.Result(this.queryType.test(queryResults), queryResults);
    }

    @Override
    public List<Condition> getConditions() {
        return new ArrayList<>(this.conditionList);
    }

    @Override
    public QueryType getType() {
        return this.queryType;
    }

    public void setQueryType(QueryType queryType) {
        this.queryType = queryType == null ? QueryType.ALL : queryType;
    }

}
