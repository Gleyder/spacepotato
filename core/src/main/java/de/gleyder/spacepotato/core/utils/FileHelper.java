package de.gleyder.spacepotato.core.utils;

import de.gleyder.spacepotato.core.configuration.ConfigHelper;
import org.apache.commons.io.FileUtils;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@Deprecated
public class FileHelper {

    public static void copyFile(String loadout, String datafolder, JavaPlugin plugin) {
        try {
            FileUtils.copyToFile(getLoadout(plugin, loadout + ConfigHelper.YAML_EXTENSION), new File(plugin.getDataFolder() + "/" + datafolder + ConfigHelper.YAML_EXTENSION));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getTempPath(JavaPlugin plugin) {
        return plugin.getDataFolder() + "/tmp";
    }

    /**
     * Return an input stream of the given resource name in the loadout folder
     *
     * @param plugin        a plugin
     * @param filename      a path to the file with file extension
     * @return
     */
    public static InputStream getLoadout(JavaPlugin plugin, String filename) {
        return plugin.getResource("loadout/" + filename);
    }

    /**
     * Exports
     *
     * @param plugin
     * @param resourceName
     * @param output
     * @throws Exception
     */
    public static void exportResource(JavaPlugin plugin, String resourceName, String output) throws Exception {
        InputStream stream = null;
        OutputStream resStreamOut = null;
        try {
            stream = plugin.getResource(resourceName);//note that each / is a directory down in the "jar tree" been the jar the root of the tree
            if(stream == null) {
                plugin.getLogger().info("No Loadout found for file " + resourceName);
                return;
            }

            int readBytes;
            byte[] buffer = new byte[4096];
            resStreamOut = new FileOutputStream(output);
            while ((readBytes = stream.read(buffer)) > 0) {
                resStreamOut.write(buffer, 0, readBytes);
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (stream != null)
                stream.close();
            if (resStreamOut != null)
                resStreamOut.close();
        }
    }

}
