package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;

/**
 * Translates an argument into an integer.
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class IntegerInterpreter implements Interpreter<Integer> {

    /**
     * Translates an argument into an integer
     *
     * @param argument                          an argument
     * @return                                  an integer
     * @throws InterpreterException     if the argument cannot be translated
     */
    @Override
    public Integer translate(String argument) throws InterpreterException {
        try {
            return Integer.valueOf(argument);
        } catch (NumberFormatException e) {
            throw new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_INTEGER.getKeyableMessage(true)
                    .replace("argument", argument)
                    .replace("min", Integer.MIN_VALUE)
                    .replace("max", Integer.MAX_VALUE)
                    .build()
            );
        }
    }

    @Override
    public String getTabDescription() {
        return TabCompletionsHelper.getAsSingleTypeString("number");
    }


    @Override
    public Class<Integer> getClassType() {
        return Integer.class;
    }

}
