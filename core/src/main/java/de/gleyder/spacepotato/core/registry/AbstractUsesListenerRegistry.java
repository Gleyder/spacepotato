package de.gleyder.spacepotato.core.registry;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.utils.UsesListener;
import org.bukkit.event.Listener;
import org.jetbrains.annotations.NotNull;

/**
 * Abstract registry for clickable registries.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class AbstractUsesListenerRegistry<K, V> extends AbstractKeyedRegistry<K, V> implements UsesListener {

    private final SpacePotatoRegistry spacePotatoRegistry;

    protected Listener listener;

    /**
     * Creates a AbstractClickableRegistry
     *
     * @param spacePotatoRegistry       a space potato registry
     */
    public AbstractUsesListenerRegistry(@NotNull SpacePotatoRegistry spacePotatoRegistry) {
        this.spacePotatoRegistry = spacePotatoRegistry;
    }

    /**
     * Initializes the listener
     *
     * @param listener      a listener
     */
    protected void init(Listener listener) {
        this.listener = listener;
        this.spacePotatoRegistry.registerListener(listener);
    }

    @Override
    public boolean contains(K k) {
        return this.getMap().containsKey(k);
    }

    /**
     * Unregisters the listener
     */
    @Override
    public void unregisterListeners() {
        this.spacePotatoRegistry.unregisterListener(this.listener);
    }

}
