package de.gleyder.spacepotato.core.clickable.inventory.interfaces;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

/**
 * Class for a simple inventory click action
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class SimpleInventoryClickAction implements InventoryClickAction {

    /**
     * Called when the player makes a left click on an item
     *
     * @param player            a player
     * @param inventory         the player's inventory
     * @param item              the clicked item
     */
    public void onLeftClick(Player player, Inventory inventory, ItemStack item) {}

    /**
     * Called when the player makes a right click on an item
     *
     * @param player            a player
     * @param inventory         the player's inventory
     * @param item              the clicked item
     */
    public void onRightClick(Player player, Inventory inventory, ItemStack item) {}

    /**
     * Called when the player makes a shift left click on an item
     *
     * @param player            a player
     * @param inventory         the player's inventory
     * @param item              the clicked item
     */
    public void onLeftShiftClick(Player player, Inventory inventory, ItemStack item) {}

    /**
     * Called when the player makes a shift right click on an item
     *
     * @param player            a player
     * @param inventory         the player's inventory
     * @param item              the clicked item
     */
    public void onRightShiftClick(Player player, Inventory inventory, ItemStack item) {}

    /**
     * Called when the player makes a middle click (mouse wheel) click on an item
     *
     * @param player            a player
     * @param inventory         the player's inventory
     * @param item              the clicked item
     */
    public void onMiddleClick(Player player, Inventory inventory, ItemStack item) {}

    /**
     * Called of a player clicks an item in an inventory
     *
     * @param player            a Player
     * @param inventory         an Inventory
     * @param item              an ItemStack
     * @param clickType         a ClickType
     * @return                  if the event should be cancelled
     */
    @Override
    public final boolean onClick(@NotNull Player player, @NotNull Inventory inventory, @NotNull ItemStack item, @NotNull ClickType clickType) {
        switch (clickType) {
            case LEFT:
                this.onLeftClick(player, inventory, item);
                break;
            case RIGHT:
                this.onRightClick(player, inventory, item);
                break;
            case SHIFT_LEFT:
                this.onLeftShiftClick(player, inventory, item);
                break;
            case SHIFT_RIGHT:
                this.onRightShiftClick(player, inventory, item);
                break;
            case MIDDLE:
                this.onMiddleClick(player, inventory, item);
                break;
        }
        return true;
    }

}
