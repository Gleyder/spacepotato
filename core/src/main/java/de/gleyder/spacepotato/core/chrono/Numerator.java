package de.gleyder.spacepotato.core.chrono;

import de.gleyder.spacepotato.core.chrono.interfaces.ChronoPart;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Counts up or down.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("WeakerAccess")
public class Numerator implements ChronoPart {

    final static long SECONDS_TO_TICKS;

    static {
        SECONDS_TO_TICKS = 20;
    }

    protected int startValue;
    protected long rate;
    protected Order order;
    protected final AtomicInteger atomicInteger;
    protected final JavaPlugin plugin;

    private final Runnable runnable;
    private int taskId;

    /**
     * Creates a Numerator
     *
     * @param startValue    the start startValue
     * @param order         the order
     * @param unit          the unit
     */
    public Numerator(int startValue, @NonNull Order order, @NonNull ChronoUnit unit, @NotNull JavaPlugin plugin) {
        this(startValue, order, unit.getDuration().getSeconds() * SECONDS_TO_TICKS, plugin);
    }

    /**
     * Creates a Numerator
     *
     * @param startValue     the start startValue
     * @param order     the order
     * @param rate      the rate in ticks
     */
    public Numerator(int startValue, @NonNull Order order, long rate, @NotNull JavaPlugin plugin) {
        this.startValue = startValue;
        this.order = order;
        this.taskId = -3;
        this.rate = rate;
        this.plugin = plugin;
        this.atomicInteger = new AtomicInteger(this.startValue);
        this.runnable = () -> {
            switch (this.order) {
                case ASC:
                    if (this.atomicInteger.incrementAndGet() == this.startValue) {
                        this.onFinish();
                        this.stop();
                    } else {
                        this.onUpdate();
                    }
                    break;
                case DESC:
                    if (this.atomicInteger.decrementAndGet() == 0) {
                        this.onFinish();
                        this.stop();
                    } else {
                        this.onUpdate();
                    }
                    break;
            }
        };
    }

    /**
     * Called on pause
     */
    public void onPause() {}

    /**
     * Called on resume
     */
    public void onResume() {}

    /**
     * Called on finish
     */
    public void onFinish() {}

    private void onAbort() { }

    /**
     * Runs the numerator
     */
    private void run() {
        this.onUpdate();
        this.taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(this.plugin, this.runnable, this.rate, this.rate);
    }

    public void abort() {
        if (!this.isRunning())
            return;

        this.reset();
        this.onAbort();
    }

    /**
     * Starts the numerator
     */
    @Override
    public final void start() {
        if (this.isRunning() || this.isPaused())
            return;

        this.run();
        this.onStart();
    }

    /**
     * Stops the numerator
     */
    public final void pause() {
        if (!this.isRunning() || this.isPaused())
            return;

        this.cancelTask(-2);
        this.onPause();
    }

    /**
     * Resumes the numerator
     */
    public final void resume() {
        if (!this.isPaused())
            return;

        this.run();
        this.onResume();
    }

    private void reset() {
        this.cancelTask(-3);
        this.atomicInteger.set(this.startValue);
    }

    /**
     * Stops the numerator
     */
    @Override
    public final void stop() {
        this.reset();
        this.onStop();
    }

    /**
     * Checks if the numerator is idle
     *
     * @return           a boolean
     */
    public final boolean isIdle() {
        return this.taskId == -3;
    }

    /**
     * Checks if the numerator is idle
     *
     * @return           a boolean
     */
    @Override
    public final boolean isRunning() {
        return this.taskId > -1;
    }

    /**
     * Checks if the numerator is paused
     *
     * @return           a boolean
     */
    public final boolean isPaused() {
        return this.taskId == -2;
    }

    /**
     * Returns the current value
     *
     * @return           an int
     */
    public int getCurrentValue() {
        return this.atomicInteger.get();
    }

    /**
     * Returns the start value
     *
     * @return           an int
     */
    public int getStartValue() {
        return this.startValue;
    }

    /**
     * Cancels the task
     *
     * @param respond       the respond
     */
    private void cancelTask(int respond) {
        Bukkit.getScheduler().cancelTask(this.taskId);
        this.taskId = respond;
    }

    /**
     * Defines the how the numerator should count
     */
    public enum Order {
        /**
         * Counts up
         */
        ASC,
        /**
         * Counts down
         */
        DESC
    }

    /**
     * Class to make countdowns
     *
     * @author Gleyder
     */
    public abstract static class Countdown extends Numerator {

        /**
         * Creates a Countdown
         *
         * @param startValue        the start value
         * @param unit              the time unit
         */
        public Countdown(int startValue, @NotNull ChronoUnit unit, @NotNull JavaPlugin javaPlugin) {
            super(startValue, Order.DESC, unit, javaPlugin);
        }

    }

    /**
     * Class to make countups
     *
     * @author Gleyder
     */
    public abstract static class Countup extends Numerator  {

        /**
         * Creates a Countup
         *
         * @param startValue        the start value
         * @param unit              the time unit
         */
        public Countup(int startValue, @NotNull ChronoUnit unit, @NotNull JavaPlugin javaPlugin) {
            super(startValue, Order.ASC, unit, javaPlugin);
        }

    }

}
