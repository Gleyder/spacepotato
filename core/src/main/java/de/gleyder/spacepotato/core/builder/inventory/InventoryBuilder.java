package de.gleyder.spacepotato.core.builder.inventory;

import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates an InventoryBuilder
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.interfaces.IBuilder
 */
public class InventoryBuilder implements IBuilder<Inventory> {

    @Getter
    private final Inventory inventory;

    public InventoryBuilder(Inventory inventory, boolean clone) {
        this(inventory, null, clone);
    }

    /**
     * Creates the InventoryBuilder
     *
     * @param inventory     the inventory
     */
    public InventoryBuilder(Inventory inventory, String title, boolean clone) {
        if (clone) {
            if (title == null)
                this.inventory = Bukkit.createInventory(null, inventory.getSize());
            else
                this.inventory = Bukkit.createInventory(null, inventory.getSize(), title);
            this.inventory.setStorageContents(inventory.getStorageContents());
        } else {
            this.inventory = inventory;
        }
    }

    public InventoryBuilder(Inventory inventory) {
        this(inventory, false);
    }

    /**
     * Creates the InventoryBuilder
     *
     * @param rows      the rows
     * @param title     the title
     */
    public InventoryBuilder(int rows, String title) {
        this.inventory = Bukkit.createInventory(null, rows*9, title);
    }

    /**
     * Creates the InventoryBuilder
     *
     * @param type      the type
     * @param title     the title
     */
    public InventoryBuilder(InventoryType type, String title) {
        this.inventory = Bukkit.createInventory(null, type, title);
    }

    /**
     * Sets an item
     *
     * @param material      the material
     * @param index         the index
     * @return              this
     */
    public InventoryBuilder setItem(Material material, int index) {
        this.inventory.setItem(index, new ItemStack(material));
        return this;
    }

    /**
     * Sets the item
     *
     * @param item      the item
     * @param index     the index
     * @return          this
     */
    public InventoryBuilder setItem(ItemStack item, int index) {
        this.inventory.setItem(index, item);
        return this;
    }

    /**
     * Sets the item
     *
     * @param item      the item
     * @param from      the from slot
     * @param to        the to slot
     * @return          this
     */
    public InventoryBuilder setItem(ItemStack item, int from, int to) {
        if (item == null)
            return this;

        for (int slot = from; slot <= to; slot++) {
            this.inventory.setItem(slot, item);
        }
        return this;
    }

    /**
     * Sets the item
     *
     * @param item          the item
     * @param indices       the indices
     * @return              this
     */
    public InventoryBuilder setItems(ItemStack item, int... indices) {
        for (int index : indices) {
            this.inventory.setItem(index, item);
        }
        return this;
    }

    /**
     * Sets the space
     *
     * @param from      the from index
     * @param to        the to index
     * @return          this
     */
    public InventoryBuilder space(int from, int to) {
        this.setItem(new ItemStack(Material.AIR), from, to);
        return this;
    }

    /**
     * Sets the space
     *
     * @param slots     the slots
     * @return          this
     */
    public InventoryBuilder space(int... slots) {
        for (int slotLoop : slots) {
            this.setItem(new ItemStack(Material.AIR), slotLoop);
        }
        return this;
    }

    /**
     * Replaces an item stack
     *
     * @param item              the item
     * @param replacement       the replacement
     * @return                  this
     */
    public InventoryBuilder replace(ItemStack item, ItemStack replacement) {
        List<Integer> replace = new ArrayList<>();
        for (int slot = 0; slot < this.inventory.getSize(); slot++) {
            if (this.inventory.getItem(slot) == null) {
                if (item.getType() == Material.AIR) {
                    replace.add(slot);
                }
            } else if (this.inventory.getItem(slot).equals(item)) {
                replace.add(slot);
            }
        }

        for (Integer loopSlot : replace) {
            this.inventory.setItem(loopSlot, replacement);
        }
        return this;
    }

    /**
     * Removes an item
     *
     * @param items     the item
     * @return
     */
    public InventoryBuilder removeItem(ItemStack... items) {
        for (ItemStack loop : items) {
            this.replace(loop, null);
        }
        return this;
    }

    /**
     * Removes an array of materials
     *
     * @param materials     the materials
     * @return              this
     */
    public InventoryBuilder removeMaterial(Material... materials) {
        for (Material materialLoop : materials) {
            this.inventory.remove(materialLoop);
        }
        return this;
    }

    /**
     * Adds an array of items
     *
     * @param items     the items
     * @return          this
     */
    public InventoryBuilder addItem(ItemStack... items) {
        this.inventory.addItem(items);
        return this;
    }

    /**
     * Fills the inventory with a material
     *
     * @param material      the material
     * @return              this
     */
    public InventoryBuilder fill(Material material) {
        return this.fill(new ItemStack(material));
    }

    /**
     * Fills the inventory with an item stack
     *
     * @param item      an item stack
     * @return          this
     */
    public InventoryBuilder fill(ItemStack item) {
        for (int i = 0; i < this.inventory.getStorageContents().length; i++) {
            this.inventory.setItem(i, item);
        }
        return this;
    }

    /**
     * Builds the inventory
     *
     * @return      this
     */
    @Override
    public Inventory build() {
        return this.inventory;
    }

}
