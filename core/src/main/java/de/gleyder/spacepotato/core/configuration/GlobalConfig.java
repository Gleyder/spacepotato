package de.gleyder.spacepotato.core.configuration;

/**
 * Config for the space potato api
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface GlobalConfig {

    String prefix();
    String defaultLang();

}
