package de.gleyder.spacepotato.core.gui.navigable;

import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryItem;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import de.gleyder.spacepotato.core.gui.InventoryConstants;
import de.gleyder.spacepotato.core.gui.elements.Element;
import de.gleyder.spacepotato.core.gui.impl.GUIInventoryImpl;
import de.gleyder.spacepotato.core.gui.impl.GUIInventoryRegistry;
import de.gleyder.spacepotato.core.utils.Matrix;
import de.gleyder.spacepotato.core.utils.Position;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class InventoryPage extends GUIInventoryImpl {

    private final static int ROWS;
    final static int SIZE;

    static {
        ROWS = 6;
        SIZE = (6-2) * (InventoryConstants.ROW_LENGTH-2);
    }

    @Getter
    private final List<ClickableInventoryItem> clickableInventoryItemList;

    private final Position anchor;

    InventoryPage(GUIInventoryRegistry guiInventoryRegistry, ClickableInventoryRegistry registry, String title) {
        super(guiInventoryRegistry, registry, title, ROWS);
        this.clickableInventoryItemList = new ArrayList<>();
        this.anchor = new Position(1, 1);
    }

    @Override
    public void constructStatic() {
        super.constructStatic();
        this.update();
    }

    private void update() {
        Matrix<ClickableInventoryItem> matrix = new Matrix<>();
        int counter = 0;

        for (int y = 3; y >= 0; y--) {
            for (int x = 0; x <= 6 ; x++) {
                Position position = new Position(x, y);

                this.staticInventory.getInventory().setItem(this.translation.getSlot(position.add(this.anchor)), new ItemStack(Material.AIR));
                if (counter <= this.clickableInventoryItemList.size()-1) {
                    ClickableInventoryItem clickableInventoryItem = this.clickableInventoryItemList.get(counter);
                    matrix.set(position, clickableInventoryItem);
                }

                counter++;
            }
        }

        Element element = new Element(new Position(), Element.Type.DYNAMIC);
        element.getMatrix().combine(matrix);

        this.elementList.add(element);
    }

}
