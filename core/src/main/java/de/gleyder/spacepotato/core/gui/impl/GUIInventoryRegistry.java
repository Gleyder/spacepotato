package de.gleyder.spacepotato.core.gui.impl;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import de.gleyder.spacepotato.core.gui.GUIInventory;
import de.gleyder.spacepotato.core.gui.exceptions.GUIInventoryRegistryException;
import de.gleyder.spacepotato.core.registry.AbstractUsesListenerRegistry;
import de.gleyder.spacepotato.core.utils.UsesListener;
import lombok.Getter;
import org.apache.commons.lang.ClassUtils;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GUIInventoryRegistry extends AbstractUsesListenerRegistry<UUID, GUIInventory> implements UsesListener {

    @Getter
    private final ActiveInventoryManager activeInventoryManager;

    public GUIInventoryRegistry(@NotNull SpacePotatoRegistry spacePotatoRegistry, ClickableInventoryRegistry inventoryRegistry) {
        super(spacePotatoRegistry);
        this.init(new GUIInventoryListener(this, inventoryRegistry));
        this.activeInventoryManager = new ActiveInventoryManager(inventoryRegistry);
    }

    public void checkRegistration(GUIInventory guiInventory) {
        if (!this.contains(guiInventory.getUniqueId()))
            throw new GUIInventoryRegistryException("GUI Inventory with Name {} in Class {} is not registered", guiInventory.getTitle(), ClassUtils.getShortClassName(guiInventory.getClass()));
    }

    @Override
    public void register(GUIInventory guiInventory) {
        this.getMap().put(guiInventory.getUniqueId(), guiInventory);
    }

}
