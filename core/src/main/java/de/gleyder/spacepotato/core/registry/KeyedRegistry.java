package de.gleyder.spacepotato.core.registry;

import java.util.List;

/**
 * Interface for keyed registries.
 * A keyed registry is a registry is basically a hash map.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface KeyedRegistry<K, V> {

    /**
     * Registers a value
     *
     * @param v     a value
     */
    void register(V v);

    /**
     * Registers an iterable of values
     *
     * @param iterable      an iterable ov values
     */
    void register(Iterable<V> iterable);

    /**
     * Unregisters an iterable of values
     *
     * @param iterable      an iterable ov values
     */
    void unregister(Iterable<K> iterable);

    /**
     * Unregisters a value
     *
     * @param k     the key
     */
    void unregister(K k);

    /**
     * Checks if a value
     *
     * @param k     a key
     * @return      if it contains the key
     */
    boolean contains(K k);

    /**
     * Returns a value
     *
     * @param k     a key
     * @return      a value
     */
    V get(K k);

    /**
     * Unregisters all
     */
    void unregisterAll();

    /**
     * Unregisters all
     *
     * @return      all values
     */
    List<V> getAll();

}
