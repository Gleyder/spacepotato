package de.gleyder.spacepotato.core.gui.elements;

import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryItem;
import de.gleyder.spacepotato.core.clickable.inventory.interfaces.InventoryClickAction;
import de.gleyder.spacepotato.core.utils.Position;
import lombok.Getter;
import org.bukkit.inventory.ItemStack;

/**
 * Represents a button in an gui inventory
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class Button extends Element {

    @Getter
    private final ClickableInventoryItem item;

    /**
     * Creates a button
     *
     * @param position      a position
     * @param action        an action
     * @param itemStack     an item stack
     * @param type          a type
     */
    public Button(Position position, InventoryClickAction action, ItemStack itemStack, Type type) {
        super(position, type);
        this.item = new ClickableInventoryItem(action, itemStack);
        this.matrix.set(new Position(), this.item);
    }

}
