package de.gleyder.spacepotato.core.gui;

/**
 * Stores inventory constants
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class InventoryConstants {

    public static final int ROW_LENGTH, CHEST_COLUMNS, DOUBLE_CHEST_COLUMN;

    static {
        ROW_LENGTH = 9;
        CHEST_COLUMNS = 3;
        DOUBLE_CHEST_COLUMN = CHEST_COLUMNS*2;
    }

}
