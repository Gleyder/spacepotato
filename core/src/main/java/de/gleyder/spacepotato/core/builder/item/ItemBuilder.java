package de.gleyder.spacepotato.core.builder.item;

import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.NonNull;
import org.apache.commons.lang.ClassUtils;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.Repairable;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

/**
 * Base ItemBuilder.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ItemBuilder implements IBuilder<ItemStack> {

    private final ItemStack itemStack;
    private final ItemMeta itemMeta;

    /**
     * Creates an ItemBuilder
     *
     * @param material      a material
     * @param amount        an amount
     */
    public ItemBuilder(@NonNull Material material, int amount) {
        this.itemStack = new ItemStack(material, amount);
        this.itemMeta = this.itemStack.getItemMeta();
    }

    /**
     * Creates an ItemBuilder and sets the amount to one
     *
     * @param material      a material
     */
    public ItemBuilder(@NonNull Material material) {
        this(material, 1);
    }

    /**
     * Creates an ItemBuilder from an item stack which is used
     *
     * @param itemStack     an ItemStack
     */
    public ItemBuilder(@NonNull ItemStack itemStack) {
        this(itemStack, false);
    }

    /**
     * Creates an ItemBuilder
     *
     * @param itemStack     an ItemStack
     * @param clone         if it should be copied
     */
    public ItemBuilder(@NonNull ItemStack itemStack, boolean clone) {
        this.itemStack = clone ? itemStack.clone() : itemStack;
        this.itemMeta = itemStack.getItemMeta();
    }

    /**
     * Sets the amount
     *
     * @param amount        an amount
     * @return              this builder
     */
    public ItemBuilder setAmount(int amount) {
        this.itemStack.setAmount(amount);
        return this;
    }

    /**
     * Sets the type
     *
     * @param material      a material
     * @return              this builder
     */
    public ItemBuilder setType(@NonNull Material material) {
        this.itemStack.setType(material);
        return this;
    }

    /**
     * Adds an enchantment
     *
     * @param enchantment       an enchantment
     * @param level             the level
     * @return                  this builder
     */
    public ItemBuilder addEnchantment(@NonNull Enchantment enchantment, int level) {
        this.itemMeta.addEnchant(enchantment, level, true);
        return this;
    }

    /**
     * Adds a map of enchantments
     *
     * @param enchantmentMap        a map of enchantments
     * @return                      this builder
     */
    public ItemBuilder addEnchantments(@NonNull Map<Enchantment, Integer> enchantmentMap) {
        enchantmentMap.forEach(this::addEnchantment);
        return this;
    }

    /**
     * Removes an array of enchantments
     *
     * @param enchantments          an array of enchantments
     * @return                      this builder
     */
    public ItemBuilder removeEnchantments(@NonNull Enchantment... enchantments) {
        for (Enchantment enchantment : enchantments) {
            this.itemStack.removeEnchantment(enchantment);
        }
        return this;
    }

    /**
     * Removes all enchantments
     *
     * @return      this builder
     */
    public ItemBuilder removeAllEnchantments() {
        this.itemMeta.getEnchants().clear();
        return this;
    }


    /**
     * Adds a list of ItemFlags
     *
     * @param itemFlagList      a list of ItemFlags
     * @return                  this builder
     */
    public ItemBuilder addItemFlags(@NonNull Iterable<ItemFlag> itemFlagList) {
        itemFlagList.forEach(this.itemMeta::addItemFlags);
        return this;
    }

    /**
     * Adds an array of ItemFlags
     *
     * @param itemFlags     an array of ItemFlags
     * @return              this builder
     */
    public ItemBuilder addItemFlags(@NonNull ItemFlag... itemFlags) {
        this.itemMeta.addItemFlags(itemFlags);
        return this;
    }

    /**
     * Removes an array of ItemFlags
     *
     * @param itemFlags     an array of ItemFlags
     * @return              this builder
     */
    public ItemBuilder removeItemFlags(@NonNull ItemFlag... itemFlags) {
        this.itemMeta.removeItemFlags(itemFlags);
        return this;
    }

    /**
     * Removes all item flags
     *
     * @return      this builder
     */
    public ItemBuilder removesAllItemFlags() {
        this.itemMeta.getItemFlags().clear();
        return this;
    }

    /**
     * Sets if the item should be unbreakable
     *
     * @param unbreakable       if the item should be unrbeakable
     * @return                  this builder
     */
    public ItemBuilder setUnbreakable(boolean unbreakable) {
        this.itemMeta.setUnbreakable(unbreakable);
        return this;
    }

    /**
     * Sets a list of strings as lore
     *
     * @param lore      a list of string
     * @return          this builder
     */
    public ItemBuilder setLore(@NonNull Iterable<String> lore) {
        List<String> listLore = new ArrayList<>();
        lore.forEach(listLore::add);
        this.itemMeta.setLore(listLore);
        return this;
    }

    /**
     * Sets an array of string as lore
     *
     * @param lore      an array of string
     * @return          this builder
     */
    public ItemBuilder setLore(@NonNull String... lore) {
        this.itemMeta.setLore(Arrays.asList(lore));
        return this;
    }

    /**
     * Adds an array of strings as lore
     *
     * @param lore      an array strings
     * @return          this builder
     */
    public ItemBuilder addLore(@NonNull String... lore) {
        List<String> currentLore = this.itemMeta.hasLore() ? this.itemMeta.getLore() : new ArrayList<>();
        currentLore.addAll(Arrays.asList(lore));
        this.itemMeta.setLore(currentLore);
        return this;
    }

    /**
     * Adds a list of strings as lore
     *
     * @param lore      a list of strings
     * @return
     */
    public ItemBuilder addLore(@NonNull Iterable<String> lore) {
        List<String> currentLore = this.itemMeta.hasLore() ? this.itemMeta.getLore() : new ArrayList<>();
        lore.forEach(currentLore::add);
        this.itemMeta.setLore(currentLore);
        return this;
    }

    /**
     * Sets the damage of the item
     *
     * @param damage        the damage
     * @return              this builder
     */
    public ItemBuilder setDamage(int damage) {
        this.checkMeta(Damageable.class);

        Damageable damageable = (Damageable) this.itemMeta;
        damageable.setDamage(damage);
        return this;
    }

    /**
     * Sets the repair cost
     *
     * @param cost      the repair cost
     * @return          this builder
     */
    public ItemBuilder setRepairCost(int cost) {
        this.checkMeta(Repairable.class);

        Repairable repairable = (Repairable) this.itemMeta;
        repairable.setRepairCost(cost);
        return this;
    }

    /**
     * Sets the display name
     *
     * @param displayName       the display name
     * @return                  this builder
     */
    public ItemBuilder setDisplayName(@NonNull String displayName) {
        this.itemMeta.setDisplayName(displayName);
        return this;
    }

    /**
     * Adds an attribute with AttributeModifier.Operation.ADD_NUMBER
     *
     * @param attribute     an attribute
     * @param amount        an amount
     * @param slot          a slot
     * @return              this builder
     */
    public ItemBuilder addAttribute(Attribute attribute, int amount, EquipmentSlot slot) {
        return this.addAttribute(attribute, amount, AttributeModifier.Operation.ADD_NUMBER, slot);
    }

    /**
     * Adds an attribute
     *
     * @param attribute     an attribute
     * @param amount        an amount
     * @param operation     an operation
     * @param slot          a slot
     * @return              this builder
     */
    public ItemBuilder addAttribute(Attribute attribute, int amount, AttributeModifier.Operation operation, EquipmentSlot slot) {
        UUID uniqueId = UUID.randomUUID();
        AttributeModifier attributeModifier = new AttributeModifier(uniqueId, uniqueId.toString(), amount, operation, slot);
        this.itemMeta.addAttributeModifier(attribute, attributeModifier);
        return this;
    }

    /**
     * Converts to a PotionBuilder
     *
     * @return           an PotionBuilder
     */
    public PotionBuilder toPotionBuilder() {
        return new PotionBuilder(this.build(), false);
    }

    /**
     * Converts to a BookBuilder
     *
     * @return          an BookBuilder
     */
    public BookBuilder toBookBuilder() {
        return new BookBuilder(this.build(), false);
    }

    /**
     * Convert to a EnchantedBookBuilder
     *
     * @return          an EnchantedBookBuilder
     */
    public EnchantedBookBuilder toEnchantedBookBuilder() {
        return new EnchantedBookBuilder(this.build());
    }

    /**
     * Converts to a BannerBuilder
     *
     * @return          a BannerBuilder
     */
    public BannerBuilder toBannerBuilder() {
        return new BannerBuilder(this.build());
    }

    /**
     * Converts to a leather armor builder
     *
     * @return      a leather armor builder
     */
    public LeatherArmorBuilder toLeatherArmorBuilder() {
        return new LeatherArmorBuilder(this.build());
    }

    /**
     * Converts this builder to a persistent data builder
     *
     * @param plugin        an instance of a plugin
     * @return              a persistent data builder
     */
    public PersistentDataBuilder toPersistentDataBuilder(JavaPlugin plugin) {
        return new PersistentDataBuilder(this.build(), false, plugin);
    }

    /**
     * Converts this builder to a skull builder
     *
     * @return      this builder
     */
    public SkullBuilder toSkullBuilder() {
        return new SkullBuilder(this.build(), false);
    }

    /**
     * Checks if the builder's meta is assignable from the given meta
     *
     * @param meta      the meta which should be assignable
     */
    private void checkMeta(Class<?> meta) {
        if (!(this.itemMeta.getClass().isAssignableFrom(meta)))
            throw new ClassCastException("The item " + this.itemStack + " has the meta " + this.itemMeta + " which is not an instance of " + ClassUtils.getShortClassName(meta));
    }

    /**
     * Builds the ItemStack
     *
     * @return      an ItemStack
     */
    @Override
    public ItemStack build() {
        this.itemStack.setItemMeta(this.itemMeta);
        return this.itemStack;
    }

}
