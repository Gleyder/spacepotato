package de.gleyder.spacepotato.core.gui.elements;

import de.gleyder.spacepotato.core.utils.Matrix;
import de.gleyder.spacepotato.core.utils.Position;

/**
 * Creates primitive shapes
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PrimitiveShapes {

    /**
     * Create a rectangle
     *
     * @param xSize     the x size
     * @param ySize     the y size
     * @param t         the type of the matrix
     * @return          a matrix
     */
    public static <T> Matrix<T> createRectangle(int xSize, int ySize, T t) {
        Matrix<T> matrix = new Matrix<>();
        for (int x = 0; x < xSize; x++) {
            for (int y = 0; y < ySize; y++) {
                Position position = new Position(x, y);
                matrix.set(position, t);
            }
        }
        return matrix;
    }

    /**
     * Creates a square
     *
     * @param size      the size
     * @return          the matrix
     */
    public static <T> Matrix<T> createSquare(int size, T t) {
        return createRectangle(size, size, t);
    }

}
