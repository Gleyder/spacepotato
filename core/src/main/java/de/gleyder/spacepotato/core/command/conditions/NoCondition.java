package de.gleyder.spacepotato.core.command.conditions;

import de.gleyder.spacepotato.core.command.interfaces.Condition;
import org.bukkit.command.CommandSender;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class NoCondition implements Condition<CommandSender> {

    @Override
    public boolean test(CommandSender sender) {
        return true;
    }

    @Override
    public String getMessage() {
        return "";
    }

}
