package de.gleyder.spacepotato.core.analyzer.item;

import de.gleyder.spacepotato.core.analyzer.QueryResult;
import de.gleyder.spacepotato.core.analyzer.QueryType;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Analyses an Inventory.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class InventoryAnalyzer {

    @Getter
    private final Inventory inventory;

    /**
     * Creates an InventoryAnalyzer
     *
     * @param inventory         an inventory
     */
    public InventoryAnalyzer(@NonNull Inventory inventory) {
        this.inventory = inventory;
    }

    /**
     * Searches for items
     *
     * @param pattern       the reference for the query
     * @param type          the query type
     * @param queries       an array of queries
     * @return              a list of item stacks
     */
    public List<ItemStack> searchItems(@NonNull ItemStack pattern, @NonNull QueryType type, @NonNull ItemSearchQuery... queries) {
        return this.searchItems(pattern, type, new HashSet<>(Arrays.asList(queries)));
    }

    /**
     * Searches for items
     *
     * @param pattern       the reference for the query
     * @param type          the query type
     * @param queries       a list of queries
     * @return              a list of item stacks
     */
    public List<ItemStack> searchItems(@NonNull ItemStack pattern, @NonNull QueryType type, @NonNull Set<ItemSearchQuery> queries) {
        return this.searchItems(pattern, type.getPredicate(), queries.stream().map(ItemSearchQuery::getBiPredicate).collect(Collectors.toSet()));
    }
    /**
     * Searches for items
     *
     * @param pattern   the pattern
     * @param type      the type
     * @param queries   the query
     * @return          a list of item stacks
     */
    @SuppressWarnings("all")
    public List<ItemStack> searchItems(@NonNull ItemStack pattern,
                                       @NonNull Predicate<List<QueryResult<Object>>> type,
                                       @NonNull Set<BiPredicate<ItemStack, ItemStack>> queries)  {

        List<ItemStack> itemList = new ArrayList<>();

        for (ItemStack storageContent : this.inventory.getStorageContents()) {
            if (storageContent == null)
                continue;

            /*
             * Tests every item for the given values and adds the result to a list
             */
            LinkedList<QueryResult<Object>> resultList = new LinkedList<>();
            for (BiPredicate<ItemStack, ItemStack> query : queries) {
                resultList.add(new QueryResult(storageContent, query.test(pattern, storageContent)));
            }
            /*
             * The ItemSearchQueryType tests if the the item should be added to final list
             */
            if (type.test(resultList))
                itemList.add(storageContent);
        }

        return itemList;
    }

    /**
     * Gets sum of amount from all items
     *
     * @return      an integer
     */
    @SuppressWarnings("unused")
    public int getOccupiedSlots() {
        return this.searchItems(new ItemStack(Material.AIR), QueryType.NONE, ItemSearchQuery.MATERIAL).size();
    }

    /**
     * Gets sum of amount from all items
     *
     * @return      an integer
     */
    @SuppressWarnings("unused")
    public int getAmount() {
        int amount = 0;
        for (ItemStack content : this.inventory.getStorageContents()) {
            if (content.getType() == Material.AIR)
                continue;
            amount += content.getAmount();
        }
        return amount;
    }

}