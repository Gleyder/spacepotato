package de.gleyder.spacepotato.core.clickable.mob;

import de.gleyder.spacepotato.core.clickable.mob.enums.LivingEntityType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Represents a mob which can be clicked.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@RequiredArgsConstructor
public abstract class SimpleClickableMob {

    @Getter
    protected final LivingEntityType type;

    @Getter
    protected final Location location;

    @Getter
    protected final boolean cancelInteractEvent;

    @Setter(AccessLevel.PACKAGE)
    @Getter(AccessLevel.PACKAGE)
    UUID uniqueId;

    @Setter
    @Getter
    protected LivingEntity entity;

    /**
     * Called if a player clicks the mob
     *
     * @param player         a player
     */
    public abstract void onClick(Player player);

    /**
     * Called after the mob was spawned
     */
    public void onCreate() {}

    /**
     * Called after the mob was spawned
     */
    private void onSpawn() {}

    /**
     * Called after the mob was despawned
     */
    private void onDespawn() {}

    /**
     * Gets the entitie's class
     *
     * @param <T>
     * @return
     */
    @SuppressWarnings("all")
    public <T extends LivingEntity> T getSpecificEntity() {
        return (T) this.getEntity().getClass().cast(this.type.getEntity());
    }

    /**
     * Spawns the mob
     */
    public final void spawn() {
        if (this.entity != null)
            return;

        World world = this.location.getWorld();

        this.entity = (LivingEntity) world.spawnEntity(this.location, this.type.getType());
        this.onCreate();
        this.onSpawn();
    }

    /**
     * Despawns the mob
     */
    public final void despawn() {
        if (this.entity == null)
            return;

        this.entity.remove();
        this.onDespawn();
        this.entity = null;
    }

}