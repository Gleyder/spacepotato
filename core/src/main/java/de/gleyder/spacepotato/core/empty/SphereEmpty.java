package de.gleyder.spacepotato.core.empty;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

/**
 * Base class for a sphere empty
 *
 * @author Gleyder
 * @version 0.1
 */
public abstract class SphereEmpty extends Empty {

    @Getter
    private final Location centre;

    @Getter
    private final float radius;

    /**
     * Creates a SphereEmpty
     *
     * @param centre        the centre
     * @param radius        the radius
     */
    protected SphereEmpty(Location centre, float radius) {
        super(centre.getWorld());
        this.centre = centre;
        this.radius = radius;
    }

    /**
     * Checks if player joined or left the empty
     *
     * @param playerLocation        the player's location
     * @return                      a PlayerEmptyChangeType
     */
    @Override
    public final PlayerEmptyChangeType check(Location playerLocation) {
        double distance = this.centre.distance(playerLocation);

        if (distance <= this.radius) {
            return PlayerEmptyChangeType.JOIN;
        } else {
            return PlayerEmptyChangeType.LEAVE;
        }
    }

    /**
     * Called when the outline should be calculated
     */
    @Override
    public final void calculateOutline() {
        Vector centre = new Vector(0, 0, 0);
        final double PI = Math.PI;
        double increment = 6;
        double multiplicator = this.radius * 0.4;

        Set<Vector> vectorSet = new HashSet<>();

        for (double theta = 0D; theta < PI; theta = theta + PI/(increment*multiplicator)) {
            for (double varphi = 0D; varphi < PI; varphi = varphi + PI/(increment*multiplicator)) {
                double x = this.radius * Math.sin(theta) * Math.cos(varphi);
                double z = this.radius * Math.sin(theta) *  Math.sin(varphi);
                double y = this.radius * Math.cos(theta);

                Vector base = centre.clone().add(new Vector(x, y, z));
                Vector mirror = base.clone().multiply(-1);
                vectorSet.add(base);
                vectorSet.add(mirror);
            }
        }

        Vector globalVector = this.centre.clone().toVector();
        vectorSet.forEach(vector -> this.markedPoints.add(globalVector.clone().add(vector)));
    }

}
