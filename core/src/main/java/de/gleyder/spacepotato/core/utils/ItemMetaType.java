package de.gleyder.spacepotato.core.utils;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.meta.*;

import java.util.Arrays;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@Deprecated
public enum ItemMetaType {

    BANNER(BannerMeta.class, Arrays.stream(Material.values()).filter(material -> material.name().endsWith("_BANNER")).toArray(value -> new Material[0])),
    BOOK(BookMeta.class, Material.WRITTEN_BOOK, Material.WRITABLE_BOOK),
    CROSSBOW_META(CrossbowMeta.class, Material.CROSSBOW),
    DAMAGEABLE(Damageable.class, Arrays.stream(Material.values()).filter(material ->
            material.name().endsWith("_SWORD") || material.name().endsWith("_SHOVEL") || material.name().endsWith("_PICKAXE") || material.name().endsWith("_AXE")
    ).toArray(value -> new Material[0])),
    FIREWORK(FireworkMeta.class, Material.FIREWORK_ROCKET),
    TROPICAL_FISH_BUCKET(TropicalFishBucketMeta.class, Material.TROPICAL_FISH_BUCKET)
    ;

    @Getter
    final Material[] materials;

    @Getter
    final Class<?> metaClass;

    ItemMetaType(Class<?> clazz, Material... materials) {
        this.materials = materials;
        this.metaClass = clazz;
    }

}
