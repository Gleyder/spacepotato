package de.gleyder.spacepotato.core.command.util;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class TabCompletionsHelper {

    public static List<String> getAsTypeString(String type) {
        return Lists.newArrayList("[<" + type + ">]");
    }

    public static String getAsSingleTypeString(String type) {
        return "[<" + type + ">]";
    }

    public static String[] getAsTabCompletion(String... strings) {
        return strings;
    }

    public static <T> String[] getAsTabCompletion(T[] array, Function<T, String> function) {
        return Arrays.stream(array).map(function).toArray(String[]::new);
    }

    public static <T> String[] getAsTabCompletion(List<T> list, Function<T, String> function) {
        return list.stream().map(function).toArray(String[]::new);
    }

    public static String[] add(String[] array, String... otherArray) {
        String[] newArray = Arrays.copyOf(array, array.length + otherArray.length);
        for (int i = 0; i < otherArray.length; i++) newArray[i+array.length] = otherArray[i];
        return newArray;
    }

}
