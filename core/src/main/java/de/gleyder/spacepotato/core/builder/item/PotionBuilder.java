package de.gleyder.spacepotato.core.builder.item;

import de.gleyder.spacepotato.core.builder.item.exceptions.MaterialNotSupportedException;
import de.gleyder.spacepotato.core.builder.item.exceptions.UnexpectedPotionBuilderInitException;
import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.NonNull;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

/**
 * Builder for potions.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public final class PotionBuilder implements IBuilder<ItemStack> {

    private final ItemStack itemStack;
    private final PotionMeta potionMeta;

    /**
     * Creates an PotionBuilder
     *
     * @param type      a type
     */
    public PotionBuilder(@NonNull Type type) {
        switch (type) {
            case NORMAL:
                this.itemStack = new ItemStack(Material.POTION, 1);
                break;
            case SPLASH:
                this.itemStack = new ItemStack(Material.SPLASH_POTION, 1);
                break;
            case LINGERING:
                this.itemStack = new ItemStack(Material.LINGERING_POTION, 1);
                break;
            default:
                throw new UnexpectedPotionBuilderInitException("Unexpected ArgumentSupplierError in PotionBuilder constructor");
        }
        this.potionMeta = (PotionMeta) this.itemStack.getItemMeta();
    }

    /**
     * Creates a PotionBuilder
     *
     * @param itemStack     an ItemStack
     * @param copy          if the item should be copied
     */
    public PotionBuilder(@NonNull ItemStack itemStack, boolean copy) {
        switch (itemStack.getType()) {
            case LINGERING_POTION: case SPLASH_POTION: case POTION:
                if (copy) {
                    this.itemStack = itemStack.clone();
                } else {
                    this.itemStack = itemStack;
                }
                this.potionMeta = (PotionMeta) itemStack.getItemMeta();
                break;
            default:
                throw new MaterialNotSupportedException(itemStack.getType() + " is not a potion");
        }
    }

    /**
     * Creates an PotionBuilder
     *
     * @param itemStack     an ItemStack
     */
    public PotionBuilder(@NonNull ItemStack itemStack) {
        this(itemStack, false);
    }

    /**
     * Adds an CustomPotionEffect
     *
     * @param effectType        the effect type
     * @param duration          the duration
     * @param amplifier         the amplifier
     * @param particles         if it should have particles
     * @return                  this builder
     */
    public PotionBuilder addCustomPotionEffect(PotionEffectType effectType, int duration, int amplifier, boolean particles) {
        return this.addCustomPotionEffect(new PotionEffect(effectType, duration, amplifier, false, particles, true), false);
    }

    /**
     * Adds an CustomPotionEffect
     *
     * @param effectType        the effect type
     * @param duration          the duration
     * @param amplifier         the amplifier
     * @param ambient           if it should be ambient
     * @param particles         if it should have particles
     * @return                  this builder
     */
    public PotionBuilder addCustomPotionEffect(PotionEffectType effectType, int duration, int amplifier, boolean ambient, boolean particles) {
        return this.addCustomPotionEffect(new PotionEffect(effectType, duration, amplifier, ambient, particles, true), false);
    }

    /**
     * Adds an CustomPotionEffect
     *
     * @param effectType        the effect type
     * @param duration          the duration
     * @param amplifier         the amplifier
     * @param ambient           if it should be ambient
     * @param particles         if it should have particles
     * @param icon              if it should have an icon
     * @return                  this builder
     */
    public PotionBuilder addCustomPotionEffect(PotionEffectType effectType, int duration, int amplifier, boolean ambient, boolean particles, boolean icon) {
        return this.addCustomPotionEffect(new PotionEffect(effectType, duration, amplifier, ambient, particles, icon), false);
    }

    /**
     * Adds an CustomPotionEffect
     *
     * @param effectType        the effect type
     * @param duration          the duration
     * @param amplifier         the amplifier
     * @param ambient           if it should be ambient
     * @param particles         if it should have particles
     * @param icon              if it should have an icon
     * @param overwrite         if it should overwrite the effect if the player has it already
     * @return                  this builder
     */
    public PotionBuilder addCustomPotionEffect(PotionEffectType effectType, int duration, int amplifier, boolean ambient, boolean particles, boolean icon, boolean overwrite) {
        return this.addCustomPotionEffect(new PotionEffect(effectType, duration, amplifier, ambient, particles, icon), overwrite);
    }

    /**
     * Adds an CustomPotionEffect
     *
     * @return                  this builder
     */
    public PotionBuilder addCustomPotionEffect(PotionEffect potionEffect, boolean overwrite) {
        this.potionMeta.addCustomEffect(potionEffect, overwrite);
        return this;
    }

    /**
     * Sets the base potion data
     *
     * @param type                      a potion type
     * @param modifiedPotionData        a ModifiedPotionData
     * @return                          this builder
     */
    public PotionBuilder setBasePotionData(PotionType type, ModifiedPotionData modifiedPotionData) {
        boolean extended, upgraded;
        switch (modifiedPotionData) {
            case NORMAL: default:
                extended = false;
                upgraded = false;
                break;
            case EXTENDED:
                extended = true;
                upgraded = false;
                break;
            case UPGRADED:
                extended = false;
                upgraded = true;
        }
        return this.setBasePotionData(new PotionData(type, extended, upgraded));
    }

    /**
     * Sets the base potion data
     *
     * @param type          the type
     * @param extended      if it should be extended
     * @param upgraded      if it should be upgraded
     * @return              this builder
     */
    public PotionBuilder setBasePotionData(PotionType type, boolean extended, boolean upgraded) {
        return this.setBasePotionData(new PotionData(type, extended, upgraded));
    }

    /**
     * Sets a PotionData
     *
     * @param potionData    a PotionData
     * @return
     */
    public PotionBuilder setBasePotionData(PotionData potionData) {
        this.potionMeta.setBasePotionData(potionData);
        return this;
    }

    /**
     * Sets the color
     *
     * @param color     a color
     * @return
     */
    public PotionBuilder setColor(Color color) {
        this.potionMeta.setColor(color);
        return this;
    }

    /**
     * Converts it to an ItemBuilder
     *
     * @return      an ItemBuilder
     */
    public ItemBuilder toItemBuilder() {
        return new ItemBuilder(this.build());
    }

    /**
     * Builds the ItemStack
     *
     * @return      an ItemStack
     */
    @Override
    public ItemStack build() {
        this.itemStack.setItemMeta(this.potionMeta);
        return this.itemStack;
    }

    /**
     * Used for PotionData
     *
     * @author Gleyder
     */
    public enum ModifiedPotionData {
        /**
         * the potion is upgraded
         */
        UPGRADED,

        /**
         *  the potion is extended
         */
        EXTENDED,

        /**
         * the potion is none of the above
         */
        NORMAL
    }

    /**
     * Used to define the potion type
     *
     * @author Gleyder
     */
    public enum Type {
        /**
         * the potion is a lingering potion
         */
        LINGERING,

        /**
         * the potion is a lingering potion
         */
        SPLASH,

        /**
         * the potion is a drinkable potion
         */
        NORMAL
    }

}
