package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class PlayerInterpreter implements Interpreter<Player> {

    @Override
    public Player translate(String argument) throws InterpreterException {
        return Bukkit.getOnlinePlayers()
                .stream()
                .filter(filterPlayer -> filterPlayer.getDisplayName().equals(argument))
                .findFirst()
                .orElseThrow((Supplier<InterpreterException>) () -> new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_PLAYER.getKeyableMessage(false)
                        .replace("argument", argument)
                        .build()
                ));
    }

    @Override
    public Supplier<List<String>> getDynamicTabCompletion() {
        return () -> Bukkit.getOnlinePlayers()
                .stream()
                .map(Player::getDisplayName)
                .collect(Collectors.toList());
    }

    @Override
    public Class<Player> getClassType() {
        return Player.class;
    }

}
