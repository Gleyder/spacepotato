package de.gleyder.spacepotato.core.command.enums;

/**
 * Enum for argument types.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum ArgumentType {

    /**
     * Indicates that an argument cannot change it's value
     */
    STATIC,

    /**
     * Indicates that an argument can have in theory ever value
     */
    MUTABLE;

}
