package de.gleyder.spacepotato.core.clickable.inventory.interfaces;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 */
public class BlockingClickAction implements InventoryClickAction {

    @Override
    public boolean onClick(@NotNull Player player, @NotNull Inventory inventory, @NotNull ItemStack item, @NotNull ClickType clickType) {
        return true;
    }

}
