package de.gleyder.spacepotato.core.utils;

import lombok.NonNull;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Some material utils.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public final class MaterialUtils {

    private MaterialUtils() {
        throw new UnsupportedOperationException();
    }

    /**
     * Gets the key of a material
     *
     * @param material      a material
     * @return              a string
     */
    public static String getKey(@NonNull Material material) {
        return material.getKey().getKey();
    }

    /**
     * Gets all materials as string list
     *
     * @return      a list of strings
     */
    public static List<String> getAsStringKeyList() {
        return Arrays.stream(Material.values()).map(material -> material.getKey().getKey()).collect(Collectors.toList());
    }

    /**
     * Checks if an item is null considering that the item could be null
     *
     * @param itemStack     an item
     * @return              if the item is air or null
     */
    public static boolean isAir(ItemStack itemStack) {
        if (itemStack == null)
            return true;
        return itemStack.getType() == Material.AIR;
    }

}
