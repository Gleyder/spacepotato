package de.gleyder.spacepotato.core.clickable.mob;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.registry.AbstractUsesListenerRegistry;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 * Registry for clickable mobs.
 *
 * @author Gleyder
 * @version 1.0
 * @see 1.0
 */
public class ClickableMobRegistry extends AbstractUsesListenerRegistry<UUID, ClickableMob> {

    public ClickableMobRegistry(@NotNull SpacePotatoRegistry spacePotatoRegistry) {
        super(spacePotatoRegistry);
        this.init(new ClickableMobListener(this));
    }

    @Override
    public void register(ClickableMob clickableMob) {
        this.getMap().put(clickableMob.getEntity().getUniqueId(), clickableMob);
    }

}
