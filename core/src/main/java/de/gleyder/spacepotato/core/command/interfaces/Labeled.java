package de.gleyder.spacepotato.core.command.interfaces;

/**
 * Indicates that an entity can be labeled.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface Labeled {

    String getLabel();

}
