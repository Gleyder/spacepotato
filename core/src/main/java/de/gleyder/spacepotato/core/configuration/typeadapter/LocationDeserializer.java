package de.gleyder.spacepotato.core.configuration.typeadapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import de.gleyder.spacepotato.core.configuration.pojos.SaveLocation;
import org.bukkit.Location;

import java.lang.reflect.Type;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class LocationDeserializer implements JsonDeserializer<Location> {

    @Override
    public Location deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        SaveLocation saveLocation = context.deserialize(json, SaveLocation.class);
        return saveLocation.toLocation();
    }

}
