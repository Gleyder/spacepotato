package de.gleyder.spacepotato.core.configuration.handler.loader;

import de.gleyder.spacepotato.core.configuration.ConfigHelper;
import de.gleyder.spacepotato.core.configuration.handler.FileLoader;
import org.jetbrains.annotations.NotNull;

import java.io.File;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SaveFileLoader implements FileLoader {

    @Override
    public void load(@NotNull String filename, @NotNull File loadoutFile, @NotNull File dataFolderFile, @NotNull ConfigHelper configHelper) {
        if (!dataFolderFile.exists())
            configHelper.copyFromLoadout(filename);
    }

}
