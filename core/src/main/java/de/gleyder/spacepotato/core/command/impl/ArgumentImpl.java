package de.gleyder.spacepotato.core.command.impl;

import de.gleyder.spacepotato.core.command.entities.Argument;
import de.gleyder.spacepotato.core.command.enums.ArgumentType;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

/**
 * Implementation of Argument.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@ToString
public class ArgumentImpl implements Argument {

    @Getter
    protected final ArgumentType type;

    /**
     * Creates an ArgumentImpl
     *
     * @param type      a type
     */
    ArgumentImpl(@NonNull ArgumentType type) {
        this.type = type;
    }

}
