package de.gleyder.spacepotato.core.utils;

import lombok.Getter;
import lombok.NonNull;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import static org.bukkit.Material.*;

/**
 * Stores colors with related items
 * Names are the same as the wool blocks.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum SpacePotatoColor {

    BLACK(ChatColor.BLACK,  Color.fromRGB(0, 0, 0), DyeColor.BLACK, BLACK_WOOL, BLACK_BANNER, BLACK_DYE),
    DARK_BLUE(ChatColor.DARK_BLUE, Color.fromRGB(0, 0, 170), DyeColor.BLUE, BLUE_WOOL, BLUE_BANNER, BLUE_DYE),
    GREEN(ChatColor.DARK_GREEN, Color.fromRGB(0, 170, 0), DyeColor.GREEN, GREEN_WOOL, GREEN_BANNER, GREEN_DYE),
    CYAN(ChatColor.DARK_AQUA, Color.fromRGB(0, 170, 170), DyeColor.CYAN, CYAN_WOOL, CYAN_BANNER, CYAN_DYE),
    DARK_RED(ChatColor.DARK_RED, Color.fromRGB(170, 0, 0), DyeColor.RED, RED_WOOL, RED_BANNER, RED_DYE),
    PURPLE(ChatColor.DARK_PURPLE, Color.fromRGB(170, 0, 170), DyeColor.PURPLE, PURPLE_WOOL, PURPLE_BANNER, PURPLE_DYE),
    ORANGE(ChatColor.GOLD, Color.fromRGB(255, 170, 0), DyeColor.ORANGE, ORANGE_WOOL, ORANGE_BANNER, ORANGE_DYE),
    LIGHT_GRAY(ChatColor.GRAY, Color.fromRGB(170, 170, 170), DyeColor.LIGHT_GRAY, LIGHT_GRAY_WOOL, LIGHT_GRAY_BANNER, LIGHT_GRAY_DYE),
    GRAY(ChatColor.DARK_GRAY, Color.fromRGB(85, 85, 85), DyeColor.GRAY, GRAY_WOOL, GRAY_BANNER, GRAY_DYE),
    BLUE(ChatColor.BLUE, Color.fromRGB(85, 85, 255), DyeColor.BLUE, BLUE_CONCRETE, BLUE_BANNER, LAPIS_LAZULI),
    LIME(ChatColor.GREEN, Color.fromRGB(85, 255, 85), DyeColor.LIME, GREEN_WOOL, GREEN_BANNER, LIME_DYE),
    AQUA(ChatColor.AQUA, Color.fromRGB(85, 255, 255), DyeColor.LIGHT_BLUE,  LIGHT_BLUE_WOOL, LIGHT_BLUE_BANNER, LIGHT_BLUE_DYE),
    RED(ChatColor.RED, Color.fromRGB(255, 85, 85), DyeColor.RED, RED_CONCRETE, RED_BANNER, REDSTONE),
    MAGENTA(ChatColor.LIGHT_PURPLE, Color.fromRGB(255, 85, 255), DyeColor.MAGENTA, LIGHT_BLUE_WOOL, LIGHT_BLUE_BANNER, MAGENTA_DYE),
    YELLOW(ChatColor.YELLOW, Color.fromRGB(255, 255, 85), DyeColor.YELLOW, YELLOW_WOOL, YELLOW_BANNER, YELLOW_DYE),
    WHITE(ChatColor.WHITE, Color.fromRGB(255, 255, 255), DyeColor.WHITE, WHITE_WOOL, WHITE_BANNER, WHITE_DYE)
    ;

    @Getter
    private final ChatColor chatColor;

    @Getter
    private final Color color;

    @Getter
    private final DyeColor dyeColor;

    @Getter
    private final Material block, banner, dye;

    /**
     * Creates a RainstormColor
     *
     * @param chatColor     the chat color
     * @param color         the color
     * @param block         the block material
     * @param banner        the banner material
     * @param dye           the dye material
     */
    SpacePotatoColor(@NonNull ChatColor chatColor, @NonNull Color color, @NonNull DyeColor dyeColor, @NonNull Material block, @NonNull Material banner, @NonNull Material dye) {
        this.chatColor = chatColor;
        this.color = color;
        this.block = block;
        this.dyeColor = dyeColor;
        this.banner = banner;
        this.dye = dye;
    }

    /**
     * Returns a wool item
     *
     * @return      an ItemStack
     */
    public ItemStack getBlockItem() {
        return new ItemStack(this.block);
    }

    /**
     * Returns a banner item
     *
     * @return      an ItemStack
     */
    public ItemStack getBannerItem() {
        return new ItemStack(this.banner);
    }

    /**
     * Returns a dye item
     *
     * @return      an ItemStack
     */
    public ItemStack getDyeItem() {
        return new ItemStack(this.dye);
    }

}
