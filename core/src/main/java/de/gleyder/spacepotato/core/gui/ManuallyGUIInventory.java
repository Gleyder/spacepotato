package de.gleyder.spacepotato.core.gui;

/**
 * Represents a manually gui inventory
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface ManuallyGUIInventory extends GUIInventory {

    /**
     * Builds the inventory
     */
    void update();

}
