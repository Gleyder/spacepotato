package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Translates an argument into a world
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class WorldInterpreter implements Interpreter<World> {

    /**
     * Translates an argument into a world
     *
     * @param argument                          an argument
     * @return                                  a world
     * @throws InterpreterException     if the argument cannot be translated
     */
    @Override
    public World translate(String argument) throws InterpreterException {
        World world = Bukkit.getWorld(argument);
        if (world == null) {
            throw new InterpreterException(
                    CoreMessages.COMMAND_INTERPRETER_EXCEPTION_WORLD.getKeyableMessage(true)
                            .replace("argument", argument)
                            .build()
            );
        }
        return world;
    }

    @Override
    public Supplier<List<String>> getDynamicTabCompletion() {
        return () -> Bukkit.getWorlds().stream().map(World::getName).collect(Collectors.toList());
    }

    @Override
    public Class<World> getClassType() {
        return World.class;
    }

}
