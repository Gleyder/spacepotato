package de.gleyder.spacepotato.core.clickable.mob;

import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

/**
 * Listener for clickable mobs
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see org.bukkit.event.Listener
 */
@AllArgsConstructor
public class ClickableMobListener implements Listener {

    private final ClickableMobRegistry registry;

    @EventHandler
    public void onClick(PlayerInteractEntityEvent event) {
        SimpleClickableMob clickableMob = this.registry.get(event.getRightClicked().getUniqueId());
        if (clickableMob == null)
            return;
        if (event.getHand() != EquipmentSlot.HAND)
            return;

        event.setCancelled(clickableMob.isCancelInteractEvent());
        clickableMob.onClick(event.getPlayer());
    }

}
