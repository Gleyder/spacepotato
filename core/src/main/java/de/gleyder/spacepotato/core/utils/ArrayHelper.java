package de.gleyder.spacepotato.core.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for arrays.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@Deprecated
public class ArrayHelper {

    /**
     * Returns an array as string
     * For example if you input an array with following arguments {"hello", "world"}
     * the method returns ["hello", "world"]
     *
     * @param list      an array
     * @param <T>       the type of the array
     * @return          a String
     */
    public static <T> String getAsString(T[] list) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0; i < list.length; i++) {
            stringBuilder.append("\"").append(list[i].toString()).append("\"");

            if (i != list.length - 1) {
                stringBuilder.append(", ");
            }
        }
        return stringBuilder.append("]").toString();
    }

    /**
     * Returns an array as string
     * For example if you input an array with following arguments {"hello", "world"}
     * the method returns following: hello world
     *
     * @param list      an array
     * @param <T>       the type of the array
     * @return          a String
     */
    public static <T> String getAsSimpleString(T[] list) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < list.length; i++) {
            stringBuilder.append(list[i].toString());

            if (i != list.length - 1) {
                stringBuilder.append(" ");
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Returns a list of string with values that are in both arrays
     *
     * @param array         an array
     * @param otherArray    an other array
     * @param <A>           the type of one array
     * @param <OA>          the type of the other array
     * @return              a list of Strings
     */
    public static <A, OA> List<String> getSameValues(A[] array, OA[] otherArray) {
        List<String> list = new ArrayList<>();
        for (A a : array) {
            for (OA oa : otherArray) {
                if (a.equals(oa))
                    list.add(a.toString());
            }
        }
        return list;
    }

}
