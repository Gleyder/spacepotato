package de.gleyder.spacepotato.core.clickable.item.interfaces;

import lombok.NonNull;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface ItemClickAction {

    boolean onClick(@NonNull Player player,
                 @NonNull ItemStack itemStack,
                 @NonNull EquipmentSlot slot,
                 @Nullable Action action,
                 @Nullable Block clickedBlock,
                 @Nullable Entity clickedEntity);

}
