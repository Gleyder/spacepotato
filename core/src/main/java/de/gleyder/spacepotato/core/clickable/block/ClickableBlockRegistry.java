package de.gleyder.spacepotato.core.clickable.block;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.registry.AbstractUsesListenerRegistry;
import org.bukkit.block.Block;

/**
 * Registry for clickable blocks.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ClickableBlockRegistry extends AbstractUsesListenerRegistry<Block, ClickableBlock> {

    public ClickableBlockRegistry(SpacePotatoRegistry spacePotatoRegistry) {
        super(spacePotatoRegistry);
        this.init(new ClickableBlockListener(this));
    }

    @Override
    public void register(ClickableBlock clickableBlock) {
        this.getMap().put(clickableBlock.getBlock(), clickableBlock);
    }

}
