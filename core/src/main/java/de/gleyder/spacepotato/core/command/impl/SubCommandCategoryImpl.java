package de.gleyder.spacepotato.core.command.impl;

import de.gleyder.spacepotato.core.command.entities.SubCommandCategory;
import de.gleyder.spacepotato.core.command.enums.LabelType;
import de.gleyder.spacepotato.core.command.enums.SenderType;
import lombok.Getter;
import lombok.ToString;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventPriority;

import java.util.List;
import java.util.function.Function;

/**
 * Implementation of SubCommandCategory.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@ToString
public class SubCommandCategoryImpl extends AbstractCommandPart implements SubCommandCategory {

    @Getter
    protected final LabelType labelType;

    @Getter
    protected final EventPriority priority;

    /**
     * Creates a SubCommandCategoryImpl
     *
     * @param arguments     a list of arguments
     * @param type          a type
     * @param conditionContainer     a conditionContainer
     * @param labelType     a label type
     */
    public SubCommandCategoryImpl(List<ArgumentImpl> arguments, SenderType type, ConditionContainerImpl conditionContainer, LabelType labelType, EventPriority priority, Function<CommandSender, Object> senderConverter) {
        super(arguments, type, conditionContainer, senderConverter);
        this.labelType = labelType == null ? LabelType.getDefault() : labelType;
        this.priority = priority == null ? EventPriority.NORMAL : priority;
    }

}
