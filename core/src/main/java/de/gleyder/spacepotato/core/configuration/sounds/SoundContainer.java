package de.gleyder.spacepotato.core.configuration.sounds;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@Getter
@AllArgsConstructor
public class SoundContainer {

    private final Sound sound;
    private final SoundCategory category;
    private float volume, pitch;

}
