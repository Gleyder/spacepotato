package de.gleyder.spacepotato.core.gui;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import de.gleyder.spacepotato.core.gui.impl.GUIInventoryRegistry;

/**
 * Creates an instance of an gui inventory
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface InventoryInstanceCreator<T> {

    T create(SpacePotatoRegistry registry, GUIInventoryRegistry guiInventoryRegistry, ClickableInventoryRegistry inventoryRegistry);

}
