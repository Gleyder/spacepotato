package de.gleyder.spacepotato.core.gui.exceptions;

import de.gleyder.spacepotato.core.utils.exceptions.BaseRuntimeException;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GUIInventoryRegistryException extends BaseRuntimeException {

    public GUIInventoryRegistryException(String message, Object... objects) {
        super(message, objects);
    }

}
