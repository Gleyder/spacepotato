package de.gleyder.spacepotato.core.command.entities;

import de.gleyder.spacepotato.core.command.enums.LabelType;
import org.bukkit.event.EventPriority;

/**
 * Represents a sub command category
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.command.entities.CommandPart
 */
public interface SubCommandCategory extends CommandPart {

    LabelType getLabelType();
    EventPriority getPriority();

}
