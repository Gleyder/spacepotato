package de.gleyder.spacepotato.core.utils.string;

import lombok.NonNull;

import java.util.HashMap;

/**
 * Replaces a key with a value.
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.utils.string.SuppliedString
 */
@SuppressWarnings({"UnusedReturnValue"})
public class KeyReplacer extends SuppliedString {

    private final HashMap<String, String> hashMap;

    /**
     * Creates a KeReplacer
     *
     * @param rootString    a object
     */
    public KeyReplacer(@NonNull String rootString) {
        super(rootString);
        this.hashMap = new HashMap<>();
    }

    /**
     * Replaces the first key in order
     *
     * @param key       a key
     * @param value    a value
     * @return          this
     */
    public KeyReplacer replace(@NonNull String key, @NonNull Object value) {
        this.hashMap.put(key, value.toString());
        return this;
    }

    /**
     * Builds the string
     *
     * @return      a String
     */
    public String build(Order order) {
        switch (order) {
            case KEY_REPLACER_FIRST:
                this.replaceRootString();
                return super.build();
            case SUPPLIED_STRING_FIRST:
                super.build();
                return this.replaceRootString();
        }
        return this.rootString;
    }

    @Override
    public String build() {
        return this.build(Order.KEY_REPLACER_FIRST);
    }

    /**
     * Replaces the keys with values
     *
     * @return      a string
     */
    private String replaceRootString() {
        this.hashMap.forEach((key, value) -> this.rootString = this.rootString.replace(this.prefix + key + this.suffix, value));
        return this.rootString;
    }

    /**
     * Builds the string
     *
     * @return      a String
     */
    @Override
    public String toString() {
        return this.build();
    }

    public enum Order {

        /**
         * Indicates that the strings if first supplied via the SuppliedString (replaces {} with values)
         * After that is replaces keys with values ({key} replace with value)
         */
        SUPPLIED_STRING_FIRST,

        /**
         * Vice versa with SUPPLIED_STRING_FIRST
         */
        KEY_REPLACER_FIRST
    }

}