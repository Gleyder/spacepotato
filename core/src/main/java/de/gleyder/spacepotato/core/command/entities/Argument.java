package de.gleyder.spacepotato.core.command.entities;

import de.gleyder.spacepotato.core.command.enums.ArgumentType;

/**
 * Represents an argument
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface Argument {

    ArgumentType getType();

}
