package de.gleyder.spacepotato.core.command.entities;

import de.gleyder.spacepotato.core.analyzer.QueryResult;
import de.gleyder.spacepotato.core.analyzer.QueryType;
import de.gleyder.spacepotato.core.command.interfaces.Condition;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.command.CommandSender;

import java.util.List;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface ConditionContainer {

    Result test(CommandSender sender);
    List<Condition> getConditions();
    QueryType getType();

    @AllArgsConstructor
    class Result {

        @Getter
        private final boolean successes;

        private final List<QueryResult<Condition>> queryResults;

        public void sendMessage(CommandSender sender) {
            for (QueryResult<Condition> queryResult : this.queryResults) {
                if (queryResult.isSuccesses())
                    continue;

                sender.sendMessage(queryResult.getContent().getMessage());
            }
        }

    }

}
