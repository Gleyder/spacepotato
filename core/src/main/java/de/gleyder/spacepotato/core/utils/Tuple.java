package de.gleyder.spacepotato.core.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@Getter
@AllArgsConstructor
public class Tuple<V, O> {

    private final V value;
    private final O otherValue;

}
