package de.gleyder.spacepotato.core.clickable.inventory;

import lombok.AllArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Listener for clickable inventories.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class ClickableInventoryListener implements Listener {

    private final ClickableInventoryRegistry registry;

    /**
     * Called when a player clicks an item
     *
     * @param event     an InventoryClickEvent
     */
    @EventHandler
    public void onInventoryInteract(InventoryClickEvent event) {
        if (event.getClickedInventory() == null)
            return;

        ClickableInventory clickableInventory = this.registry.get(event.getClickedInventory());
        if (clickableInventory == null)
            return;

        Player player = (Player) event.getWhoClicked();
        ItemStack item = event.getCurrentItem();
        if (item == null)
            return;

        clickableInventory.getActions().forEach(clickableItem -> {
            if (clickableItem.getType() == ClickableInventoryItem.Type.STACK) {
                if (!clickableItem.getItemStack().equals(item))
                    return;
            } else {
                if (clickableItem.getMaterial() != item.getType())
                    return;
            }

            boolean cancel = clickableItem.onClick(player, event.getClickedInventory(), item, event.getClick());
            event.setCancelled(cancel);
        });
    }

}
