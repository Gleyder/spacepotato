package de.gleyder.spacepotato.core.interfaces;

/**
 * Interface for builders.
 *
 * @param <T>       the object to build
 * @author Gleyder
 */
public interface IBuilder<T> {

    /**
     * Builds the object
     *
     * @return      the object
     */
    T build();

}
