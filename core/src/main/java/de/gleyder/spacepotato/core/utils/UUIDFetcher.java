package de.gleyder.spacepotato.core.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.NonNull;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 * Fetcher for uuids
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public final class UUIDFetcher {

    private static final String UUID_URL = "https://api.mojang.com/users/profiles/minecraft/%s?at=%d";
    private static final String NAME_URL = "https://api.mojang.com/user/profiles/%s/names";

    private static Gson gson = new GsonBuilder().registerTypeAdapter(UUID.class, new UUIDTypeAdapter()).create();
    private static ExecutorService pool = Executors.newCachedThreadPool();
    private static Map<UUID, String> nameCache;
    private static Map<String, UUID> uuidCache;

    private String name;
    private UUID id;

    static {
        uuidCache = new HashMap<>();
        nameCache = new HashMap<>();
    }

    private UUIDFetcher() {}

    /**
     * Fetches the uuid asynchronously and passes it to the consumer
     *
     * @param name      The username
     * @param action    Task to do with the uuid
     */
    public static void getUUID(@NonNull String name, @NonNull Consumer<UUID> action) {
        pool.execute(() -> action.accept(getUUID(name)));
    }

    /**
     * Fetches the uuid synchronously and returns it
     *
     * @param name  The name
     * @return      The uuid
     */
    public static UUID getUUID(@NonNull String name) {
        return getUUIDAt(name, System.currentTimeMillis());
    }

    /**
     * Fetches the uuid asynchronously for a specified time and
     * passes the result to the consumer
     *
     * @param name          The name
     * @param timestamp     Time when the player had this name in milliseconds
     * @param action        Task to do with the uuid
     */
    public static void getUUIDAt(@NonNull String name, long timestamp, @NonNull Consumer<UUID> action) {
        pool.execute(() -> action.accept(getUUIDAt(name, timestamp)));
    }

    /**
     * Fetches the uuid synchronously for a specified name and time
     *
     * @param name      The name
     * @param timestamp Time when the player had this name in milliseconds
     */
    public static UUID getUUIDAt(@NonNull String name, long timestamp) {
        name = name.toLowerCase();
        if (uuidCache.containsKey(name)) {
            return uuidCache.get(name);
        }
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(String.format(UUID_URL, name, timestamp/1000)).openConnection();
            connection.setReadTimeout(5000);
            UUIDFetcher data = gson.fromJson(new BufferedReader(new InputStreamReader(connection.getInputStream())), UUIDFetcher.class);
            uuidCache.put(name, data.id);
            nameCache.put(data.id, data.name);
            return data.id;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Fetches the name asynchronously and passes it to the consumer
     *
     * @param uuid      The uuid
     * @param action    Task to do with the uuid
     */
    public static void getName(@NonNull UUID uuid, @NonNull Consumer<String> action) {
        pool.execute(() -> action.accept(getName(uuid)));
    }

    /**
     * Fetches the name synchronously and returns it
     *
     * @param uuid  The uuid
     * @return      The name
     */
    public static String getName(@NonNull UUID uuid) {
        if (nameCache.containsKey(uuid)) {
            return nameCache.get(uuid);
        }
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(String.format(NAME_URL, UUIDTypeAdapter.fromUUID(uuid))).openConnection();
            connection.setReadTimeout(5000);
            UUIDFetcher[] nameHistory = gson.fromJson(new BufferedReader(new InputStreamReader(connection.getInputStream())), UUIDFetcher[].class);
            UUIDFetcher currentNameData = nameHistory[nameHistory.length - 1];
            uuidCache.put(currentNameData.name.toLowerCase(), uuid);
            nameCache.put(uuid, currentNameData.name);
            return currentNameData.name;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}