package de.gleyder.spacepotato.core.command.supplier;

import de.gleyder.spacepotato.core.command.entities.Argument;
import de.gleyder.spacepotato.core.command.entities.MutableArgument;
import de.gleyder.spacepotato.core.command.enums.ArgumentType;
import de.gleyder.spacepotato.core.command.enums.InterpreterMode;
import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.input.InputArgument;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import de.gleyder.spacepotato.core.utils.string.ListSuppliedString;
import lombok.Getter;
import org.apache.commons.lang.ClassUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Gets information from an input argument list
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ArgumentSupplier {

    @Getter
    private final SupplierValuesImpl supplierValuesImpl;
    
    public ArgumentSupplier() {
        this.supplierValuesImpl = new SupplierValuesImpl();
    }

    /**
     * Supplies the command
     *
     * @param inputArgumentList     the input arguments
     * @param argumentList          the sub command's arguments
     */
    public void supply(List<InputArgument> inputArgumentList, List<Argument> argumentList) {
        this.supplierValuesImpl.clear();

        /*
         * Counter to count for which method parameter slot the value is.
         * Starts at 1 because the sender is always 0 (so first)
         */
        for (int i = 0; i < argumentList.size(); i++) {
            Argument argument = argumentList.get(i);
            /*
             * StaticArguments are skipped because as these are static
             * you can't get any valuable information from them.
             */
            if (argument.getType() == ArgumentType.STATIC)
                continue;

            //Gets the current input and mutable argument
            InputArgument inputArgument = inputArgumentList.get(i);
            MutableArgument mutableArgument = (MutableArgument) argument;

            if (inputArgument.getInputs().size() > 1 && !mutableArgument.areMultiArgumentsAllowed()) {
                this.supplierValuesImpl.add("error#" + i, new InterpreterException(CoreMessages.COMMAND_ARGUMENT$SUPPLIER_MULTI$ARGS$NOT$ALLOWED.getMessage(true, inputArgument.getInputs())));
                continue;
            }

            /*
             * In this list all translated arguments are added, even exceptions
             */
            List<Object> objectList = new ArrayList<>();

            if (mutableArgument.getMode() == InterpreterMode.MERGED) {
                String string = ListSuppliedString.fromList(inputArgument.getInputs());
                inputArgument.getInputs().clear();
                inputArgument.getInputs().add(string);
            }

            // Loops through all input argument inputs and tries to translate them
            for (String input : inputArgument.getInputs()) {
                try {
                    Object object = mutableArgument.getInterpreter().translate(input);

                    if (object == null) {
                        objectList.add(new NullPointerException(CoreMessages.COMMAND_INTERPRETER_RETURNS$NULL.getKeyableMessage(true)
                                .replace("interpreter", ClassUtils.getShortClassName(mutableArgument.getInterpreter().getClass()))
                                .build())
                        );
                        continue;
                    }

                    object = mutableArgument.getConverter().apply(object);

                    mutableArgument.getOption().test(object);

                    objectList.add(object);
                } catch (Exception e) {
                    objectList.add(e);
                }
            }

            /*
             * Puts the values into the supplied objects.
             * If the list only contains one value, the raw argument is inserted, otherwise the complete list
             *
             * The key is a number of no key is set, otherwise the key
             */
            if (mutableArgument.getKey().isEmpty()) {
                this.supplierValuesImpl.add(objectList.size() == 1 ? objectList.get(0) : objectList);
            } else {
                this.supplierValuesImpl.add(mutableArgument.getKey(), objectList.size() == 1 ? objectList.get(0) : objectList);
            }
        }
    }

    public void addValues(SupplierValuesImpl supplierValuesImpl) {
        this.supplierValuesImpl.merge(supplierValuesImpl);
    }

    public List<Exception> getExceptions() {
        return this.supplierValuesImpl.values()
                .stream()
                .filter(object -> object instanceof Exception)
                .map(object -> (Exception) object)
                .collect(Collectors.toList());
    }

    public boolean hasExceptions() {
        return this.hasExceptions(this.supplierValuesImpl.values());
    }

    private boolean hasExceptions(Iterable<Object> iterable) {
        for (Object value : iterable) {
            if (value instanceof List) {
                //noinspection unchecked
                List<Object> objectList = (List) value;

                for (Object object : objectList) {
                    if (object instanceof Exception)
                        return true;
                }
            } else {
                if (value instanceof Exception)
                    return true;
            }
        }
        return false;
    }
    
}
