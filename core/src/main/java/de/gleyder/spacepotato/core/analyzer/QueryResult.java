package de.gleyder.spacepotato.core.analyzer;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Stores information about a query result
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@Getter
@AllArgsConstructor
public class QueryResult<T> {

    private final T content;
    private final boolean successes;

}
