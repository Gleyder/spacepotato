package de.gleyder.spacepotato.core.command.options;

import de.gleyder.spacepotato.core.command.enums.OptionType;
import de.gleyder.spacepotato.core.command.exceptions.OptionException;
import de.gleyder.spacepotato.core.command.interfaces.Option;

public class StringOption implements Option<String> {

    private final String[] strings;
    private final OptionType type;
    private final boolean caseSensitive;

    public StringOption(OptionType type, String... strings) {
        this(type, false, strings);
    }

    public StringOption(OptionType type, boolean caseSensitivity, String... strings) {
        this.strings = strings;
        this.type = type;
        this.caseSensitive = caseSensitivity;
    }

    @Override
    public void test(String argument) throws OptionException {
        switch (this.type) {
            case ALLOWED:
                for (String string : this.strings) {
                    if (this.compare(string, argument))
                        return;
                }
                throw new OptionException("§c" + argument + " is disallowed");
            case DISALLOWED:
                for (String string : this.strings) {
                    if (this.compare(string, argument))
                        throw  new OptionException("§c" + argument + " is disallowed");
                }
                break;
        }
    }

    private boolean compare(String str, String otherStr) {
        if (this.caseSensitive) {
            return str.equals(otherStr);
        } else {
            return str.equalsIgnoreCase(otherStr);
        }
    }

}
