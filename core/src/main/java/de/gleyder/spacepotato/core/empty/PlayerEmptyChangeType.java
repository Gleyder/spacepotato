package de.gleyder.spacepotato.core.empty;

public enum PlayerEmptyChangeType {
    JOIN, LEAVE
}
