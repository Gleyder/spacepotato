package de.gleyder.spacepotato.core.utils;

import java.util.HashMap;
import java.util.List;

/**
 * Utils for Maps
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class MapUtils {

    /**
     * Merges two list to one map
     *
     * @param list          a list
     * @param otherList     a list
     * @return              a map
     */
    public static <T1, T2> HashMap<T1, T2> combine(List<T1> list, List<T2> otherList) {
        HashMap<T1, T2> hashMap = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            T1 t1 = list.get(i);
            T2 t2 = otherList.get(i);
            if (t2 == null)
                break;
            hashMap.put(t1, t2);
        }
        return hashMap;
    }

}
