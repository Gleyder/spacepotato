package de.gleyder.spacepotato.core.gui.impl;

import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import lombok.AllArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

/**
 * Listeners the removes clickable inventories
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class GUIInventoryListener implements Listener {

    private final GUIInventoryRegistry registry;
    private final ClickableInventoryRegistry clickableInventoryRegistry;

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if (!(event.getPlayer() instanceof Player))
            return;
        if (!this.clickableInventoryRegistry.contains(event.getInventory()))
            return;
        Player player = (Player) event.getPlayer();


        System.out.println("Event! " + event.getReason() + " " + registry);
        this.registry.getAll().forEach(guiInventory -> ((GUIInventoryImpl) guiInventory).getActiveInventoryManager().unregister(player));
    }

}
