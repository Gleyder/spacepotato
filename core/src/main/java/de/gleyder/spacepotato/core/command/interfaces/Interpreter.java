package de.gleyder.spacepotato.core.command.interfaces;

import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interpreters.StringInterpreter;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Interface for Interpreter.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Describable
 */
public interface Interpreter<T> extends Describable {

    T translate(String argument) throws InterpreterException;

    Class<T> getClassType();

    default List<String> getTabCompletion() {
        return new ArrayList<>();
    }

    default String getTabDescription() {
        return TabCompletionsHelper.getAsSingleTypeString(this.getClassType().getSimpleName());
    }

    default Supplier<List<String>> getDynamicTabCompletion() {
        return ArrayList::new;
    }

    @Override
    default String getDescription() {
        return "No Description";
    }

    @SuppressWarnings("unchecked")
    static Interpreter<Object> getDefault() {
        return (Interpreter) new StringInterpreter();
    }

}
