package de.gleyder.spacepotato.core.builder.item;

import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.NonNull;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PersistentDataBuilder implements IBuilder<ItemStack> {

    private final PersistentDataContainer dataContainer;
    private final ItemStack itemStack;
    private final ItemMeta itemMeta;
    private final JavaPlugin plugin;

    public PersistentDataBuilder(ItemStack itemStack, boolean clone, JavaPlugin plugin) {
        this.itemStack = clone ? itemStack.clone() : itemStack;
        this.itemMeta = this.itemStack.getItemMeta();
        this.dataContainer = this.itemMeta.getPersistentDataContainer();
        this.plugin = plugin;
    }

    /**
     * Adds a map of custom tags
     *
     * @param customTagMap      a map of custom tags
     * @return                  this builder
     */
    public PersistentDataBuilder addCustomTags(@NonNull HashMap<String, String> customTagMap) {
        customTagMap.forEach(this::setCustomTag);
        return this;
    }

    public PersistentDataBuilder setCustomTag(@NonNull String key, @NonNull String value) {
        this.setCustomTag(key, value, PersistentDataType.STRING);
        return this;
    }

    /**
     * Sets a custom tag
     *
     * @param key       the key
     * @param value     the value
     * @return
     */
    public PersistentDataBuilder setCustomTag(@NonNull String key, @NonNull Object value, @NonNull PersistentDataType dataType) {
        this.dataContainer.set(this.getNamespace(key), dataType, value);
        return this;
    }

    /**
     * Removes a custom tag
     *
     * @param key       the key
     * @return          the value
     */
    public PersistentDataBuilder removeCustomTag(@NonNull String key) {
        this.dataContainer.remove(this.getNamespace(key));
        return this;
    }

    public ItemBuilder toItemBuilder() {
        return new ItemBuilder(this.itemStack, false);
    }

    @Override
    public ItemStack build() {
        this.itemStack.setItemMeta(this.itemMeta);
        return this.itemStack;
    }

    private NamespacedKey getNamespace(@NotNull String key) {
        return new NamespacedKey(this.plugin, key);
    }

}

