package de.gleyder.spacepotato.core.utils;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * A position contains a y and x value.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@ToString
@AllArgsConstructor
@EqualsAndHashCode
public class Position {

    @Getter
    private int x, y;

    /**
     * Creates a Position
     */
    public Position() {
        this(0);
    }

    /**
     * Creates a Position
     *
     * @param xy        the x and y value
     */
    public Position(int xy) {
        this.x = xy;
        this.y = xy;
    }

    /**
     * Adds a position
     *
     * @param position      position
     * @return              this position
     */
    public Position add(Position position) {
        this.x += position.getX();
        this.y += position.getY();
        return this;
    }

    /**
     * Subtracts a position
     *
     * @param position      position
     * @return              this position
     */
    public Position subtract(Position position) {
        this.x -= position.getX();
        this.y -= position.getY();
        return this;
    }

    /**
     * Multiplies a position
     *
     * @param position      position
     * @return              this position
     */
    public Position multiply(Position position) {
        this.x *= position.getX();
        this.y *= position.getY();
        return this;
    }

    /**
     * Divides a position
     *
     * @param position      position
     * @return              this position
     */
    public Position divide(Position position) {
        this.x /= position.getX();
        this.y /= position.getY();
        return this;
    }

    /**
     * Sets the x position
     *
     * @param x     an integer
     * @return      this position
     */
    public Position setX(int x) {
        this.x = x;
        return this;
    }

    /**
     * Sets the y position
     *
     * @param y     an integer
     * @return      this position
     */
    public Position setY(int y) {
        this.y = y;
        return this;
    }

    /**
     * Sets the position
     *
     * @param position      a position
     * @return              this position
     */
    public Position set(Position position) {
        this.x = position.getX();
        this.y = position.getY();
        return this;
    }

    /**
     * Duplicates this position
     *
     * @return      the duplicated position
     */
    public Position duplicate() {
        return new Position(this.x, this.y);
    }

}
