package de.gleyder.spacepotato.core.clickable.hotbar.interfaces;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

/**
 * Interfaces for HotBarClickActions.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface HotbarClickAction {

    /**
     * Fired of a players clicks if block or in the air
     *
     * @param player            a player
     * @param clickedBlock      a clicked block
     *                          {null} if no block was clicked
     * @param action            an action
     * @return                  if the event should be cancelled
     */
    boolean onClick(Player player, Block clickedBlock, Action action);

}
