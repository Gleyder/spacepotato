package de.gleyder.spacepotato.core.clickable.block.interfaces;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

/**
 * Interface for BlockClickActions
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface BlockClickAction {

    /**
     * Executed if a player clicks a block
     *
     * @param player        a player
     * @param block         a block
     * @param action        an action
     * @return              if the event should be cancelled
     */
    boolean onClick(Player player, Block block, Action action);

}
