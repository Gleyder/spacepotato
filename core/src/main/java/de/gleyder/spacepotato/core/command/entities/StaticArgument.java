package de.gleyder.spacepotato.core.command.entities;

import de.gleyder.spacepotato.core.command.interfaces.Labeled;

/**
 * Represents a static argument
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.command.entities.Argument
 * @see de.gleyder.spacepotato.core.command.interfaces.Labeled
 */
public interface StaticArgument extends Argument, Labeled {

}
