package de.gleyder.spacepotato.core.clickable.inventory;

import de.gleyder.spacepotato.core.clickable.inventory.interfaces.EmptyItemModifyAction;
import de.gleyder.spacepotato.core.clickable.inventory.interfaces.InventoryClickAction;
import de.gleyder.spacepotato.core.clickable.inventory.interfaces.ItemModifyAction;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 */
public class ClickableInventoryItem {

    private final Object item;

    @Setter
    @Getter
    private InventoryClickAction action;

    @Getter
    private final Type type;

    @Setter
    @Getter
    private ItemModifyAction itemModifyAction;

    public ClickableInventoryItem(@NotNull InventoryClickAction action, ItemModifyAction itemModifyAction, @NotNull ItemStack item) {
        this(action, item);
        this.itemModifyAction = itemModifyAction == null ? new EmptyItemModifyAction() : itemModifyAction;
    }

    public ClickableInventoryItem(@NotNull InventoryClickAction action, @NotNull ItemStack item) {
        this.action = action;
        this.item = item;
        this.type = Type.STACK;
        this.itemModifyAction = new EmptyItemModifyAction();
    }

    public ClickableInventoryItem(@NotNull InventoryClickAction action, @NotNull Material material) {
        this.action = action;
        this.item = material;
        this.type = Type.MATERIAL;
        this.itemModifyAction = new EmptyItemModifyAction();
    }

    public final boolean onClick(Player player, Inventory inventory, ItemStack itemStack, ClickType clickType) {
        return this.getAction().onClick(player, inventory, itemStack, clickType);
    }

    public ItemStack getItemStack() {
        if (this.getType() == Type.MATERIAL)
            throw new IllegalStateException("Item is a Material and not an ItemStack");
        return (ItemStack) this.item;
    }

    public Material getMaterial() {
        if (this.getType() == Type.STACK)
            throw new IllegalStateException("Item is an ItemStack and not a Material");
        return (Material) this.item;
    }

    public ItemStack getItem() {
        if (this.type == Type.STACK)
            return this.getItemStack();
        else
            return new ItemStack(this.getMaterial());
    }

    public enum Type {
        MATERIAL, STACK
    }

    public boolean equals(ClickableInventoryItem obj) {
        switch (this.getType()) {
            case MATERIAL:
                return obj.getMaterial() == obj.getMaterial();
            case STACK:
                return obj.getItemStack().equals(obj.getItemStack());
        }
        return false;
    }

}
