package de.gleyder.spacepotato.core.builder.command;

import de.gleyder.spacepotato.core.command.entities.SubCommand;
import de.gleyder.spacepotato.core.command.entities.SubCommandCategory;
import de.gleyder.spacepotato.core.command.impl.SubCommandCategoryImpl;
import de.gleyder.spacepotato.core.command.impl.SubCommandImpl;
import de.gleyder.spacepotato.core.command.logic.CommandAdapter;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * Builder for SubCommands.
 * The same instance can be reused after calling build() or buildAndAdd().
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.builder.command.AbstractCommandPartBuilder
 */
public class SubCommandBuilder extends AbstractCommandPartBuilder<SubCommand, SubCommandBuilder> {

    private final CommandAdapter adapter;
    private final TreeSet<SubCommandCategoryImpl> categories;
    private String description;

    /**
     * Creates the SubCommandBuilder
     *
     * @param adapter       an adapter
     */
    public SubCommandBuilder(CommandAdapter adapter) {
        this.adapter = adapter;
        this.categories = new TreeSet<>(Comparator.comparingInt(o -> o.getPriority().getSlot()));
    }

    /**
     * Sets the description
     *
     * @param description       a description
     * @return                  this builder
     */
    public SubCommandBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Sets the description
     *
     * @param categories     an array ofcategories
     * @return               this builder
     */
    public SubCommandBuilder addCategories(SubCommandCategory... categories) {
        for (SubCommandCategory category : categories) {
            this.categories.add((SubCommandCategoryImpl) category);
        }
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void reset() {
        super.reset();
        this.description = null;
        this.categories.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubCommand build() {
        SubCommandImpl subCommand = new SubCommandImpl(
                this.argumentList,
                this.senderType,
                this.categories,
                this.conditionContainer,
                this.description,
                this.senderConverter
        );
        this.tryAutoSetMethodName();
        subCommand.getExecutor().setMethodName(this.methodName);
        this.reset();
        return subCommand;
    }

    /**
     * Builds an adds it the command
     */
    public void buildAndAdd() {
        this.adapter.getSubCommandList().add(this.build());
    }

}
