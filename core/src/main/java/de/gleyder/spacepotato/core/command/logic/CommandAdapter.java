package de.gleyder.spacepotato.core.command.logic;

import de.gleyder.spacepotato.core.builder.command.SubCommandBuilder;
import de.gleyder.spacepotato.core.builder.command.SubCommandCategoryBuilder;
import de.gleyder.spacepotato.core.command.entities.SubCommand;
import de.gleyder.spacepotato.core.command.impl.SubCommandImpl;
import de.gleyder.spacepotato.core.command.interfaces.Representable;
import de.gleyder.spacepotato.core.command.tab.TabCompletionContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a command
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Representable
 */
public abstract class CommandAdapter implements Representable {

    private final static SubCommandCategoryBuilder CATEGORY_BUILDER;

    static {
        CATEGORY_BUILDER = new SubCommandCategoryBuilder();
    }

    protected final String label;
    protected final String description;

    private final TabCompletionContainer tabCompletionContainer;
    private final SubCommandBuilder builder;

    @SuppressWarnings("WeakerAccess")
    protected final List<String> aliases;

    @SuppressWarnings("WeakerAccess")
    protected final List<SubCommand> subCommandList;

    /**
     * Creates a CommandAdapter
     *
     * @param label     a label
     */
    public CommandAdapter(String label, String description, List<String> aliases) {
        this.label = label;
        this.description = description;
        this.aliases = aliases;

        this.subCommandList = new ArrayList<>();
        this.tabCompletionContainer = new TabCompletionContainer();
        this.builder = new SubCommandBuilder(this);
    }

    public CommandAdapter(String label) {
        this(label, "No Description", new ArrayList<>());
    }

    /**
     * Defines the commands
     */
    public abstract void define();

    /**
     * Generates the tab completions
     */
    void generate() {
        this.subCommandList.forEach(this.tabCompletionContainer::add);
    }

    /*
     * -------------------------------
     *
     *              Getter
     *
     * -------------------------------
     */

    @SuppressWarnings("unchecked")
    final List<SubCommandImpl> getSubCommandsImpl() {
        return (List) this.subCommandList;
    }

    protected final SubCommandCategoryBuilder getCategoryBuilder() {
        return CATEGORY_BUILDER;
    }

    @Override
    public final String getLabel() {
        return this.label;
    }

    @Override
    public final String getDescription() {
        return this.description;
    }

    public final List<SubCommand> getSubCommandList() {
        return this.subCommandList;
    }

    @SuppressWarnings("WeakerAccess")
    public final TabCompletionContainer getTabCompletionContainer() {
        return this.tabCompletionContainer;
    }

    public final SubCommandBuilder getBuilder() {
        return this.builder;
    }

    public final List<String> getAliases() {
        return this.aliases;
    }

}
