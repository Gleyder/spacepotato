package de.gleyder.spacepotato.core.configuration.messages;

import lombok.AllArgsConstructor;

import java.util.HashMap;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class MessageFile {

    private final HashMap<String, String> messageMap;

    public String getMessage(String path) {
        return this.messageMap.get(path);
    }
}
