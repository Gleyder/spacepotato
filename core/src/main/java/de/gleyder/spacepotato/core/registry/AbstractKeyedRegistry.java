package de.gleyder.spacepotato.core.registry;

import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Abstract implementation for a keyed registry
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class AbstractKeyedRegistry<K, V> implements KeyedRegistry<K, V> {

    @Getter
    private final HashMap<K, V> map;

    public AbstractKeyedRegistry() {
        this.map = new HashMap<>();
    }

    @Override
    public void register(Iterable<V> iterable) {
        iterable.forEach(this::register);
    }

    @Override
    public void unregister(Iterable<K> iterable) {
        iterable.forEach(this::unregister);
    }

    @Override
    public void unregister(K k) {
        this.map.remove(k);
    }

    @Override
    public V get(K k) {
        return this.map.get(k);
    }

    @Override
    public void unregisterAll() {
        this.map.clear();
    }

    @Override
    public List<V> getAll() {
        return new ArrayList<>(this.map.values());
    }

    @Override
    public boolean contains(K k) {
        return this.map.containsKey(k);
    }

}
