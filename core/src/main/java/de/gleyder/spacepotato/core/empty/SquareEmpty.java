package de.gleyder.spacepotato.core.empty;

import de.gleyder.spacepotato.core.empty.excpetions.LocationsNotInSameWorldException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Base class for square empties
 *
 * @author Gleyder
 * @version 0.1
 */
public abstract class SquareEmpty extends Empty {

    private final HashMap<Character, CoordinateRange> rangeMap;

    @Getter
    private double density;

    /**
     * Creates a square empty
     *
     * @param loc1      a location
     * @param loc2      an other location
     * @param density   the density of the outline
     */
    @SuppressWarnings("WeakerAccess")
    public SquareEmpty(@NonNull Location loc1, @NonNull Location loc2, double density) {
        super(loc1.getWorld());
        this.density = density;
        if (!loc1.getWorld().equals(loc2.getWorld())) {
            throw new LocationsNotInSameWorldException("Locations are not in the same world");
        }
        this.rangeMap = new HashMap<>();
        if (loc1.getX() < loc2.getX()) {
            this.rangeMap.put('x', new CoordinateRange(loc1.getX(), loc2.getX()));
        } else {
            this.rangeMap.put('x' , new CoordinateRange(loc2.getX(), loc1.getX()));
        }

        if (loc1.getY() < loc2.getY()) {
            this.rangeMap.put('y', new CoordinateRange(loc1.getY(), loc2.getY()));
        } else {
            this.rangeMap.put('y', new CoordinateRange(loc2.getY(), loc1.getY()));
        }

        if (loc1.getZ() < loc2.getZ()) {
            this.rangeMap.put('z', new CoordinateRange(loc1.getZ(), loc2.getZ()));
        } else {
            this.rangeMap.put('z', new CoordinateRange(loc2.getZ(), loc1.getZ()));
        }
    }

    /**
     * Creates a square empty
     *
     * @param loc1      a location
     * @param loc2      an other location
     */
    public SquareEmpty(@NonNull Location loc1, @NonNull Location loc2) {
        this(loc1, loc2, 1);
    }

    /**
     * Called when the outline should be calculated
     */
    @Override
    public final void calculateOutline() {
        this.markedPoints.clear();
        /*
         * Calculates the walls
         */
        for (double z = this.getRangeZ().getMin(); z < this.getRangeZ().getMax(); z+=this.density) {
            Set<Vector> vectorSet = new HashSet<>();

            for (double y = this.getRangeY().getMin(); y < this.getRangeY().getMax(); y+=this.density) {
                vectorSet.add(new Vector(this.getRangeX().getMin(), y, z));
            }

            double deltaX = this.getRangeX().getMax() - this.getRangeX().getMin() - this.density;
            vectorSet.forEach(vector -> {
                Vector newVector = vector.clone();
                newVector.add(new Vector(deltaX, 0, 0));
                this.markedPoints.add(vector);
                this.markedPoints.add(newVector);
            });
        }

        /*
         * Calculates the walls
         */
        for (double x = this.getRangeX().getMin()+this.density; x < this.getRangeX().getMax()-this.density; x+=this.density) {
            Set<Vector> vectorSet = new HashSet<>();

            for (double y = this.getRangeY().getMin(); y < this.getRangeY().getMax(); y+=this.density) {
                vectorSet.add(new Vector(x, y, this.getRangeZ().getMin()));
            }

            double deltaZ = this.getRangeZ().getMax() - this.getRangeZ().getMin() - this.density;
            vectorSet.forEach(vector -> {
                Vector newVector = vector.clone();
                newVector.add(new Vector(0, 0, deltaZ));
                this.markedPoints.add(vector);
                this.markedPoints.add(newVector);
            });
        }


        /*
         * Calculates the floor and the ceiling
         */
        for (double x = this.getRangeX().getMin()+this.density; x < this.getRangeX().getMax()-this.density; x+=this.density) {
            Set<Vector> vectorSet = new HashSet<>();

            for (double z = this.getRangeZ().getMin()+this.density; z < this.getRangeZ().getMax()-this.density; z+=this.density) {
                vectorSet.add(new Vector(x, this.getRangeY().getMin(), z));
            }

            double deltaY = this.getRangeY().getMax() - this.getRangeY().getMin() - this.density;
            vectorSet.forEach(vector -> {
                Vector newVector = vector.clone();
                newVector.add(new Vector(0, deltaY, 0));
                this.markedPoints.add(vector);
                this.markedPoints.add(newVector);
            });
        }
    }

    /**
     * Checks if player joined or left the empty
     *
     * @param playerLocation        the player's location
     * @return                      a PlayerEmptyChangeType
     */
    @Override
    public final PlayerEmptyChangeType check(Location playerLocation) {
        /*
         * Checks if a player is in the x, y, and z range
         */
        if (!(playerLocation.getX() >= this.getRangeX().getMin() && playerLocation.getX() <= this.getRangeX().getMax())) {
            return PlayerEmptyChangeType.LEAVE;
        }
        if (!(playerLocation.getY() >= this.getRangeY().getMin() && playerLocation.getY() <= this.getRangeY().getMax())) {
            return PlayerEmptyChangeType.LEAVE;
        }
        if (!(playerLocation.getZ() >= this.getRangeZ().getMin() && playerLocation.getZ() <= this.getRangeZ().getMax())) {
            return PlayerEmptyChangeType.LEAVE;
        }
        return PlayerEmptyChangeType.JOIN;
    }

    @SuppressWarnings("WeakerAccess")
    public CoordinateRange getRangeX() {
        return this.rangeMap.get('x');
    }

    @SuppressWarnings("WeakerAccess")
    public CoordinateRange getRangeY() {
        return this.rangeMap.get('y');
    }

    @SuppressWarnings("WeakerAccess")
    public CoordinateRange getRangeZ() {
        return this.rangeMap.get('z');
    }

    public void setDensity(double density) {
        this.density = density;
        this.calculateOutline();
    }

    @AllArgsConstructor
    @Getter
    private static class CoordinateRange {
        private final double min, max;
    }

}
