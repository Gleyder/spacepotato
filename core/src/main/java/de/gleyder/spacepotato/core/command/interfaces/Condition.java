package de.gleyder.spacepotato.core.command.interfaces;

import org.bukkit.command.CommandSender;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface Condition<S extends CommandSender> {

    boolean test(S sender);
    String getMessage();

}
