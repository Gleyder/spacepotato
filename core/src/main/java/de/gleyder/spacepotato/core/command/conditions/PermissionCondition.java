package de.gleyder.spacepotato.core.command.conditions;

import de.gleyder.spacepotato.core.command.interfaces.Condition;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import org.bukkit.entity.Player;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PermissionCondition implements Condition<Player> {

    private final String permission;

    public PermissionCondition(String permission) {
        this.permission = permission;
    }

    @Override
    public boolean test(Player player) {
        if (this.permission.equalsIgnoreCase("vanilla.op")) {
            return player.isOp();
        } else if (!this.permission.isEmpty()) {
            return player.hasPermission(this.permission);
        } else {
            return true;
        }
    }

    @Override
    public String getMessage() {
        return CoreMessages.COMMAND_BASE_NO$PERM.getMessage(true);
    }

}
