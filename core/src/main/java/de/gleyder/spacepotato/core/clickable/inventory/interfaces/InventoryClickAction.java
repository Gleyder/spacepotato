package de.gleyder.spacepotato.core.clickable.inventory.interfaces;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

/**
 * Interface for an InventoryClickAction.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface InventoryClickAction {

    /**
     * Called of a player clicks an item in an inventory
     *
     * @param player            a Player
     * @param inventory         an Inventory
     * @param item              an ItemStack
     * @param clickType         a ClickType
     * @return                  if the event should be cancelled
     */
    boolean onClick(@NotNull Player player, @NotNull Inventory inventory, @NotNull ItemStack item, @NotNull ClickType clickType);

}
