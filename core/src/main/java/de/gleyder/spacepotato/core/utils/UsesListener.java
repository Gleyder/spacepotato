package de.gleyder.spacepotato.core.utils;

/**
 * Indicates that a class uses or needs a listener to work.
 * UnregisterListener unregisters the associated listener
 * to free the object that it can be garbage collected
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface UsesListener {

    /**
     * Unregisters all uses listeners
     */
    void unregisterListeners();

}
