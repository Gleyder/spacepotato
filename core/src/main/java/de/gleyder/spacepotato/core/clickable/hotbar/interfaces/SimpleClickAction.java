package de.gleyder.spacepotato.core.clickable.hotbar.interfaces;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

/**
 * Simple implementation for a HotbarClickAction
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class SimpleClickAction implements HotbarClickAction {

    /**
     * Splits the input in their own methods
     *
     * @param player            a player
     * @param clickedBlock      a clicked block
     *                          {null} if no block was clicked
     * @param action            an action
     * @return
     */
    @Override
    public boolean onClick(Player player, Block clickedBlock, Action action) {
        switch (action) {
            case LEFT_CLICK_AIR:
                this.onLeftClickAir(player);
                return false;
            case RIGHT_CLICK_AIR:
                this.onRightClickAir(player);
                return false;
            case RIGHT_CLICK_BLOCK:
                this.onRightClickBlock(player, clickedBlock);
                return false;
            case LEFT_CLICK_BLOCK:
                this.onLeftClickBlock(player, clickedBlock);
                return false;
        }
        return true;
    }

    /**
     * Called if player makes a left click in the air
     *
     * @param player        a player
     */
    public final void onLeftClickAir(Player player) {
        if (player.isSneaking()) {
            this.onLeftShiftClick(player);
        } else {
            this.onLeftClick(player);
        }
    }

    /**
     * Called if player makes a right click in the air
     *
     * @param player        a player
     */
    public final void onRightClickAir(Player player) {
        if (player.isSneaking()) {
            this.onRightShiftClick(player);
        } else {
            this.onRightClick(player);
        }
    }

    /**
     * Called if a player makes a left click on a block
     *
     * @param player            a player
     * @param clickedBlock      a block
     */
    public final void onLeftClickBlock(Player player, Block clickedBlock) {
        if (player.isSneaking()) {
            this.onLeftShiftClick(player);
        } else {
            this.onLeftClick(player);
        }
    }

    /**
     * Called if player makes a right click on a block
     *
     * @param player            a player
     * @param clickedBlock      a block
     */
    public final void onRightClickBlock(Player player, Block clickedBlock) {
        if (player.isSneaking()) {
            this.onRightShiftClick(player);
        } else {
            this.onRightClick(player);
        }
    }

    /**
     * Called if player makes a left click
     *
     * @param player        the player
     */
    public void onLeftClick(Player player) {}

    /**
     * Called  if a player makes a right click
     *
     * @param player        the player
     */
    public void onRightClick(Player player) {}

    /**
     * Called if player makes a shift left click
     *
     * @param player        the player
     */
    public void onLeftShiftClick(Player player) {}

    /**
     * Called if a player makes a shift right click
     *
     * @param player        the player
     */
    public void onRightShiftClick(Player player) {}

}
