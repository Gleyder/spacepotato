package de.gleyder.spacepotato.core.gui.navigable;

import de.gleyder.spacepotato.core.builder.item.ItemBuilder;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryItem;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import de.gleyder.spacepotato.core.clickable.inventory.interfaces.BlockingClickAction;
import de.gleyder.spacepotato.core.clickable.inventory.interfaces.LeftClickInventoryClickAction;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import de.gleyder.spacepotato.core.gui.elements.Button;
import de.gleyder.spacepotato.core.gui.elements.Element;
import de.gleyder.spacepotato.core.gui.elements.ElementCreator;
import de.gleyder.spacepotato.core.gui.impl.GUIInventoryRegistry;
import de.gleyder.spacepotato.core.utils.Position;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class NavigableInventory<T> {

    @Getter
    private final List<T> clickableItemList;
    private final List<InventoryPage> pageList;
    private final ClickableInventoryRegistry clickableInventoryRegistry;
    private final GUIInventoryRegistry guiInventoryRegistry;
    private final Button closeButton;
    private final ClickableInventoryItem blank;

    public NavigableInventory(@NotNull GUIInventoryRegistry guiInventoryRegistry, @NotNull JavaPlugin plugin, @NotNull ClickableInventoryRegistry clickableInventoryRegistry) {
        this.clickableItemList = new ArrayList<>();
        this.pageList = new ArrayList<>();
        this.clickableInventoryRegistry = clickableInventoryRegistry;
        this.guiInventoryRegistry = guiInventoryRegistry;
        this.closeButton = new Button(new Position(), new LeftClickInventoryClickAction() {
            @Override
            public void onClick(Player player, Inventory inventory, ItemStack item) {
                player.closeInventory();
            }
        }, new ItemBuilder(Material.BARRIER)
                .setDisplayName(CoreMessages.INVENTORY_CLOSE$BUTTON$LABEL.getMessage(false))
                .build(), Element.Type.STATIC);
        this.blank = new ClickableInventoryItem(new BlockingClickAction(), new ItemBuilder(Material.GRAY_WOOL)
                .setDisplayName("")
                .toPersistentDataBuilder(plugin)
                .setCustomTag("type", "blank")
                .build());
    }

    public void open(Player player) {
        this.pageList.get(0).open(player);
    }

    public abstract ClickableInventoryItem toItem(T t);

    public void build() {
        //Clears the inventories
        this.pageList.forEach(inventoryPage -> this.guiInventoryRegistry.unregister(inventoryPage.getUniqueId()));

        this.pageList.clear();

        //Calculates the needed pages
        int pages = this.clickableItemList.size() / InventoryPage.SIZE;
        if (this.clickableItemList.size() % InventoryPage.SIZE != 0)
            pages++;

        int startIndex = 0;
        int stopIndex = InventoryPage.SIZE;

        for (int i = 0; i < pages; i++) {
            if (i == pages-1)
                stopIndex = this.clickableItemList.size();

            //Creates the inventory page
            InventoryPage inventoryPage = new InventoryPage(
                    this.guiInventoryRegistry,
                    this.clickableInventoryRegistry,
                    CoreMessages.INVENTORY_NAVIGABLE_PAGE$NAME.getMessage(false, i+1)
            );
            this.guiInventoryRegistry.register(inventoryPage);
            //Sets the padding
            new ElementCreator(inventoryPage).setPadding(1, Material.BLACK_STAINED_GLASS_PANE);

            //Adds the close button
            inventoryPage.addElements(this.closeButton);

            //Copies the items into the inventory
            this.clickableItemList.subList(startIndex, stopIndex).forEach(t -> inventoryPage.getClickableInventoryItemList().add(this.toItem(t)));

            inventoryPage.constructStatic();

            startIndex += InventoryPage.SIZE;
            stopIndex += InventoryPage.SIZE;

            this.pageList.add(inventoryPage);
        }

        for (int i = 0; i < this.pageList.size(); i++) {
            InventoryPage previousPage = this.getPage(i-1);
            InventoryPage inventoryPage = this.getPage(i);
            InventoryPage nextPage = this.getPage(i+1);

            this.setNavigationItem(inventoryPage, previousPage, new ItemBuilder(Material.BLUE_WOOL)
                    .setDisplayName(CoreMessages.INVENTORY_PREVIOUS$BUTTON$LABEL.getMessage(false))
                    .build(), 7);

            this.setNavigationItem(inventoryPage, nextPage, new ItemBuilder(Material.GREEN_WOOL)
                    .setDisplayName(CoreMessages.INVENTORY_NEXT$BUTTON$LABEL.getMessage(false))
                    .build(), 8);
        }
    }

    private void setNavigationItem(InventoryPage page, InventoryPage otherPage, ItemStack itemStack, int slot) {
        if (otherPage != null) {
            page.addElements(new Button(new Position(slot, 0), new OpenInventoryAction(otherPage), itemStack, Element.Type.DYNAMIC));
        } else {
            page.addElements(Element.createSingleItem(new Position(slot, 0), Element.Type.DYNAMIC, this.blank));
        }
    }

    private InventoryPage getPage(int index) {
        try {
            return this.pageList.get(index);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public void addIterable(Iterable<T> ts) {
        ts.forEach(this::add);
    }

    public void removeIterable(Iterable<T> ts) {
        ts.forEach(this::remove);
    }

    @SafeVarargs
    public final void add(T... t) {
        this.clickableItemList.addAll(Arrays.asList(t));
    }

    @SafeVarargs
    public final void remove(T... t) {
        this.clickableItemList.removeAll(Arrays.asList(t));
    }

    public void clear() {
        this.clickableItemList.clear();
    }

}
