package de.gleyder.spacepotato.core.utils;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ListHelper {

    @SuppressWarnings("UnusedReturnValue")
    public static <T> boolean saveRemove(Collection<T> list, Predicate<T> predicate) {
        T toRemove = null;
        for (T t : list) {
            if (!predicate.test(t))
                continue;

            toRemove = t;
            break;
        }

        if (toRemove == null)
            return false;

        list.remove(toRemove);
        return true;
    }

}
