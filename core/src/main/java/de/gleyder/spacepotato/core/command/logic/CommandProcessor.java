package de.gleyder.spacepotato.core.command.logic;

import de.gleyder.spacepotato.core.command.entities.Argument;
import de.gleyder.spacepotato.core.command.entities.StaticArgument;
import de.gleyder.spacepotato.core.command.entities.SubCommand;
import de.gleyder.spacepotato.core.command.entities.SubCommandCategory;
import de.gleyder.spacepotato.core.command.enums.ArgumentType;
import de.gleyder.spacepotato.core.command.enums.SenderType;
import de.gleyder.spacepotato.core.command.impl.ConditionContainerImpl;
import de.gleyder.spacepotato.core.command.input.InputArgument;
import de.gleyder.spacepotato.core.command.input.InputParser;
import de.gleyder.spacepotato.core.command.supplier.ArgumentSupplier;
import de.gleyder.spacepotato.core.command.supplier.SupplierValuesImpl;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Processor for the command
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see org.bukkit.command.CommandExecutor
 * @see org.bukkit.command.TabCompleter
 */
public class CommandProcessor extends Command {

    private final static InputParser PARSER;

    static {
        PARSER = new InputParser();
    }

    private final CommandAdapter adapter;

    /**
     * Creates a CommandProcessor
     *
     * @param adapter       an adapter
     */
    CommandProcessor(CommandAdapter adapter) {
        super(adapter.getLabel(), adapter.getDescription(), "", adapter.getAliases());
        this.adapter = adapter;
    }

    @Override
    public boolean execute(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
        SenderType commandType = SenderType.of(sender);
        if (commandType == SenderType.UNDEFINED) {
            sender.sendMessage(CoreMessages.COMMAND_BASE_SENDER$NOT$SUPPORTED.getMessage(true));
            return true;
        }

        List<InputArgument> inputArgumentList = PARSER.parseCommand(args);

        for (SubCommand subCommand : this.adapter.getSubCommandList()) {
            if (subCommand.getType() != commandType && subCommand.getType() != SenderType.UNDEFINED)
                continue;

            List<InputArgument> copyOfInputArgumentList = inputArgumentList;

            if (subCommand.hasEndless())
                copyOfInputArgumentList = PARSER.parseEndless(args, subCommand.getLastArgumentIndex()-1);

            if (!this.compare(copyOfInputArgumentList, subCommand.getArguments()))
                continue;

            ConditionContainerImpl.Result result = subCommand.getConditionContainer().test(sender);
            if (!result.isSuccesses()) {
                result.sendMessage(sender);
                return true;
            }

            ArgumentSupplier supplier = new ArgumentSupplier();
            supplier.supply(copyOfInputArgumentList, subCommand.getArguments());

            if (supplier.hasExceptions()) {
                supplier.getExceptions().forEach(exception -> sender.sendMessage(exception.getMessage()));
                return true;
            }

            for (SubCommandCategory category : subCommand.getCategories()) {
                if (category.getExecutor().getMethod() == null)
                    continue;
                SupplierValuesImpl values = new SupplierValuesImpl();

                if (!category.getExecutor().prepare(sender, supplier.getSupplierValuesImpl(), values))
                    return true;

                supplier.getSupplierValuesImpl().merge(values);
            }

            subCommand.getExecutor().execute(sender, supplier.getSupplierValuesImpl());
            return true;
        }
        sender.sendMessage(CoreMessages.COMMAND_BASE_NOT$EXISTING.getMessage(true));
        return true;
    }

    @Override
    public @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args) throws IllegalArgumentException {
        if (!(sender instanceof Player))
            return new ArrayList<>();
        List<InputArgument> inputArgumentList = PARSER.parseCommand(args);
        String currentArg = args[args.length-1];

        if (currentArg.isEmpty() || currentArg.equals(String.valueOf(PARSER.getPrefix()))) {
            inputArgumentList.add(new InputArgument("dummy"));
        }

        List<String> stringList = this.adapter.getTabCompletionContainer().get(inputArgumentList, (Player) sender);
        return this.filter(stringList, currentArg);
    }

    private List<String> filter(List<String> stringList, String pattern) {
        return stringList.stream()
                .map(string -> {
                    if (!pattern.isEmpty() && pattern.startsWith(String.valueOf(PARSER.getPrefix()))) {
                        return PARSER.getPrefix() + string;
                    } else if (!pattern.isEmpty() && pattern.endsWith(String.valueOf(PARSER.getSuffix()))) {
                        return string + PARSER.getSuffix();
                    } else {
                        return string;
                    }
                })
                .filter(string -> {
                    if (string.isEmpty())
                        return true;
                    else
                        return string.contains(pattern);
                })
                .collect(Collectors.toList());
    }

    private boolean compare(List<InputArgument> inputArgumentList, List<Argument> argumentList) {
        if (inputArgumentList.size() == 0 && argumentList.size() == 0)
            return true;
        if (inputArgumentList.size() != argumentList.size())
            return false;

        for (int i = 0; i < argumentList.size(); i++) {
            Argument argument = argumentList.get(i);
            InputArgument inputArgument = inputArgumentList.get(i);

            if (argument.getType() == ArgumentType.STATIC) {
                if (inputArgument.getInputs().size() > 1)
                    return false;

                StaticArgument staticArgument = (StaticArgument) argument;

                if (!staticArgument.getLabel().equalsIgnoreCase(inputArgument.getInputs().get(0))) {
                    return false;
                }
            }
        }
        return true;
    }

}
