package de.gleyder.spacepotato.core.command.interpreters;


import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;

/**
 * Translates an argument into a byte.
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class ByteInterpreter implements Interpreter<Byte> {

    /**
     * Translates an argument into a byte.
     *
     * @param argument                          an argument
     * @return                                  a byte
     * @throws InterpreterException             if the argument cannot be translated
     */
    @Override
    public Byte translate(String argument) throws InterpreterException {
        try {
            return Byte.valueOf(argument);
        } catch (NumberFormatException e) {
            throw new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_BYTE.getKeyableMessage(true)
                    .replace("argument", argument)
                    .replace("min", Byte.MIN_VALUE)
                    .replace("max", Byte.MAX_VALUE)
                    .build()
            );
        }
    }

    @Override
    public String getTabDescription() {
        return TabCompletionsHelper.getAsSingleTypeString("number");
    }

    @Override
    public Class<Byte> getClassType() {
        return Byte.class;
    }

}
