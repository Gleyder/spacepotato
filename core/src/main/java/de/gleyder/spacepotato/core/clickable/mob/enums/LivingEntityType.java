package de.gleyder.spacepotato.core.clickable.mob.enums;

import lombok.Getter;
import org.bukkit.entity.*;

/**
 * Enum to store all living entities.
 *
 * @author Gleyer
 * @since 1.0
 * @version 1.0
 */
public enum LivingEntityType {

    ELDER_GUARDIAN(EntityType.ELDER_GUARDIAN, ElderGuardian.class),
    WITHER_SKELETON(EntityType.WITHER_SKELETON, WitherSkeleton.class),
    STRAY(EntityType.STRAY, Stray.class),
    HUSK(EntityType.HUSK, Husk.class),
    ZOMBIE_VILLAGER(EntityType.ZOMBIE_VILLAGER, ZombieVillager.class),
    SKELETON_HORSE(EntityType.SKELETON_HORSE, SkeletonHorse.class),
    ZOMBIE_HORSE(EntityType.ZOMBIE_HORSE, ZombieHorse.class),
    ARMOR_STAND(EntityType.ARMOR_STAND, ArmorStand.class),
    DONKEY(EntityType.DONKEY, Donkey.class),
    MULE(EntityType.MULE, Mule.class),
    EVOKER(EntityType.EVOKER, Evoker.class),
    VEX(EntityType.VEX, Vex.class),
    VINDICATOR(EntityType.VINDICATOR, Vindicator.class),
    ILLUSIONER(EntityType.ILLUSIONER, Illusioner.class),
    CREEPER(EntityType.CREEPER, Creeper.class),
    SKELETON(EntityType.SKELETON, Skeleton.class),
    SPIDER(EntityType.SPIDER, Spider.class),
    GIANT(EntityType.GIANT, Giant.class),
    ZOMBIE(EntityType.ZOMBIE, Zombie.class),
    SLIME(EntityType.SLIME, Slime.class),
    GHAST(EntityType.GHAST, Ghast.class),
    PIG_ZOMBIE(EntityType.PIG_ZOMBIE, PigZombie.class),
    ENDERMAN(EntityType.ENDERMAN, Enderman.class),
    CAVE_SPIDER(EntityType.CAVE_SPIDER, CaveSpider.class),
    SILVERFISH(EntityType.SILVERFISH, Silverfish.class),
    BLAZE(EntityType.BLAZE, Blaze.class),
    MAGMA_CUBE(EntityType.MAGMA_CUBE, MagmaCube.class),
    ENDER_DRAGON(EntityType.ENDER_DRAGON, EnderDragon.class),
    WITHER(EntityType.WITHER, Wither.class),
    BAT(EntityType.BAT, Bat.class),
    WITCH(EntityType.WITCH, Witch.class),
    ENDERMITE(EntityType.ENDERMITE, Endermite.class),
    GUARDIAN(EntityType.GUARDIAN, Guardian.class),
    SHULKER(EntityType.SHULKER, Shulker.class),
    PIG(EntityType.PIG, Pig.class),
    SHEEP(EntityType.SHEEP, Sheep.class),
    COW(EntityType.COW, Cow.class),
    CHICKEN(EntityType.CHICKEN, Chicken.class),
    SQUID(EntityType.SQUID, Squid.class),
    WOLF(EntityType.WOLF, Wolf.class),
    MUSHROOM_COW(EntityType.MUSHROOM_COW, MushroomCow.class),
    SNOWMAN(EntityType.SNOWMAN, Snowman.class),
    OCELOT(EntityType.OCELOT, Ocelot.class),
    IRON_GOLEM(EntityType.IRON_GOLEM, IronGolem.class),
    HORSE(EntityType.HORSE, Horse.class),
    RABBIT(EntityType.RABBIT, Rabbit.class),
    POLAR_BEAR(EntityType.POLAR_BEAR, PolarBear.class),
    LLAMA(EntityType.LLAMA, Llama.class),
    PARROT(EntityType.PARROT, Parrot.class),
    VILLAGER(EntityType.VILLAGER, Villager.class),
    TURTLE(EntityType.TURTLE, Turtle.class),
    PHANTOM(EntityType.PHANTOM, Phantom.class),
    COD(EntityType.COD, Cod.class),
    SALMON(EntityType.SALMON, Salmon.class),
    PUFFERFISH(EntityType.PUFFERFISH, PufferFish.class),
    TROPICAL_FISH(EntityType.TROPICAL_FISH, TropicalFish.class),
    DROWNED(EntityType.DROWNED, Drowned.class),
    DOLPHIN(EntityType.DOLPHIN, Dolphin.class),
    WANDERING(EntityType.WANDERING_TRADER, WanderingTrader.class),
    TRADER_LLAMA(EntityType.TRADER_LLAMA, TraderLlama.class),
    PILLAGER(EntityType.PILLAGER, Pillager.class),
    FOX(EntityType.FOX, Fox.class),
    CAT(EntityType.CAT, Cat.class)
    ;

    @Getter
    private final EntityType type;

    @Getter
    private final Class<? extends LivingEntity> entity;


    /**
     * Creates a LivingEntityType
     *
     * @param type              an entity type
     * @param entityClass       the corresponding class
     */
    LivingEntityType(EntityType type, Class<? extends LivingEntity> entityClass) {
        this.type = type;
        this.entity = entityClass;
    }

    /**
     * Returns a living entity type from an entity type
     *
     * @param type      an entity type
     * @return          {null} if the entity is not a living entity
     */
    public static LivingEntityType fromEntityType(EntityType type) {
        for (LivingEntityType value : LivingEntityType.values()) {
            if (value.type == type)
                return value;
        }
        return null;
    }

}
