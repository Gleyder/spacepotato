package de.gleyder.spacepotato.core.configuration.typeadapter;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Type;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ItemStackSerializer implements JsonSerializer<ItemStack> {

    @Override
    public JsonElement serialize(ItemStack src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(ItemStackUtil.itemStackArrayToBase64(new ItemStack[]{src}));
    }

}
