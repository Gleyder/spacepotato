package de.gleyder.spacepotato.core.clickable.hotbar;

import com.google.common.collect.ImmutableMap;
import de.gleyder.spacepotato.core.clickable.hotbar.interfaces.HotbarClickAction;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.function.Predicate;

/**
 * Listener for ClickableHotbars.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ClickableHotbar<O> {

    private final HashMap<Object, HotbarClickAction> actions;

    @Getter
    private final O owner;

    @Getter(AccessLevel.PACKAGE)
    private final Predicate<Player> predicate;

    /**
     * Creates a ClickableHotbar
     */
    public ClickableHotbar(@NotNull O owner, @NotNull Predicate<Player> predicate) {
        this.actions = new HashMap<>();
        this.owner = owner;
        this.predicate = predicate;
    }

    public static ClickableHotbar<Player> createPlayerHotbar(@NotNull Player player) {
        return new ClickableHotbar<>(player, player::equals);
    }

    public static ClickableHotbar<String> createGlobalHotbar(@NotNull String id) {
        return new ClickableHotbar<>(id, player -> true);
    }

    /**
     * Returns a map of objects and HotbarClickActions
     * The object may be player or a string
     *
     * @return      an immutable list of objects and HotbarClickActions
     */
    public ImmutableMap<Object, HotbarClickAction> getActions() {
        return new ImmutableMap.Builder<Object, HotbarClickAction>().putAll(this.actions).build();
    }

    /**
     * Sets the action for a material
     *
     * @param material      a material
     * @param action        a HotbarClickAction
     */
    public void setAction(Material material, HotbarClickAction action) {
        this.actions.put(material, action);
    }

    /**
     * Sets a HotbarClickAction for an ItemStack
     *
     * @param itemStack     an ItemStack
     * @param action        a HotbarClickAction
     */
    public void setAction(ItemStack itemStack, HotbarClickAction action) {
        this.actions.put(itemStack, action);
    }

    /**
     * Removes a HotbarClickAction from an ItemStack
     *
     * @param itemStack     an ItemStack
     */
    public void removeAction(ItemStack itemStack) {
        this.actions.remove(itemStack);
    }

    /**
     * Removes a HotbarClickAction from a Material
     *
     * @param material      a Material
     */
    public void removeAction(Material material) {
        this.actions.remove(material);
    }

}
