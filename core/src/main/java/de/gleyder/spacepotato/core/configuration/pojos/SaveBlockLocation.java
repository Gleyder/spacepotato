package de.gleyder.spacepotato.core.configuration.pojos;

import lombok.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SaveBlockLocation {

    public SaveBlockLocation(Location location) {
        this(location.getWorld().getName(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    private String world;
    private int x, y, z;

    public Location toLocation() {
        return new Location(Bukkit.getWorld(this.world), this.x, this.y, this.z);
    }

}
