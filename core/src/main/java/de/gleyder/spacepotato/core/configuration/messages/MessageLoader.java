package de.gleyder.spacepotato.core.configuration.messages;

import de.gleyder.spacepotato.core.SpacePotatoCorePlugin;
import de.gleyder.spacepotato.core.configuration.ConfigHelper;
import de.gleyder.spacepotato.core.configuration.TemporaryFile;
import de.gleyder.spacepotato.core.configuration.bulkvalue.BulkValueLoader;
import de.gleyder.spacepotato.core.configuration.handler.LoadoutHandler;
import de.gleyder.spacepotato.core.configuration.handler.loader.ConfigLoader;
import org.apache.commons.lang.reflect.FieldUtils;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Loads the messages
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class MessageLoader {

    private final static String LANGDIR;

    static {
        LANGDIR = "lang";
    }

    private final ConfigHelper configHelper;
    private final Class<? extends Enum<?>> messagesEnum;
    private final JavaPlugin plugin;
    private final String prefix, defaultLang;
    private final Logger logger;
    private final LoadoutHandler handler;
    private final List<String> pathList;
    private final String pathPrefix;

    public MessageLoader(@NotNull JavaPlugin plugin, @NotNull Class<? extends Enum<?>> messagesEnum) {
        this(plugin, SpacePotatoCorePlugin.getGlobalConfig().prefix(), SpacePotatoCorePlugin.getGlobalConfig().defaultLang(), "", messagesEnum);
    }

    @SuppressWarnings("WeakerAccess")
    public MessageLoader(@NotNull JavaPlugin plugin, @NotNull String prefix, @NotNull String defaultLang, String pathPrefix, @NotNull Class<? extends Enum<?>> messagesEnum)  {
        this.configHelper = new ConfigHelper(plugin);
        this.messagesEnum = messagesEnum;
        this.plugin = plugin;
        this.prefix = prefix;
        this.defaultLang = defaultLang;
        this.logger = LoggerFactory.getLogger(this.getClass());
        this.handler = new LoadoutHandler(this.plugin);
        this.pathPrefix = pathPrefix == null ? "" : pathPrefix;
        this.pathList = new ArrayList<>();
        this.loadEnumPaths();
    }

    private void loadLanguages() {
        TemporaryFile temporaryFile = this.configHelper.getTempFromLoadout(LANGDIR + "/lang.yaml");
        List<String> langFiles = YamlConfiguration.loadConfiguration(temporaryFile.getFile()).getStringList("lang");
        langFiles.forEach(s -> {
            this.handler.addFile(LANGDIR + "/" + s + ".yaml", new ConfigLoader(this.pathList));
        });
        temporaryFile.delete();
    }

    private void loadEnumPaths() {
        for (Enum<?> enumConstant : this.messagesEnum.getEnumConstants()) {
            String enumPath = enumConstant.name()
                    .toLowerCase()
                    .replace("_", ".")
                    .replace("$", "-");

            this.pathList.add(enumPath);
            try {
                FieldUtils.writeField(enumConstant, "path", enumPath, true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        if (!this.pathPrefix.isEmpty()) {
            List<String> list = this.pathList.stream().map(string -> this.pathPrefix + "." + string).collect(Collectors.toList());
            this.pathList.clear();
            this.pathList.addAll(list);
        }
    }

    public void load() {
        this.loadLanguages();

        //Loads the language files
        this.handler.load();

        //Loads the file directory
        File langDir = new File(this.configHelper.getDataFolderPath(LANGDIR));
        String path = this.configHelper.getDataFolderPath(LANGDIR);

        //Checks if the file exists
        if (!langDir.exists())
            throw new NullPointerException("The directory \"" + path + "\" does not exists");

        String[] langFiles = langDir.list();

        //Checks if the file is a directory
        if (langFiles == null)
            throw new NullPointerException("\"" + path + "\" is not a directory");

        //Checks if the directory has files
        if (langFiles.length == 0)
            throw new NullPointerException("The directory \"" + path + "\" has no files");

        HashMap<String, MessageFile> fileHashMap = new HashMap<>();
        for (String lang : langFiles) {
            File file = new File(this.configHelper.getDataFolderPath(LANGDIR + "/" + lang));
            lang = lang.replace(".yaml", "");

            BulkValueLoader<String> valueLoader = new BulkValueLoader<>(file, this.pathList, (loaderPath, cfg) -> {
                String string = cfg.getString(loaderPath);
                if (string == null)
                    string = "Could not load: " + loaderPath;
                return string;
            });
            MessageFile messageFile = new MessageFile(valueLoader.toMap());
            fileHashMap.put(lang, messageFile);
            this.logger.info("Loaded language \"" + lang + "\"");
        }

        this.injectStaticField("MESSAGE_MAP", new MessageMap(fileHashMap, this.defaultLang));
        this.injectStaticField("PREFIX", this.prefix);
        this.injectStaticField("PLUGIN", this.plugin);
    }

    private void injectStaticField(String name, Object value) {
        try {
            FieldUtils.writeStaticField(this.messagesEnum, name, value, true);
        } catch (IllegalArgumentException ignored) {
            this.logger.warn("No field with the name {} were found in {}", name, this.messagesEnum.getName());
        } catch (IllegalAccessException ignored) {
            this.logger.warn("The field with the name {} were in {} is final or cannot be made accessible", name, this.messagesEnum.getName());
        }
    }

}
