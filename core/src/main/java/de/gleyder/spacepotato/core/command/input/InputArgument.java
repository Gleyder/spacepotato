package de.gleyder.spacepotato.core.command.input;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a sender input
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@ToString
public class InputArgument {

    /**
     * Creates an InputArgument
     *
     * @param label     a label
     */
    public InputArgument(String label) {
        this.inputs = new ArrayList<>();
        this.inputs.add(label);
    }

    /**
     * Creates an InputArgument
     */
    public InputArgument() {
        this.inputs = new ArrayList<>();
    }

    @Getter
    List<String> inputs;

    public boolean isMulti() {
        return this.inputs.size() > 1;
    }

}
