package de.gleyder.spacepotato.core.analyzer.item;

import lombok.Getter;
import org.bukkit.inventory.ItemStack;

import java.util.function.BiPredicate;

/**
 * Enum for search queries.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum ItemSearchQuery {

    /**
     * Compares the material
     */
    MATERIAL((pattern, content) -> pattern.getType() == content.getType()),

    /**
     * Compares the amount
     */
    AMOUNT((pattern, content) -> pattern.getAmount() == content.getAmount()),

    /**
     * Compares the item meta
     */
    META((pattern, content) -> {
        if (pattern.hasItemMeta() && content.hasItemMeta())
            return pattern.getItemMeta() == content.getItemMeta();
        else
            return false;
    }),

    /**
     * Compares the max stack size
     */
    STACK_SIZE((pattern, content) -> pattern.getMaxStackSize() == content.getMaxStackSize()),

    /**
     * Compares the display name
     */
    DISPLAY_NAME((pattern, content) -> {
        if (!pattern.hasItemMeta()) return false;
        if (!content.hasItemMeta()) return false;
        return pattern.getItemMeta().getDisplayName().equals(content.getItemMeta().getDisplayName());
    }),

    /**
     * Compares the flags
     */
    FLAGS((pattern, content) -> {
        if (!pattern.hasItemMeta()) return false;
        if (!content.hasItemMeta()) return false;
        return pattern.getItemMeta().getItemFlags().containsAll(content.getItemMeta().getItemFlags());
    }),

    /**
     * Compares the lore
     */
    @SuppressWarnings("ConstantConditions")
    LORE((pattern, content) -> {
        if (!pattern.hasItemMeta() || pattern.getItemMeta().hasLore()) return false;
        if (!content.hasItemMeta() || content.getItemMeta().hasLore()) return false;
        return pattern.getItemMeta().getLore().containsAll(content.getItemMeta().getLore());
    }),

    /**
     * Compares the item stack
     */
    ITEM(ItemStack::equals),
    ;

    @Getter
    private BiPredicate<ItemStack, ItemStack> biPredicate;

    /**
     * Creates an ItemSearchQuery
     *
     * @param biPredicate       a biPredicate
     */
    ItemSearchQuery(BiPredicate<ItemStack, ItemStack> biPredicate) {
        this.biPredicate = biPredicate;
    }



}
