package de.gleyder.spacepotato.core.clickable.inventory.interfaces;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

/**
 * Implementation from InventoryClickAction for a simple left click in an inventory.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class LeftClickInventoryClickAction implements InventoryClickAction {

    /**
     * Called if player left clicks an item in an inventory
     *
     * @param player        a Player
     * @param inventory     an Inventory
     * @param item          an ItemStack
     */
    public void onClick(Player player, Inventory inventory, ItemStack item) {}

    /**
     * Called of a player clicks an item in an inventory
     *
     * @param player            a Player
     * @param inventory         an Inventory
     * @param item              an ItemStack
     * @param clickType         a ClickType
     * @return                  if the event should be cancelled
     */
    @Override
    public final boolean onClick(@NotNull Player player, @NotNull Inventory inventory, @NotNull ItemStack item, @NotNull ClickType clickType) {
        switch (clickType) {
            case LEFT: case SHIFT_LEFT:
                this.onClick(player, inventory, item);
                break;
        }
        return true;
    }

}
