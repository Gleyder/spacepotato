package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;

/**
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class StringInterpreter implements Interpreter<String> {

    @Override
    public String translate(String argument){
        return argument;
    }

    @Override
    public String getDescription() {
        return CoreMessages.COMMAND_INTERPRETER_DESCRIPTION_STRING.getMessage(false);
    }

    @Override
    public String getTabDescription() {
        return TabCompletionsHelper.getAsSingleTypeString("String");
    }

    @Override
    public Class<String> getClassType() {
        return String.class;
    }

}
