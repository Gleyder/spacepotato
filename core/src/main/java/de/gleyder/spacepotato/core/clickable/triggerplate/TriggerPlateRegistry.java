package de.gleyder.spacepotato.core.clickable.triggerplate;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.registry.AbstractUsesListenerRegistry;
import org.bukkit.block.Block;
import org.jetbrains.annotations.NotNull;

/**
 * Registry for trigger plates.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class TriggerPlateRegistry extends AbstractUsesListenerRegistry<Block, TriggerPlate> {

    /**
     * Creates a TriggerPlateRegistry
     *
     * @param spacePotatoRegistry       a space potato registry
     */
    public TriggerPlateRegistry(@NotNull SpacePotatoRegistry spacePotatoRegistry) {
        super(spacePotatoRegistry);
        this.init(new TriggerPlateListener(this));
    }

    /**
     * Registers a trigger plate
     *
     * @param triggerPlate      a trigger plate
     */
    @Override
    public void register(TriggerPlate triggerPlate) {
        this.getMap().put(triggerPlate.getBlock(), triggerPlate);
    }

}
