package de.gleyder.spacepotato.core.command.interfaces;

/**
 * Indicates that a class can be described and labeled.
 *
 * @author Gleyder
 * @version 1.0
 * @since 2.0
 *
 * @see Describable
 * @see de.gleyder.spacepotato.core.command.interfaces.Labeled
 */
public interface Representable extends Describable, Labeled {

}
