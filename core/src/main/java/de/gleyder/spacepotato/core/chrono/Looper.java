package de.gleyder.spacepotato.core.chrono;

import de.gleyder.spacepotato.core.chrono.interfaces.ChronoPart;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;

/**
 * Loops an executable with a frequency.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class Looper implements ChronoPart {

    @SuppressWarnings("WeakerAccess")
    protected long frequency;

    @SuppressWarnings("WeakerAccess")
    protected boolean instantStart;
    protected JavaPlugin plugin;
    private int taskId;

    /**
     * Creates a Looper
     *
     * @param frequency         how frequent it updates in tickes
     * @param instantStart      if it should starts instantly
     */
    public Looper(long frequency, boolean instantStart, JavaPlugin plugin) {
        this.frequency = frequency;
        this.instantStart = instantStart;
        this.taskId = -1;
        this.plugin = plugin;
    }

    public Looper(@NonNull Duration frequency, boolean instantStart, @NotNull JavaPlugin plugin) {
        this(frequency.getSeconds() * Numerator.SECONDS_TO_TICKS, instantStart, plugin);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void start() {
        if (this.isRunning())
            return;

        this.onPreStart();
        this.taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(this.plugin, this::onUpdate, this.instantStart ? 0 : this.frequency, this.frequency);
        this.onStart();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void stop() {
        Bukkit.getScheduler().cancelTask(this.taskId);
        this.onStop();
    }

    /**
     * Checks if its running
     *
     * @return      a boolean
     */
    @Override
    public final boolean isRunning() {
        return this.taskId != -1;
    }

}
