package de.gleyder.spacepotato.core.gui.impl;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import de.gleyder.spacepotato.core.gui.AutomaticGUIInventory;
import de.gleyder.spacepotato.core.gui.GUIInventory;
import de.gleyder.spacepotato.core.gui.InventoryInstanceCreator;
import de.gleyder.spacepotato.core.gui.ManuallyGUIInventory;
import de.gleyder.spacepotato.core.gui.navigable.NavigableInventory;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;

/**
 * Creates gui inventories
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GUIInventoryFactory {

    private final SpacePotatoRegistry spacePotatoRegistry;
    private final ClickableInventoryRegistry clickableInventoryRegistry;

    @Getter
    private final GUIInventoryRegistry guiInventoryRegistry;

    public GUIInventoryFactory(@NotNull SpacePotatoRegistry spacePotatoRegistry, @NotNull ClickableInventoryRegistry clickableInventoryRegistry) {
        this.spacePotatoRegistry = spacePotatoRegistry;
        this.clickableInventoryRegistry = clickableInventoryRegistry;
        this.guiInventoryRegistry = new GUIInventoryRegistry(spacePotatoRegistry, clickableInventoryRegistry);
    }

    public GUIInventoryFactory(@NotNull SpacePotatoRegistry spacePotatoRegistry) {
        this(spacePotatoRegistry, new ClickableInventoryRegistry(spacePotatoRegistry));
    }

    public ManuallyGUIInventory createManuallyGUIInventory(@NotNull String title, int rows) {
        return this.create((InventoryInstanceCreator<ManuallyGUIInventory>) (registry, guiInventoryRegistry, inventoryRegistry) ->
                new ManuallyGUIInventoryImpl(this.guiInventoryRegistry, inventoryRegistry, title, rows)
        );
    }

    public AutomaticGUIInventory createAutomaticGUIInventory(@NotNull String title, int rows, Duration updateRate) {
        return this.create((InventoryInstanceCreator<AutomaticGUIInventory>) (registry, guiInventoryRegistry, inventoryRegistry) ->
                new AutomaticGUIInventoryImpl(guiInventoryRegistry, registry.getPlugin(), inventoryRegistry, title, rows, updateRate)
        );
    }

    public <T extends NavigableInventory<?>> T createNavigableInventory(@NotNull InventoryInstanceCreator<T> creator) {
        return creator.create(this.spacePotatoRegistry, this.guiInventoryRegistry, this.clickableInventoryRegistry);
    }

    public <T extends GUIInventory> T create(@NotNull InventoryInstanceCreator<T> creator) {
        T gui =  creator.create(this.spacePotatoRegistry, this.guiInventoryRegistry, this.clickableInventoryRegistry);
        this.guiInventoryRegistry.register(gui);
        return gui;
    }

}
