package de.gleyder.spacepotato.core.utils;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class BlockSelector implements UsesListener {

    @Getter
    private final Set<Block> blockSet;

    private final int maxBlocks;
    private final SpacePotatoRegistry potatoRegistry;
    private final Listener listener;
    private final Consumer<List<Block>> consumer;

    public BlockSelector(@NonNull SpacePotatoRegistry potatoRegistry, int maxBlocks, @NonNull Consumer<List<Block>> consumer) {
        this.blockSet = new HashSet<>();
        this.maxBlocks = maxBlocks;
        this.potatoRegistry = potatoRegistry;
        this.listener = new BlockListener();
        this.potatoRegistry.registerListener(this.listener);
        this.consumer = consumer;
    }

    @Override
    public void unregisterListeners() {
        this.potatoRegistry.unregisterListener(this.listener);
    }

    private class BlockListener implements Listener {

        @EventHandler(priority = EventPriority.HIGHEST)
        public void onBlockBreak(BlockBreakEvent event) {
            event.setCancelled(true);
            if (blockSet.contains(event.getBlock())) {
                CoreMessages.BLOCK$SELECTOR_ALREADY$ADDED.sendMessage(event.getPlayer(), true);
                return;
            }

            blockSet.add(event.getBlock());
            CoreMessages.BLOCK$SELECTOR_BLOCK$SELECTED.sendMessage(event.getPlayer(), true);

            if (blockSet.size() == maxBlocks) {
                consumer.accept(new ArrayList<>(blockSet));
                unregisterListeners();
            }
        }

    }

}
