package de.gleyder.spacepotato.core.broadcast;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashSet;

/**
 * Allows to add and remove players manually
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ManualBroadcastChannel extends AbstractBroadcastChannel {

    private final HashSet<Player> playerHashSet;

    /**
     * Creates a ManualBroadcastChannel
     */
    public ManualBroadcastChannel() {
        this(null);
    }

    /**
     * Creates a ManualBroadcastChannel
     *
     * @param collection        a collection of players
     */
    public ManualBroadcastChannel(Collection<Player> collection) {
        this.playerHashSet = collection == null ? new HashSet<>() : new HashSet<>(collection);
    }

    /**
     * Adds a player
     *
     * @param player        a player
     */
    public void addPlayer(@NotNull Player player) {
        this.playerHashSet.add(player);
    }

    /**
     * Adds a collection of players
     *
     * @param playerCollection      a collection of players
     */
    public void addPlayers(@NotNull Collection<Player> playerCollection) {
        this.playerHashSet.addAll(playerCollection);
    }

    /**
     * Removes a player
     *
     * @param player        a player
     */
    public void removePlayer(@NotNull Player player) {
        this.playerHashSet.remove(player);
    }

    /**
     * Removes a collection of players
     *
     * @param playerCollection      a collection of players
     */
    public void removePlayers(@NotNull Collection<Player> playerCollection) {
        this.playerHashSet.removeAll(playerCollection);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Player> getPlayers() {
        return this.playerHashSet;
    }

}
