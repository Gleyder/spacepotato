package de.gleyder.spacepotato.core.clickable.triggerplate.interfaces;

import de.gleyder.spacepotato.core.clickable.triggerplate.TriggerPlateType;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

/**
 * Interfaces for trigger plate actions.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface TriggerPlateAction {

    /**
     * Executed if a player walks over a trigger plate (pressure plate).
     *
     * @param player    the player
     * @param type      the trigger plate type
     * @param block     the block
     * @return          if the event should be cancelled
     */
    boolean onTrigger(Player player, TriggerPlateType type, Block block);

}
