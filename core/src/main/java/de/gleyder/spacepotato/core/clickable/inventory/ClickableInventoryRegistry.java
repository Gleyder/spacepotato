package de.gleyder.spacepotato.core.clickable.inventory;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.registry.AbstractUsesListenerRegistry;
import org.bukkit.inventory.Inventory;
import org.jetbrains.annotations.NotNull;

/**
 * Registry for ClickableInventories.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ClickableInventoryRegistry extends AbstractUsesListenerRegistry<Inventory, ClickableInventory> {

    public ClickableInventoryRegistry(@NotNull SpacePotatoRegistry spacePotatoRegistry) {
        super(spacePotatoRegistry);
        this.init(new ClickableInventoryListener(this));
    }

    @Override
    public void register(ClickableInventory clickableInventory) {
        this.getMap().put(clickableInventory.getInventory(), clickableInventory);
    }

}
