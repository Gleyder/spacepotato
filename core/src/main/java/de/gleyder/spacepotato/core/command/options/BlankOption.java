package de.gleyder.spacepotato.core.command.options;

import de.gleyder.spacepotato.core.command.interfaces.Option;

/**
 * @author Gleyder
 */
public class BlankOption implements Option<Object> {

    @Override
    public void test(Object o) {
    }

    @Override
    public String getDescription() {
        return "Keine Weiteren Bedingungen";
    }

}
