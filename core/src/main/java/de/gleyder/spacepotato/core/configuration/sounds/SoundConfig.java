package de.gleyder.spacepotato.core.configuration.sounds;

import org.bukkit.Sound;
import org.bukkit.SoundCategory;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface SoundConfig {

    Sound sound();
    SoundCategory category();
    float volume();
    float pitch();

}
