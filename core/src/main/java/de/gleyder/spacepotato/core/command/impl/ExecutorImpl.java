package de.gleyder.spacepotato.core.command.impl;

import de.gleyder.spacepotato.core.command.annotations.ExecutorMethod;
import de.gleyder.spacepotato.core.command.annotations.Input;
import de.gleyder.spacepotato.core.command.entities.Executor;
import de.gleyder.spacepotato.core.command.exceptions.CommandRegistryException;
import de.gleyder.spacepotato.core.command.exceptions.ExecutorException;
import de.gleyder.spacepotato.core.command.logic.CommandAdapter;
import de.gleyder.spacepotato.core.command.supplier.SupplierValues;
import de.gleyder.spacepotato.core.command.supplier.SupplierValuesImpl;
import de.gleyder.spacepotato.core.utils.string.StringUtil;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.bukkit.command.CommandSender;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Implementation of Executor.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@ToString
public class ExecutorImpl implements Executor {

    protected String methodName;

    @Setter
    @Getter
    protected Method method;

    @Setter
    @Getter
    protected CommandAdapter commandAdapter;

    protected Function<CommandSender, Object> senderConverter;

    /**
     * Creates an ExecutorImpl
     */
    ExecutorImpl(Function<CommandSender, Object> senderConverter) {
        this.methodName = StringUtil.EMPTY;
        this.senderConverter = sender -> sender;
        this.senderConverter = senderConverter == null ? sender -> sender : senderConverter;
    }

    public void setMethodName(String methodName) {
        this.methodName = StringUtil.emptyIfNull(methodName);
    }

    @Override
    public String getName() {
        return this.methodName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(CommandSender sender, SupplierValues values) {
        this.invoke(sender, (SupplierValuesImpl) values, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepare(@NonNull CommandSender sender, @NonNull SupplierValues values, SupplierValues extraValues) {
        Object result = this.invoke(sender, (SupplierValuesImpl) values, extraValues == null ? null : (SupplierValuesImpl) extraValues);

        if (result == null)
            throw new ExecutorException("No return value for method {} in command {}", this.methodName, this.commandAdapter.getClass().getSimpleName());
        if (result instanceof InvocationTargetException)
            throw new ExecutorException("An error occurred in method {} in command {}. Check previous console for the error", this.methodName, this.commandAdapter.getClass().getSimpleName());
        if (!(result instanceof Boolean))
            throw new ExecutorException("Wrong return value for method {} in command {}", this.methodName, this.commandAdapter.getClass().getSimpleName());
        return (boolean) result;
    }

    /**
     * Searches for the method in the command adapter
     *
     * @param clazz     the command adapter clazz
     * @param force     if the method should be search if already a method is set
     */
    private void searchForMethod(Class<?> clazz, boolean force) {
        if (this.methodName.isEmpty())
            throw new CommandRegistryException("A command with with no method name were found in class {}", clazz.getSimpleName());

        if (force && this.method != null)
            return;

        for (Method method : clazz.getMethods()) {
            if (!method.isAnnotationPresent(ExecutorMethod.class))
                continue;
            ExecutorMethod executorMethod = method.getAnnotation(ExecutorMethod.class);
            if (!executorMethod.value().isEmpty()) {
                if (!executorMethod.value().equals(this.methodName))
                    continue;

                this.method = method;
                return;
            } else {
                if (!method.getName().equals(this.methodName))
                    continue;

                this.method = method;
                return;
            }
        }

        throw new CommandRegistryException("Couldn't find method with name {} in class {}", this.methodName, clazz.getSimpleName());
    }

    public void searchForMethod(Class<?> clazz) {
        this.searchForMethod(clazz, false);
    }

    private Object invoke(@NonNull CommandSender sender, @NonNull SupplierValuesImpl values, SupplierValuesImpl extraValues) {
        Object objSender = this.senderConverter.apply(sender);
        values = new SupplierValuesImpl(values);
        List<Object> paramList = new ArrayList<>();
        Parameter[] parameters = this.method.getParameters();

        if (extraValues != null)
            values.getSeamlessValues().addFirst(extraValues);
        values.getSeamlessValues().addFirst(objSender);

        for (Parameter parameter : parameters) {
            Object object;

            if (!parameter.isAnnotationPresent(Input.class)) {
                object = values.pop();
            } else {
                Input input = parameter.getAnnotation(Input.class);
                object = values.get(input.value());
            }
            if (object == null)
                throw new ExecutorException("Missing object for argument {} in method {} for command {}", parameter.getName(), this.methodName, this.commandAdapter.getClass().getName());

            Class<?> classType = parameter.getType();

            if (classType.isAssignableFrom(List.class) && !(object instanceof List)) {
                List<Object> objectList = new ArrayList<>();
                objectList.add(object);

                paramList.add(objectList);
            } else {
                paramList.add(object);
            }
        }

        try {
            return this.method.invoke(this.commandAdapter, paramList.toArray());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return e;
        } catch (IllegalAccessException e) {
            throw new ExecutorException("Couldn't execute the method because the access is denied");
        } catch (IllegalArgumentException e) {
            throw new ExecutorException("Argument type mismatch in method {} for command {}. Command needs {} but got {}", this.methodName, this.commandAdapter.getClass().getSimpleName(), Arrays.asList(parameters), paramList);
        }
    }

}
