package de.gleyder.spacepotato.core.clickable.triggerplate;

import de.gleyder.spacepotato.core.clickable.triggerplate.interfaces.TriggerPlateAction;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.block.Block;

/**
 * Class for a trigger plate
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class TriggerPlate {

    @Getter
    private final Block block;

    @Setter
    @Getter
    private TriggerPlateAction action;

    /**
     * Creates a TriggerPlate
     *
     * @param block     the block
     */
    public TriggerPlate(Block block) {
        this.block = block;
    }

}
