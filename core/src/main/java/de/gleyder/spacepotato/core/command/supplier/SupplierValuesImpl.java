package de.gleyder.spacepotato.core.command.supplier;

import lombok.Getter;
import lombok.ToString;

import java.util.*;

/**
 * Stores values for the ArgumentSupplier.
 *
 * @author Gleyder
 * @version 1.0
 * @since 3.0
 */
@ToString
public class SupplierValuesImpl implements SupplierValues {

    @Getter
    private final HashMap<String, Object> values;

    @Getter
    private final Deque<Object> seamlessValues;

    /**
     * Creates a SupplierValues
     */
    public SupplierValuesImpl() {
        this.values = new HashMap<>();
        this.seamlessValues = new ArrayDeque<>();
    }

    public SupplierValuesImpl(SupplierValuesImpl supplierValuesImpl) {
        this();
        this.merge(supplierValuesImpl);
    }

    /**
     * Adds an object with a key
     *
     * @param key       a key
     * @param value        a value
     */
    @Override
    public <V> void add(String key, V value) {
        this.values.put(key,value);
    }

    /**
     * Adds an object without a key. The values
     * will be added after the normal inputs
     *
     * @param value     a value
     */
    @Override
    public <V> void add(V value) {
        this.seamlessValues.add(value);
    }

    public Object get(String key) {
        return this.values.get(key);
    }

    public Object pop() {
        return this.seamlessValues.pop();
    }

    /**
     * Merges to values together
     *
     * @param supplierValuesImpl        another value
     */
    public void merge(SupplierValuesImpl supplierValuesImpl) {
        this.seamlessValues.addAll(supplierValuesImpl.getSeamlessValues());
        this.values.putAll(supplierValuesImpl.getValues());
    }

    /**
     * Clears the values
     */
    public void clear() {
        this.seamlessValues.clear();
        this.values.clear();
    }

    /**
     * Returns a list containing all values
     *
     * @return      a list
     */
    public List<Object> values() {
        List<Object> objectList = new ArrayList<>();
        objectList.addAll(this.seamlessValues);
        objectList.addAll(this.values.values());
        return objectList;
    }

}
