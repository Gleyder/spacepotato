package de.gleyder.spacepotato.core.command.entities;

import de.gleyder.spacepotato.core.command.enums.SenderType;

import java.util.List;

/**
 * Represents the base part of a command
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface CommandPart {

    ConditionContainer getConditionContainer();
    List<Argument> getArguments();
    SenderType getType();
    Executor getExecutor();

}
