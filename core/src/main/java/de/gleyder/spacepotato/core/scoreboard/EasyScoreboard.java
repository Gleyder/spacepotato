package de.gleyder.spacepotato.core.scoreboard;

import de.gleyder.spacepotato.core.scoreboard.impl.Line;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.RenderType;
import org.bukkit.scoreboard.Scoreboard;

/**
 * Interface for an easy scoreboard
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface EasyScoreboard {

    /**
     * Adds an entry
     *
     * @param entry     an entry
     */
    void addLine(String entry);

    /**
     * Adds an entry
     *
     * @param entry     an entry
     * @param key       a key
     * @param rule      a rule
     */
    void addLine(String entry, String key, InsertionRule rule);

    /**
     * Creates a board
     *
     * @param force     if the old one should be cleard
     */
    void create(boolean force);

    /**
     * Removes the board
     */
    void remove();

    /**
     * Updates the board
     */
    void update();

    /**
     * Adds an array of players
     *
     * @param players       an array of players
     */
    void addPlayers(Player... players);

    /**
     * Adds an player iterable
     *
     * @param playerIterable        an iterable of players
     */
    void addPlayerIterable(Iterable<Player> playerIterable);

    /*
     * --- Getter ---
     */

    Scoreboard getScoreboard();
    String getDisplayName();
    String getName();
    String getCriteria();
    RenderType getRenderType();
    DisplaySlot getDisplaySlot();
    Line getLine(String key);

    void setDisplayName(@NonNull String displayName);
    void setDisplaySlot(@NonNull DisplaySlot slot);
    void setRenderType(@NonNull RenderType type);

}
