package de.gleyder.spacepotato.core.gui.elements;

import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryItem;
import de.gleyder.spacepotato.core.utils.Matrix;
import de.gleyder.spacepotato.core.utils.Position;
import lombok.Getter;

/**
 * Represents an element in a gui inventory.
 * An element may contain multiple clickable items at any place
 * in the inventory or outside of it
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class Element {

    @Getter
    protected final Matrix<ClickableInventoryItem> matrix;

    @Getter
    protected final Position position;

    @Getter
    protected final Type type;

    /**
     * Creates an element
     *
     * @param position      a position
     * @param type          the type
     */
    public Element(Position position, Type type) {
        this.position = position;
        this.type = type;
        this.matrix = new Matrix<>();
    }

    /**
     * Creates an element
     *
     * @param x         the x coordinate
     * @param y         the y coordinate
     * @param type      the type
     */
    public Element(int x, int y, Type type) {
        this.matrix = new Matrix<>();
        this.position = new Position(x, y);
        this.type = type;
    }

    /**
     * Creates an element with only one item
     *
     * @param position          a position
     * @param type              the type
     * @param clickableInventoryItem     a clickable item
     * @return                  an element
     */
    public static Element createSingleItem(Position position, Type type, ClickableInventoryItem clickableInventoryItem) {
        Element element = new Element(position, type);
        element.getMatrix().set(new Position(), clickableInventoryItem);
        return element;
    }

    /**
     * Enum for element types
     */
    public enum Type {
        STATIC, DYNAMIC
    }

    /**
     * Duplicates the element
     *
     * @return      the duplicated element
     */
    public Element duplicate() {
        Element newElement = new Element(this.position.getX(), this.position.getY(), this.type);
        this.matrix.getAll().forEach((position1, clickableItem) -> newElement.matrix.set(position1.duplicate(), clickableItem));
        return newElement;
    }

}
