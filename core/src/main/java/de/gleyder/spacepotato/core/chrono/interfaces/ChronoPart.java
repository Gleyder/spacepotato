package de.gleyder.spacepotato.core.chrono.interfaces;

/**
 * Interface for a time based executor
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface ChronoPart {

    /**
     * Called on pre start
     */
    default void onPreStart() {};

    /**
     * Called on start
     */
    default void onStart() {};

    /**
     * Called on stop
     */
    default void onStop() {};

    /**
     * Called on update
     */
    default void onUpdate() {};

    /**
     * Starts it
     */
    void start();

    /**
     * Stops it
     */
    void stop();

    /**
     * Checks if its running
     *
     * @return      a boolean
     */
    boolean isRunning();

}
