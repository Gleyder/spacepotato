package de.gleyder.spacepotato.core.gui.impl;

import com.google.common.collect.ImmutableList;
import de.gleyder.spacepotato.core.builder.inventory.InventoryBuilder;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventory;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryItem;
import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import de.gleyder.spacepotato.core.clickable.inventory.interfaces.EmptyClickAction;
import de.gleyder.spacepotato.core.gui.GUIInventory;
import de.gleyder.spacepotato.core.gui.PositionTranslation;
import de.gleyder.spacepotato.core.gui.elements.Element;
import de.gleyder.spacepotato.core.utils.ListHelper;
import de.gleyder.spacepotato.core.utils.MaterialUtils;
import de.gleyder.spacepotato.core.utils.Matrix;
import de.gleyder.spacepotato.core.utils.Position;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Represents a gui inventory
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class GUIInventoryImpl implements GUIInventory {

    /*
     * ----------------- Attributes ---------------
     */

    // ----- Private Finals -----

    @Getter(AccessLevel.PACKAGE)
    private final ActiveInventoryManager activeInventoryManager;

    private final GUIInventoryRegistry guiInventoryRegistry;

    @Getter
    private final UUID uniqueId;

    // ----- Protected Finals -----

    protected final List<Element> elementList;

    @Getter(AccessLevel.PACKAGE)
    protected final ClickableInventoryRegistry inventoryRegistry;

    @Getter
    protected final String title;

    @Getter
    protected final int rows;

    protected final PositionTranslation translation;

    // ----- Protected Vars -----

    @Getter
    @Setter
    protected ClickableInventoryItem background;

    protected ClickableInventory staticInventory;

    /*
     * ----------------- Constructors ---------------
     */

    /**
     * Creates a GUIInventory
     *
     * @param registry      a registry
     * @param title         a title
     * @param rows          the amount of rows
     */
    public GUIInventoryImpl(@NotNull GUIInventoryRegistry guiInventoryRegistry, @NotNull ClickableInventoryRegistry registry, @NotNull String title, int rows) {
        this.title = title;
        this.rows = rows;
        this.inventoryRegistry = registry;
        this.activeInventoryManager = guiInventoryRegistry.getActiveInventoryManager();
        this.guiInventoryRegistry = guiInventoryRegistry;

        this.translation = new PositionTranslation(this.rows);
        this.elementList = new ArrayList<>();
        this.background = new ClickableInventoryItem(new EmptyClickAction(false), Material.AIR);

        this.staticInventory = new ClickableInventory(new InventoryBuilder(rows, title).build());
        this.uniqueId = UUID.randomUUID();
    }

    /*
     * ----------------- Public Methods ---------------
     */

    // ----- Element -----

    /**
     * Adds an array of elements
     *
     * @param elements      an array of elements
     */
    @Override
    public void addElements(Element... elements) {
        this.elementList.addAll(Arrays.asList(elements));
    }

    /**
     * Removes an element
     *
     * @param element       an element
     */
    @SuppressWarnings("WeakerAccess")
    public void removeElement(Element element) {
        this.elementList.remove(element);
    }

    @Override
    public void removeElements(Element... elements) {
        for (Element element : elements)
            this.removeElement(element);
    }

    @Override
    public void removeElements(Position... positions) {
        for (Position position : positions)
            this.removeElement(position);
    }

    private void removeElement(Position position) {
        ListHelper.saveRemove(this.elementList, element -> element.getPosition().equals(position));
    }

    /**
     * Returns an element at the specific position
     *
     * @param position      a position
     * @return              an element
     */
    @Override
    public Element getElement(Position position) {
        return this.elementList
                .stream()
                .filter(element -> element.getPosition().equals(position))
                .findFirst()
                .orElse(null);
    }

    /**
     * Returns an immutable list of elements
     *
     * @return      an immutable list of elements
     */
    @Override
    public ImmutableList<Element> getElements() {
        return ImmutableList.<Element>builder()
                .addAll(this.elementList)
                .build();
    }

    // ----- Inventory build methods

    /**
     * Constructs the static inventory
     */
    public void constructStatic() {
        this.clearInventory(this.staticInventory);
        Matrix<ClickableInventoryItem> matrix = new Matrix<>();

        this.merge(matrix, Element.Type.STATIC);
        this.fillBackground(this.staticInventory);

        matrix.getAll().forEach((position, clickableItem) -> {
            int slot = this.translation.getSlot(position);
            if (slot == -1)
                return;

            ItemStack itemStack = clickableItem.getItem();
            this.staticInventory.getInventory().setItem(slot, itemStack);
            this.staticInventory.setAction(itemStack, clickableItem.getAction());
        });
    }

    /**
     * Constructs the dynamic inventory
     *
     * @param player        a player
     * @return              a clickable inventory
     */
    private ClickableInventory constructDynamic(Player player) {
        this.guiInventoryRegistry.checkRegistration(this);

        ClickableInventory dynamicClickableInventory = new ClickableInventory(this.staticInventory, this.title);
        Matrix<ClickableInventoryItem> matrix = new Matrix<>();

        this.merge(matrix, Element.Type.DYNAMIC);

        matrix.getAll().forEach((position, clickableItem) -> {
            int slot = this.translation.getSlot(position);

            ItemStack itemStack = clickableItem.getItem();
            itemStack = clickableItem.getItemModifyAction().onCreate(player, itemStack);
            dynamicClickableInventory.getInventory().setItem(slot, itemStack);
            dynamicClickableInventory.setAction(itemStack, clickableItem.getAction());
        });

        this.fillBackground(dynamicClickableInventory);
        this.activeInventoryManager.register(player, dynamicClickableInventory);
        return dynamicClickableInventory;
    }

    /**
     * Opens the inventory
     *
     * @param player        a player
     */
    @Override
    public void open(Player player) {
        player.openInventory(this.getInventory(player));
    }

    /*
     * ----------------- Getter Method ---------------
     */

    /**
     * Gets the inventory for a player
     *
     * @param player        a player
     * @return              an inventory
     */
    @Override
    public Inventory getInventory(Player player) {
        return this.constructDynamic(player).getInventory();
    }

    /*
     * ----------------- UsesListener unregister Method ---------------
     */

    @Override
    public void unregisterListeners() {
        this.guiInventoryRegistry.unregister(this.getUniqueId());
    }

    /*
     * ----------------- Private Methods ---------------
     */

    /**
     * Merges the matrix of the elements into another matrix
     *
     * @param matrix        the matrix
     * @param type          the element type
     */
    private void merge(Matrix<ClickableInventoryItem> matrix, Element.Type type) {
        this.elementList.stream()
                .filter(element -> element.getType() == type)
                .forEach(element -> matrix.merge(element.getMatrix(), element.getPosition()));
    }

    /**
     * Clears the inventory
     *
     * @param clickableInventory        an inventory
     */
    private void clearInventory(ClickableInventory clickableInventory) {
        clickableInventory.clearActions();
        for (int i = 0; i < clickableInventory.getInventory().getStorageContents().length; i++)
            clickableInventory.getInventory().setItem(i, new ItemStack(Material.AIR));
    }

    /**
     * Fills the background
     *
     * @param clickableInventory        a clickable inventory
     */
    private void fillBackground(ClickableInventory clickableInventory) {
        Inventory inventory = clickableInventory.getInventory();

        for (int i = 0; i < inventory.getStorageContents().length; i++) {
            ItemStack itemStack = inventory.getItem(i);
            if (!MaterialUtils.isAir(itemStack))
                continue;

            inventory.setItem(i, this.background.getItem());
        }
        clickableInventory.setAction(this.background.getItem(), this.background.getAction());
    }

}
