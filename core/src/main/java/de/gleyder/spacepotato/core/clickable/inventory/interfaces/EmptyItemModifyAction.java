package de.gleyder.spacepotato.core.clickable.inventory.interfaces;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class EmptyItemModifyAction implements ItemModifyAction {

    @Override
    public ItemStack onCreate(@NotNull Player player, @NotNull ItemStack itemStack) {
        return itemStack;
    }

}
