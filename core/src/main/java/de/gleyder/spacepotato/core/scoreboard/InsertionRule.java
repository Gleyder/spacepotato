package de.gleyder.spacepotato.core.scoreboard;

/**
 * Rules for an entry insertion
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum InsertionRule {

    /**
     * The entry is inserted at the front of the array
     */
    FIRST,

    /**
     * The entry is inserted at the end of the array
     */
    LAST
}
