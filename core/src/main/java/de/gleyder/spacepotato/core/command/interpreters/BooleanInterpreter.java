package de.gleyder.spacepotato.core.command.interpreters;

import com.google.common.collect.Lists;
import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;

import java.util.List;

/**
 * Translates a string into a boolean.
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class BooleanInterpreter implements Interpreter<Boolean> {

    /**
     * Translates an argument into a boolean
     *
     * @param argument                          an argument
     * @return                                  a boolean
     * @throws InterpreterException     if the argument cannot be translated
     */
    @Override
    public Boolean translate(String argument) throws InterpreterException {
        if (!(argument.equalsIgnoreCase("true") || argument.equalsIgnoreCase("false")))
            throw new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_BOOLEAN.getKeyableMessage(true)
                    .replace("argument", argument)
                    .build()
            );
        return Boolean.valueOf(argument);
    }

    /**
     * Returns only the values 'true' or 'false'
     *
     * @return      a list of strings
     */
    @Override
    public List<String> getTabCompletion() {
        return Lists.newArrayList("true", "false");
    }


    @Override
    public Class<Boolean> getClassType() {
        return Boolean.class;
    }

}
