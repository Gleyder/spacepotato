package de.gleyder.spacepotato.core.command.entities;

import de.gleyder.spacepotato.core.command.interfaces.Describable;

import java.util.List;

/**
 * Represents a sub command
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.command.entities.CommandPart
 * @see de.gleyder.spacepotato.core.command.interfaces.Describable
 */
public interface SubCommand extends CommandPart, Describable {

    boolean hasEndless();
    int getLastArgumentIndex();
    List<SubCommandCategory> getCategories();

}
