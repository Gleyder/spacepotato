package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;


/**
 * Translates an argument into a float.
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class FloatInterpreter implements Interpreter<Float> {

    /**
     * Translates an argument into a float
     *
     * @param argument                          an argument
     * @return                                  a float
     * @throws InterpreterException     if the argument cannot be translated
     */
    @Override
    public Float translate(String argument) throws InterpreterException {
        try {
            return Float.valueOf(argument);
        } catch (NumberFormatException e) {
            throw new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_FLOAT.getKeyableMessage(true)
                    .replace("argument", argument)
                    .replace("min", Float.MIN_VALUE)
                    .replace("max", Float.MAX_VALUE)
                    .build()
            );
        }
    }

    @Override
    public String getTabDescription() {
        return TabCompletionsHelper.getAsSingleTypeString("number");
    }


    @Override
    public Class<Float> getClassType() {
        return Float.class;
    }

}
