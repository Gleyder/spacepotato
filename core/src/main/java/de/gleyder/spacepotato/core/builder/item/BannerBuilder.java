package de.gleyder.spacepotato.core.builder.item;

import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.NonNull;
import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import java.util.Arrays;
import java.util.List;

/**
 * Builder for banner.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class BannerBuilder implements IBuilder<ItemStack> {

    private final ItemStack item;
    private final BannerMeta meta;

    /**
     * Creates a BannerBuilder
     *
     * @param item      an ItemStack
     * @param copy      if it should be copied
     */
    public BannerBuilder(@NonNull ItemStack item, boolean copy) {
        this.item = copy ? item.clone() : item;
        this.meta = (BannerMeta) this.item.getItemMeta();
    }

    /**
     * Creates a BannerBuilder
     *
     * @param item      an ItemStack
     */
    public BannerBuilder(@NonNull ItemStack item) {
        this(item, false);
    }

    /**
     * Adds a pattern
     *
     * @param pattern       a pattern
     * @return              this BannerBuilder
     */
    public BannerBuilder addPattern(Pattern pattern) {
        this.meta.addPattern(pattern);
        return this;
    }

    /**
     * Adds a pattern
     *
     * @param color     a DyeColor
     * @param type      a PatternType
     * @return          this BannerBuilder
     */
    public BannerBuilder addPattern(DyeColor color , PatternType type) {
        this.meta.addPattern(new Pattern(color, type));
        return this;
    }

    /**
     * Sets a list of patterns
     *
     * @param patterns      a list of patterns
     * @return              this BannerBuilder
     */
    public BannerBuilder setPatterns(List<Pattern> patterns) {
        this.meta.setPatterns(patterns);
        return this;
    }

    /**
     * Sets an array of patterns
     *
     * @param patterns      an array of patterns
     * @return              this BannerBuilder
     */
    public BannerBuilder setPatterns(Pattern... patterns) {
        this.setPatterns(Arrays.asList(patterns));
        return this;
    }

    /**
     * Adds a list of patterns
     *
     * @param patterns      a list of patterns
     * @return              this BannerBuilder
     */
    public BannerBuilder addPatterns(List<Pattern> patterns) {
        List<Pattern> patternList = this.meta.getPatterns();
        patternList.addAll(patterns);
        this.meta.setPatterns(patternList);
        return this;
    }

    /**
     * Adds an array of patterns
     *
     * @param patterns      an array of patterns
     * @return              this BannerBuilder
     */
    public BannerBuilder addPatterns(Pattern... patterns) {
        this.addPatterns(Arrays.asList(patterns));
        return this;
    }

    /**
     * Converts this BannerBuilder to an ItemBuilder
     *
     * @return          an ItemBuilder
     */
    public ItemBuilder toItemBuilder() {
        return new ItemBuilder(this.build());
    }

    /**
     * Builds the ItemStack
     *
     * @return      an ItemStack
     */
    @Override
    public ItemStack build() {
        this.item.setItemMeta(this.meta);
        return this.item;
    }

}
