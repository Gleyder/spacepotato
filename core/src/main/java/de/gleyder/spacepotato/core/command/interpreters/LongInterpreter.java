package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;

/**
 * Translates an argument into a long
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class LongInterpreter implements Interpreter<Long> {


    /**
     * Translates an argument into a long
     *
     * @param argument                          an argument
     * @return                                  an long
     * @throws InterpreterException     if the argument cannot be translated
     */
    @Override
    public Long translate(String argument) throws InterpreterException {
        try {
            return Long.valueOf(argument);
        } catch (NumberFormatException e) {
            throw new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_LONG.getKeyableMessage(true)
                    .replace("argument", argument)
                    .replace("min", Long.MIN_VALUE)
                    .replace("max", Long.MAX_VALUE)
                    .build()
            );
        }
    }

    @Override
    public String getTabDescription() {
        return TabCompletionsHelper.getAsSingleTypeString("number");
    }


    @Override
    public Class<Long> getClassType() {
        return Long.class;
    }

}
