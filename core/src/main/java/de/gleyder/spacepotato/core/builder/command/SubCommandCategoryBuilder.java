package de.gleyder.spacepotato.core.builder.command;

import de.gleyder.spacepotato.core.command.entities.SubCommandCategory;
import de.gleyder.spacepotato.core.command.enums.LabelType;
import de.gleyder.spacepotato.core.command.impl.SubCommandCategoryImpl;
import org.bukkit.event.EventPriority;
import org.jetbrains.annotations.NotNull;

/**
 * Builder for SubCommandCategories.
 * The same instance can be reused after calling build()
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SubCommandCategoryBuilder extends AbstractCommandPartBuilder<SubCommandCategory, SubCommandCategoryBuilder> {

    private LabelType labelType;
    private EventPriority eventPriority;

    /**
     * Sets the labelType
     *
     * @param labelType     a labelType
     * @return              this builder
     */
    public SubCommandCategoryBuilder setLabelType(@NotNull LabelType labelType) {
        this.labelType = labelType;
        return this;
    }

    public SubCommandCategoryBuilder setEventPriority(@NotNull EventPriority eventPriority) {
        this.eventPriority = eventPriority;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void reset() {
        super.reset();
        this.labelType = null;
        this.eventPriority = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubCommandCategory build() {
        SubCommandCategoryImpl subCommandCategory = new SubCommandCategoryImpl(
                this.argumentList,
                this.senderType,
                this.conditionContainer,
                this.labelType,
                this.eventPriority,
                this.senderConverter
        );
        this.tryAutoSetMethodName();
        subCommandCategory.getExecutor().setMethodName(this.methodName);
        this.reset();
        return subCommandCategory;
    }

}
