package de.gleyder.spacepotato.core.builder.item.exceptions;

import lombok.NonNull;

/**
 * Thrown if a builder gets an item which is not supported.
 *
 * Example:
 * If you parse a potion to a book builder
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class MaterialNotSupportedException extends RuntimeException {

    /**
     * Creates a MaterialNotSupportedException
     *
     * @param message       the message
     */
    public MaterialNotSupportedException(@NonNull String message) {
        super(message);
    }
}
