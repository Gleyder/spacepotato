package de.gleyder.spacepotato.core.command.impl;

import de.gleyder.spacepotato.core.command.entities.StaticArgument;
import de.gleyder.spacepotato.core.command.enums.ArgumentType;
import de.gleyder.spacepotato.core.utils.string.StringUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Implementation of StaticArgument.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@ToString
public class StaticArgumentImpl extends ArgumentImpl implements StaticArgument {

    @Setter
    @Getter
    protected String label;

    /**
     * Creates a StaticArgumentImpl
     *
     * @param label     a label
     */
    public StaticArgumentImpl(String label) {
        super(ArgumentType.STATIC);
        this.label = StringUtil.emptyIfNull(label);
    }

}
