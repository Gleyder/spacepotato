package de.gleyder.spacepotato.core.command.enums;

/**
 * Enum for an interpreter modes
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum InterpreterMode {

    /**
     * Indicates that the interpreter translates the argument individually
     */
    SINGLE,

    /**
     * Indicates that interpreter translates the argument as one
     */
    MERGED;

    /**
     * Returns the default mode
     *
     * @return      a mode
     */
    public static InterpreterMode getDefault() {
        return MERGED;
    }

}
