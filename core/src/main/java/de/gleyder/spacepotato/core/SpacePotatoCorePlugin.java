package de.gleyder.spacepotato.core;

import de.gleyder.spacepotato.core.configuration.GlobalConfig;
import de.gleyder.spacepotato.core.configuration.SpacePotatoConfigBinder;
import de.gleyder.spacepotato.core.configuration.handler.LoadoutHandler;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import de.gleyder.spacepotato.core.configuration.messages.MessageLoader;
import de.gleyder.spacepotato.core.configuration.sounds.SoundConfig;
import lombok.Getter;
import org.bukkit.NamespacedKey;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main class.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SpacePotatoCorePlugin extends JavaPlugin {

    @Getter
    private static GlobalConfig globalConfig;

    @Getter
    private final SpacePotatoConfigBinder spacePotatoConfigBinder;

    private final LoadoutHandler loadoutHandler;

    @Getter
    private MessageLoader messageLoader;

    /**
     * Creates a SpacePotatoCorePlugin
     */
    public SpacePotatoCorePlugin() {
        this.loadoutHandler = new LoadoutHandler(this);
        this.spacePotatoConfigBinder = new SpacePotatoConfigBinder(this, null, "globalConfig");
        this.loadoutHandler.addFile(this.spacePotatoConfigBinder, GlobalConfig.class, SoundConfig.class);
    }

    /**
     * Called on load
     */
    @Override
    public void onLoad() {
        this.loadoutHandler.load();
        globalConfig = this.spacePotatoConfigBinder.bind("", GlobalConfig.class);

        this.messageLoader = new MessageLoader(this, CoreMessages.class);
        this.messageLoader.load();
    }

    /**
     * Returns the bukkit Namespace
     *
     * @param key       a key
     * @return          a NamespacedKey
     */
    public static NamespacedKey getBukkitNamespace(String key)  {
        return NamespacedKey.minecraft(key);
    }

}
