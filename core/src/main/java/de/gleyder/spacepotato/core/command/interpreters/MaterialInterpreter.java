package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import de.gleyder.spacepotato.core.utils.MaterialUtils;
import org.bukkit.Material;

import java.util.List;

/**
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class MaterialInterpreter implements Interpreter<Material> {

    @Override
    public Material translate(String argument) throws InterpreterException {
        Material material = Material.getMaterial(argument.toUpperCase());

        if (material != null) {
            return material;
        } else {
            throw new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_MATERIAL.getKeyableMessage(false)
                    .replace("argument", argument)
                    .build()
            );
        }
    }

    @Override
    public List<String> getTabCompletion() {
        return MaterialUtils.getAsStringKeyList();
    }

    @Override
    public Class<Material> getClassType() {
        return Material.class;
    }

}
