package de.gleyder.spacepotato.core.gui.elements;

import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryItem;
import de.gleyder.spacepotato.core.clickable.inventory.interfaces.BlockingClickAction;
import de.gleyder.spacepotato.core.gui.GUIInventory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Material;

/**
 * Creates elements.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class ElementCreator {

    @Getter
    private final GUIInventory guiInventory;

    /**
     * Set the padding
     *
     * @param thickness     the thickness
     * @param material      the material
     */
    public void setPadding(int thickness, Material material) {
        ClickableInventoryItem paddingItem = new ClickableInventoryItem(new BlockingClickAction(), material);

        Element topPadding = new Element(0, Math.max(this.getInventoryHigh()-thickness, 0), Element.Type.STATIC);
        topPadding.getMatrix().combine(PrimitiveShapes.createRectangle(9, thickness, paddingItem));

        Element bottomPadding = new Element(0, 0, Element.Type.STATIC);
        bottomPadding.getMatrix().combine(PrimitiveShapes.createRectangle(9, thickness, paddingItem));

        Element leftPadding = new Element(0, 0, Element.Type.STATIC);
        leftPadding.getMatrix().combine(PrimitiveShapes.createRectangle(thickness, this.getInventoryHigh(), paddingItem));

        Element rightPadding = leftPadding.duplicate();
        rightPadding.getPosition().setX(9-thickness);

        this.guiInventory.addElements(topPadding, bottomPadding, leftPadding, rightPadding);
    }

    /**
     * Gets the inventory high
     *
     * @return      the high
     */
    private int getInventoryHigh() {
        return this.guiInventory.getRows();
    }

}
