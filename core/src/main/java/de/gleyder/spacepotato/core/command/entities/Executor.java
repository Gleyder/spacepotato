package de.gleyder.spacepotato.core.command.entities;

import de.gleyder.spacepotato.core.command.supplier.SupplierValues;
import org.bukkit.command.CommandSender;

import java.lang.reflect.Method;

/**
 * Represents the executor to execute the methods
 * for the commands
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface Executor {

    Method getMethod();
    String getName();

    /**
     * Executes a method
     *
     * @param sender        a sender
     * @param values        a supplier values
     */
    void execute(CommandSender sender, SupplierValues values);

    /**
     * Executes a category method
     *
     * @param sender            a sender
     * @param values            a supplier values
     * @param extraValues       a supplier values which should be added to the normal
     * @return                  if it was successful
     */
    boolean prepare(CommandSender sender, SupplierValues values, SupplierValues extraValues);

}
