package de.gleyder.spacepotato.core.chat;

import org.bukkit.entity.Player;

/**
 * Formats the output string for chat
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface ChatFormatter {

    String format(Player player, String message);

}
