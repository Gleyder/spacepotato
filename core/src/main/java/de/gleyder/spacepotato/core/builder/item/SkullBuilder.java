package de.gleyder.spacepotato.core.builder.item;

import com.destroystokyo.paper.profile.PlayerProfile;
import de.gleyder.spacepotato.core.builder.item.exceptions.MaterialNotSupportedException;
import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.UUID;

/**
 * Builder for skulls with a skin.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SkullBuilder implements IBuilder<ItemStack> {

    private final ItemStack item;
    private final SkullMeta meta;

    /**
     * Creates a SkullBuilder
     */
    public SkullBuilder() {
        this(new ItemStack(Material.SKELETON_SKULL), false);
    }

    /**
     * Creates an SkullBuilder
     *
     * @param itemStack     an ItemStack
     * @param copy          if the item should be copied
     */
    public SkullBuilder(@NonNull ItemStack itemStack, boolean copy) {
        if (itemStack.getType() != Material.PLAYER_HEAD)
            throw new MaterialNotSupportedException("Only Material.PLAYER_HEAD is supported");

        if (copy)
            this.item = itemStack.clone();
        else
            this.item = itemStack;
        this.meta = (SkullMeta) this.item.getItemMeta();
    }

    /**
     * Sets the owner
     *
     * @param uuid      the uuid
     * @return          this Builder
     */
    public SkullBuilder setOwner(UUID uuid) {
        if (uuid == null) {
            this.meta.setOwningPlayer(null);
            return this;
        }

        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
        this.meta.setOwningPlayer(offlinePlayer);
        return this;
    }

    public SkullBuilder setProfile(PlayerProfile profile) {
        this.meta.setPlayerProfile(profile);
        return this;
    }

    /**
     * Converts it to a ItemBuilder
     *
     * @return      an ItemBuilder
     */
    public ItemBuilder toItemBuilder() {
        return new ItemBuilder(this.build());
    }

    /**
     * Builds the ItemStack
     *
     * @return      an ItemStack
     */
    @Override
    public ItemStack build() {
        this.item.setItemMeta(this.meta);
        return this.item;
    }

}