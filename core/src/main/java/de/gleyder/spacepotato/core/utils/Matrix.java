package de.gleyder.spacepotato.core.utils;

import java.util.HashMap;

/**
 * Stores values in a matrix
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class Matrix<T> {

    private final HashMap<Position, T> map;

    /**
     * Creates a Matrix
     */
    public Matrix() {
        this.map = new HashMap<>();
    }

    /**
     * Sets a value at a position
     *
     * @param position      a position
     * @param t             a value
     */
    public void set(Position position, T t) {
        if (this.get(position) != null) {
            this.map.replace(position, t);
        }

        this.map.put(position, t);
    }

    /**
     * Removes a position
     *
     * @param position      a position
     */
    public void remove(Position position) {
        this.set(position, null);
    }

    /**
     * Gets the type of position
     *
     * @param x     the x value
     * @param y     the y value
     * @return      the type
     */
    public T get(int x, int y) {
        return this.map.get(new Position(x, y));
    }

    /**
     * Gets the type of position
     *
     * @param position      a position
     * @return              the type
     */
    public T get(Position position) {
        return this.map.get(position);
    }

    /**
     * Clear the matrix
     */
    public void clear() {
        this.map.clear();
    }

    /**
     * Returns the content of the matrix
     *
     * @return      a map
     */
    public HashMap<Position, T> getAll() {
        return this.map;
    }

    /**
     * Rotates the matrix item's
     *
     * @param angle     the angle
     */
    public void rotate(double angle) {
        for (Position position : this.map.keySet()) {
            int x = position.getX(), y = position.getY();

            position.setX((int) Math.round(x * Math.cos(angle) - y * Math.sin(angle)));
            position.setY((int) Math.round(y * Math.cos(angle) + x * Math.sin(angle)));
        }
    }

    /**
     * Combines the matrix
     *
     * @param matrix        another matrix
     */
    public void combine(Matrix<T> matrix) {
        this.map.putAll(matrix.map);
    }

    /**
     * Merges two matrix with an anchor
     *
     * @param matrix        a matrix
     * @param anchor        an anchor
     */
    public void merge(Matrix<T> matrix, Position anchor) {
        matrix.getAll().forEach((position, t) -> {
            Position newPos = position.duplicate().add(anchor);
            this.set(newPos, t);
        });
    }

}
