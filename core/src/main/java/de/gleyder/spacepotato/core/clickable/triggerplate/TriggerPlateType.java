package de.gleyder.spacepotato.core.clickable.triggerplate;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

/**
 * Enum for types of trigger plates.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum TriggerPlateType {

    STONE(Material.STONE_PRESSURE_PLATE),
    GOLD(Material.LIGHT_WEIGHTED_PRESSURE_PLATE),
    IRON(Material.HEAVY_WEIGHTED_PRESSURE_PLATE),
    OAK(Material.OAK_PRESSURE_PLATE),
    SPRUCE(Material.SPRUCE_PRESSURE_PLATE),
    BIRCH(Material.BIRCH_PRESSURE_PLATE),
    JUNGLE(Material.JUNGLE_PRESSURE_PLATE),
    ACACIA(Material.ACACIA_PRESSURE_PLATE),
    DARK_OAK(Material.DARK_OAK_PLANKS)
    ;

    @Getter
    private final Material material;

    /**
     * Creates a TriggerPlateType
     *
     * @param material      a material
     */
    TriggerPlateType(Material material) {
        this.material = material;
    }

    public ItemStack getItem() {
        return new ItemStack(this.material);
    }

    /**
     * Returns the trigger plate type of a block
     *
     * @param block     a block
     * @return          a plate type
     *                  {null} if the block is no pressure plate
     */
    public static TriggerPlateType fromBlock(Block block) {
        return fromMaterial(block.getType());
    }

    /**
     * Returns the trigger plate type of a block
     *
     * @param material     a material
     * @return              a plate type
     *                      {null} if the block is no pressure plate
     */
    public static TriggerPlateType fromMaterial(Material material) {
        for (TriggerPlateType type : values()) {
            if (type.material == material)
                return type;
        }
        return null;
    }

}
