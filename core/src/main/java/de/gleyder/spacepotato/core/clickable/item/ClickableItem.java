package de.gleyder.spacepotato.core.clickable.item;

import de.gleyder.spacepotato.core.analyzer.item.ItemSearchQuery;
import de.gleyder.spacepotato.core.clickable.item.interfaces.ItemClickAction;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ClickableItem {

    @Getter
    private final ItemStack itemStack;

    private final ItemSearchQuery itemSearchQuery;

    @Getter(AccessLevel.PROTECTED)
    private final ItemClickAction action;

    @Getter
    private final UUID uuid;

    public ClickableItem(@NonNull ItemStack itemStack, @NonNull ItemSearchQuery itemSearchQuery, @NonNull ItemClickAction action) {
        this(itemStack, itemSearchQuery, action, UUID.randomUUID());
    }

    public ClickableItem(@NonNull ItemStack itemStack,
                         @NonNull ItemSearchQuery itemSearchQuery,
                         @NonNull ItemClickAction action,
                         @NonNull UUID uuid) {
        this.itemStack = itemStack;
        this.itemSearchQuery = itemSearchQuery;
        this.action = action;
        this.uuid = uuid;
    }

    protected boolean test(@NonNull ItemStack itemStack) {
        return this.itemSearchQuery.getBiPredicate().test(this.itemStack, itemStack);
    }

}
