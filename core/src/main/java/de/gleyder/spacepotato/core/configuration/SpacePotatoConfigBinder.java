package de.gleyder.spacepotato.core.configuration;

import de.gleyder.spacepotato.core.utils.string.StringUtil;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;
import org.cfg4j.provider.ConfigurationProvider;
import org.cfg4j.provider.ConfigurationProviderBuilder;
import org.cfg4j.source.ConfigurationSource;
import org.cfg4j.source.context.environment.Environment;
import org.cfg4j.source.context.environment.ImmutableEnvironment;
import org.cfg4j.source.context.filesprovider.ConfigFilesProvider;
import org.cfg4j.source.files.FilesConfigurationSource;
import org.cfg4j.source.reload.ReloadStrategy;
import org.cfg4j.source.reload.strategy.ImmediateReloadStrategy;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Paths;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SpacePotatoConfigBinder {

    @Getter
    private final ConfigurationProvider configurationProvider;

    @Getter
    private final String filename;

    /**
     * Creates a SpacePotatoConfig
     *
     *
     * @param plugin        a plugin
     * @param path          the path the file, but not the file itself
     * @param file          the file name without extension
     */
    public SpacePotatoConfigBinder(@NotNull JavaPlugin plugin, String path, @NotNull String file) {
        path = StringUtil.emptyIfNull(path);
        file = ConfigHelper.withExtension(file, ConfigHelper.YAML_EXTENSION);

        if (path.isEmpty())
            this.filename = file;
        else
            this.filename = path + "/" + file;

        //Copies the filename to a final variable for lambda expression
        @NotNull String finalFile = file;
        ConfigFilesProvider provider = () -> Paths.get(finalFile);
        ConfigurationSource source = new FilesConfigurationSource(provider);
        Environment environment = new ImmutableEnvironment(plugin.getDataFolder().getAbsolutePath() + "/" + path);
        ReloadStrategy reloadStrategy = new ImmediateReloadStrategy();

        this.configurationProvider = new ConfigurationProviderBuilder()
                .withConfigurationSource(source)
                .withEnvironment(environment)
                .withReloadStrategy(reloadStrategy)
                .build();
    }

    public <T> T bind(@NotNull String prefix, Class<T> tClass) {
        return this.configurationProvider.bind(prefix, tClass);
    }

}
