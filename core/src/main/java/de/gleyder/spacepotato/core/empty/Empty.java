package de.gleyder.spacepotato.core.empty;

import com.google.common.collect.ImmutableList;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

/**
 * Base class for empties.
 *
 * @author Gleyder
 * @version 0.1
 */
public abstract class Empty {

    final Set<Player> overlappingPlayers;

    protected final Set<Vector> markedPoints;

    @Getter
    protected final World world;

    @Setter
    @Getter
    protected boolean drawOutline;

    /**
     * Creates an Empty
     *
     * @param world     the empty's world
     */
    public Empty(World world) {
        this.markedPoints = new HashSet<>();
        this.overlappingPlayers = new HashSet<>();
        this.world = world;
    }

    /**
     * Called when the empty is loaded
     */
    public void onLoad() { }

    /**
     * Called when the empty is unloaded
     */
    public void onUnload() { }

    /**
     * Called when the outline should be calculated
     */
    public void calculateOutline() { }

    /**
     * Marks the outline
     */
    public final void markOutline() {
        this.markedPoints.forEach(vector -> this.world.spawnParticle(Particle.FLAME, vector.toLocation(this.world), 1, 0, 0, 0, 0));
    }

    /**
     * Called when a players leaves or joins the empty
     *
     * @param player            the player
     * @param changeType        if the player joined or left
     */
    public abstract void onPlayerChangeEvent(Player player, PlayerEmptyChangeType changeType);

    /**
     * Checks if player joined or left the empty
     *
     * @param playerLocation        the player's location
     * @return                      a PlayerEmptyChangeType
     */
    public abstract PlayerEmptyChangeType check(Location playerLocation);

    /**
     * Returns an immutable list of overlapping players
     *
     * @return                  an immutable list
     */
    public final ImmutableList<Player> getOverlappingPlayers() {
        return new ImmutableList.Builder<Player>()
                .addAll(this.overlappingPlayers)
                .build();
    }

}
