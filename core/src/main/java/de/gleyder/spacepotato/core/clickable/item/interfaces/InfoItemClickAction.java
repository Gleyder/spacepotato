package de.gleyder.spacepotato.core.clickable.item.interfaces;

import de.gleyder.spacepotato.core.utils.string.SuppliedString;
import lombok.NonNull;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class InfoItemClickAction implements ItemClickAction {

    @Override
    public boolean onClick(@NonNull Player player, @NonNull ItemStack itemStack, @NonNull EquipmentSlot slot, @Nullable Action action, @Nullable Block clickedBlock, @Nullable Entity clickedEntity) {
        SuppliedString suppliedString;
        if (clickedBlock != null) {
            suppliedString = new SuppliedString("§aMaterial: {}\n§6X: {}\nY: {}\nZ: {}\n§bEquipmentSlot: {}\n§eAction: {}\n§1=================");
            Location location = clickedBlock.getLocation();

            suppliedString.addValues(clickedBlock.getType());
            suppliedString.addValues(location.getBlockX(), location.getBlockY(), location.getBlockZ());
            suppliedString.addValues(slot);
            suppliedString.addValues(action);
        } else if (clickedEntity != null) {
            suppliedString = new SuppliedString("§aUUID: {}\nType: §6{}\nX: {}\nY: {}\nZ: {}\nPitch: {}\nYaw: {}\n§bEquipmentSlot: {}\n§1=================");
            Location location = clickedEntity.getLocation();

            suppliedString.addValues(clickedEntity.getUniqueId());
            suppliedString.addValues(clickedEntity.getType());
            suppliedString.addValues(location.getX(), location.getY(), location.getZ(), location.getPitch(), location.getYaw());
            suppliedString.addValues(slot);
        } else {
            suppliedString = new SuppliedString("§aMaterial: {}\n§bEquipmentSlot: {}\n§eAction: {}\n§1=================");
            suppliedString.addValues(Material.AIR);
            suppliedString.addValues(slot);
            suppliedString.addValues(action);
        }

        player.sendMessage(suppliedString.build());
        return true;
    }

}
