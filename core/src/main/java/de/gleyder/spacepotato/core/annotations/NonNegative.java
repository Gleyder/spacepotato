package de.gleyder.spacepotato.core.annotations;

import javax.annotation.meta.TypeQualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Indicates that the field cannot be negative
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@TypeQualifier(
        applicableTo = Number.class
)
@Retention(RetentionPolicy.SOURCE)
public @interface NonNegative {

    /**
     * If zero is included
     *
     * @return      a boolean
     */
    boolean includeZero() default true;

}
