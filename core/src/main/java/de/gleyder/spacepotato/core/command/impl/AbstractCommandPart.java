package de.gleyder.spacepotato.core.command.impl;

import com.google.common.collect.ImmutableList;
import de.gleyder.spacepotato.core.command.entities.Argument;
import de.gleyder.spacepotato.core.command.entities.CommandPart;
import de.gleyder.spacepotato.core.command.enums.SenderType;
import lombok.Getter;
import lombok.ToString;
import org.bukkit.command.CommandSender;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.function.Function;

/**
 * Implementation of CommandPart.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@ToString
public abstract class AbstractCommandPart implements CommandPart {

    protected final Deque<ArgumentImpl> arguments;

    @Getter
    protected final SenderType type;

    @Getter
    protected final ExecutorImpl executor;

    @Getter
    protected ConditionContainerImpl conditionContainer;

    /**
     * Creates an AbstractCommandPart
     *
     * @param arguments     a list of arguments
     * @param type          a type
     */
    AbstractCommandPart(List<ArgumentImpl> arguments, SenderType type, ConditionContainerImpl conditionContainer, Function<CommandSender, Object> senderConverter) {
        this.arguments = arguments == null ? new ArrayDeque<>() : new ArrayDeque<>(arguments);
        this.type = type == null ? SenderType.getDefault() : type;
        this.executor = new ExecutorImpl(senderConverter);
        this.conditionContainer = conditionContainer == null ? new ConditionContainerImpl(null) : conditionContainer;
    }

    @Override
    public ImmutableList<Argument> getArguments() {
        return new ImmutableList.Builder<Argument>().addAll(this.arguments).build();
    }

}
