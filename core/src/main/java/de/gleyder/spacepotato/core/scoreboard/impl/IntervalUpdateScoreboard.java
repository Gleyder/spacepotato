package de.gleyder.spacepotato.core.scoreboard.impl;

import de.gleyder.spacepotato.core.chrono.Looper;
import de.gleyder.spacepotato.core.scoreboard.ScoreboardCreation;
import lombok.NonNull;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.RenderType;
import org.jetbrains.annotations.NotNull;

/**
 * Implementation for an interval scoreboard
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.scoreboard.impl.AbstractEasyScoreboard
 */
public class IntervalUpdateScoreboard extends AbstractEasyScoreboard {

    private final Looper looper;

    /**
     * {@inheritDoc}
     *
     *  Starts the lopper instant
     */
    public IntervalUpdateScoreboard(@NonNull ScoreboardCreation creation, @NonNull String displayName, String name, String criteria, RenderType renderType, DisplaySlot displaySlot, @NotNull JavaPlugin plugin) {
        super(creation, displayName, name, criteria, renderType, displaySlot);
        this.looper = new IntervallLooper(plugin);
        this.looper.start();
    }

    /**
     * Starts the updater
     */
    public void start() {
        this.looper.start();
    }

    /**
     * Stops the updater
     */
    public void stop() {
        this.looper.stop();
    }

    /**
     * Impl for the looper
     */
    private class IntervallLooper extends Looper {

        IntervallLooper(JavaPlugin plugin) {
            super(10, true, plugin);
        }

        @Override
        public void onUpdate() {
            update();
        }

    }

}
