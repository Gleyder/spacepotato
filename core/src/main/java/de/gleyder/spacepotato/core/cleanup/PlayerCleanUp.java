package de.gleyder.spacepotato.core.cleanup;

import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

/**
 * Class for player methods.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public final class PlayerCleanUp {
    
    /**
     * Clean the players attributes.
     *
     * @param player the player to be cleaned up
     */
    public static void cleanPlayer(Player player) {
        player.setExp(0.0F);
        player.setLevel(0);
        player.setFoodLevel(20);
        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
        player.setFireTicks(0);
        player.setExhaustion(0.0F);
        
        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }
    }
    
    /**
     * Reset the players attributes.
     *
     * @param player the player to be cleaned up
     */
    public static void resetPlayer(Player player) {
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20.0D);
        player.setFlySpeed(0.2F);
        player.setWalkSpeed(0.2F);
        PlayerCleanUp.cleanPlayer(player);
    }
    
    /**
     * Clear the players inventory and armor slots.
     *
     * @param player the player to be cleaned up
     */
    public static void clearInventory(Player player) {
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.getInventory().setItemInOffHand(null);
    }
    
    /**
     * Clear the players inventory and armor slots without the given items.
     *
     * @param player the player to be cleaned up
     */
    public static void clearInventory(Player player, ItemStack... ignore) {
        for (ItemStack item : player.getInventory().getContents()) {
            if (item == null || item.getType().equals(Material.AIR))
                continue;
            
            if (!containsItem(item, ignore)) {
                player.getInventory().remove(item);
            }
        }
        
        for (ItemStack item : player.getInventory().getArmorContents()) {
            if (item == null || item.getType().equals(Material.AIR))
                continue;
            
            if (!containsItem(item, ignore)) {
                player.getInventory().remove(item);
            }
        }
    }
    
    /**
     * Check if an item is a subset of an item array.
     *
     * @param item     subset item
     * @param superset superset of items     *
     * @return if an item is a subset of superset
     */
    private static boolean containsItem(ItemStack item, ItemStack... superset) {
        for (ItemStack loopItem : superset) {
            if (item.getType().equals(loopItem.getType()))
                return true;
        }
        return false;
    }
    
}
