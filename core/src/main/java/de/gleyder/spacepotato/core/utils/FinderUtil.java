package de.gleyder.spacepotato.core.utils;

import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

/**
 * Class to find something in list or maps.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class FinderUtil {

    /**
     * Checks if a certain character sequence is in one of the elements
     *
     * @param iterable      a iterable
     * @param string        a string
     * @return              if it founds something or not
     */
    public static boolean containsString(Iterable<String> iterable, String string) {
        for (String s : iterable) {
            if (s.contains(string))
                return true;
        }
        return false;
    }

    /**
     * Checks if certain situation appears in the iterable
     *
     * @param iterable      a iterable
     * @param predicate     a predicate
     * @return              if it founds something or not
     */
    public static <T> T find(Iterable<T> iterable, Predicate<T> predicate) {
        for (T t : iterable) {
            if (predicate.test(t))
                return t;
        }
        return null;
    }

    /**
     * Checks if certain situation appears in the map
     *
     * @param map               the map
     * @param biPredicate       the bi predicate
     * @return                  if it founds something or not
     */
    public static <K, V> V find(Map<K, V> map, BiPredicate<K, V> biPredicate) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (biPredicate.test(entry.getKey(), entry.getValue()))
                return entry.getValue();
        }
        return null;
    }

}
