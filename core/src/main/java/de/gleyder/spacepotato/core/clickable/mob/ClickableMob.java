package de.gleyder.spacepotato.core.clickable.mob;

import de.gleyder.spacepotato.core.clickable.mob.enums.LivingEntityType;
import lombok.NonNull;
import org.bukkit.Location;

/**
 * Represents a clickable mob which is just for decoration and to be clicked
 *
 * @author Gleyder
 * @since 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.clickable.mob.SimpleClickableMob
 */
public abstract class ClickableMob extends SimpleClickableMob {

    private final String name;

    /**
     * Creates a ClickableMob
     *
     * @param type          a living entity type
     * @param name          a name
     * @param location      a location
     */
    public ClickableMob(@NonNull LivingEntityType type, @NonNull String name, @NonNull Location location) {
        super(type, location, true);
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate() {
        this.entity.setCustomNameVisible(true);
        this.entity.setCustomName(this.name);
        this.entity.setSilent(true);
        this.entity.setInvulnerable(true);
        this.entity.setCanPickupItems(false);
        this.entity.setCollidable(false);
        this.entity.setAI(false);
    }

}
