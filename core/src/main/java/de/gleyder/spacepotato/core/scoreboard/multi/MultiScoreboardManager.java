package de.gleyder.spacepotato.core.scoreboard.multi;

import de.gleyder.spacepotato.core.annotations.NonNegative;
import de.gleyder.spacepotato.core.chrono.Looper;
import de.gleyder.spacepotato.core.scoreboard.ScoreboardCreation;
import de.gleyder.spacepotato.core.scoreboard.impl.ManuelUpdateScoreboard;
import lombok.NonNull;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.RenderType;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

/**
 * Manages individually scoreboards for players.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class MultiScoreboardManager {

    private final Updater updater;
    private final HashMap<Player, ManuelUpdateScoreboard> scoreboardMap;
    private final String displayName;
    private final DisplaySlot slot;
    private final RenderType type;

    public MultiScoreboardManager(@NonNegative(includeZero = false) int ticks,
                                  @NonNull JavaPlugin plugin,
                                  @NonNull String displayName,
                                  @NonNull DisplaySlot slot,
                                  @NonNull RenderType type) {
        if (ticks <= 0)
            throw new IllegalArgumentException("Ticks must be greater than 0");
        this.updater = new Updater(ticks, plugin);
        this.scoreboardMap = new HashMap<>();
        this.displayName = displayName;
        this.slot = slot;
        this.type = type;
    }

    protected abstract void onCreate(Player player, ManuelUpdateScoreboard scoreboard);
    protected abstract void onUpdate(Player player, ManuelUpdateScoreboard scoreboard);

    public void start() {
        this.updater.start();
    }

    public void stop() {
        this.updater.stop();
    }

    public void addPlayer(Player player) {
        ManuelUpdateScoreboard scoreboard = new ManuelUpdateScoreboard(ScoreboardCreation.NEW, this.displayName, null, null, this.type, this.slot);
        this.onCreate(player, scoreboard);

        this.scoreboardMap.put(player, scoreboard);
        player.setScoreboard(scoreboard.getScoreboard());
    }

    public void removePlayer(Player player) {
        this.scoreboardMap.remove(player);
    }

    private class Updater extends Looper {

        private Updater(int ticks, @NonNull JavaPlugin plugin) {
            super(Duration.ofMillis(ticks * 80), true, plugin);
        }

        @Override
        public void onUpdate() {
            for (Map.Entry<Player, ManuelUpdateScoreboard> entry : MultiScoreboardManager.this.scoreboardMap.entrySet()) {
                MultiScoreboardManager.this.onUpdate(entry.getKey(), entry.getValue());
                entry.getValue().update();
            }
        }

    }

}
