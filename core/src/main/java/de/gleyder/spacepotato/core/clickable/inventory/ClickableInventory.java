package de.gleyder.spacepotato.core.clickable.inventory;

import com.google.common.collect.ImmutableList;
import de.gleyder.spacepotato.core.builder.inventory.InventoryBuilder;
import de.gleyder.spacepotato.core.clickable.inventory.interfaces.InventoryClickAction;
import de.gleyder.spacepotato.core.utils.ListHelper;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a clickable inventory.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ClickableInventory {

    private final Set<ClickableInventoryItem> clickableInventoryItems;

    @Getter
    private final Inventory inventory;

    /**
     * Creates a ClickableInventory
     *
     * @param inventory     an inventory
     */
    public ClickableInventory(Inventory inventory) {
        this.inventory = inventory;
        this.clickableInventoryItems = new HashSet<>();
    }

    public ClickableInventory(ClickableInventory clickableInventory, String title) {
        this.inventory = new InventoryBuilder(clickableInventory.getInventory(), title, true).build();
        this.clickableInventoryItems = new HashSet<>();
        this.clickableInventoryItems.addAll(clickableInventory.clickableInventoryItems);
    }

    public void addAction(ClickableInventoryItem clickableInventoryItem) {
        this.clickableInventoryItems.add(clickableInventoryItem);
    }

    /**
     * Sets an action for an item
     *
     * @param item          an ItemStack
     * @param action        an InventoryClickAction
     */
    public void setAction(ItemStack item, InventoryClickAction action) {
        this.clickableInventoryItems.add(new ClickableInventoryItem(action, item));
    }

    /**
     * Removes an action from a material
     *
     * @param material      a Material
     */
    public void removeAction(Material material) {
        ListHelper.saveRemove(this.clickableInventoryItems, inList -> inList.getType() == ClickableInventoryItem.Type.MATERIAL && inList.getMaterial() == material);
    }

    public void removeAction(ClickableInventoryItem item) {
        ListHelper.saveRemove(this.clickableInventoryItems, inList -> inList.equals(inList));
    }

    /**
     * Removes an action from an item
     *
     * @param item      an ItemStack
     */
    public void removeAction(ItemStack item) {
        ListHelper.saveRemove(this.clickableInventoryItems, inList -> inList.getType() == ClickableInventoryItem.Type.STACK && inList.getItemStack().equals(item));
    }

    /**
     * Clears all actions
     */
    public void clearActions() {
        this.clickableInventoryItems.clear();
    }

    /**
     * Returns an immutable map of actions
     *
     * @return          an ImmutableMap of InventoryClickActions
     */
    public List<ClickableInventoryItem> getActions() {
        return new ImmutableList.Builder<ClickableInventoryItem>().addAll(this.clickableInventoryItems).build();
    }

}
