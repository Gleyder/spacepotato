package de.gleyder.spacepotato.core.configuration.typeadapter;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import de.gleyder.spacepotato.core.configuration.pojos.SaveLocation;
import org.bukkit.Location;

import java.lang.reflect.Type;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class LocationSerializer implements JsonSerializer<Location> {

    @Override
    public JsonElement serialize(Location src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(new SaveLocation(src));
    }

}
