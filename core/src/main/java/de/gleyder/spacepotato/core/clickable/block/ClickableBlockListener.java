package de.gleyder.spacepotato.core.clickable.block;

import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

/**
 * Listener for clickable blocks.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class ClickableBlockListener implements Listener {

    private final ClickableBlockRegistry blockRegistry;

    /**
     * Executed all clickable block onClick() methods if player clicks block
     *
     * @param event     the event
     */
    @EventHandler
    public void onBlockClick(PlayerInteractEvent event) {
        if (event.getClickedBlock() == null)
            return;
        if (event.getHand() != EquipmentSlot.HAND)
            return;
        if ( !(event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_BLOCK) )
            return;

        this.blockRegistry.getMap().forEach((block, clickableBlock) -> {
            if (!clickableBlock.getBlock().getLocation().equals(event.getClickedBlock().getLocation()))
                return;

            boolean bool = clickableBlock.getAction().onClick(event.getPlayer(), event.getClickedBlock(), event.getAction());
            event.setCancelled(bool);
        });
    }

    /**
     * Removes the ClickableBlock if it gets destroyed
     *
     * @param event     the event
     */
    @EventHandler
    public void onBlockBreakEvent(BlockBreakEvent event) {
        this.blockRegistry.getMap().forEach((block, clickableBlock) -> {
            if (!clickableBlock.getBlock().getLocation().equals(event.getBlock().getLocation()))
                return;

            this.blockRegistry.unregister(event.getBlock());
        });
    }

}
