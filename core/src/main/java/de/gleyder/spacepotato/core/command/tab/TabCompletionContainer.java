package de.gleyder.spacepotato.core.command.tab;

import de.gleyder.spacepotato.core.command.entities.Argument;
import de.gleyder.spacepotato.core.command.entities.MutableArgument;
import de.gleyder.spacepotato.core.command.entities.StaticArgument;
import de.gleyder.spacepotato.core.command.entities.SubCommand;
import de.gleyder.spacepotato.core.command.enums.ArgumentType;
import de.gleyder.spacepotato.core.command.impl.ConditionContainerImpl;
import de.gleyder.spacepotato.core.command.input.InputArgument;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;
import de.gleyder.spacepotato.core.utils.string.ListSuppliedString;
import de.gleyder.spacepotato.core.utils.string.SuppliedString;
import lombok.*;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author Gleyder
 */
public class TabCompletionContainer {

    @Getter
    private final HashMap<List<CompletionArgument>, TabCompletion> map;

    public TabCompletionContainer() {
        this.map = new HashMap<>();
    }

    public void add(SubCommand subCommand) {
        List<Argument> builderList = new ArrayList<>();
        for (Argument argument : subCommand.getArguments()) {
            builderList.add(argument);
            this.addInternal(builderList, (ConditionContainerImpl) subCommand.getConditionContainer());
        }
    }

    public void info() {
        var listSuppliedString = new ListSuppliedString<>(this.map.entrySet(), entry -> {
            SuppliedString suppliedString = new SuppliedString("Prefix: {}\nTab: {}");
            suppliedString.addMultipleValues(entry.getKey(), entry.getValue());
            return suppliedString.build();
        });
        System.out.println(listSuppliedString.build());
    }

    private void addInternal(List<Argument> argumentList, ConditionContainerImpl conditionContainer) {
        Argument argument;
        if (argumentList.size() == 0)
            return;
        else if (argumentList.size() == 1) {
            argument = argumentList.get(0);
            argumentList = new ArrayList<>();
        } else {
            argument = argumentList.get(argumentList.size()-1);
            argumentList = argumentList.subList(0, argumentList.size()-1);
        }

        TabCompletion tabCompletion = this.getCompletion(this.fromArguments(argumentList));

        if (tabCompletion == null) {
            tabCompletion = new TabCompletion();
            this.map.put(this.fromArguments(argumentList), tabCompletion);
        }

        if (argument.getType() == ArgumentType.STATIC) {
            StaticArgument staticArgument = (StaticArgument) argument;
            Completion completion = new Completion(null, conditionContainer);
            completion.getValues().add(staticArgument.getLabel());

            tabCompletion.getCompletionList().add(completion);
        } else {
            MutableArgument mutableArgument = (MutableArgument) argument;
            Interpreter<?> interpreter = mutableArgument.getInterpreter();

            Completion completion = new Completion(interpreter.getDynamicTabCompletion(), conditionContainer);

            if (interpreter.getDynamicTabCompletion().get().isEmpty() && interpreter.getTabCompletion().isEmpty()) {
                if (!mutableArgument.getCompletion().isEmpty()) {
                    completion.getValues().add(TabCompletionsHelper.getAsSingleTypeString(mutableArgument.getCompletion()));
                } else {
                    completion.getValues().add(interpreter.getTabDescription());
                }
            } else {
                if (!interpreter.getDynamicTabCompletion().get().isEmpty())
                    completion.setDynamicValues(interpreter.getDynamicTabCompletion());
                completion.getValues().addAll(interpreter.getTabCompletion());
            }

            tabCompletion.getCompletionList().add(completion);
        }
    }

    public List<String> get(List<InputArgument> inputArgumentList, Player player) {
        inputArgumentList = this.cutLast(inputArgumentList);

        TabCompletion completion = this.getCompletion(this.fromInputArguments(inputArgumentList));
        if (completion == null)
            return new ArrayList<>();

        return completion.get(player);
    }

    private List<CompletionArgument> fromInputArguments(List<InputArgument> inputArgumentList) {
        return inputArgumentList.stream()
                .map(inputArgument -> {
                    if (inputArgument.isMulti())
                        return new CompletionArgument(null);
                    else
                        return new CompletionArgument(inputArgument.getInputs().get(0));
                })
                .collect(Collectors.toList());
    }

    private List<CompletionArgument> fromArguments(List<Argument> argumentList) {
        return argumentList.stream()
                .map(argument -> {
                    if (argument.getType() == ArgumentType.STATIC) {
                        return new CompletionArgument(((StaticArgument) argument).getLabel());
                    } else {
                        return new CompletionArgument(null);
                    }
                })
                .collect(Collectors.toList());
    }

    private TabCompletion getCompletion(List<CompletionArgument> argumentList) {
        for (Map.Entry<List<CompletionArgument>, TabCompletion> entry : this.map.entrySet()) {
            if (entry.getKey().size() == 0 && argumentList.size() == 0)
                return entry.getValue();

            if (entry.getKey().size() != argumentList.size())
                continue;

            boolean failed = false;
            for (int i = 0; i < entry.getKey().size(); i++) {
                CompletionArgument key = entry.getKey().get(i);
                CompletionArgument argument = argumentList.get(i);

                if (key.isMutable())
                    continue;

                if (!key.getLabel().equalsIgnoreCase(argument.getLabel())) {
                    failed = true;
                    break;
                }
            }

            if (!failed)
                return entry.getValue();
        }
        return null;
    }

    private <T> List<T> cutLast(@NonNull List<T> list) {
        if (list.size() <= 1)
            return new ArrayList<>();

        return list.subList(0, list.size()-1);
    }

    @ToString
    private static class TabCompletion {

        @Getter
        private final List<Completion> completionList;

        TabCompletion() {
            this.completionList = new ArrayList<>();
        }

        public List<String> get(Player player) {
            List<String> stringList = new ArrayList<>();

            for (Completion completion : this.completionList) {
                if (!completion.getConditionContainer().test(player).isSuccesses())
                    continue;

                if (completion.getDynamicValues() != null)
                    stringList.addAll(completion.getDynamicValues().get());

                stringList.addAll(completion.getValues());
            }
            return stringList;
        }

    }

    private static class Completion {

        @Getter
        private final Set<String> values;

        @Getter
        private final ConditionContainerImpl conditionContainer;

        @Setter
        @Getter
        private Supplier<List<String>> dynamicValues;

        Completion(Supplier<List<String>> dynamicList, @NotNull ConditionContainerImpl conditionContainer) {
            this.values = new HashSet<>();
            this.dynamicValues = dynamicList == null ? ArrayList::new : dynamicList;
            this.conditionContainer = conditionContainer;
        }

    }

    @ToString
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    private static class CompletionArgument {

        @Getter
        private final String label;

        boolean isMutable() {
            return this.label == null;
        }

    }

}
