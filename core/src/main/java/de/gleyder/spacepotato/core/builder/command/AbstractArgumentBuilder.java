package de.gleyder.spacepotato.core.builder.command;

/**
 * Base builder to build Arguments.
 * The same instance can be reused after calling build().
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public abstract class AbstractArgumentBuilder<I, B> {

    String description;

    /**
     * Sets the description
     *
     * @param description       a description
     * @return                  this builder
     */
    @SuppressWarnings("unused")
    public B setDescription(String description) {
        this.description = description;
        return this.getThis();
    }

    /**
     * Resets the builder's attributes.
     */
    protected void reset() {
        this.description = null;
    }

    /**
     * Builds the argument
     *
     * @return      an argument
     */
    public abstract I build();

    /**
     * Returns itself
     *
     * @return      this builder
     */
    @SuppressWarnings("unchecked")
    private B getThis() {
        return (B) this;
    }

}
