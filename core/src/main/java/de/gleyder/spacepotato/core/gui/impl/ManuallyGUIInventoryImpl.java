package de.gleyder.spacepotato.core.gui.impl;

import de.gleyder.spacepotato.core.clickable.inventory.ClickableInventoryRegistry;
import de.gleyder.spacepotato.core.gui.ManuallyGUIInventory;

/**
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ManuallyGUIInventoryImpl extends GUIInventoryImpl implements ManuallyGUIInventory {

    ManuallyGUIInventoryImpl(GUIInventoryRegistry guiInventoryRegistry, ClickableInventoryRegistry registry, String title, int rows) {
        super(guiInventoryRegistry, registry, title, rows);
    }

    public void update() {
        this.constructStatic();
    }

}
