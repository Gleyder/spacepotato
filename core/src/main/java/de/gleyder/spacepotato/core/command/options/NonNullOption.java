package de.gleyder.spacepotato.core.command.options;

import de.gleyder.spacepotato.core.command.exceptions.OptionException;
import de.gleyder.spacepotato.core.command.interfaces.Option;
import org.bukkit.ChatColor;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class NonNullOption implements Option<Integer> {

    @Override
    public void test(Integer integer) throws OptionException {
        if (integer < 0)
            throw new OptionException(ChatColor.RED + "Integer must be non negative");
    }

}
