package de.gleyder.spacepotato.core.builder.item;

import de.gleyder.spacepotato.core.interfaces.IBuilder;
import de.gleyder.spacepotato.core.utils.SpacePotatoColor;
import lombok.NonNull;
import org.bukkit.DyeColor;
import org.bukkit.entity.TropicalFish;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.TropicalFishBucketMeta;

/**
 * ItemBuilder for TropicalFishBucket.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class TropicalFishBucketBuilder implements IBuilder<ItemStack> {

    private final ItemStack item;
    private final TropicalFishBucketMeta meta;

    /**
     * Creates a TropicalFishBucketBuilder
     *
     * @param item      an ItemStack
     * @param copy      if it should be copied
     */
    public TropicalFishBucketBuilder(@NonNull ItemStack item, boolean copy) {
        this.item = copy ? item.clone() : item;
        this.meta = (TropicalFishBucketMeta) this.item.getItemMeta();
    }

    /**
     * Creates a TropicalFishBucketBuilder
     *
     * @param item
     */
    public TropicalFishBucketBuilder(@NonNull ItemStack item) {
        this(item, false);
    }

    /**
     * Sets the body color
     *
     * @param color         a DyeColor
     * @return              this builder
     */
    public TropicalFishBucketBuilder setBodyColor(DyeColor color) {
        this.meta.setBodyColor(color);
        return this;
    }

    /**
     * Sets the body color
     *
     * @param color         a RainstormColor
     * @return              this builder
     */
    public TropicalFishBucketBuilder setBodyColor(SpacePotatoColor color) {
        this.meta.setBodyColor(color.getDyeColor());
        return this;
    }

    /**
     * Sets tropical fish pattern
     *
     * @param pattern       a pattern
     * @return              this builder
     */
    public TropicalFishBucketBuilder setPattern(TropicalFish.Pattern pattern) {
        this.meta.setPattern(pattern);
        return this;
    }

    /**
     * Converts it to an ItemBuilder
     *
     * @return              this builder
     */
    public ItemBuilder toItemBuilder() {
        return new ItemBuilder(this.build());
    }

    /**
     * Builds the ItemStack
     *
     * @return      an ItemStack
     */
    @Override
    public ItemStack build() {
        this.item.setItemMeta(this.meta);
        return this.item;
    }

}
