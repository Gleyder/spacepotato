package de.gleyder.spacepotato.core.broadcast;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

/**
 * Broadcast sounds or messages to a group of players
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface BroadcastChannel {

    /**
     * Creates a broadcast channel from the online players
     *
     * @return      a broadcast channel
     */
    static SuppliedBroadcastChannel fromOnlinePlayers() {
        //noinspection unchecked
        return new SuppliedBroadcastChannel(() -> (List<Player>) Bukkit.getOnlinePlayers());
    }

    /**
     * Sends a broadcast message
     *
     * @param message       a message
     */
    void sendMessage(@NotNull String message);

    /**
     * Plays a sound at the given location
     *
     * @param location      a location
     * @param sound         a sound
     * @param category      a category
     * @param volume        a volume
     * @param pitch         a pitch
     */
    void playSound(@NotNull Location location, @NotNull Sound sound, @NotNull SoundCategory category, float volume, float pitch);

    /**
     * Broadcast a sound
     *
     * @param sound         a sound
     * @param category      a category
     * @param volume        a volume
     * @param pitch         a pitch
     */
    void playSound(@NotNull Sound sound, @NotNull SoundCategory category, float volume, float pitch);

    /**
     * Broadcasts a sound
     *
     * @param sound         a sound
     * @param category      a category
     */
    void playSound(@NotNull Sound sound, @NotNull SoundCategory category);

    /**
     * Broadcast a sound
     *
     * @param sound     a sound
     */
    void playSound(@NotNull Sound sound);

    /**
     * Checks if a player is in the channel
     *
     * @param player        a player
     * @return              if a player is in the channel
     */
    boolean contains(@NotNull Player player);

    Collection<Player> getPlayers();

}
