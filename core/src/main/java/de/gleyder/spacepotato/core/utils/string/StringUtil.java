package de.gleyder.spacepotato.core.utils.string;

/**
 * Util for Strings.
 *
 * @author Gleyder
 * @version 1.0
 * @since 3.0
 */
public class StringUtil {

    public final static String EMPTY;

    static {
        EMPTY = "";
    }

    /**
     * Returns an empty string if the input one ist null
     *
     * @param string        a string
     * @return              a string
     */
    public static String emptyIfNull(String string) {
        return string == null ? EMPTY : string;
    }

}
