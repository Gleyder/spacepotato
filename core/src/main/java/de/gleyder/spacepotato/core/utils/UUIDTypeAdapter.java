package de.gleyder.spacepotato.core.utils;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.UUID;

/**
 * Adapter for uuid's
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@NoArgsConstructor
public class UUIDTypeAdapter extends TypeAdapter<UUID> {

    /**
     * Writes a uuid
     *
     * @param out               the output
     * @param value             the value
     * @throws IOException      an exception
     */
    @Override
    public void write(JsonWriter out, UUID value) throws IOException {
        out.value(fromUUID(value));
    }

    /**
     * Reads a uuid
     *
     * @param in                the input
     * @return                  the value
     * @throws IOException      an exception
     */
    @Override
    public UUID read(JsonReader in) throws IOException {
        return fromString(in.nextString());
    }

    /**
     * Returns a uuid as string
     *
     * @param value     a uuid
     * @return          a string
     */
    public static String fromUUID(UUID value) {
        return value.toString().replace("-", "");
    }

    /**
     * Returns a string as uuid
     *
     * @param input     an input
     * @return          a uuid
     */
    private UUID fromString(String input) {
        return UUID.fromString(input.replaceFirst(
                "(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5"));
    }

}
