package de.gleyder.spacepotato.core.utils.string;

import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.Getter;
import lombok.Setter;

import java.util.function.Function;

/**
 * Builds a string from an iterable.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.interfaces.IBuilder
 */
@Getter
@Setter
public class ListSuppliedString<T> implements IBuilder<String> {

    private final Iterable<T> iterable;
    private final Function<T, String> lineFunction;


    private String header, footer,
            separator = "\n";

    /**
     * Creates a ListSuppliedString
     *
     * @param iterable          a iterable
     * @param lineFunction      a function
     */
    public ListSuppliedString(Iterable<T> iterable, Function<T, String> lineFunction) {
        this.iterable = iterable;
        this.lineFunction = lineFunction;
    }

    /**
     * Creates a ListSuppliedString
     *
     * @param iterable          a iterable
     * @param lineFunction      a function
     * @param header            a header
     * @param footer            a footer
     */
    public ListSuppliedString(Iterable<T> iterable, Function<T, String> lineFunction, String header, String footer) {
        this.iterable = iterable;
        this.lineFunction = lineFunction;
        this.header = header;
        this.footer = footer;
    }

    /**
     * Builds the string
     *
     * @return      a string
     */
    @Override
    public String build() {
        StringBuilder builder = new StringBuilder();
        if (this.header != null)
            builder.append(this.header).append(this.separator);

        this.iterable.forEach(t -> builder.append(this.lineFunction.apply(t)).append(this.separator));

        if (builder.toString().isEmpty())
            builder.append("No content");
        else
            builder.deleteCharAt(builder.toString().length()-1);

        if (this.footer != null)
            builder.append(this.separator).append(this.footer);
        return builder.toString();
    }

    /**
     * Builds the string
     *
     * @return      a string
     */
    @Override
    public String toString() {
        return this.build();
    }

    public static <T> String fromList(Iterable<T> iterable) {
        return fromList(iterable, Object::toString);
    }

    public static <T> String fromList(Iterable<T> iterable, Function<T, String> function) {
        StringBuilder stringBuilder = new StringBuilder();
        for (T t : iterable) {
            stringBuilder.append(function.apply(t)).append(" ");
        }
        String string = stringBuilder.toString();
        if (string.isEmpty())
            return "";
        return stringBuilder.deleteCharAt(string.length()-1).toString();
    }

}
