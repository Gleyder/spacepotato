package de.gleyder.spacepotato.core.builder.command;

import de.gleyder.spacepotato.core.command.entities.StaticArgument;
import de.gleyder.spacepotato.core.command.impl.StaticArgumentImpl;

/**
 * Builder for StaticArguments.
 * The same instance can be reused after calling build().
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.builder.command.AbstractArgumentBuilder
 */
public class StaticArgumentBuilder extends AbstractArgumentBuilder<StaticArgument, StaticArgumentBuilder> {

    private String label;

    /**
     * Sets the label
     *
     * @param label     a label
     * @return          this builder
     */
    public StaticArgumentBuilder setLabel(String label) {
        this.label = label;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StaticArgument build() {
        StaticArgumentImpl staticArgument = new StaticArgumentImpl(this.label);
        this.reset();
        return staticArgument;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        super.reset();
        this.label = null;
    }

}
