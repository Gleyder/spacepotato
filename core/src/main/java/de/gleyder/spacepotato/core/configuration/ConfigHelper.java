package de.gleyder.spacepotato.core.configuration;

import org.apache.commons.io.FileUtils;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ConfigHelper {

    @SuppressWarnings({"WeakerAccess", "RedundantSuppression"})
    public static final String YAML_EXTENSION, JSON_EXTENSION;
    private static final String LOADOUT_PATH;

    static {
        YAML_EXTENSION = ".yaml";
        JSON_EXTENSION = ".json";
        LOADOUT_PATH = "loadout";
    }

    public static String withExtension(String string, String extension) {
        return string.replace(extension, "") + extension;
    }

    private final JavaPlugin plugin;

    public ConfigHelper(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Returns an input stream of the from the resource name
     *
     * @param filename      path to file with file extension
     * @return              an input stream
     */
    public InputStream getFromLoadout(@NotNull String filename) {
        InputStream inputStream = this.plugin.getResource(LOADOUT_PATH + "/" + filename);
        if (inputStream == null)
            throw new NullPointerException("No file found for \"" + LOADOUT_PATH + "/" + filename + "\"");
        return inputStream;
    }

    /**
     * Returns a temporary file
     *
     * @param filename      path to file with extension
     * @return              a temporary file
     */
    public TemporaryFile getTempFromLoadout(String filename) {
        try {
            File tempFile = new File(this.getTempPath() + "/" + filename);
            FileUtils.copyInputStreamToFile(this.getFromLoadout(filename), tempFile);
            return new TemporaryFile(tempFile);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings("WeakerAccess")
    public String getDataFolderPath(String filename) {
        return this.plugin.getDataFolder() + "/" + filename;
    }

    /**
     * Returns the path to the temporary folder
     *
     * @return      a path
     */
    public String getTempPath() {
        return this.plugin.getDataFolder() + "/tmp";
    }

    /**
     * Copies a file from the loadout to the data folder
     *
     * @param filename      a file name
     */
    public void copyFromLoadout(String filename) {
        try {
            FileUtils.copyToFile(this.getFromLoadout(filename), this.getFileInDataFolder(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a file from the plugin data folder
     *
     * @param filename      a file name with file extension
     * @return              a file name
     */
    public File getFileInDataFolder(String filename) {
        return new File(this.plugin.getDataFolder() + "/" + filename);
    }

}
