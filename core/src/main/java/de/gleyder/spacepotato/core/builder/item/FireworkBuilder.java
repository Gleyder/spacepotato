package de.gleyder.spacepotato.core.builder.item;

import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.NonNull;
import org.bukkit.FireworkEffect;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.List;

/**
 * Builder for fireworks
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class FireworkBuilder implements IBuilder<ItemStack> {

    private final ItemStack item;
    private final FireworkMeta meta;

    /**
     * Creates a FireworkBuilder
     *
     * @param item          an ItemStack
     * @param copy          if it should be cpied
     */
    public FireworkBuilder(@NonNull ItemStack item, boolean copy) {
        this.item = copy ? item.clone() : item;
        this.meta = (FireworkMeta) this.item.getItemMeta();
    }

    /**
     * Creates a FireworkBuilder
     *
     * @param item          an ItemStack
     */
    public FireworkBuilder(@NonNull ItemStack item) {
        this(item, false);
    }

    /**
     * Sets the power
     *
     * @param power     the power
     * @return          this FireworkBuilder
     */
    public FireworkBuilder setPower(int power) {
        this.meta.setPower(power);
        return this;
    }

    /**
     * Adds an FireworkEffect
     *
     * @param effect    the FireworkEffect
     * @return          this FireworkBuilder
     */
    public FireworkBuilder addEffect(FireworkEffect effect) {
        this.meta.addEffect(effect);
        return this;
    }

    /**
     * Adds an array of FireworkEffects
     *
     * @param effects       an array of FireworkEffects
     * @return              this FireworkBuilder
     */
    public FireworkBuilder addEffect(FireworkEffect... effects) {
        this.meta.addEffects(effects);
        return this;
    }

    /**
     * Adds a list of FireworkEffects
     *
     * @param effects       a list of FireworkEffects
     * @return              this FireworkBuilder
     */
    public FireworkBuilder addEffects(List<FireworkEffect> effects) {
        this.meta.addEffects(effects.toArray(new FireworkEffect[0]));
        return this;
    }

    /**
     * Converts it to the default builder
     *
     * @return       an ItemBuilder
     */
    public ItemBuilder toItemBuilder() {
        return new ItemBuilder(this.build());
    }

    /**
     * Builds the ItemStack
     *
     * @return           this FireworkBuilder
     */
    @Override
    public ItemStack build() {
        this.item.setItemMeta(this.meta);
        return this.item;
    }

}
