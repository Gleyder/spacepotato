package de.gleyder.spacepotato.core.configuration.handler;

import de.gleyder.spacepotato.core.configuration.ConfigHelper;
import de.gleyder.spacepotato.core.configuration.TemporaryFile;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.InputStream;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class LoadoutFile {

    private final ConfigHelper configHelper;
    private final String filename;
    private final FileLoader fileLoader;

    LoadoutFile(@NotNull ConfigHelper configHelper, @NotNull String filename, @NotNull FileLoader fileLoader) {
        this.configHelper = configHelper;
        this.filename = filename;
        this.fileLoader = fileLoader;
    }

    public void load() {
        InputStream stream = this.configHelper.getFromLoadout(this.filename);
        if (stream == null)
            throw new NullPointerException("File \"" + this.filename + "\" not found");

        File dataFolderFile = this.configHelper.getFileInDataFolder(this.filename);
        TemporaryFile loadoutFile = this.configHelper.getTempFromLoadout(this.filename);
        if (loadoutFile == null)
            throw new NullPointerException("LoadoutFile for \"" + this.filename +"\" is null");

        this.fileLoader.load(this.filename, loadoutFile.getFile(), dataFolderFile, this.configHelper);
        loadoutFile.delete();
    }

}
