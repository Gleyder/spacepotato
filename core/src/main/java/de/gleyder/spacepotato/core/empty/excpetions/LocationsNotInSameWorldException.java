package de.gleyder.spacepotato.core.empty.excpetions;

/**
 * Thrown if two locations are not in the same world.
 * Used for the square empty
 *
 * @author Gleyder
 * @version 0.1
 */
public class LocationsNotInSameWorldException extends RuntimeException {

    /**
     * Creates a LocationsNotInSameWorldException
     *
     * @param message   the message
     */
    public LocationsNotInSameWorldException(String message) {
        super(message);
    }

}
