package de.gleyder.spacepotato.core.command.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Indicates a parameter that the values it gets from the command hash map with the given key
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Input {

    String value();

}
