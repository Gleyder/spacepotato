package de.gleyder.spacepotato.core.configuration.handler;

import de.gleyder.spacepotato.core.configuration.ConfigHelper;
import de.gleyder.spacepotato.core.configuration.SpacePotatoConfigBinder;
import de.gleyder.spacepotato.core.configuration.SpacePotatoSave;
import de.gleyder.spacepotato.core.configuration.handler.loader.ConfigLoader;
import de.gleyder.spacepotato.core.configuration.handler.loader.SaveFileLoader;
import org.apache.commons.io.FileUtils;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Handles the loadout files.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class LoadoutHandler {

    private final List<LoadoutFile> fileList;
    private final ConfigHelper configHelper;
    private final JavaPlugin plugin;

    /**
     * Creates a LoadoutHandler
     *
     * @param plugin        a plugin
     */
    public LoadoutHandler(@NotNull JavaPlugin plugin) {
        this.fileList = new ArrayList<>();
        this.configHelper = new ConfigHelper(plugin);
        this.plugin = plugin;
    }

    /**
     * Adds a file
     *
     * @param filename      Filename with extension
     * @param fileLoader    a file loader
     */
    public void addFile(@NotNull String filename, @NotNull FileLoader fileLoader) {
        this.fileList.add(new LoadoutFile(this.configHelper, filename, fileLoader));
    }

    /**
     *
     * @param spacePotatoConfigBinder       a space potato config
     * @param classes                       a class
     */
    public void addFile(@NotNull SpacePotatoConfigBinder spacePotatoConfigBinder, @NotNull Class<?>... classes) {
        List<String> pathList = new ArrayList<>();
        for (Class<?> aClass : classes) {
            pathList.addAll(Arrays.stream(aClass.getMethods()).map(Method::getName).collect(Collectors.toList()));
        }
        this.addFile(spacePotatoConfigBinder.getFilename(), new ConfigLoader(pathList));
    }

    /**
     * Creates a save file which is added to the handler
     *
     * @param filename      the filename
     * @param clazz         the class
     * @param <T>           the type
     * @return              the save file
     */
    public <T> SpacePotatoSave<T> createSaveFile(@NotNull String filename, @NotNull Class<T> clazz) {
        SpacePotatoSave<T> save = new SpacePotatoSave<T>(this.plugin, filename, clazz);
        this.addFile(ConfigHelper.withExtension(filename, ConfigHelper.JSON_EXTENSION), new SaveFileLoader());
        return save;
    }

    /**
     * Loads the files
     */
    public void load() {
        this.fileList.forEach(LoadoutFile::load);

        try {
            FileUtils.deleteDirectory(new File(this.configHelper.getTempPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
