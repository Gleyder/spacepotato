package de.gleyder.spacepotato.core.command.options;

import de.gleyder.spacepotato.core.command.enums.OptionType;
import de.gleyder.spacepotato.core.command.exceptions.OptionException;
import de.gleyder.spacepotato.core.command.interfaces.Option;

/**
 * @author Gleyder
 */
public class EnumOption<T extends Enum<?>> implements Option<T> {

    private final T[] enums;
    private final OptionType type;

    public EnumOption(OptionType type, T... ts) {
        this.enums = ts;
        this.type = type;
    }

    @Override
    public void test(T argument) throws OptionException {
        switch (this.type) {
            case ALLOWED:
                for (T t: this.enums) {
                    if (t == argument)
                        return;
                }
                throw new OptionException("§c" + argument + " is disallowed");
            case DISALLOWED:
                for (T t : this.enums) {
                    if (t == argument)
                        throw  new OptionException("§c" + argument + " is disallowed");
                }
                break;
        }
    }



}
