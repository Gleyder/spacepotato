package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import lombok.Getter;

/**
 * Stores common used interpreters.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public enum Interpreters {

    STRING(new StringInterpreter()),
    BYTE(new ByteInterpreter()),
    SHORT(new ShortInterpreter()),
    INTEGER(new IntegerInterpreter()),
    LONG(new LongInterpreter()),
    FLOAT(new FloatInterpreter()),
    DOUBLE(new DoubleInterpreter()),
    BOOlEAN(new BooleanInterpreter()),
    PLAYER(new PlayerInterpreter()),
    MATERIAL(new MaterialInterpreter()),
    ENCHANTMENT(new EnchantmentInterpreter()),
    WORLD(new WorldInterpreter()),
    UUID(new UUIDInterpreter())
    ;

    @Getter
    final Interpreter<Object> interpreter;

    /**
     * Creates an enum type 'Interpreters'
     *
     * @param interpreter       an interpreter
     */
    @SuppressWarnings("unchecked")
    Interpreters(Interpreter<?> interpreter) {
        this.interpreter = (Interpreter<Object>) interpreter;
    }

}
