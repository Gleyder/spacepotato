package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;

import java.util.UUID;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class UUIDInterpreter implements Interpreter<UUID> {

    @Override
    public UUID translate(String argument) {
        return UUID.fromString(argument);
    }

    @Override
    public String getTabDescription() {
        return TabCompletionsHelper.getAsSingleTypeString("uuid");
    }


    @Override
    public Class<UUID> getClassType() {
        return UUID.class;
    }

}
