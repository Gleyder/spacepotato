package de.gleyder.spacepotato.core.gui;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import de.gleyder.spacepotato.core.utils.Position;

/**
 * Translates the position from matrix to
 * inventory style and vice versa
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class PositionTranslation {

    private final BiMap<Position, Integer> biMap;
    private Integer rows;

    /**
     * Creates a PositionTranslation
     *
     * @param rows      the amount of rows
     */
    public PositionTranslation(Integer rows) {
        this.biMap = HashBiMap.create();
        this.rows = rows;
        this.generateTranslations();
    }

    /**
     * Generates the translations
     */
    private void generateTranslations() {
        int slotCounter = 0;
        for (int y = this.rows - 1; y >= 0; y--) {
            for (int x = 0; x < 9; x++) {
                this.biMap.put(new Position(x, y), slotCounter);
                slotCounter++;
            }
        }
    }

    /**
     * Returns the slot of a position
     *
     * @param position      a position
     * @return              a slot
     */
    public int getSlot(Position position) {
        try {
            return this.biMap.get(position);
        }  catch (NullPointerException ex) {
            return -1;
        }
    }

    /**
     * Returns the position of a slot
     *
     * @param slot      a slot
     * @return          a position
     */
    public Position getPosition(int slot) {
        return this.biMap.inverse().get(slot);
    }

}
