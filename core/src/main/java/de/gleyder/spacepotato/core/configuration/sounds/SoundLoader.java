package de.gleyder.spacepotato.core.configuration.sounds;

import de.gleyder.spacepotato.core.configuration.ConfigHelper;
import de.gleyder.spacepotato.core.configuration.bulkvalue.BulkValueLoader;
import de.gleyder.spacepotato.core.configuration.handler.LoadoutHandler;
import de.gleyder.spacepotato.core.configuration.handler.loader.ConfigLoader;
import org.apache.commons.lang.reflect.FieldUtils;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class SoundLoader {

    private final LoadoutHandler handler;
    private final List<String> pathList;
    private final JavaPlugin plugin;
    private final Logger logger;
    private final Class<Enum<?>> enumClass;
    private final String filename;

    public SoundLoader(@NotNull JavaPlugin plugin, @NotNull Class<Enum<?>> enumClass) {
        this(plugin, "sounds", enumClass);
    }

    @SuppressWarnings("WeakerAccess")
    public SoundLoader(@NotNull JavaPlugin plugin, @NotNull String filename, @NotNull Class<Enum<?>> enumClass) {
        this.filename = ConfigHelper.withExtension(filename, ConfigHelper.YAML_EXTENSION);
        this.handler = new LoadoutHandler(plugin);
        this.plugin = plugin;
        this.enumClass = enumClass;
        this.pathList = Arrays.stream(enumClass.getMethods()).map(Method::getName).collect(Collectors.toList());
        this.handler.addFile(this.filename, new ConfigLoader(this.pathList));
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public void load() {
        this.handler.load();
        File file = new ConfigHelper(this.plugin).getFileInDataFolder(this.filename);

        BulkValueLoader<SoundContainer> valueLoader = new BulkValueLoader<>(file, this.pathList, (path, cfg) -> {
            Sound sound = Sound.valueOf(cfg.getString(path));
            SoundCategory category = SoundCategory.valueOf(cfg.getString(path));
            float volume = (float) cfg.getDouble(path);
            float pitch = (float) cfg.getDouble(path);
            return new SoundContainer(sound, category, volume, pitch);
        });

        HashMap<String, SoundContainer> containerHashMap = valueLoader.toMap();
        for (Enum<?> enumConstant : this.enumClass.getEnumConstants()) {
            String enumPath = enumConstant.name()
                    .toLowerCase()
                    .replace("_", ".")
                    .replace("$", "-");

            Field field = FieldUtils.getField(this.enumClass, "container", true);
            try {
                FieldUtils.writeField(field, this.enumClass, containerHashMap.get(enumPath), true);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        this.injectStaticField("PLUGIN", this.plugin);
    }

    private void injectStaticField(@SuppressWarnings("SameParameterValue") String name, Object value) {
        try {
            FieldUtils.writeStaticField(this.enumClass, name, value, true);
        } catch (IllegalArgumentException ignored) {
            this.logger.warn("No field with the name {} were found in {}", name, this.enumClass.getName());
        } catch (IllegalAccessException ignored) {
            this.logger.warn("The field with the name {} were in {} is final or cannot be made accessible", name, this.enumClass.getName());
        }
    }

}
