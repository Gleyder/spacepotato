package de.gleyder.spacepotato.core.command.exceptions;

import de.gleyder.spacepotato.core.utils.exceptions.BaseRuntimeException;
import de.gleyder.spacepotato.core.utils.string.SuppliedString;

/**
 * Thrown if an errors occurs in the command executor.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see BaseRuntimeException
 */
public class ExecutorException extends RuntimeException {

    /**
     * Creates a ExecutorException
     *
     * @param message       a message
     * @param objects       an array of objects
     */
    public ExecutorException(String message, Object... objects) {
        super(new SuppliedString(message).addMultipleValues(objects).build());
    }

}
