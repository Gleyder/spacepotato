package de.gleyder.spacepotato.core.gui.navigable;

import de.gleyder.spacepotato.core.clickable.inventory.interfaces.LeftClickInventoryClickAction;
import lombok.AllArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
class OpenInventoryAction extends LeftClickInventoryClickAction {

    private final InventoryPage page;

    @Override
    public void onClick(Player player, Inventory inventory, ItemStack item) {
        this.page.open(player);
    }

}
