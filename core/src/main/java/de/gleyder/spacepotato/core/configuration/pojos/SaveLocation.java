package de.gleyder.spacepotato.core.configuration.pojos;

import lombok.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SaveLocation {

    public SaveLocation(Location location) {
        this(location.getWorld().getName(), location.getX(), location.getY(), location.getZ(), location.getPitch(), location.getYaw());
    }

    private String world;
    private double x, y, z;
    private float pitch, yaw;

    public Location toLocation() {
        return new Location(Bukkit.getWorld(this.world), this.x, this.y, this.z, this.yaw, this.pitch);
    }

}
