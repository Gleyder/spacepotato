package de.gleyder.spacepotato.core.builder.item;

import de.gleyder.spacepotato.core.interfaces.IBuilder;
import de.gleyder.spacepotato.core.utils.SpacePotatoColor;
import lombok.NonNull;
import org.bukkit.Color;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/**
 * Builder for Leather Armor
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class LeatherArmorBuilder implements IBuilder<ItemStack> {

    private final ItemStack item;
    private final LeatherArmorMeta meta;

    /**
     * Creates a LeatherArmorBuilder
     *
     * @param item          an ItemStack
     * @param copy          if it should be copied
     */
    public LeatherArmorBuilder(@NonNull ItemStack item, boolean copy) {
        this.item = copy ? item.clone() : item;
        this.meta = (LeatherArmorMeta) this.item.getItemMeta();
    }

    /**
     * Creates a LeatherArmorBuilder
     *
     * @param item          an ItemStack
     */
    public LeatherArmorBuilder(@NonNull ItemStack item) {
        this(item, false);
    }

    /**
     * Sets the color
     *
     * @param rainstormColor     a RainstormColor
     * @return                  this LeatherArmorBuilder
     */
    public LeatherArmorBuilder setColor(SpacePotatoColor rainstormColor) {
        this.meta.setColor(rainstormColor.getColor());
        return this;
    }

    /**
     * Sets the color
     *
     * @param color         a Color
     * @return              this LeatherArmorBuilder
     */
    public LeatherArmorBuilder setColor(Color color) {
        this.meta.setColor(color);
        return this;
    }

    /**
     * Converts it to the default builder
     *
     * @return       an ItemBuilder
     */
    public ItemBuilder toItemBuilder() {
        return new ItemBuilder(this.build());
    }

    /**
     * Builds the ItemStack
     *
     * @return      an ItemStack
     */
    @Override
    public ItemStack build() {
        this.item.setItemMeta(this.meta);
        return this.item;
    }

}
