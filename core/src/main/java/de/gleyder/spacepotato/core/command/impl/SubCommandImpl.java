package de.gleyder.spacepotato.core.command.impl;

import de.gleyder.spacepotato.core.analyzer.QueryType;
import de.gleyder.spacepotato.core.command.entities.MutableArgument;
import de.gleyder.spacepotato.core.command.entities.StaticArgument;
import de.gleyder.spacepotato.core.command.entities.SubCommand;
import de.gleyder.spacepotato.core.command.entities.SubCommandCategory;
import de.gleyder.spacepotato.core.command.enums.ArgumentType;
import de.gleyder.spacepotato.core.command.enums.LabelType;
import de.gleyder.spacepotato.core.command.enums.SenderType;
import de.gleyder.spacepotato.core.command.exceptions.CommandAdapterException;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import de.gleyder.spacepotato.core.utils.string.ListSuppliedString;
import lombok.Getter;
import lombok.ToString;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.function.Function;

/**
 * Implementation of SubCommand.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@ToString
public class SubCommandImpl extends AbstractCommandPart implements SubCommand {

    @Getter
    protected final String description;

    @Getter
    protected TreeSet<SubCommandCategoryImpl> categoryTreeSet;

    /**
     * Creates a SubCommandImpl
     *
     * @param arguments         a list of arguments
     * @param type              a sender type
     * @param conditionContainer         a conditionContainer
     * @param description       a description
     */
    public SubCommandImpl(List<ArgumentImpl> arguments, SenderType type, TreeSet<SubCommandCategoryImpl> categories, ConditionContainerImpl conditionContainer, String description, Function<CommandSender, Object> senderConverter) {
        super(arguments, type, conditionContainer, senderConverter);
        this.description = description == null ? CoreMessages.COMMAND_BASE_NO$DESCRIPTION.getMessage(false) : description;
        this.categoryTreeSet = new TreeSet<>(Comparator.comparingInt(o -> o.getPriority().getSlot()));
        if (categories != null)
            this.categoryTreeSet.addAll(categories);
    }

    @Override
    public boolean hasEndless() {
        for (ArgumentImpl argument : this.arguments) {
            if (argument.getType() == ArgumentType.STATIC)
                continue;

            MutableArgument mutableArgument = (MutableArgument) argument;
            if (mutableArgument.isEndless())
                return true;
        }
        return false;
    }

    @Override
    public int getLastArgumentIndex() {
        return this.arguments.size()-1;
    }

    /**
     * Applies the category
     */
    public void applyCategories() {
        QueryType type = null;
        for (SubCommandCategoryImpl category : this.categoryTreeSet) {
            if (category.getLabelType() == LabelType.PREFIX) {
                category.arguments.forEach(this.arguments::addFirst);
            } else {
                category.arguments.forEach(this.arguments::addLast);
            }

            this.conditionContainer.getConditionList().addAll(category.conditionContainer.getConditionList());
            type = category.conditionContainer.getQueryType();
        }

        this.conditionContainer.setQueryType(type);
    }

    /**
     * Checks if the arguments are valid
     */
    public void checkValidArguments() {
        int counter = -1, lastIndex = this.arguments.size()-1;
        for (ArgumentImpl argument : this.arguments) {
            counter++;

            if (counter == lastIndex)
                continue;
            if (argument.getType() == ArgumentType.STATIC)
                continue;
            if (((MutableArgument) argument).isEndless())
                throw new CommandAdapterException("SubCommand {} has an endless argument at pos {}. Only the last argument may be endless", this.visual(), counter);
        }
    }

    @Override
    public List<SubCommandCategory> getCategories() {
        return new ArrayList<>(this.categoryTreeSet);
    }

    /**
     * Returns a visual representation of the command
     *
     * @return      a string
     */
    public String visual() {
        return ListSuppliedString.fromList(this.arguments, argument -> {
            if (argument.getType() == ArgumentType.STATIC) {
                return ((StaticArgument) argument).getLabel();
            } else {
                return TabCompletionsHelper.getAsTypeString( ((MutableArgument) argument).getInterpreter().getClassType().getSimpleName() ).get(0);
            }
        });
    }

}
