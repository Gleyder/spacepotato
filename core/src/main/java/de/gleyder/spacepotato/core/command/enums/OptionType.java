package de.gleyder.spacepotato.core.command.enums;

/**
 * @author Gleyder
 */
public enum OptionType {

    ALLOWED, DISALLOWED

}
