package de.gleyder.spacepotato.core.interfaces;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates a 'fake abstract Method'
 * This means it can be overridden to add functionality but is not necessary
 *
 * @author Gleyer
 * @version 1.0
 * @since 1.0
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
public @interface Optional {

}
