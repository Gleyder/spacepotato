package de.gleyder.spacepotato.core.configuration.bulkvalue;

import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class BulkValueLoader<T> {

    private final List<String> pathList;
    private final BulkValueSupplier<T> supplier;
    private final File file;

    public BulkValueLoader(@NotNull File file, @NotNull List<String> pathList, @NotNull BulkValueSupplier<T> supplier) {
        this.file = file;
        this.pathList = pathList;
        this.supplier = supplier;
        this.checkFile();
    }

    public HashMap<String, T> toMap() {
        this.checkFile();

        HashMap<String, T> map = new HashMap<>();
        YamlConfiguration cfg = YamlConfiguration.loadConfiguration(this.file);
        this.pathList.forEach(path -> {
            map.put(path, this.supplier.supply(path, cfg));
        });
        return map;
    }

    private void checkFile() {
        if (!this.file.exists())
            throw new NullPointerException("File \"" + this.file.getPath() + "\" does not exists");
    }

}
