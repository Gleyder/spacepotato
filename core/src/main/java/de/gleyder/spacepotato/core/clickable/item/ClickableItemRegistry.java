package de.gleyder.spacepotato.core.clickable.item;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import de.gleyder.spacepotato.core.registry.AbstractUsesListenerRegistry;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ClickableItemRegistry extends AbstractUsesListenerRegistry<UUID, ClickableItem> {

    public ClickableItemRegistry(@NotNull SpacePotatoRegistry spacePotatoRegistry) {
        super(spacePotatoRegistry);
        this.init(new ClickableItemListener(this));
    }

    @Override
    public void register(ClickableItem clickableItem) {
        this.getMap().put(clickableItem.getUuid(), clickableItem);
    }

}
