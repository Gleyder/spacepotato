package de.gleyder.spacepotato.core.command.interfaces;

import de.gleyder.spacepotato.core.command.exceptions.OptionException;
import de.gleyder.spacepotato.core.command.options.BlankOption;

/**
 * Interface for Option.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Describable
 */
public interface Option<T> extends Describable {

    void test(T t) throws OptionException;

    @Override
    default String getDescription() {
        return "No Description";
    }

    @SuppressWarnings("unchecked")
    static Option<Object> getDefault() {
        return (Option) new BlankOption();
    }

}
