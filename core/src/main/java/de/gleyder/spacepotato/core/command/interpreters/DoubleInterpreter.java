package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.command.util.TabCompletionsHelper;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;

/**
 * Translates an argument into a double.
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class DoubleInterpreter implements Interpreter<Double> {

    /**
     * Translates an argument into a double
     *
     * @param argument                          an argument
     * @return                                  a double
     * @throws InterpreterException     if the argument cannot be translated
     */
    @Override
    public Double translate(String argument) throws InterpreterException {
        try {
            return Double.valueOf(argument);
        } catch (NumberFormatException e) {
            throw new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_DOUBLE.getKeyableMessage(true)
                    .replace("argument", argument)
                    .replace("min", Double.MIN_VALUE)
                    .replace("max", Double.MAX_VALUE)
                    .build());
        }
    }

    @Override
    public String getTabDescription() {
        return TabCompletionsHelper.getAsSingleTypeString("number");
    }

    @Override
    public Class<Double> getClassType() {
        return Double.class;
    }

}
