package de.gleyder.spacepotato.core.gui;

import com.google.common.collect.ImmutableList;
import de.gleyder.spacepotato.core.gui.elements.Element;
import de.gleyder.spacepotato.core.utils.Position;
import de.gleyder.spacepotato.core.utils.UsesListener;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.UUID;

/**
 * Represents a gui inventory
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface GUIInventory extends UsesListener {

    void addElements(Element... elements);
    void removeElements(Element... elements);
    void removeElements(Position... positions);
    Element getElement(Position position);

    void open(Player player);

    Inventory getInventory(Player player);
    ImmutableList<Element> getElements();
    int getRows();
    UUID getUniqueId();
    String getTitle();

}
