package de.gleyder.spacepotato.core.builder.item;

import de.gleyder.spacepotato.core.builder.item.exceptions.MaterialNotSupportedException;
import de.gleyder.spacepotato.core.interfaces.IBuilder;
import lombok.NonNull;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Builder for enchanted books.
 *
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public final class EnchantedBookBuilder implements IBuilder<ItemStack> {

    private final ItemStack item;
    private final EnchantmentStorageMeta meta;

    /**
     * Creates an EnchantedBookBuilder
     *
     * @param item      an item
     */
    public EnchantedBookBuilder(@NonNull ItemStack item) {
        this(item, false);
    }

    /**
     * Creates an EnchantedBookBuilder
     *
     * @param item      the item
     * @param copy      if it should be copied
     */
    public EnchantedBookBuilder(@NonNull ItemStack item, boolean copy) {
        if (item == null)
            throw new NullPointerException("Item is null");
        if (item.getType() != Material.ENCHANTED_BOOK)
            throw new MaterialNotSupportedException("Only Material.ENCHANTED_BOOK is supported");

        if (copy)
            this.item = item.clone();
        else
            this.item = item;
        this.meta = (EnchantmentStorageMeta) item.getItemMeta();
    }

    /**
     * Creates an EnchantedBookBuilder
     */
    public EnchantedBookBuilder() {
        this(new ItemStack(Material.ENCHANTED_BOOK), false);
    }

    /**
     * Adds an enchantment
     *
     * @param enchantment       an enchantment
     * @param level             a level
     * @return                  this
     */
    public EnchantedBookBuilder addEnchantment(@NonNull Enchantment enchantment, int level) {
        this.meta.addStoredEnchant(enchantment, level, true);
        return this;
    }

    /**
     * Adds a map of enchantments
     *
     * @param map       the HashMap
     * @return          this
     */
    public EnchantedBookBuilder addEnchantments(@NonNull HashMap<Enchantment, Integer> map) {
        map.forEach(this::addEnchantment);
        return this;
    }

    /**
     * Removes an enchantment
     *
     * @param enchantment       an enchantment
     * @return                  this
     */
    public EnchantedBookBuilder removeEnchantment(@NonNull Enchantment enchantment) {
        this.meta.removeStoredEnchant(enchantment);
        return this;
    }

    /**
     * Removes an array of enchantment
     *
     * @param enchantments      an array of enchantment
     * @return                  this
     */
    public EnchantedBookBuilder removeEnchantment(@NonNull Enchantment... enchantments) {
        for (Enchantment enchantment : enchantments) {
            this.removeEnchantment(enchantment);
        }
        return this;
    }

    /**
     * Removes a list of enchantments
     *
     * @param enchantmentList       a list of enchantments
     * @return                      this
     */
    public EnchantedBookBuilder removeEnchantment(@NonNull List<Enchantment> enchantmentList) {
        enchantmentList.forEach(this::removeEnchantment);
        return this;
    }

    /**
     * Removes a set of enchantment
     *
     * @param enchantmentSet        a set of enchantment
     * @return                      this
     */
    public EnchantedBookBuilder removeEnchantment(@NonNull Set<Enchantment> enchantmentSet) {
        enchantmentSet.forEach(this::removeEnchantment);
        return this;
    }

    /**
     * Converts it to the default builder
     *
     * @return       an ItemBuilder
     */
    public ItemBuilder toItemBuilder() {
        return new ItemBuilder(this.build());
    }

    /**
     * Builds the ItemStack
     *
     * @return      an ItemStack
     */
    @Override
    public ItemStack build() {
        this.item.setItemMeta(this.meta);
        return this.item;
    }

}
