package de.gleyder.spacepotato.core.configuration.bulkvalue;

import org.bukkit.configuration.file.YamlConfiguration;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public interface BulkValueSupplier<T> {

    T supply(String path, YamlConfiguration cfg);

}
