package de.gleyder.spacepotato.core.command.interpreters;

import de.gleyder.spacepotato.core.SpacePotatoCorePlugin;
import de.gleyder.spacepotato.core.command.exceptions.InterpreterException;
import de.gleyder.spacepotato.core.command.interfaces.Interpreter;
import de.gleyder.spacepotato.core.configuration.messages.CoreMessages;
import org.bukkit.enchantments.Enchantment;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Translates an argument into an enchantment
 *
 * @author Gleyder
 * @since 1.0
 * @version 1.0
 *
 * @see de.gleyder.spacepotato.core.command.interfaces.Interpreter
 */
public class EnchantmentInterpreter implements Interpreter<Enchantment> {

    @Override
    public Enchantment translate(String argument) throws InterpreterException {
        Enchantment enchantment = Enchantment.getByKey(SpacePotatoCorePlugin.getBukkitNamespace(argument.toLowerCase()));
        if (enchantment == null) {
            throw new InterpreterException(CoreMessages.COMMAND_INTERPRETER_EXCEPTION_ENCHANTMENT.getKeyableMessage(false)
                    .replace("argument", argument)
                    .build()
            );
        }
        return enchantment;
    }

    @Override
    public List<String> getTabCompletion() {
        return Arrays.stream(Enchantment.values()).map(enchantment -> enchantment.getKey().getKey()).collect(Collectors.toList());
    }

    @Override
    public Class<Enchantment> getClassType() {
        return Enchantment.class;
    }

}
