package de.gleyder.spacepotato.core.configuration.handler.loader;

import de.gleyder.spacepotato.core.configuration.ConfigHelper;
import de.gleyder.spacepotato.core.configuration.handler.FileLoader;
import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
public class ConfigLoader implements FileLoader {

    private final List<String> pathList;
    private final Logger logger;

    public ConfigLoader(@NotNull List<String> pathList) {
        this.pathList = pathList;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    public void load(@NotNull String filename, @NotNull File loadoutFile, @NotNull File dataFolderFile, @NotNull ConfigHelper configHelper) {
        if (!dataFolderFile.exists()) {
            configHelper.copyFromLoadout(filename);
            return;
        }

        YamlConfiguration loadoutCfg = YamlConfiguration.loadConfiguration(loadoutFile);
        YamlConfiguration dataFolderCfg = YamlConfiguration.loadConfiguration(dataFolderFile);

        this.pathList.forEach(path -> {
            Object loadoutValue = loadoutCfg.get(path);
            Object dataFolderValue = dataFolderCfg.get(path);

            if (loadoutValue == null) {
                this.logger.warn("LoadoutVale for path \"" + path + "\" in file \"" + loadoutCfg.getName() + "\" is null");
            } else if (dataFolderValue == null) {
                dataFolderCfg.set(path, loadoutValue);
            }
        });

        try {
            dataFolderCfg.save(dataFolderFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
