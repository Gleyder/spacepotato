package de.gleyder.spacepotato.core.chat;

import lombok.AllArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * @author Gleyder
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
public class ChatListener implements Listener {

    private final ChatManager chatManager;

    @EventHandler
    public void onChat(AsyncPlayerChatEvent chatEvent) {
        chatEvent.setCancelled(true);
        this.chatManager.sendMessage(chatEvent.getPlayer(), chatEvent.getMessage());
    }

}
