package de.gleyder.spacepotato.core.empty;

import de.gleyder.spacepotato.core.SpacePotatoRegistry;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Manager for empties.
 *
 * An empty must be registered und running to work.
 *
 * @author Gleyder
 * @version 0.1
 */
public class EmptyManager implements Listener {

    private final Set<Empty> registeredEmpties;
    private HashMap<Empty, BukkitTask> runningCalculations;
    private int markerTaskId;
    private boolean running;
    private SpacePotatoRegistry spacePotatoRegistry;

    /**
     * Creates an EmptyManager
     */
    public EmptyManager(SpacePotatoRegistry registry) {
        this.spacePotatoRegistry = registry;
        this.runningCalculations = new HashMap<>();
        this.registeredEmpties = new HashSet<>();
    }

    /**
     * Registers an empty
     *
     * @param empty     an empty
     */
    public void register(@NonNull Empty empty) {
        this.registeredEmpties.add(empty);
        empty.onLoad();
        //Starts calculating the outline async and saves the task which are calculating
        Bukkit.getScheduler().runTaskLaterAsynchronously(this.spacePotatoRegistry.getPlugin(), bukkitTask -> {
            if (!empty.isDrawOutline())
                return;

            this.runningCalculations.put(empty, bukkitTask);
            empty.calculateOutline();
            this.runningCalculations.remove(empty);
        }, 0);
    }

    /**
     * Unregisters an empty
     *
     * @param empty     an empty
     */
    public void unregister(@NonNull Empty empty) {
        this.registeredEmpties.remove(empty);
        BukkitTask task = this.runningCalculations.get(empty);
        //Stops the calculations for the outline
        if (task != null)
            task.cancel();
        empty.onUnload();
    }

    /**
     * Unregisters all empties
     */
    public void unregisterAll() {
        this.registeredEmpties.forEach(this::unregister);
    }

    /**
     * Runs the handler
     */
    public void start() {
        if (this.running)
            return;

        this.spacePotatoRegistry.registerListener(this);
        this.markerTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(this.spacePotatoRegistry.getPlugin(), () -> this.registeredEmpties.forEach(empty -> {
            if (this.runningCalculations.get(empty) != null)
                return;
            empty.markOutline();
        }), 0, 10);
        this.running = true;
    }

    /**
     * Stops the handler
     */
    public void stop() {
        if (!this.running)
            return;

        Bukkit.getScheduler().cancelTask(this.markerTaskId);
        this.spacePotatoRegistry.unregisterListener(this);
        this.running = false;
    }

    /**
     * Event for the player movement
     *
     * @param event     the event
     */
    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        //Handler must be running! Or the empties wont work
        if (!this.running)
            return;

        /*
         * Loops through all empties and checks
         * if a player joined or left an empty
         */
        Player player = event.getPlayer();
        this.registeredEmpties.forEach(empty -> {
            PlayerEmptyChangeType changeType = empty.check(player.getLocation());
            if (changeType == PlayerEmptyChangeType.JOIN) {
                if (!empty.overlappingPlayers.contains(player)) {
                    empty.overlappingPlayers.add(player);
                    empty.onPlayerChangeEvent(player, changeType);
                }
            } else {
                if (empty.overlappingPlayers.contains(player)) {
                    empty.overlappingPlayers.remove(player);
                    empty.onPlayerChangeEvent(player, changeType);
                }
            }
        });
    }

}
